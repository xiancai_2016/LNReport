package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/10/22
 */
public interface CommunicationStatisticsService {
    List queryComStatis(Map map);
    List queryComStatisInfo(Map map);
}
