/*
 * For com.royal.art
 * Copyright [2015/11/26] By FCHEN
 */
package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.SysDict;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.SysDictMapper;
import com.lerning.edu.services.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * FootballDictServiceImpl
 *  字典
 * @author FCHEN
 * @date 2015/11/26
 */
@Service
public class SysDictServiceImpl extends BaseServiceImpl<SysDict> implements SysDictService {

    @Autowired
    private SysDictMapper sysDictMapper;

    @Override
    public BaseMapper<SysDict> getMapper() {
        return this.sysDictMapper;
    }
    /**
     * 根据父字典key 获取子字典数据
     * @param pKey
     * @return List
     */
    @Override
    public List<SysDict> getSysDictByPKey(String pKey) {
        SysDict dict = new SysDict();
        dict.setpItemKey(pKey);
        dict.setDeleted(false);
        return sysDictMapper.queryList(dict);
    }

    /**
     * 根据日期查询库存和日期
     * @param sysDict
     * @return List
     */
    @Override
    public List<SysDict> getItemAndNum(SysDict sysDict) {
        return sysDictMapper.getItemAndNum(sysDict);
    }

    @Override
    public List<SysDict> queryDay(String pKey) {
        return sysDictMapper.queryDay(pKey);
    }
}
