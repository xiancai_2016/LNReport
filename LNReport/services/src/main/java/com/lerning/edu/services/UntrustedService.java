package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/3
 */
public interface UntrustedService {
    List queryCampusAndCount(Map map);
    List queryDetail(Map map);
}
