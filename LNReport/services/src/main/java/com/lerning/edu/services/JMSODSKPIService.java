package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/10/25
 */
public interface JMSODSKPIService {
    List queryKPIByCampus(Map map);

    void updateAdd(Map map);

    List queryKPIByCC(Map dateMap);

    void updateAddByCC(Map map);
}
