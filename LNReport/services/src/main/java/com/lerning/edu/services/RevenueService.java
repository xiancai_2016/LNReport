package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/5
 */
public interface RevenueService {
    List queryRevenueByWeek(Map map);

    int insertRevenues(List revenues);//锁定DSR revenue

    List queryRevenuesData(Map dateMap);
    Map queryRevenueSum(Map dateMap);
}
