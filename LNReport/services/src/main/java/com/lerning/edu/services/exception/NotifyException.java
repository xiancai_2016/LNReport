/*
 * For com.royal.art
 * Copyright [2015/11/28] By FCHEN
 */
package com.lerning.edu.services.exception;

import com.lerning.edu.commons.exception.AbstractException;
import com.lerning.edu.services.exception.msg.NotifyErrorMsg;

/**
 * NotifyException
 * 推送消息类异常
 * @author FCHEN
 * @date 2015/11/28
 */
public class NotifyException extends AbstractException {


    private static final long serialVersionUID = 2775156672554029911L;

    public NotifyException(int code, String msg) {
        super(code, msg);
    }

    public NotifyException(NotifyErrorMsg errorMsg) {
        super(errorMsg.getCode(), errorMsg.getMsg());
    }


}
