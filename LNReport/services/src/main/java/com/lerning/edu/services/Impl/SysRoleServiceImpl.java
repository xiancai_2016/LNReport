package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.SysRole;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.SysRoleMapper;
import com.lerning.edu.services.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by FCHEN on 2016/12/12.
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole> implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public BaseMapper<SysRole> getMapper() {
        return sysRoleMapper;
    }

    @Override
    public SysRole getRoleByName(String name) {
        SysRole r = new SysRole();
        r.setName(name);
        return sysRoleMapper.getRoleByName(r);
    }

    /**
     * 维护角色与菜单权限关系
     *
     * @param sysRole
     * @return
     */
    @Override
    public int deleteRoleResource(SysRole sysRole) {
        return sysRoleMapper.deleteRoleResource(sysRole);
    }

    @Override
    public int insertRoleResource(SysRole sysRole) {
        return sysRoleMapper.insertRoleResource(sysRole);
    }

    @Override
    public int deleteRoleReport(SysRole sysRole) {
        return sysRoleMapper.deleteRoleReport(sysRole);
    }

    @Override
    public int insertRoleReport(SysRole sysRole) {
        return sysRoleMapper.insertRoleReport(sysRole);
    }

//    @Override
//    public void saveRole(SysRole sysRole) {
//        if( sysRole.getId() == null ){ //add
//            sysRoleMapper.add(sysRole);
//        }else{//edit
//            sysRoleMapper.update(sysRole);
//        }
//        System.out.println("=========================="+sysRole.getId());
//        // 更新角色与菜单关联
//        sysRoleMapper.deleteRoleResource(sysRole);
//        if (sysRole.getResourceList().size() > 0){
//            insertRoleResource(sysRole);
//        }
//    }
}
