package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.CommunicationStatisticsMapper;
import com.lerning.edu.services.CommunicationStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/10/22
 */
@Service
public class CommunicationStatisticsServiceImpl implements CommunicationStatisticsService{

    @Autowired
    public CommunicationStatisticsMapper communicationStatisticsMapper;

    @Override
    public List queryComStatis(Map map) {
        return communicationStatisticsMapper.queryComStatis(map);
    }

    @Override
    public List queryComStatisInfo(Map map) {
        return communicationStatisticsMapper.queryComStatisInfo(map);
    }
}
