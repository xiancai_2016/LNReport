package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/21
 */
public interface ChannelService{
    /***
     * 按财务月和校区查询2018年三大渠道的业绩和报名费
     * @param map
     * @return
     */
    Map queryAll(Map map);

    /***
     * 按财务月和校区查询2017年三大渠道的业绩和报名费
     * @param map
     * @return
     */
    Map queryAllByChannel2017(Map map);

    /***
     * 查询2018额外提前续班
     * @param map
     * @return
     */
    Map queryByOdsReNew18(Map map);

    int insertDsrByCampus(List channels);//锁定校区dsr(批量)
    int insertDsrByCampusSum(Map totalMap);//汇总锁定校区dsr
    int insertDsr(Map map);//锁定校区dsr(单条)

    List queryChannelsLockData(Map dateMap);//查询dsr锁定数据
    List queryChannelsLockDataSum(Map dateMap);//查询dsr锁定数据汇总

}
