package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.UntrustedMapper;
import com.lerning.edu.services.UntrustedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/3
 */
@Service
public class UntrustedServiceImpl implements UntrustedService {

    @Autowired
    private UntrustedMapper untrustedMapper;

    @Override
    public List queryCampusAndCount(Map map) {
        return untrustedMapper.queryCampusAndCount(map);
    }

    @Override
    public List queryDetail(Map map){
        return untrustedMapper.queryDetail(map);
    }
}
