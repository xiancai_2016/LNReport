/*
 * For com.royal.art
 * Copyright [2015/11/14] By FCHEN
 */
package com.lerning.edu.services.Impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lerning.edu.beans.page.PageList;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.services.BaseService;
import java.util.List;

/**
 * BaseServiceImpl
 * 基础base实现
 * @author FCHEN
 * @date 2015/11/14
 */
public abstract class BaseServiceImpl<T> implements BaseService<T> {

    public abstract BaseMapper<T> getMapper();

    /**
     * 添加
     * @param bean
     * @return
     */
    public int add(T bean) {
        return getMapper().add(bean);
    }

    /**
     * 更新
     * @param bean
     * @return
     */
    public int update(T bean) {
        return getMapper().update(bean);
    }

    /**
     * 删除
     * @param bean
     * @return
     */
    public int delete(T bean) {
        return getMapper().delete(bean);
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    public T queryById(long id) {
        return getMapper().queryById(id);
    }

    /**
     * 根据列表查询
     * @param bean
     * @return
     */
    public List<T> queryList(T bean) {
        return getMapper().queryList(bean);
    }

    /**
     * 是否存在
     * @param bean
     * @return
     */
    public int exists(T bean){
        return getMapper().exists(bean);
    }

    /**
     * 查询所有数据列表
     * @param bean
     * @return
     */
    public List<T> findAllList(T bean) {
        return getMapper().findAllList(bean);
    }

    /**
     * 查询所有数据列表
     * @see public List<T> findAllList(T entity)
     * @return
     */
    @Deprecated
    public List<T> findAllList(){
        return getMapper().findAllList();
    }

    /**
     * 获取最大的排序值
     * @return
     */
    public int getMaxSeq(){
        return getMapper().getMaxSeq();
    }

    /**
     * 分页查询
     * @param pageList
     * @param bean
     * @return
     */
    public PageList<T> queryPage(PageList<T> pageList, T bean) {
        Page<T> page;
        int pageStart ;
        if(pageList.getiDisplayLength() == -1){//不分页
            pageStart = 1;
            page = PageHelper.startPage(pageStart, 10000,false);
        }else{
            pageStart = pageList.getiDisplayStart()/pageList.getiDisplayLength() + 1;
            page = PageHelper.startPage(pageStart, pageList.getiDisplayLength());
        }
        getMapper().queryList(bean);
        pageList.setiTotalRecords(page.getTotal());
        pageList.setiTotalDisplayRecords(pageList.getiTotalRecords());
        pageList.setAaData(page.getResult());
        return pageList;
    }

//    /**
//     * 数据范围过滤
//     * @param user 当前用户对象，通过“entity.getCurrentUser()”获取
//     * @param userAlias 用户表别名，多个用“,”逗号隔开，传递空，忽略此参数
//     * @return 标准连接条件对象
//     */
//    public static String dataScopeFilter(SysUser user, String userAlias) {
//
//        StringBuilder sqlString = new StringBuilder();
//
//        // 超级管理员，跳过权限过滤
//        if (!user.isAdmin()){
//            boolean isDataScopeAll = false;
//            // 如果没有全部数据权限，并设置了用户别名，则当前权限为本人；如果未设置别名，当前无权限为已植入权限
//            if (!isDataScopeAll){
//                if (StringUtil.isNotBlank(userAlias)){
//                    for (String ua : StringUtil.split(userAlias, ",")){
//                        sqlString.append(" OR " + ua + ".id = '" + user.getId() + "'");
//                    }
//                }
//            }else{
//                // 如果包含全部权限，则去掉之前添加的所有条件，并跳出循环。
//                sqlString = new StringBuilder();
//            }
//        }
//        if (StringUtil.isNotBlank(sqlString.toString())){
//            return " AND (" + sqlString.substring(4) + ")";
//        }
//        return "";
//    }
}
