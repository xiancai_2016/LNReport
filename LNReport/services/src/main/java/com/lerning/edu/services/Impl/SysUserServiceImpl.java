/*
 * For com.royal.art
 * Copyright [2015/11/14] By FCHEN
 */
package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.SysUser;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.SysUserMapper;
import com.lerning.edu.services.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * FootballSysUserServiceImpl
 * 系统用户
 * @author FCHEN
 * @date 2015/11/14
 */
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public BaseMapper<SysUser> getMapper() {
        return sysUserMapper;
    }

    /**
     * 根据用户账号查询
     * @param account
     * @return
     */
    public SysUser queryUserByAccount(String account) {
        return sysUserMapper.queryUserByAccount(account);
    }

    /**
     * 更新用户登录次数
     * @return
     */
    public int updateLoginTimes(SysUser sysUser) {
        return sysUserMapper.updateLoginTimes(sysUser);
    }

    /**
     * 根据用户账户修改用户密码
     * @param param
     * @return
     */
    public int updateUserPwd(HashMap<String,String> param){return sysUserMapper.updateUserPwd(param);}

    /**
     * 删除用户角色关联数据
     * @param sysUser
     * @return
     */
    public int deleteUserRole(SysUser sysUser){
        return sysUserMapper.deleteUserRole(sysUser);
    }

    /**
     * 插入用户角色关联数据
     * @param sysUser
     * @return
     */
    public int insertUserRole(SysUser sysUser){
       return sysUserMapper.insertUserRole(sysUser);
    }
}
