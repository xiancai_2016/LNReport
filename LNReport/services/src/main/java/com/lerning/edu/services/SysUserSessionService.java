/*
 * For com.royal.art
 * Copyright [2015/11/14] By FCHEN
 */
package com.lerning.edu.services;

import com.lerning.edu.beans.SysUserSession;

import java.util.Map;

/**
 * @author FCHEN
 */
public interface SysUserSessionService extends BaseService<SysUserSession>{

    Boolean login(SysUserSession sysUserSession) ;

    Boolean logOut(String mobilePhone) ;

    SysUserSession queryByTokenKey(SysUserSession sysUserSession, Boolean ignoreFlag) ;

    SysUserSession queryByTokenContent(SysUserSession sysUserSession, Boolean ignoreFlag) ;

    /**
     * 添加登录token
     * @param tokenMap
     * @return
     */
    String addLoginToken(Map<String, String> tokenMap);

    /**
     * 更新token
     * @param tokenMap
     * @return
     */
    String updateLoginToken(Map<String, String> tokenMap);

}
