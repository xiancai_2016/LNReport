/*
 * For com.royal.art
 * Copyright [2015/11/26] By FCHEN
 */
package com.lerning.edu.services;

import com.lerning.edu.beans.SysDict;

import  java.util.List;

/**
 * SysDictService
 * 字典
 * @author FCHEN
 * @date 2015/11/26
 */
public interface SysDictService extends BaseService<SysDict> {
    List<SysDict> getSysDictByPKey(String pKey);

    List<SysDict> queryDay(String pKey);

    List<SysDict> getItemAndNum(SysDict sysDict);
}
