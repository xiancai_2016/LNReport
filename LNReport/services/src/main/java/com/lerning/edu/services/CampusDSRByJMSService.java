package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/10/29
 */
public interface CampusDSRByJMSService {
    List queryByUser(Map map);

    List querySaleModeByCampus(Map campusMap);

    List queryByUserForApp(Map map);

    List queryByUserOfFPShowByTinTout(Map map);

    List queryByUserOfOCShowByTinTout(Map map);

    List queryByUserOfFPShowByWalkInRef(Map map);

    List queryByUserOfOCShowByWalkInRef(Map map);

    List queryEnByUser(Map map);

    List queryEnByUserSummary(Map map);

    List queryEnByUserNew(Map map);

    List queryEnByUserSummaryNew(Map map);
    
    int queryByUserForAppCount(Map map);

    int queryByUserForLeadsCount(Map map);

    int queryCustomerOfCampusCount(Map map);

    Map queryCampus(String campusID);
}
