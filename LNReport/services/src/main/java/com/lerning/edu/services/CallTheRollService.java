package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/27
 */
public interface CallTheRollService {

    List queryStudentInfo(Map map);

    int addCallTheRoll(Map map);
}
