package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.Operator;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.OperatorMapper;
import com.lerning.edu.services.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jiangwei
 * @since 18/8/20
 */
@Service
public class OperatorServiceImpl extends BaseServiceImpl<Operator> implements OperatorService{

    @Autowired
    public OperatorMapper operatorMapper;

    @Override
    public BaseMapper<Operator> getMapper(){return operatorMapper;}
}
