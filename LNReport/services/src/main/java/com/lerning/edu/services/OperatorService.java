package com.lerning.edu.services;

import com.lerning.edu.beans.Operator;

/**
 * @author jiangwei
 * @since 18/8/20
 */
public interface OperatorService extends BaseService<Operator> {
}
