package com.lerning.edu.services;

import com.lerning.edu.beans.SysResource;

import java.util.List;

/**
 * Created by FCHEN on 2016/12/12.
 */
public interface SysResourceService extends BaseService<SysResource> {
    List<SysResource> findByUserId(SysResource sysResource);

    void saveReSource(SysResource sysResource);

    int updateSort(SysResource sysResource);
}
