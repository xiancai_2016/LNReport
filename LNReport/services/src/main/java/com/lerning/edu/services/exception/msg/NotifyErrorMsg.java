/*
 * For com.royal.art
 * Copyright [2015/11/28] By FCHEN
 */
package com.lerning.edu.services.exception.msg;

/**
 * NotifyErrorMsg
 * 推送消息异常
 * @author FCHEN
 * @date 2015/11/28
 */
public enum NotifyErrorMsg {

    /**
     * 推送消息为空
     */
    EMPTY_MSG(2000,"推送消息为空"),

    /**
     * 未被识别的消息推送方式
     */
    ILLEGAL_SENDTYPE_MSG(2000,"非法的推送方式"),

    ;


    private int code;
    private String msg;
    NotifyErrorMsg(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }







}
