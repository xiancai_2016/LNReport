package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.WorkOrder;
import com.lerning.edu.beans.WorkOrderAuditRecord;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.WorkOrderAuditRecordMapper;
import com.lerning.edu.data.mapper.WorkOrderMapper;
import com.lerning.edu.services.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/14
 */
@Service
public class WorkOrderServiceImpl extends BaseServiceImpl<WorkOrder> implements WorkOrderService {
    @Autowired
    public WorkOrderMapper workOrderMapper;
    @Autowired
    public WorkOrderAuditRecordMapper workOrderAuditRecordMapper;

    @Override
    public BaseMapper<WorkOrder> getMapper() {
        return workOrderMapper;
    }

    @Override
    public Map queryByUserId(Map map) {
        return workOrderMapper.queryByUserId(map);
    }

    @Override
    public Map queryMaxOrderNo(Map map) {
        return workOrderMapper.queryMaxOrderNo(map);
    }

    @Override
    public Map queryDetail(Map map) {
        return workOrderMapper.queryDetail(map);
    }

    @Override
    public Map queryDetail1(Map map) {
        return workOrderMapper.queryDetail1(map);
    }

    @Override
    public Map queryChangeSchoolOfClass(Map map) {
        return workOrderMapper.queryChangeSchoolOfClass(map);
    }

    @Override
    public List findWorkOrder(Map map) {
        return workOrderMapper.findWorkOrder(map);
    }

    @Override
    public int findWorkOrderCount(Map map) {
        return workOrderMapper.findWorkOrderCount(map);
    }

    @Override
    public List findWorkOrderByAdd(Map map) {
        return workOrderMapper.findWorkOrderByAdd(map);
    }

    @Override
    public int findWorkOrderCountByAdd(Map map) {
        return workOrderMapper.findWorkOrderCountByAdd(map);
    }

    @Override
    public List queryEmployeeByCfield(String account) {
        return workOrderMapper.queryEmployeeByCfield(account);
    }

    @Override
    public List queryEmployeeByUserID(String userID) {
        return workOrderMapper.queryEmployeeByUserID(userID);
    }

    @Override
    public List queryEmployeeByCfieldOfLN(String account) {
        return workOrderMapper.queryEmployeeByCfieldOfLN(account);
    }

    @Override
    public List queryEmployeeByUserIDOfLN(String userID) {
        return workOrderMapper.queryEmployeeByUserIDOfLN(userID);
    }

    @Override
    public List findMyTaskList(Map map) {
        return workOrderMapper.findMyTaskList(map);
    }

    @Override
    public int findMyTaskListCount(Map map) {
        return workOrderMapper.findMyTaskListCount(map);
    }

    @Override
    public Map queryWorkOrderRecordById(String cID) {
        return workOrderMapper.queryWorkOrderRecordById(cID);
    }

    @Override
    public List queryByRefundFee(Map map) {
        return workOrderMapper.queryByRefundFee(map);
    }

    @Override
    public List queryByShiftInfo(Map map) {
        return workOrderMapper.queryByShiftInfo(map);
    }

    @Override
    public List queryByRefundFeeLock(Map map) {
        return workOrderMapper.queryByRefundFeeLock(map);
    }

    @Override
    public List findWorkOrderOfFundFee(Map map) {
        return workOrderMapper.findWorkOrderOfFundFee(map);
    }

    @Override
    public int findWorkOrderCountOfFundFee(Map map) {
        return workOrderMapper.findWorkOrderCountOfFundFee(map);
    }

    @Override
    public void addWorkOrder(WorkOrder workOrder) {
        workOrderMapper.add(workOrder);

        Map map = new HashMap();
        map.put("orderNo", workOrder.getCorderno());
        Map workOrderDetail = workOrderMapper.queryDetail1(map);

        //添加操作记录
        WorkOrderAuditRecord wrar = new WorkOrderAuditRecord();
        wrar.setCstatus(4);
        wrar.setCauditreason("");
        wrar.setCuserid("6BE3815A-0002-4D82-8720-36B9AD484936");//侯秋叶
        wrar.setCworkorderid(workOrderDetail.get("cID").toString());

        workOrderAuditRecordMapper.add(wrar);
    }

    @Override
    public List queryOdsUser(Map map) {
        return workOrderMapper.queryOdsUser(map);
    }
}
