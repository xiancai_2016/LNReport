package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.FullAttendanceMapper;
import com.lerning.edu.services.FullAttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/6
 */
@Service
public class FullAttendanceServiceImpl implements FullAttendanceService{

    @Autowired
    public FullAttendanceMapper fullAttendanceMapper;

    @Override
    public List fullAttendance(){
        return  fullAttendanceMapper.fullAttendance();
    }

    @Override
    public Map fullAttendanceTotal(){
        return fullAttendanceMapper.fullAttendanceTotal();
    }
}
