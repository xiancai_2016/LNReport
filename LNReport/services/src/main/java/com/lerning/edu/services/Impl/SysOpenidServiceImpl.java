package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.SysOpenid;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.SysOpenidMapper;
import com.lerning.edu.services.SysOpenidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jiangwei
 * @since 18/6/5
 */
@Service
public class SysOpenidServiceImpl extends BaseServiceImpl<SysOpenid> implements SysOpenidService{
    @Autowired
    private SysOpenidMapper sysOpenidMapper;

    @Override
    public BaseMapper<SysOpenid> getMapper() {
        return sysOpenidMapper;
    }

    @Override
    public SysOpenid queryByOpenid(String openid){
        return sysOpenidMapper.queryByOpenid(openid);
    }

}
