package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/6
 */
public interface FullAttendanceService {
    List fullAttendance();
    Map fullAttendanceTotal();

}
