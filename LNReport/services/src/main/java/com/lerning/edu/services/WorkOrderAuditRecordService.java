package com.lerning.edu.services;

import com.lerning.edu.beans.WorkOrderAuditRecord;

/**
 * @author jiangwei
 * @since 18/8/15
 */
public interface WorkOrderAuditRecordService extends BaseService<WorkOrderAuditRecord> {
}
