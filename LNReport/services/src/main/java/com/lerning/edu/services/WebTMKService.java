package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/2
 */
public interface WebTMKService {

    List queryWebTMKByTMK(Map map);

    List querySaleModeByTMK();

    List queryDetail(Map map);

    List queryDetailByApp(Map map);

    List queryDetailByShow(Map map);

    List queryDetailByEn(Map map);

    List queryWebTMKByCampus(Map map);
}
