package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/13
 */
public interface ExceptionRenewService {

    List queryExceptionRenew(Map map);

    int queryExceptionRenewCount(Map map);
}
