/*
 * For com.royal.art
 * Copyright [2015/11/14] By FCHEN
 */
package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.SysUserSession;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.services.SysUserSessionService;
import com.lerning.edu.services.exception.UserException;
import com.lerning.edu.data.mapper.SysUserSessionMapper;
import com.lerning.edu.services.exception.msg.UserErrorMsg;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * FootballSysUserServiceImpl
 * 系统用户
 * @author FCHEN
 * @date 2015/11/14
 */
@Service
public class SysUserSessionServiceImpl extends BaseServiceImpl<SysUserSession> implements SysUserSessionService {

    @Autowired
    private SysUserSessionMapper sysUserSessionMapper;

    @Override
    public BaseMapper<SysUserSession> getMapper() {
        return sysUserSessionMapper;
    }

    @Override
    public Boolean login(SysUserSession sysUserSession) {

        SysUserSession userSession = queryByTokenKey(sysUserSession, Boolean.TRUE) ;

        int index = 0 ;
        if ( null == userSession ) {
            sysUserSession.setUpdatedDatetime(new Timestamp(System.currentTimeMillis()));
            sysUserSession.setCreatedDatetime(new Timestamp(System.currentTimeMillis()));
            index = sysUserSessionMapper.add(sysUserSession) ;
        } else {
            sysUserSession.setUpdatedDatetime(new Timestamp(System.currentTimeMillis()));
            index = sysUserSessionMapper.update(sysUserSession) ;
        }

        return index >= 0;
    }

    @Override
    public Boolean logOut(String mobilePhone) {
        Map params = new HashMap();
        params.put("tokenKey",mobilePhone);
        params.put("updatedDatetime",new Timestamp(System.currentTimeMillis())) ;
        params.put("actived",Boolean.FALSE);
        return sysUserSessionMapper.update(params) >= 0;
    }

    @Override
    public SysUserSession queryByTokenKey (SysUserSession sysUserSession, Boolean ignoreFlag) {
        Map params = new HashMap();
        params.put("tokenKey", sysUserSession.getTokenKey());
        params.put("ignoreFlag",ignoreFlag) ;
        return sysUserSessionMapper.queryByTokenKey(params);
    }

    @Override
    public SysUserSession queryByTokenContent (SysUserSession sysUserSession, Boolean ignoreFlag) {
        Map params = new HashMap();
        params.put("tokenContent", sysUserSession.getTokenContent());
        params.put("ignoreFlag",ignoreFlag) ;
        return sysUserSessionMapper.queryByTokenContent(params);
    }

    /**
     * 登录token
     * @param tokenMap
     * @return
     */
    @Override
    public String addLoginToken(Map<String, String> tokenMap) {
        // 登录
        Date now = new Date();
        SysUserSession sysUserSession = new SysUserSession();
        sysUserSession.setTokenKey(tokenMap.get("account"));
        sysUserSession.setTokenContent(buildPrivateToken(tokenMap,now));
        sysUserSession.setDeviceCode(tokenMap.get("deviceCode"));
        sysUserSession.setSourceCode(tokenMap.get("sourceCode"));
        sysUserSession.setExpire(new Timestamp(Long.parseLong(tokenMap.get("expire"))+System.currentTimeMillis()));
        sysUserSession.setCreatedDatetime(new Timestamp(now.getTime()));
        sysUserSession.setUpdatedDatetime(sysUserSession.getCreatedDatetime());
        if(sysUserSessionMapper.add(sysUserSession) < 1){
            throw new UserException(UserErrorMsg.ADD_LOGIN_TOKEN_FAILED);
        }
        return sysUserSession.getTokenContent();
    }

    /**
     * 更新token
     * @param tokenMap
     * @return
     */
    @Override
    public String updateLoginToken(Map<String, String> tokenMap) {
        Date now = new Date();
        SysUserSession sysUserSession = new SysUserSession();
        sysUserSession.setTokenKey(tokenMap.get("account"));
        sysUserSession.setTokenContent(buildPrivateToken(tokenMap,now));
        sysUserSession.setDeviceCode(tokenMap.get("deviceCode"));
        sysUserSession.setSourceCode(tokenMap.get("sourceCode"));
        sysUserSession.setExpire(new Timestamp(Long.parseLong(tokenMap.get("expire"))+System.currentTimeMillis()));
        sysUserSession.setUpdatedDatetime(new Timestamp(now.getTime()));
        if(sysUserSessionMapper.update(sysUserSession) < 1){
            throw new UserException(UserErrorMsg.UPDATE_LOGIN_TOKEN_FAILED);
        }
        return sysUserSession.getTokenContent();
    }

    /**
     * 生成私钥
     * @param tokenMap
     * @param now
     * @return
     */
    private String buildPrivateToken(Map<String, String> tokenMap,Date now){
        String tokenInfo = tokenMap.get("account") + tokenMap.get("deviceCode") + tokenMap.get("sourceCode") +
                tokenMap.get("expire") + tokenMap.get("key") + now.getTime();
        return DigestUtils.md5Hex(tokenInfo);
    }


}
