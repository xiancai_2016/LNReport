package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.ExceptionRenewMapper;
import com.lerning.edu.services.ExceptionRenewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/13
 */
@Service
public class ExceptionRenewServiceImpl implements ExceptionRenewService {
    @Autowired
    public ExceptionRenewMapper exceptionRenewMapper;

    @Override
    public List queryExceptionRenew(Map map){
        return exceptionRenewMapper.queryExceptionRenew(map);
    }

    @Override
    public int queryExceptionRenewCount(Map map){
        return exceptionRenewMapper.queryExceptionRenewCount(map);
    }
}
