package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/9
 */
public interface RefSituationService {
    List queryRef(Map map);

    Map queryEnByCampus(Map map);
}
