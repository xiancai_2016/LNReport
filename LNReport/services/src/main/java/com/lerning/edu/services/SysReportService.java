package com.lerning.edu.services;

import com.lerning.edu.beans.SysReport;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/5
 */
public interface SysReportService extends BaseService<SysReport> {
    SysReport queryReportById(Integer id);

    List<SysReport> findByUserId(SysReport sysReport);

    List<SysReport> queryListByReport(SysReport sysReport);

    List<Map> queryReportAndOpenId(Map map);

    int addPushRecord(Map map);

}
