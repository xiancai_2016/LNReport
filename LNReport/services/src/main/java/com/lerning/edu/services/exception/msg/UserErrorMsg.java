/*
 * For com.royal.art
 * Copyright [2015/11/25] By FCHEN
 */
package com.lerning.edu.services.exception.msg;

/**
 * UserErrorMsg
 * 用户类异常
 * @author FCHEN
 * @date 2015/11/25
 */
public enum UserErrorMsg {

    ADD_USER_ACCOUNT_FAILED(1200,"添加用户账号异常"),

    ADD_USER_INFO_FAILED(1201,"添加用户信息异常"),

    ADD_LOGIN_TOKEN_FAILED(1202,"生成用户token失败"),

    UPDATE_LOGIN_TOKEN_FAILED(1203,"更新用户token失败"),

    UNMATCHED_USER_SOURCE(1204,"用户来源不匹配"),

    ;


    private int code;
    private String msg;
    UserErrorMsg(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


}
