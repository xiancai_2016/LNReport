package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.CampusDsrNewMapper;
import com.lerning.edu.services.CampusDsrNewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/19
 */
@Service
public class CampusDsrNewServiceImpl implements CampusDsrNewService {
    @Autowired
    public CampusDsrNewMapper campusDsrNewMapper;

    @Override
    public List queryByUser(Map map) {
        return campusDsrNewMapper.queryByUser(map);
    }

    @Override
    public List queryByUserForApp(Map map) {
        return campusDsrNewMapper.queryByUserForApp(map);
    }

    @Override
    public Map queryCampus(String CampusID) {
        return campusDsrNewMapper.queryCampus(CampusID);
    }

    @Override
    public int queryByUserForAppCount(Map map) {
        return campusDsrNewMapper.queryByUserForAppCount(map);
    }

    @Override
    public int queryByUserForLeadsCount(Map map) {
        return campusDsrNewMapper.queryByUserForLeadsCount(map);
    }

    @Override
    public int queryCustomerOfCampusCount(Map map) {
        return campusDsrNewMapper.queryCustomerOfCampusCount(map);
    }

    @Override
    public List queryByUserOfFPShowByTinTout(Map map) {
        return campusDsrNewMapper.queryByUserOfFPShowByTinTout(map);
    }

    @Override
    public List queryByUserOfOCShowByTinTout(Map map) {
        return campusDsrNewMapper.queryByUserOfOCShowByTinTout(map);
    }

    @Override
    public List queryByUserOfFPShowByWalkInRef(Map map) {
        return campusDsrNewMapper.queryByUserOfFPShowByWalkInRef(map);
    }

    @Override
    public List queryByUserOfOCShowByWalkInRef(Map map) {
        return campusDsrNewMapper.queryByUserOfOCShowByWalkInRef(map);
    }

    @Override
    public List queryEnByUser(Map map) {
        return campusDsrNewMapper.queryEnByUser(map);
    }

    @Override
    public List queryEnByUserSummary(Map map){
        return campusDsrNewMapper.queryEnByUserSummary(map);
    }

    @Override
    public List queryDsrByUserSummaryDetail(Map map){
        return campusDsrNewMapper.queryDsrByUserSummaryDetail(map);
    }

    @Override
    public List queryEnByUserNew(Map map) {
        return campusDsrNewMapper.queryEnByUserNew(map);
    }

    @Override
    public List queryEnByUserSummaryNew(Map map){
        return campusDsrNewMapper.queryEnByUserSummaryNew(map);
    }

    @Override
    public List queryDsrByUserSummaryDetailNew(Map map){
        return campusDsrNewMapper.queryDsrByUserSummaryDetailNew(map);
    }

    @Override
    public List queryDsrByUserSummaryDetailByReNew(Map map){
        return campusDsrNewMapper.queryDsrByUserSummaryDetailByReNew(map);
    }

    @Override
    public int insertDSRByCCASByTin(Map map){
        return campusDsrNewMapper.insertDSRByCCASByTin(map);
    }

    @Override
    public int insertDSRByCCASByTout(Map map){
        return campusDsrNewMapper.insertDSRByCCASByTout(map);
    }

    @Override
    public int insertDSRByCCASByWin(Map map){
        return campusDsrNewMapper.insertDSRByCCASByWin(map);
    }

    @Override
    public int insertEnByUser(List list){
        return campusDsrNewMapper.insertEnByUser(list);
    }

}
