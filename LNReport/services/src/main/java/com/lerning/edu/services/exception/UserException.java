package com.lerning.edu.services.exception;

import com.lerning.edu.commons.exception.AbstractException;
import com.lerning.edu.services.exception.msg.UserErrorMsg;

/**
 * UserException
 * 用户类异常
 * @author RICK
 * @date 2015/11/25
 */
public class UserException extends AbstractException {


    private static final long serialVersionUID = -2344605145208735919L;

    public UserException(int code, String msg) {
        super(code, msg);
    }

    public UserException(UserErrorMsg msg){
        super(msg.getCode(), msg.getMsg());
    }

}
