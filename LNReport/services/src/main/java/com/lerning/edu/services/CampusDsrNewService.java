package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/19
 */
public interface CampusDsrNewService {
    List queryByUser(Map map);

    Map queryCampus(String CampusID);

    List queryByUserForApp(Map map);

    int queryByUserForAppCount(Map map);

    int queryByUserForLeadsCount(Map map);

    int queryCustomerOfCampusCount(Map map);

    List queryByUserOfFPShowByTinTout(Map map);

    List queryByUserOfOCShowByTinTout(Map map);

    List queryByUserOfFPShowByWalkInRef(Map map);

    List queryByUserOfOCShowByWalkInRef(Map map);

    List queryEnByUser(Map map);

    List queryEnByUserSummary(Map map);

    List queryDsrByUserSummaryDetail(Map map);

    List queryEnByUserNew(Map map);

    List queryEnByUserSummaryNew(Map map);

    List queryDsrByUserSummaryDetailNew(Map map);

    List queryDsrByUserSummaryDetailByReNew(Map map);

    int insertDSRByCCASByTin(Map map);
    int insertDSRByCCASByTout(Map map);
    int insertDSRByCCASByWin(Map map);

    int insertEnByUser(List list);

}
