package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.DSRByCCSAMapper;
import com.lerning.edu.services.DSRByCCSAService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/22
 */
@Service
public class DSRByCCSAServiceImpl implements DSRByCCSAService {

    @Autowired
    public DSRByCCSAMapper dsrByCCSAMapper;

    @Override
    public List queryCCSAList(Map map){
        return dsrByCCSAMapper.queryCCSAList(map);

    }

}
