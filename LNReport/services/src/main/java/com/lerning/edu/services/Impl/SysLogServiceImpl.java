package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.SysLog;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.SysLogMapper;
import com.lerning.edu.services.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by FCHEN on 2016/12/23.
 */
@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLog> implements SysLogService {

    @Autowired
    private SysLogMapper sysLogMapper;

    @Override
    public BaseMapper<SysLog> getMapper() {
        return sysLogMapper;
    }
}
