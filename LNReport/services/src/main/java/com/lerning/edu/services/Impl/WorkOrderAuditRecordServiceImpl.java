package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.WorkOrderAuditRecord;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.WorkOrderAuditRecordMapper;
import com.lerning.edu.services.WorkOrderAuditRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jiangwei
 * @since 18/8/15
 */
@Service
public class WorkOrderAuditRecordServiceImpl extends BaseServiceImpl<WorkOrderAuditRecord> implements WorkOrderAuditRecordService {
    @Autowired
    private WorkOrderAuditRecordMapper workOrderAuditRecordMapper;

    @Override
    public BaseMapper<WorkOrderAuditRecord> getMapper() {
        return workOrderAuditRecordMapper;
    }
}
