package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.WebTMKMapper;
import com.lerning.edu.services.WebTMKService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/2
 */
@Service
public class WebTMKServiceImpl implements WebTMKService {

    @Autowired
    public WebTMKMapper webTMKMapper;

    @Override
    public List queryWebTMKByTMK(Map map) {
        return webTMKMapper.queryWebTMKByTMK(map);
    }

    @Override
    public List querySaleModeByTMK() {
        return webTMKMapper.querySaleModeByTMK();
    }

    @Override
    public List queryDetail(Map map) {
        return webTMKMapper.queryDetail(map);
    }

    @Override
    public List queryDetailByApp(Map map) {
        return webTMKMapper.queryDetailByApp(map);
    }

    @Override
    public List queryDetailByShow(Map map) {
        return webTMKMapper.queryDetailByShow(map);
    }

    @Override
    public List queryDetailByEn(Map map) {
        return webTMKMapper.queryDetailByEn(map);
    }

    @Override
    public List queryWebTMKByCampus(Map map){
        return webTMKMapper.queryWebTMKByCampus(map);
    }

}
