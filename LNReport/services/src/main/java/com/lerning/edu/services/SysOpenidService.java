package com.lerning.edu.services;

import com.lerning.edu.beans.SysOpenid;

/**
 * @author jiangwei
 * @since 18/6/5
 */
public interface SysOpenidService extends BaseService<SysOpenid> {
    SysOpenid queryByOpenid(String openid);
}
