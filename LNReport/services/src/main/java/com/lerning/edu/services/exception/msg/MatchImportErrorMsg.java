/*
 * For com.royal.art
 * Copyright [2015/12/11] By FCHEN
 */
package com.lerning.edu.services.exception.msg;

/**
 * MatchImportErrorMsg
 * 赛事信息导入异常消息
 * @author FCHEN
 * @date 2015/12/11
 */
public enum MatchImportErrorMsg {

    EMPTY_FILE(1320,"导入文件数据为空"),

    NOT_BLANK(1321,"第%s行第%s列不允许为空"),

    WRONG_FORMAL(1322,"第%s行第%s列格式错误"),

    WRONG_AREA(1323,"第%s行省市区不对应")

    ;


    private int code;
    private String msg;
    MatchImportErrorMsg(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
