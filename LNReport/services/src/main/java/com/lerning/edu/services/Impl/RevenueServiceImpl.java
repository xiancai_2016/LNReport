package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.RevenueMapper;
import com.lerning.edu.services.RevenueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/5
 */
@Service
public class RevenueServiceImpl implements RevenueService {

    @Autowired
    private RevenueMapper revenueMapper;

    @Override
    public List queryRevenueByWeek(Map map) {
        return revenueMapper.queryRevenueByWeek(map);
    }

    @Override
    public int insertRevenues(List revenues) {
        return revenueMapper.insertRevenues(revenues);
    }

    @Override
    public List queryRevenuesData(Map dateMap) {
        return revenueMapper.queryRevenuesData(dateMap);
    }

    @Override
    public Map queryRevenueSum(Map dateMap) {
        return revenueMapper.queryRevenueSum(dateMap);
    }
}
