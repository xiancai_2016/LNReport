package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.CallTheRollMapper;
import com.lerning.edu.services.CallTheRollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/27
 */
@Service
public class CallTheRollServiceImpl implements CallTheRollService {

    @Autowired
    public CallTheRollMapper callTheRollMapper;

    @Override
    public List queryStudentInfo(Map map) {
        return callTheRollMapper.queryStudentInfo(map);
    }

    @Override
    public int addCallTheRoll(Map map) {
        return callTheRollMapper.addCallTheRoll(map);
    }
}
