package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.CampusDSRByJMSMapper;
import com.lerning.edu.services.CampusDSRByJMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/10/29
 */
@Service
public class CampusDSRByJMSServiceImpl implements CampusDSRByJMSService{
    @Autowired
    private CampusDSRByJMSMapper campusDSRByJMSMapper;

    @Override
    public List queryByUser(Map map) {
        return campusDSRByJMSMapper.queryByUser(map);
    }

    @Override
    public List querySaleModeByCampus(Map campusMap) {
        return campusDSRByJMSMapper.querySaleModeByCampus(campusMap);
    }

    @Override
    public List queryByUserForApp(Map map) {
        return campusDSRByJMSMapper.queryByUserForApp(map);
    }

    @Override
    public List queryByUserOfFPShowByTinTout(Map map) {
        return campusDSRByJMSMapper.queryByUserOfFPShowByTinTout(map);
    }

    @Override
    public List queryByUserOfOCShowByTinTout(Map map) {
        return campusDSRByJMSMapper.queryByUserOfOCShowByTinTout(map);
    }

    @Override
    public List queryByUserOfFPShowByWalkInRef(Map map) {
        return campusDSRByJMSMapper.queryByUserOfFPShowByWalkInRef(map);
    }

    @Override
    public List queryByUserOfOCShowByWalkInRef(Map map) {
        return campusDSRByJMSMapper.queryByUserOfOCShowByWalkInRef(map);
    }

    @Override
    public List queryEnByUser(Map map) {
        return campusDSRByJMSMapper.queryEnByUser(map);
    }

    @Override
    public List queryEnByUserSummary(Map map){
        return campusDSRByJMSMapper.queryEnByUserSummary(map);
    }

    @Override
    public List queryEnByUserNew(Map map) {
        return campusDSRByJMSMapper.queryEnByUserNew(map);
    }

    @Override
    public List queryEnByUserSummaryNew(Map map){
        return campusDSRByJMSMapper.queryEnByUserSummaryNew(map);
    }

    @Override
    public int queryByUserForAppCount(Map map) {
        return campusDSRByJMSMapper.queryByUserForAppCount(map);
    }

    @Override
    public int queryByUserForLeadsCount(Map map) {
        return campusDSRByJMSMapper.queryByUserForLeadsCount(map);
    }

    @Override
    public int queryCustomerOfCampusCount(Map map) {
        return campusDSRByJMSMapper.queryCustomerOfCampusCount(map);
    }

    @Override
    public Map queryCampus(String campusID) {
        return campusDSRByJMSMapper.queryCampus(campusID);
    }
}
