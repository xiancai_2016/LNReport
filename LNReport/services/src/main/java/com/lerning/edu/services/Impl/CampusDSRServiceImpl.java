package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.CampusDSRMapper;
import com.lerning.edu.services.CampusDSRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/5/17
 */
@Service
public class CampusDSRServiceImpl implements CampusDSRService {

    @Autowired
    private CampusDSRMapper campusDSRMapper;

    @Override
    public List queryDepartList() {
        return campusDSRMapper.queryDepartList();
    }

    @Override
    public List queryTIN(Map map) {
        return campusDSRMapper.queryTIN(map);
    }

    @Override
    public List queryREF(Map map) {
        return campusDSRMapper.queryREF(map);
    }

    @Override
    public List queryRENEW(Map map) {
        return campusDSRMapper.queryRENEW(map);
    }

    @Override
    public List queryFinanceMonths() {
        return campusDSRMapper.queryFinanceMonths();
    }

    @Override
    public List queryNaturalMonths() {
        return campusDSRMapper.queryNaturalMonths();
    }

    @Override
    public Map queryFinanceMonth(int AutoID) {
        return campusDSRMapper.queryFinanceMonth(AutoID);
    }

    @Override
    public Map queryNaturalMonth(int AutoID) {
        return campusDSRMapper.queryNaturalMonth(AutoID);
    }

    @Override
    public List chargeAndRefund(Map map) {
        return campusDSRMapper.chargeAndRefund(map);
    }

    @Override
    public Map totalChargeAndRefund(Map map) {
        return campusDSRMapper.totalChargeAndRefund(map);
    }

    public List queryClassCountByK(Map map) {
        return campusDSRMapper.queryClassCountByK(map);
    }

    public List queryStudentCount(Map map) {
        return campusDSRMapper.queryStudentCount(map);
    }

    @Override
    public List queryFinanceMonthOfDq(Map dqd) {
        return campusDSRMapper.queryFinanceMonthOfDq(dqd);
    }

    @Override
    public List queryNatureMonthOfDq(Map dqd) {
        return campusDSRMapper.queryNatureMonthOfDq(dqd);
    }

    @Override
    public Map querySCKCount(Map map) {
        return campusDSRMapper.querySCKCount(map);
    }

    @Override
    public int insertChargeAndRefund(List chargeAndRefund) {
        return campusDSRMapper.insertChargeAndRefund(chargeAndRefund);
    }

    @Override
    public int insertClassCount(List queryClassCount) {
        return campusDSRMapper.insertClassCount(queryClassCount);
    }

    @Override
    public int insertSKC(Map map) {
        return campusDSRMapper.insertSKC(map);
    }

    @Override
    public int insertStudentCount(List queryStudentCount) {
        return campusDSRMapper.insertStudentCount(queryStudentCount);
    }

    @Override
    public List queryStudentCountOfLock(Map map) {
        return campusDSRMapper.queryStudentCountOfLock(map);
    }

    @Override
    public List queryStudentCountOfLockAll(Map map) {
        return campusDSRMapper.queryStudentCountOfLockAll(map);
    }

    @Override
    public List chargeAndRefundOfLockAll(Map map) {
        return campusDSRMapper.chargeAndRefundOfLockAll(map);
    }

    @Override
    public List chargeAndRefundOfLock(Map map) {
        return campusDSRMapper.chargeAndRefundOfLock(map);
    }

    @Override
    public Map totalChargeAndRefundLock(Map map) {
        return campusDSRMapper.totalChargeAndRefundLock(map);
    }

    @Override
    public List queryClassCountByKOfLockAll(Map map) {
        return campusDSRMapper.queryClassCountByKOfLockAll(map);
    }

    @Override
    public List queryClassCountByKOfLock(Map map) {
        return campusDSRMapper.queryClassCountByKOfLock(map);
    }

    @Override
    public List queryDepart(Map map){
        return campusDSRMapper.queryDepart(map);
    }
    @Override
    public List queryEmployee(Map map){
        return campusDSRMapper.queryEmployee(map);
    }

    @Override
    public List queryUserOfDeparts(Map map){
        return campusDSRMapper.queryUserOfDeparts(map);
    }
    @Override

    public Map queryDepartByEmployee(Map map){
        return campusDSRMapper.queryDepartByEmployee(map);
    }

    @Override
    public List queryDepartListByWorkOrder(Map map){
        return campusDSRMapper.queryDepartListByWorkOrder(map);
    }

    @Override
    public List queryDepartById(Map map){
        return campusDSRMapper.queryDepartById(map);
    }
}
