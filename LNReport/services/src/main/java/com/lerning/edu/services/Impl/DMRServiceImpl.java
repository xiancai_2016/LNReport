package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.DMRMapper;
import com.lerning.edu.services.DMRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/17
 */
@Service
public class DMRServiceImpl implements DMRService {
    @Autowired
    private DMRMapper dmrMapper;

    @Override
    public List queryDmr(Map map){
        return dmrMapper.queryDmr(map);
    }
}
