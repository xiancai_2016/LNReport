/*
 * For com.royal.art
 * Copyright [2015/12/11] By FCHEN
 */
package com.lerning.edu.services.exception;

import com.lerning.edu.commons.exception.AbstractException;
import com.lerning.edu.services.exception.msg.MatchImportErrorMsg;

/**
 * MatchImportException
 * 导入赛事类异常
 * @author FCHEN
 * @date 2015/12/11
 */
public class MatchImportException extends AbstractException {

    private static final long serialVersionUID = -8628521223663747387L;

    public MatchImportException(int code, String msg) {
        super(code, msg);
    }

    public MatchImportException(MatchImportErrorMsg errorMsg, Object...obj) {
        super(errorMsg.getCode(), String.format(errorMsg.getMsg(),obj));
    }
}
