package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.SysReport;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.SysReportMapper;
import com.lerning.edu.services.SysReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/5
 */
@Service
public class SysReportServiceImpl extends BaseServiceImpl<SysReport> implements SysReportService{
    @Autowired
    private SysReportMapper sysReportMapper;

    @Override
    public BaseMapper<SysReport> getMapper(){return sysReportMapper;}

    @Override
    public SysReport queryReportById(Integer id){
        return sysReportMapper.queryReportById(id);
    }

    @Override
    public List<SysReport> findByUserId(SysReport sysReport){
        return sysReportMapper.findByUserId(sysReport);
    }

    @Override
    public List<SysReport> queryListByReport(SysReport sysReport){
        return sysReportMapper.queryListByReport(sysReport);
    }

    @Override
    public List<Map> queryReportAndOpenId(Map map){
        return sysReportMapper.queryReportAndOpenId(map);
    }

    @Override
    public int addPushRecord(Map map){
        return sysReportMapper.addPushRecord(map);
    }

}
