/*
 * For com.royal.art
 * Copyright [2015/11/14] By FCHEN
 */
package com.lerning.edu.services;

import com.lerning.edu.beans.SysUser;

import java.util.HashMap;

/**
 * SysUserService
 * 系统用户
 * @author FCHEN
 * @date 2015/11/14
 */
public interface SysUserService extends BaseService<SysUser>{

    /**
     * 根据用户账号查询用户信息
     * @param account
     * @return
     */
    SysUser queryUserByAccount(String account);

    /**
     * 更新用户登录次数
     * @param sysUser
     * @return
     */
    int updateLoginTimes(SysUser sysUser);

    /**
     * 根据用户账户修改用户密码
     * @param param
     * @return
     */
    int updateUserPwd(HashMap<String, String> param);

    /**
     * 删除用户角色关联数据
     * @param sysUser
     * @return
     */
    int deleteUserRole(SysUser sysUser);

    /**
     * 插入用户角色关联数据
     * @param sysUser
     * @return
     */
    int insertUserRole(SysUser sysUser);
}
