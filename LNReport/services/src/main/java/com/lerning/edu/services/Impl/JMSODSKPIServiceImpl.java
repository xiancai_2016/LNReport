package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.JMSODSKPIMapper;
import com.lerning.edu.services.JMSODSKPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/10/25
 */
@Service
public class JMSODSKPIServiceImpl implements JMSODSKPIService{

    @Autowired
    public JMSODSKPIMapper jmsodskpiMapper;

    @Override
    public List queryKPIByCampus(Map map) {
        return jmsodskpiMapper.queryKPIByCampus(map);
    }

    @Override
    public void updateAdd(Map map) {
        jmsodskpiMapper.update(map);

        jmsodskpiMapper.addReNew(map);
        jmsodskpiMapper.addRef(map);
        jmsodskpiMapper.addMkt(map);
    }

    @Override
    public List queryKPIByCC(Map dateMap) {
        return jmsodskpiMapper.queryKPIByCC(dateMap);
    }

    @Override
    public void updateAddByCC(Map map) {
        jmsodskpiMapper.updateByCC(map);

        jmsodskpiMapper.addReNewByCC(map);
        jmsodskpiMapper.addRefByCC(map);
        jmsodskpiMapper.addMktByCC(map);
    }
}
