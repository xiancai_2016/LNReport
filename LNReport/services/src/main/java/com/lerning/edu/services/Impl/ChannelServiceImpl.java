package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.ChannelMapper;
import com.lerning.edu.services.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/21
 */
@Service
public class ChannelServiceImpl implements ChannelService{
    @Autowired
    public ChannelMapper channelMapper;

    @Override
    public Map queryAll(Map map){
        return channelMapper.queryAll(map);
    }

    @Override
    public  Map queryAllByChannel2017(Map map){
        return channelMapper.queryAllByChannel2017(map);
    }

    @Override
    public Map queryByOdsReNew18(Map map){
        return channelMapper.queryByOdsReNew18(map);
    }

    @Override
    public int insertDsrByCampus(List channels){
        return channelMapper.insertDsrByCampus(channels);
    }

    @Override
    public int insertDsrByCampusSum(Map totalMap){
        return channelMapper.insertDsrByCampusSum(totalMap);
    }

    @Override
    public int insertDsr(Map map){
        return channelMapper.insertDsr(map);
    }
    @Override
    public List queryChannelsLockData(Map dateMap){
        return  channelMapper.queryChannelsLockData(dateMap);
    }
    @Override
    public List queryChannelsLockDataSum(Map dateMap){
        return channelMapper.queryChannelsLockDataSum(dateMap);
    }
}
