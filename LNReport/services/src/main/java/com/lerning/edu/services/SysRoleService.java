package com.lerning.edu.services;

import com.lerning.edu.beans.SysRole;

/**
 * Created by FCHEN on 2016/12/12.
 */
public interface SysRoleService extends BaseService<SysRole> {
    SysRole getRoleByName(String name);

    /**
     * 维护角色与菜单权限关系
     * @param sysRole
     * @return
     */
    int deleteRoleResource(SysRole sysRole);

    int insertRoleResource(SysRole sysRole);

    /**
     * 维护角色与菜单報表关系
     * @param sysRole
     * @return
     */
    int deleteRoleReport(SysRole sysRole);

    int insertRoleReport(SysRole sysRole);

//    void saveRole(SysRole sysRole);
}
