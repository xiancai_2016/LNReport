package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.RefSituationMapper;
import com.lerning.edu.services.RefSituationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/9
 */
@Service
public class RefSituationServiceImpl implements RefSituationService{
    @Autowired
    public RefSituationMapper refSituationMapper;

    @Override
    public List queryRef(Map map){
        return  refSituationMapper.queryRef(map);
    }

    @Override
    public Map queryEnByCampus(Map map){
        return refSituationMapper.queryEnByCampus(map);
    }

}
