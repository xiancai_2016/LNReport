package com.lerning.edu.services;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/29
 */
public interface DSRMonthKPIService {

    //查询完成业绩
    Map queryAll(Map map);
    //查询18年提前续费的业绩
    Map queryRenewByCmpus(Map map);
    //查询指标
    List queryCampusKpi(Map map);

    int insertDsrMonthKPI(List list);//锁定指标业绩数据

    List queryDsrMonthKpiLockData(Map dateMap);
    Map queryDsrMonthKpiLockDataSum(Map dateMap);
}
