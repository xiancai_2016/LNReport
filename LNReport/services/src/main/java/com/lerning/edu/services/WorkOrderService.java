package com.lerning.edu.services;

import com.lerning.edu.beans.WorkOrder;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/14
 */
public interface WorkOrderService extends BaseService<WorkOrder> {

    Map queryByUserId(Map map);

    Map queryMaxOrderNo(Map map);

    Map queryDetail(Map map);

    Map queryDetail1(Map map);

    Map queryChangeSchoolOfClass(Map map);

    List findWorkOrder(Map map);

    int findWorkOrderCount(Map map);

    List findWorkOrderByAdd(Map map);

    int findWorkOrderCountByAdd(Map map);

    List queryEmployeeByCfield(String account);

    List queryEmployeeByUserID(String userID);

    List queryEmployeeByCfieldOfLN(String account);

    List queryEmployeeByUserIDOfLN(String userID);

    List findMyTaskList(Map map);

    int findMyTaskListCount(Map map);

    Map queryWorkOrderRecordById(String cID);

    List queryByRefundFee(Map map);
    List queryByShiftInfo(Map map);

    List queryByRefundFeeLock(Map map);

    List findWorkOrderOfFundFee(Map map);

    int findWorkOrderCountOfFundFee(Map map);

    void addWorkOrder(WorkOrder workOrder);

    List queryOdsUser(Map map);
}
