package com.lerning.edu.services.Impl;

import com.lerning.edu.data.mapper.DSRMonthKPIMapper;
import com.lerning.edu.services.DSRMonthKPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/29
 */
@Service
public class DSRMonthKPISericeImpl implements DSRMonthKPIService {

    @Autowired
    private DSRMonthKPIMapper dsrMonthKPIMapper;

    @Override
    public Map queryAll(Map map) {
        return dsrMonthKPIMapper.queryAll(map);
    }

    @Override
    public Map queryRenewByCmpus(Map map) {
        return dsrMonthKPIMapper.queryRenewByCmpus(map);
    }

    @Override
    public List queryCampusKpi(Map map) {
        return dsrMonthKPIMapper.queryCampusKpi(map);
    }

    @Override
    public int insertDsrMonthKPI(List list) {
        return dsrMonthKPIMapper.insertDsrMonthKPI(list);
    }
    @Override
    public List queryDsrMonthKpiLockData(Map dateMap){
        return dsrMonthKPIMapper.queryDsrMonthKpiLockData(dateMap);
    }
    @Override
    public Map queryDsrMonthKpiLockDataSum(Map dateMap){
        return dsrMonthKPIMapper.queryDsrMonthKpiLockDataSum(dateMap);
    }
}
