package com.lerning.edu.services.Impl;

import com.lerning.edu.beans.Student;
import com.lerning.edu.data.mapper.BaseMapper;
import com.lerning.edu.data.mapper.StudentMapper;
import com.lerning.edu.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/7
 */
@Service
public class StudentServiceImpl extends BaseServiceImpl<Student> implements StudentService{

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public BaseMapper<Student> getMapper() {
        return studentMapper;
    }

    @Override
    public List findStudent(Map map){
        return studentMapper.findStudent(map);
    }

    @Override
    public int findStudentCount(Map map){
        return studentMapper.findStudentCount(map);
    }

    @Override
    public Map findStudentOne(Map map){
        return studentMapper.findStudentOne(map);
    }

    @Override
    public List findClass(Map map){
        return studentMapper.findClass(map);
    }

    @Override
    public List queryResumeList(Map map){
        return studentMapper.queryResumeList(map);
    }

    @Override
    public List findClassByStop(Map map){
        return studentMapper.findClassByStop(map);
    }

    @Override
    public List findClassByCampus(Map map){
        return studentMapper.findClassByCampus(map);
    }

    @Override
    public List findClassByResume(Map map){
        return studentMapper.findClassByResume(map);
    }

    @Override
    public Student queryByUserId(String userID){
        return studentMapper.queryByUserId(userID);
    }

    @Override
    public int findClassByCampusCount(Map map){
        return studentMapper.findClassByCampusCount(map);
    }

    @Override
    public List queryReceipt(Map map){
        return studentMapper.queryReceipt(map);
    }

    @Override
    public  Map queryElectronicBalance(Map map){
        return studentMapper.queryElectronicBalance(map);
    }

    @Override
    public Map queryClassByCost(Map map){
        return studentMapper.queryClassByCost(map);
    }

    @Override
    public List queryEmployees(Map map) {
        return studentMapper.queryEmployees(map);
    }

}
