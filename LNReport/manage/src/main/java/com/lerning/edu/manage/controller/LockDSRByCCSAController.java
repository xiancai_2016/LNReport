package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.CampusDsrNewService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/22
 */
@Component
@EnableScheduling
public class LockDSRByCCSAController {
    private static Logger log = LoggerFactory.getLogger(LockDSRByCCSAController.class);

    @Autowired
    private CampusDSRService campusDSRService;
    @Autowired
    private CampusDsrNewService campusDsrNewService;

    CampusDsrNewController cnc = new CampusDsrNewController();

    NumberFormat nf = new DecimalFormat("￥,###.##");
    DecimalFormat df = new DecimalFormat("######0");

    /***
     * CC/AS业绩达成排行
     * 每周一凌晨0点35分执行(周一报表)
     */
    @Scheduled(cron = "0 35 0 ? * MON")
    public void dsrByCampusKpiForFRI() {
        //获取前一天年月日
        String lastDate = DateUtil.getLastDateStr1() + " 23:59:59";

        //获取前三天的时间
        String lastTheDate = DateUtil.getLastDateStrN(3) + " 23:59:59";

        //跨财务月时计算数据锁定日期
        Map dqd = new HashMap();
        dqd.put("dqDay", lastTheDate.subSequence(0, 7));
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dqd);//当前月
        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);

        Map dqd1 = new HashMap();
        dqd1.put("dqDay", lastDate.subSequence(0, 7));
        List FinanceMonthOfDq1 = campusDSRService.queryNatureMonthOfDq(dqd1);//当前月
        Map dateMap1 = (HashMap) FinanceMonthOfDq1.get(0);

        //财务月的后一个周五跟财务月最后一天在同一个月,则锁定下个财务月的数据
        if (DateUtil.compare_date(dateMap1.get("EndDate").toString(), lastTheDate)) {
            dateMap.put("dqDay", DateUtil.addMonth(lastTheDate).substring(0,7));
            FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dateMap);//当前月
            dateMap = (HashMap) FinanceMonthOfDq.get(0);
        }

        if (!DateUtil.compare_date(dateMap.get("EndDate").toString(), lastDate)) {
            lastDate = dateMap.get("EndDate").toString();
        }
        //跨财务月时计算数据锁定日期

        String beninDate = dateMap.get("BeginDate").toString();

        String TIN = "27F05A82-5601-45B2-83A5-8360A641F41F,65C4B6C0-465E-427B-96BB-85225704BA9D";
        String TOUT = "FBE4B6F5-E5C9-4D36-BBB2-F31EE975F076";
        String WalkIN = "EADC6B6D-4E9F-4DB2-B8DC-0739F72E2507";
        String CM = "559FF86A-AC29-46FE-84AD-CE774B8EFB79";

        List depars = campusDSRService.queryDepartList();
        if (depars.size() > 0) {
            for (Object obj : depars) {
                Map dMap = (HashMap) obj;

                log.info(dMap.get("cNAME").toString());

                Map map = new HashMap();
                String CampusID = dMap.get("cID").toString();

                map.put("CampusID", dMap.get("cID"));
                map.put("BeginDate", beninDate.substring(0,10));
                map.put("EndDate", lastDate.substring(0,10));

                map.put("FBeginDate", beninDate);
                map.put("FEndDate", lastDate);
                map.put("SaleModeIds", TIN);
                List tinByUser = campusDsrNewService.queryByUserForApp(map);
                List tinByUserFPShow = campusDsrNewService.queryByUserOfFPShowByTinTout(map);
                List tinByUserOCShow = campusDsrNewService.queryByUserOfOCShowByTinTout(map);
                List tinEnByUser = campusDsrNewService.queryEnByUserNew(map);

                map.put("SaleModeIds", TOUT);
                List toutByUser = campusDsrNewService.queryByUserForApp(map);
                List toutByUserFPShow = campusDsrNewService.queryByUserOfFPShowByTinTout(map);
                List toutByUserOCShow = campusDsrNewService.queryByUserOfOCShowByTinTout(map);
                List toutEnByUser = campusDsrNewService.queryEnByUserNew(map);

                map.put("SaleModeIds", WalkIN);
                List winByUser = campusDsrNewService.queryByUser(map);
                List winByUserFPShow = campusDsrNewService.queryByUserOfFPShowByWalkInRef(map);
                List winByUserOCShow = campusDsrNewService.queryByUserOfOCShowByWalkInRef(map);
                List winEnByUser = campusDsrNewService.queryEnByUserNew(map);

                //合并app ,FreshPresShow,OCShow和En
                List tinList = getCampusOfUser(tinByUser, CampusID, TIN, beninDate, lastDate, "app", tinByUserFPShow, tinByUserOCShow, tinEnByUser);
                List toutList = getCampusOfUser(toutByUser, CampusID, TOUT, beninDate, lastDate, "app", toutByUserFPShow, toutByUserOCShow, toutEnByUser);
                List winList = getCampusOfUser(winByUser, CampusID, WalkIN, beninDate, lastDate, "leads", winByUserFPShow, winByUserOCShow, winEnByUser);

                Map listMap = new HashMap();
                listMap.put("text",lastDate.substring(0,10));
                listMap.put("tinList",tinList);
                listMap.put("toutList",toutList);
                listMap.put("winList",winList);

                Map ebsMap = new HashMap();
                ebsMap.put("BeginDate", beninDate);
                ebsMap.put("EndDate", lastDate);
                ebsMap.put("CampusID", dMap.get("cID"));
                ebsMap.put("text", lastDate.substring(0, 7));

                List enByUserSummary = campusDsrNewService.queryEnByUserSummaryNew(ebsMap);

                if (tinList.size() > 0) {
                    campusDsrNewService.insertDSRByCCASByTin(listMap);
                }

                if (toutList.size() > 0) {
                    campusDsrNewService.insertDSRByCCASByTout(listMap);
                }

                if (winList.size() > 0) {
                    campusDsrNewService.insertDSRByCCASByWin(listMap);
                }

                if (enByUserSummary.size() > 0) {
                    campusDsrNewService.insertEnByUser(enByUserSummary);
                }
            }
        }
        log.info("DSR BY CC/AS 锁定成功");
    }

    public List getCampusOfUser(List list, String cID, String SaleModeIds, String BeginDate, String EndDate, String type, List fpShow, List ocShow, List enByUser) {
        List returnList = new ArrayList();

        List fpList = new ArrayList();
        List ocList = new ArrayList();

        if (list.size() > 0) {
            for (Object obj : list) {
                Map map = (HashMap) obj;

                map.put("CashInValue", nf.format(map.get("CashIn")));
                map.put("oc1", map.get("oc"));
                map.put("Contracts1", map.get("Contracts"));

                BigDecimal fp = new BigDecimal(map.get("fp").toString());
                BigDecimal app = new BigDecimal(map.get("app").toString());

                String CampusID = map.get("cLevelString").toString();
                CampusID = CampusID.replace("|", "");

                if (!StringUtil.isEmpty(CampusID)) {
                    Map tDepart = campusDsrNewService.queryCampus(CampusID);
                    map.put("userCampus", tDepart.get("cName"));
                    map.put("userCampusID", tDepart.get("cID"));

                    if (!StringUtil.divideValue(app)) {
                        map.put("ShowRatio", 0);
                    } else {
                        map.put("ShowRatio", df.format(fp.divide(app, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
                    }

                    if (cID.equals(CampusID)) {
                        returnList.add(map);
                        fpList.add(map);
                    } else {
                        Map countMap = new HashMap();
                        countMap.put("cField1", map.get("cField1"));
                        countMap.put("CampusID", cID);
                        countMap.put("SaleModeIds", SaleModeIds);
                        countMap.put("BeginDate", BeginDate);
                        countMap.put("EndDate", EndDate);

                        int countApp = 0;

                        if (type.equals("app")) {
                            countApp = campusDsrNewService.queryByUserForAppCount(countMap);
                        } else {
                            countApp = campusDsrNewService.queryByUserForLeadsCount(countMap);
                        }

                        int customerOfCampusCount = campusDsrNewService.queryCustomerOfCampusCount(countMap);

                        if (countApp > 0 && customerOfCampusCount == 1) {
                            returnList.add(map);
                            fpList.add(map);
                        }
                    }
                }

                //不在app里的员工,并且fp show不为0 (FP)
                if (fpShow.size() > 0) {
                    String cName = map.get("cField1").toString();//app
                    for (Object objApp : fpShow) {
                        Map mapApp = (HashMap) objApp;
                        String cNameFP = mapApp.get("cField1").toString();//pfShow
                        String fpCampusID = mapApp.get("cLevelString").toString();
                        fpCampusID = fpCampusID.replace("|", "");

                        int fpShowCount = (int) mapApp.get("fp");

                        if (!cName.equals(cNameFP) && cID.equals(fpCampusID) && fpShowCount > 0) {
                            mapApp.put("app", 0);
                            mapApp.put("oc", 0);
                            mapApp.put("oc1", 0);
                            mapApp.put("Contracts1", 0);
                            mapApp.put("Contracts", 0.00);
                            mapApp.put("CashIn", 0.00);
                            mapApp.put("CashInValue", nf.format(0));
                            mapApp.put("ShowRatio", 0);
                            returnList.add(mapApp);
                            fpList.add(mapApp);
                        }
                    }
                }
            }

            if (fpList.size() == 0) {
                fpList = list;
            }
            //不在app里的员工,并且OC show不为0 (OC)
            if (fpList.size() > 0 && ocShow.size() > 0) {
                for (Object objFP : fpList) {
                    Map fpMap = (HashMap) objFP;
                    String cName = fpMap.get("cField1").toString();//fp
                    for (Object objOC : ocShow) {
                        Map mapOC = (HashMap) objOC;
                        String cNameFP = mapOC.get("cField1").toString();//pfShow

                        String ocCampusID = mapOC.get("cLevelString").toString();
                        ocCampusID = ocCampusID.replace("|", "");

                        int oc = (int) mapOC.get("oc");

                        if (!cName.equals(cNameFP) && cID.equals(ocCampusID) && oc > 0) {
                            mapOC.put("app", 0);
                            mapOC.put("fp", 0);
                            mapOC.put("oc1", oc);
                            mapOC.put("Contracts1", 0);
                            mapOC.put("Contracts", 0.00);
                            mapOC.put("CashIn", 0.00);
                            mapOC.put("CashInValue", nf.format(0));
                            mapOC.put("ShowRatio", 0);
                            returnList.add(mapOC);
                            ocList.add(mapOC);
                        }
                    }
                }
            }

            if (ocList.size() == 0) {
                ocList = fpList;
            }
            //不在app里的员工,并且有En (En)
            if (ocList.size() > 0) {
                for (Object objOC : ocList) {
                    Map ocMap = (HashMap) objOC;
                    String cName = ocMap.get("cField1").toString();//oc
                    for (Object objEn : enByUser) {
                        Map mapEn = (HashMap) objEn;
                        String cNameEn = mapEn.get("cField1").toString();//En

                        String enCampusID = mapEn.get("cLevelString").toString();
                        enCampusID = enCampusID.replace("|", "");

                        Double Contracts = Double.valueOf(mapEn.get("Contracts").toString());

                        if (!cName.equals(cNameEn) && Contracts > 0 && cID.equals(enCampusID)) {
                            mapEn.put("app", 0);
                            mapEn.put("fp", 0);
                            mapEn.put("oc", 0);
                            mapEn.put("oc1", 0);
                            mapEn.put("ShowRatio", 0);
                            mapEn.put("Contracts1", Contracts);
                            mapEn.put("CashIn1", mapEn.get("CashIn"));
                            mapEn.put("CashInValue", nf.format(mapEn.get("CashIn")));
                            returnList.add(mapEn);
                        }
                    }
                }
            }
        }

        //去重
        HashSet h = new HashSet(returnList);
        returnList.clear();
        returnList.addAll(h);

        return cnc.removeDuplicate(returnList);
    }
}
