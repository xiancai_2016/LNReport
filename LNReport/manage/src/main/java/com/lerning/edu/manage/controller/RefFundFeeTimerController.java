package com.lerning.edu.manage.controller;

import com.lerning.edu.beans.Student;
import com.lerning.edu.beans.WorkOrder;
import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.manage.common.EmailSend;
import com.lerning.edu.manage.context.GlobalContext;
import com.lerning.edu.services.StudentService;
import com.lerning.edu.services.WorkOrderAuditRecordService;
import com.lerning.edu.services.WorkOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 17/9/26
 */
@Lazy(false)
@Component
@EnableScheduling
public class RefFundFeeTimerController {

    private static Logger log = LoggerFactory.getLogger(RefFundFeeTimerController.class);

    @Autowired
    public StudentService studentService;
    @Autowired
    public WorkOrderService workOrderService;
    @Autowired
    private WorkOrderAuditRecordService workOrderAuditRecordService;

    /***
     * 每天晚上23点50点执行(添加退费记录)
     */
    @Scheduled(cron = "0 55 23 * * ?")
    public void sendDayReport() throws Exception {
        //获取前一天年月日
        String lastDate = DateUtil.getLastDateStr1() + " 23:54:59";
        //获取当前时间
        String lastTheDate = DateUtil.getLastDateStrN(0) + " 23:55:00";

        Map map = new HashMap<>();
        map.put("beginTime", lastDate);
        map.put("endTime", lastTheDate);

        String emailConten = "";

        List ByRefundFee = workOrderService.queryByRefundFeeLock(map);

        if (ByRefundFee.size() > 0) {
            for (Object obj : ByRefundFee) {
                Map objMap = (HashMap) obj;
                WorkOrder workOrder = new WorkOrder();

                map.put("campusID", objMap.get("campusID"));
                map.put("cAskPerson", objMap.get("cAskPerson"));

                List employees = studentService.queryEmployees(map);
                if (employees.size() > 0) {
                    Map tEmployeeMap = (HashMap) employees.get(0);
                    workOrder.setCcreateuser(tEmployeeMap.get("cUserID").toString());
                } else {
                    workOrder.setCcreateuser("");
                }

                Student student = studentService.queryByUserId(objMap.get("cStudentUserID").toString());

                workOrder.setCuserid(objMap.get("cStudentUserID").toString());
                workOrder.setCclassid(objMap.get("cChargeID").toString());
                workOrder.setCstatus(4);//操作成功
                workOrder.setCtype(5);//退费
                workOrder.setCreason("");
                workOrder.setCstopclassdate(new Date());
                workOrder.setCrecoverydate(DateUtil.formateStringToDate(objMap.get("cAskDate").toString(), Constants.DATE_FORMART2));
                workOrder.setCisoriginal(0);
                workOrder.setCremarks("");
                workOrder.setCisprint(2);
                workOrder.setCcreatedate(DateUtil.formateStringToDate(objMap.get("cAskDate").toString(), Constants.DATE_FORMART2));
                workOrder.setResumeClassID("");

                map.put("userId", objMap.get("cStudentUserID").toString());//学生ID
                Map users = workOrderService.queryByUserId(map);
                workOrder.setCampus(users.get("cName").toString());

                map.put("campus", workOrder.getCampus());
                Map maxOrderNo = workOrderService.queryMaxOrderNo(map);

                String orderNo = DateUtil.getYear().substring(2, 4) + users.get("cAddress").toString();
                String oNo = "";
                if (maxOrderNo == null) {
                    oNo = orderNo + "0001";
                    workOrder.setCorderno(oNo);
                } else {
                    oNo = maxOrderNo.get("cOrderNo").toString();
                    workOrder.setCorderno(orderNo + StringUtil.autoGenericCode(oNo.substring(oNo.length() - 4, oNo.length()), 4));
                }

                workOrderService.addWorkOrder(workOrder);

                String content = "退费单号:" + oNo + ",学生" + student.getCname() + "(电话" + student.getCsmstel() + "), 在" + objMap.get("cAffirmDate") + "退费";

                emailConten = content + "<br>" + emailConten;
            }

            List sendEmailUser = new ArrayList();

            sendEmailUser.add(GlobalContext.getProperties("EmailUserOfCost"));


            StringBuffer sendContent = new StringBuffer("您好! <br> ");
            sendContent.append(emailConten);

            String from = "特况单任务提醒";
            String title = "特况处理完成通知！";

            //发送邮件
            EmailSend.emailSend(sendEmailUser, from, title, sendContent.toString());
        }
    }
}
