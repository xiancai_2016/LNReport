package com.lerning.edu.manage.common;

import com.lerning.edu.commons.util.ResultCode;

/**
 * @author
 */
public class ResultViewObject {

    private Integer code;

    private String messages;

    private Object result;

    public ResultViewObject() {
        this( ResultCode.SUCCESS );
    }

    public ResultViewObject( Integer code ) {
        this( code, null );
    }

    public ResultViewObject( Integer code, Object result ) {
        this( code , result,null );
    }

    public ResultViewObject( Integer code, Object result , String message ) {
        this.code = code ;
        this.messages = message ;
        this.result = result ;
    }


    public static ResultViewObject success( String message ) {
        return new ResultViewObject( ResultCode.SUCCESS, message );
    }

    public static ResultViewObject success( Object result ) {
        return new ResultViewObject( ResultCode.SUCCESS, result );
    }

    public static ResultViewObject fail( String message ) {
        return new ResultViewObject( ResultCode.FAILURE, message );
    }

    public static ResultViewObject fail( Object result ) {
        return new ResultViewObject( ResultCode.FAILURE, result );
    }

    public Integer getCode() {
        return code;
    }

    public ResultViewObject setCode(Integer code) {
        this.code = code;
        return this;
    }


}
