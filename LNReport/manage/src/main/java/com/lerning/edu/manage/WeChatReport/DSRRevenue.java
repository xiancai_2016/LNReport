package com.lerning.edu.manage.WeChatReport;

import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.manage.controller.CampusDSRController;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.RevenueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/5
 */
@Controller
@RequestMapping("wechat")
public class DSRRevenue {

    @Autowired
    private RevenueService revenueService;
    @Autowired
    private CampusDSRService campusDSRService;
    CampusDSRController cdc = new CampusDSRController();

    @RequestMapping("/revenue")
    public String dsrChannel(HttpServletRequest request,
                             @RequestParam(value = "dateTime", required = false) String dateTime) {

        Map dateMap = new HashMap();
        dateMap.put("dqDay", dateTime.subSequence(0, 7));

        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dateMap);//当前月
        Map map = (HashMap) FinanceMonthOfDq.get(0);

        dateMap.put("EndDate", dateTime);

        //锁定数据
        List revenues = revenueService.queryRevenuesData(dateMap);
        Map revenueSum = revenueService.queryRevenueSum(dateMap);

        if (revenueSum != null) {
            if (StringUtil.divideValue(new BigDecimal(revenueSum.get("totalKpi").toString()))) {
                revenueSum.put("kpiRate", new BigDecimal(revenueSum.get("wFst").toString()).multiply(new BigDecimal(100)).divide(new BigDecimal(revenueSum.get("totalKpi").toString()), 2, BigDecimal.ROUND_HALF_UP));
            } else {
                revenueSum.put("kpiRate", 0.00);
            }
        }

        request.setAttribute("revenues", revenues);
        request.setAttribute("revenueSum", revenueSum);

        request.setAttribute("dateTime", dateTime);

        request.setAttribute("reportInfo", "Revenue月度KPI达成");
        request.setAttribute("date", map.get("BeginDate").toString().substring(0, 10) + "到" + dateTime);

        return "wechar/revenue";
    }
}
