/*
 * For com.royal.art
 * Copyright [2015/11/26] By FCHEN
 */
package com.lerning.edu.manage.controller;

import com.lerning.edu.beans.SysDict;
import com.lerning.edu.beans.page.PageList;
import com.google.common.base.Splitter;
import com.lerning.edu.manage.util.UserUtils;
import com.lerning.edu.services.SysDictService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * DictController
 * 字典
 * @author FCHEN
 * @date 2015/11/26
 */
@Controller
@RequestMapping("dict")
public class SysDictController extends BaseController {

    @Autowired
    private SysDictService dictService;

    /**
     * 查询列表  不做查询数据处理
     * @param dict
     * @param request
     * @return
     */
    @RequiresPermissions("sys:dict:view")
    @RequestMapping(value = {"","/","list"})
    public String list(SysDict dict, HttpServletRequest request){
        dict.setDeleted(null);
        request.setAttribute("dict",dict);
        return "sys/dict/list";
    }

    /**
     * 异步获取list资源
     * @param pageList
     * @param dict
     * @return
     */
    @RequiresPermissions("sys:dict:view")
    @RequestMapping("asynList")
    @ResponseBody
    public PageList asynList(PageList pageList, SysDict dict){
        dict.setDeleted(null);
        return dictService.queryPage(pageList,dict);
    }

    /**
     * 下拉
     * @param pItemKey
     * @param ignores
     * @return
     */
    @RequestMapping("dictSelList")
    @ResponseBody
    public List<SysDict> asynList(String pItemKey,String ignores){
        SysDict dict = new SysDict();
        dict.setpItemKey(pItemKey);
        dict.setDeleted(false);
        if(StringUtils.isNotBlank(ignores)
                && !",".equals(ignores)){
            dict.setIgnores(Splitter.on(",").splitToList(ignores));
        }
        return dictService.queryList(dict);
    }

    /**
     * 新增修改
     * @param id
     * @return
     */
    @RequiresPermissions("sys:dict:edit")
    @RequestMapping(value = {"add","edit"},method = RequestMethod.GET)
    public String edit(Long id, HttpServletRequest request){
        SysDict dict;
        if(id == null|| id < 1l ){ //add
            dict = new SysDict();
        }else{//edit
            dict = dictService.queryById(id);
        }

        request.setAttribute("dict",dict);
        return "sys/dict/edit";
    }

    @RequiresPermissions("sys:dict:edit")
    @RequestMapping(value = "save",method = RequestMethod.POST )
    public String save(SysDict dict,HttpServletRequest request){
        int result = -1;
//        dict.setOperator(getLoginUser(request).getId());
        dict.setOperator(UserUtils.getUser().getId());
        if( dict.getId() == null ){ //add
            result = dictService.add(dict);
        }else{//edit
            result = dictService.update(dict);
        }
        return "redirect:list";
    }

    @RequiresPermissions("sys:dict:delete")
    @RequestMapping("delete")
    @ResponseBody
    public Object delete(Long id,HttpServletRequest request){
        SysDict dict = new SysDict();
        dict.setId(id);
        dict.setOperator(UserUtils.getUser().getId());
        int result = dictService.delete(dict);
        return result;
    }

    /**
     * 不存在  返回true 存在返回false,条件为空返回false
     * @param dict
     * @param request
     * @return
     */
    @RequestMapping("notExists")
    @ResponseBody
    public Object exists(SysDict dict, HttpServletRequest request){
        if(StringUtils.isBlank(dict.getItemKey())
                ||StringUtils.isBlank(dict.getpItemKey())){
            return true;
        }
        return !(dictService.exists(dict) > 0);
    }
}
