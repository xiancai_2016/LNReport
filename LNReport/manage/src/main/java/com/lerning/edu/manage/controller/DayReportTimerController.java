package com.lerning.edu.manage.controller;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.lerning.edu.commons.getopenid.WxUtils;
import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.wecharpush.HttpClientWaChatSend;
import com.lerning.edu.commons.wecharpush.TemplateData;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.SysReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 17/9/26
 */
@Lazy(false)
@Component
@EnableScheduling
public class DayReportTimerController {

    private static Logger log = LoggerFactory.getLogger(DayReportTimerController.class);

    @Autowired
    private SysReportService sysReportService;
    @Autowired
    private CampusDSRService campusDSRService;

    /***
     * 每天上午9点执行(日报表)
     */
    @Scheduled(cron = "0 57 8 * * ?")
    public void sendDayReport() {
        Gson gson = new Gson();
        String at = WxUtils.getAccessToken(Constants.GET_TOKEN_URL);

        Map map = new HashMap();
        map.put("sysReportType", "");//模板类型
        map.put("pushType", "1");//推送类型(1:按天, 2:周五推送, 3:财务月,4:自然月,5:周一推送)

        List<Map> openidAndReports = sysReportService.queryReportAndOpenId(map);
        if (openidAndReports.size() > 0) {
            for (Map rMap : openidAndReports) {

                Map mapSend = new HashMap();
                mapSend.put("touser", rMap.get("openid"));
                mapSend.put("template_id", Constants.template_id);
                mapSend.put("url", rMap.get("url") + "?type=1&CampusID=&dateTime=" + DateUtil.getYearAndMonth());

                Map param = new HashMap();
                param.put("first", new TemplateData(rMap.get("userName") + "您好, 已为您生成以下报表", "#000000"));
                param.put("keyword1", new TemplateData("日报表", "#000000"));

                param.put("keyword2", new TemplateData(DateUtil.dateToString(new Date(), Constants.DATE_FORMART) + " " + DateUtil.getWeek(), "#000000"));

                param.put("keyword3", new TemplateData(rMap.get("name").toString(), "#000000"));
                param.put("remark", new TemplateData("", "#000000"));
                mapSend.put("data", param);

                JSON jbB = JSON.parseObject(HttpClientWaChatSend.sendPostRequest(Constants.SEND_URL + at, gson.toJson(mapSend)));
                //添加推送记录
                String campusID = "";
                addPushRecord(rMap, DateUtil.getYearAndMonth(), DateUtil.getWeek(), campusID);

                log.info(jbB.toString(), rMap.get("userName") + "的" + rMap.get("name") + "报表" + "发送成功");
            }
        }
    }

    /***
     * 每周五上午9点3分执行(周五报表)
     * <p/>
     * 周一:MON,周二:TUE,周三:WED,周四:THU,周五:FRI,周六:SAT,周日:SUN
     * MON-FRI: 周一至周五
     */
    @Scheduled(cron = "0 3 9 ? * FRI")
    public void sendWeekReportOfFRI() {
        Gson gson = new Gson();
        String at = WxUtils.getAccessToken(Constants.GET_TOKEN_URL);

        Map map = new HashMap();
        map.put("sysReportType", "");//模板类型
        map.put("pushType", "2");//推送类型(1:按天, 2:周五推送, 3:财务月,4:自然月,5:周一推送)

        List<Map> openidAndReports = sysReportService.queryReportAndOpenId(map);
        if (openidAndReports.size() > 0) {
            for (Map rMap : openidAndReports) {

                Map mapSend = new HashMap();
                mapSend.put("touser", rMap.get("openid"));
                mapSend.put("template_id", Constants.template_id);
                mapSend.put("url", rMap.get("url") + "?type=2&CampusID=&dateTime=" + DateUtil.getLastDateStr1());

                Map param = new HashMap();
                param.put("first", new TemplateData(rMap.get("userName") + "您好, 已为您生成以下报表", "#000000"));
                param.put("keyword1", new TemplateData("周报表", "#000000"));

                param.put("keyword2", new TemplateData(DateUtil.dateToString(new Date(), Constants.DATE_FORMART) + " " + DateUtil.getWeek(), "#000000"));

                param.put("keyword3", new TemplateData(rMap.get("name").toString(), "#000000"));
                param.put("remark", new TemplateData("", "#000000"));
                mapSend.put("data", param);

                JSON.parseObject(HttpClientWaChatSend.sendPostRequest(Constants.SEND_URL + at, gson.toJson(mapSend)));
                //添加推送记录
                String campusID = "";
                addPushRecord(rMap, DateUtil.getLastDateStr1(), DateUtil.getWeek(), campusID);

                log.info(rMap.get("userName") + "的" + rMap.get("name") + "报表" + "发送成功");
            }
        }
    }

    /***
     * 每周一上午9点执行(周一报表)
     */
    @Scheduled(cron = "0 0 9 ? * MON")
    public void sendWeekReportOfMON() {

        //获取前一天年月日
        String lastDate = DateUtil.getLastDateStr1() + " 23:59:59";

        //获取钱三天的时间
        String lastTheDate = DateUtil.getLastDateStrN(3) + " 23:59:59";

        lastDate = dateLastDate(lastTheDate, lastDate);

        Gson gson = new Gson();
        String at = WxUtils.getAccessToken(Constants.GET_TOKEN_URL);

        Map map = new HashMap();
        map.put("sysReportType", "");//模板类型
        map.put("pushType", "5");//推送类型(1:按天, 2:周五推送, 3:财务月,4:自然月,5:周一推送)

        List<Map> openidAndReports = sysReportService.queryReportAndOpenId(map);
        if (openidAndReports.size() > 0) {
            for (Map rMap : openidAndReports) {

                Map mapSend = new HashMap();
                mapSend.put("touser", rMap.get("openid"));
                mapSend.put("template_id", Constants.template_id);
                mapSend.put("url", rMap.get("url") + "?type=2&CampusID=&dateTime=" + lastDate.substring(0, 10));

                Map param = new HashMap();
                param.put("first", new TemplateData(rMap.get("userName") + "您好, 已为您生成以下报表", "#000000"));
                param.put("keyword1", new TemplateData("周报表", "#000000"));

                param.put("keyword2", new TemplateData(DateUtil.dateToString(new Date(), Constants.DATE_FORMART) + " " + DateUtil.getWeek(), "#000000"));

                param.put("keyword3", new TemplateData(rMap.get("name").toString(), "#000000"));
                param.put("remark", new TemplateData("", "#000000"));
                mapSend.put("data", param);

                JSON jbB = JSON.parseObject(HttpClientWaChatSend.sendPostRequest(Constants.SEND_URL + at, gson.toJson(mapSend)));
                //添加推送记录
                String campusID = "";
                addPushRecord(rMap, lastDate.substring(0, 10), DateUtil.getWeek(), campusID);

                log.info(jbB.toString(), rMap.get("userName") + "的" + rMap.get("name") + "报表" + "发送成功");
            }
        }
    }

    /***
     * 每天早上10点执行(财务月月报表)
     */
    @Scheduled(cron = "0 02 10 * * ?")
    public void sendMonthReport() {
        String lastDate = DateUtil.getLastDateStr1(); //获取前一天年月日

        //根据 前一天年月 查询当前财务月开始可结束时间
        Map dqd = new HashMap();
        dqd.put("dqDay", lastDate.subSequence(0, 7));
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dqd);//当前月
        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);

        //前一天是否财务月最后一天
        String EenDate = dateMap.get("EndDate").toString().substring(0, 10);
        if (lastDate.equals(EenDate)) {
            Gson gson = new Gson();
            String at = WxUtils.getAccessToken(Constants.GET_TOKEN_URL);

            Map map = new HashMap();
            map.put("sysReportType", "");//模板类型
            map.put("pushType", "3");//推送类型(1:按天, 2:周五推送, 3:财务月,4:自然月,5:周一推送)

            List<Map> openidAndReports = sysReportService.queryReportAndOpenId(map);
            if (openidAndReports.size() > 0) {
                for (Map rMap : openidAndReports) {

                    Map mapSend = new HashMap();
                    mapSend.put("touser", rMap.get("openid"));
                    mapSend.put("template_id", Constants.template_id);
                    mapSend.put("url", rMap.get("url") + "?type=3&CampusID=&dateTime=" + lastDate.subSequence(0, 10));

                    Map param = new HashMap();
                    param.put("first", new TemplateData(rMap.get("userName") + "您好, 已为您生成以下报表", "#000000"));
                    param.put("keyword1", new TemplateData("月报表", "#000000"));

                    param.put("keyword2", new TemplateData(DateUtil.dateToString(new Date(), Constants.DATE_FORMART) + " " + DateUtil.getWeek(), "#000000"));

                    param.put("keyword3", new TemplateData(rMap.get("name").toString(), "#000000"));
                    param.put("remark", new TemplateData("", "#000000"));
                    mapSend.put("data", param);

                    JSON jbB = JSON.parseObject(HttpClientWaChatSend.sendPostRequest(Constants.SEND_URL + at, gson.toJson(mapSend)));
                    //添加推送记录
                    String campusID = "";
                    addPushRecord(rMap, lastDate.subSequence(0, 10).toString(), DateUtil.getWeek(), campusID);

                    log.info(jbB.toString(), rMap.get("userName") + "的" + rMap.get("name") + "报表" + "发送成功");
                }
            }
        }
    }

    /***
     * 每月八号早上10点执行(自然月月报表)
     */
    @Scheduled(cron = "0 04 10 8 * ?")
    public void sendMonthReport1() {
        String lastDate = DateUtil.getLastMonthDateStr2(); //获取上月当天的日期

        //根据 前一天年月 查询当前财务月开始可结束时间
        Map dqd = new HashMap();
        dqd.put("dqDay", lastDate.subSequence(0, 7));

        Gson gson = new Gson();
        String at = WxUtils.getAccessToken(Constants.GET_TOKEN_URL);

        Map map = new HashMap();
        map.put("sysReportType", "");//模板类型
        map.put("pushType", "4");//推送类型//推送类型(1:按天, 2:周五推送, 3:财务月,4:自然月,5:周一推送)

        List<Map> openidAndReports = sysReportService.queryReportAndOpenId(map);
        if (openidAndReports.size() > 0) {
            for (Map rMap : openidAndReports) {

                Map mapSend = new HashMap();
                mapSend.put("touser", rMap.get("openid"));
                mapSend.put("template_id", Constants.template_id);
                mapSend.put("url", rMap.get("url") + "?type=3&CampusID=&dateTime=" + lastDate.subSequence(0, 10));

                Map param = new HashMap();
                param.put("first", new TemplateData(rMap.get("userName") + "您好, 已为您生成以下报表", "#000000"));
                param.put("keyword1", new TemplateData("月报表", "#000000"));

                param.put("keyword2", new TemplateData(DateUtil.dateToString(new Date(), Constants.DATE_FORMART) + " " + DateUtil.getWeek(), "#000000"));

                param.put("keyword3", new TemplateData(rMap.get("name").toString(), "#000000"));
                param.put("remark", new TemplateData("", "#000000"));
                mapSend.put("data", param);

                JSON jbB = JSON.parseObject(HttpClientWaChatSend.sendPostRequest(Constants.SEND_URL + at, gson.toJson(mapSend)));

                //添加推送记录
                String campusID = "";
                addPushRecord(rMap, lastDate.subSequence(0, 10).toString(), DateUtil.getWeek(), campusID);

                log.info(jbB.toString(), rMap.get("userName") + "的" + rMap.get("name") + "报表" + "发送成功");
            }
        }
    }

    /***
     * 每周一上午9点6分执行(推送到校区)
     */
    @Scheduled(cron = "0 6 9 * * MON")
    public void sendMonthByCCSA() {
        //获取前一天年月日
        String lastDate = DateUtil.getLastDateStr1() + " 23:59:59";

        //获取钱三天的时间
        String lastTheDate = DateUtil.getLastDateStrN(3) + " 23:59:59";

        lastDate = dateLastDate(lastTheDate, lastDate);

        Gson gson = new Gson();
        String at = WxUtils.getAccessToken(Constants.GET_TOKEN_URL);
        Map map = new HashMap();
        map.put("sysReportType", "");//模板类型
        map.put("pushType", "6");//推送类型:6:校区周一报表

        List<Map> openidAndReports = sysReportService.queryReportAndOpenId(map);
        if (openidAndReports.size() > 0) {
            for (Map rMap : openidAndReports) {

                Map mapSend = new HashMap();
                mapSend.put("touser", rMap.get("openid"));
                mapSend.put("template_id", Constants.template_id);
                mapSend.put("url", rMap.get("url") + "?type=3&account=" +rMap.get("account")+ "&dateTime=" + lastDate.subSequence(0, 10));

                Map param = new HashMap();
                param.put("first", new TemplateData(rMap.get("userName") + "您好, 已为您生成以下报表", "#000000"));
                param.put("keyword1", new TemplateData("月报表", "#000000"));

                param.put("keyword2", new TemplateData(DateUtil.dateToString(new Date(), Constants.DATE_FORMART) + " " + DateUtil.getWeek(), "#000000"));

                param.put("keyword3", new TemplateData(rMap.get("name").toString(), "#000000"));
                param.put("remark", new TemplateData("", "#000000"));
                mapSend.put("data", param);

                JSON jbB = JSON.parseObject(HttpClientWaChatSend.sendPostRequest(Constants.SEND_URL + at, gson.toJson(mapSend)));

                //添加推送记录
                String campusID = "";
                addPushRecord(rMap, lastDate.subSequence(0, 10).toString(), DateUtil.getWeek(), campusID);

                log.info(jbB.toString(), rMap.get("userName") + "的" + rMap.get("name") + "报表" + "发送成功");
            }
        }
    }

    /***
     * 添加推送记录
     *
     * @param map
     * @param pushTime
     * @param week
     * @param campusID
     */
    public void addPushRecord(Map map, String pushTime, String week, String campusID) {

        map.put("pushTime", pushTime);
        map.put("week", week);
        map.put("campusID", campusID);

        sysReportService.addPushRecord(map);
    }

    /**
     * 跨财务时算出数据锁定时间和报表推送时间
     * @param lastTheDate
     * @param lastDate
     * @return
     */
    public String dateLastDate(String lastTheDate, String lastDate){
        //根据 前一天年月 查询当前财务月开始可结束时间
        Map dqd = new HashMap();
        dqd.put("dqDay", lastTheDate.subSequence(0, 7));
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dqd);//当前月
        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);

        Map dqd1 = new HashMap();
        dqd1.put("dqDay", lastDate.subSequence(0, 7));
        List FinanceMonthOfDq1 = campusDSRService.queryNatureMonthOfDq(dqd1);//当前月
        Map dateMap1 = (HashMap) FinanceMonthOfDq1.get(0);

        //财务月的后一个周五跟财务月最后一天在同一个月,则锁定下个财务月的数据
        if (DateUtil.compare_date(dateMap1.get("EndDate").toString(), lastTheDate)) {
            dateMap.put("dqDay", DateUtil.addMonth(lastTheDate).substring(0,7));
            FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dateMap);//当前月
            dateMap = (HashMap) FinanceMonthOfDq.get(0);
        }

        if (!DateUtil.compare_date(dateMap.get("EndDate").toString(), lastDate)) {
            lastDate = dateMap.get("EndDate").toString();
        }
        return lastDate;
    }

//    @Scheduled(cron = "0 0/5 * * * ?")
//    public void sendText() {
//        String lastDate = DateUtil.getLastDateStr1(); //获取前一天年月日
//
//        Gson gson = new Gson();
//        String at = WxUtils.getAccessToken(Constants.GET_TOKEN_URL);
//
//        Map map = new HashMap();
//        map.put("sysReportType", "");//模板类型
//        map.put("pushType", "99");//推送类型//推送类型(1:按天, 2:周五推送, 3:财务月,4:自然月,5:周一推送)
//
//        List<Map> openidAndReports = sysReportService.queryReportAndOpenId(map);
//        if (openidAndReports.size() > 0) {
//            for (Map rMap : openidAndReports) {
//
//                Map mapSend = new HashMap();
//                mapSend.put("touser", rMap.get("openid"));
//                mapSend.put("template_id", Constants.template_id);
//                mapSend.put("url", rMap.get("url") + "?type=3&CampusID=&dateTime=" + lastDate.subSequence(0, 10));
//
//                Map param = new HashMap();
//                param.put("first", new TemplateData(rMap.get("userName") + "您好, 已为您生成以下报表", "#000000"));
//                param.put("keyword1", new TemplateData("月报表", "#000000"));
//
//                param.put("keyword2", new TemplateData(DateUtil.dateToString(new Date(), Constants.DATE_FORMART) + " " + DateUtil.getWeek(), "#000000"));
//
//                param.put("keyword3", new TemplateData(rMap.get("name").toString(), "#000000"));
//                param.put("remark", new TemplateData("", "#000000"));
//                mapSend.put("data", param);
//
//                JSON jbB = JSON.parseObject(HttpClientWaChatSend.sendPostRequest(Constants.SEND_URL + at, gson.toJson(mapSend)));
//
//                log.info(jbB.toString(), rMap.get("userName") + "的" + rMap.get("name") + "报表" + "发送成功");
//            }
//        }
//    }
}
