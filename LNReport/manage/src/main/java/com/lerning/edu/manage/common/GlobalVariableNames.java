package com.lerning.edu.manage.common;

/**
 * GlobalVariableNames
 * 全局属性名称
 */
public final class GlobalVariableNames {

    private GlobalVariableNames(){}

    /**
     * 系统环境变量
     */
    public static String GLOBAL = "GLOBAL";

    /**
     * session user
     */
    public static String LOGINUSER = "LOGINUSER";

    /**
     *  basepath
     */
    public static String BASEPATH = "basePath";

    /**
     * ingoreAuth
     */
    public static String INGOREAUTH = "ingoreAuth";

    /**
     * 机器码
     */
    public static String MACHINE_ID = "MACHINE_ID";
}
