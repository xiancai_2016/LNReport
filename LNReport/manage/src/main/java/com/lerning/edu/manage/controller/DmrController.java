package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.DMRService;
import com.lerning.edu.services.RefSituationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/17
 */
@Controller
@RequestMapping("DMR")
public class DmrController {

    @Autowired
    private DMRService dmrService;
    @Autowired
    private RefSituationService refSituationService;
    @Autowired
    private CampusDSRService campusDSRService;

    @RequestMapping("/list")
    public String ref(HttpServletRequest request,
                      @RequestParam(value = "AutoID", required = false) String AutoID,
                      @RequestParam(value = "channelType", required = false) String channelType) {

        String pApp;
        String pShow;
        String pEn;
        String SaleModeIds;
        String appOrLeads;
        String ShowToAppOrLeads;

        String TIN = "27F05A82-5601-45B2-83A5-8360A641F41F,65C4B6C0-465E-427B-96BB-85225704BA9D";
        String TOUT = "FBE4B6F5-E5C9-4D36-BBB2-F31EE975F076";
        String WalkIN = "EADC6B6D-4E9F-4DB2-B8DC-0739F72E2507";
        String REF = "A0D6D64E-8018-4766-82A3-1A41578A7F38";
        String MC = "559FF86A-AC29-46FE-84AD-CE774B8EFB79";

        if ("2".equals(channelType)) {
            SaleModeIds = TOUT;//TOUT
            pApp = "TOUT_APP";
            pShow = "TOUT_Show";
            pEn = "TOUT_KPI";
            appOrLeads = "app";
            ShowToAppOrLeads = "ShowToApp";
        } else if ("3".equals(channelType)) {
            SaleModeIds = TIN + "," + TOUT;//TIN+TOUT
            pApp = "TIN_AND_TOUT_APP";
            pShow = "TIN_AND_TOUT_Show";
            pEn = "TIN_AND_TOUT_KPI";
            appOrLeads = "app";
            ShowToAppOrLeads = "ShowToApp";
        } else if ("4".equals(channelType)) {
            SaleModeIds = WalkIN;//Walk-IN
            pApp = "";
            pShow = "WIN_Show";
            pEn = "WIN_KPI";
            appOrLeads = "Leads";
            ShowToAppOrLeads = "ShowToLeads";
        } else if ("5".equals(channelType)) {
            SaleModeIds = TIN + "," + TOUT + "," + WalkIN + "," +MC;//MKT
            pApp = "TIN_AND_TOUT_APP";
            pShow = "MKT_Show";
            pEn = "MKT_KPI";
            appOrLeads = "app";
            ShowToAppOrLeads = "ShowToApp";
        } else if ("6".equals(channelType)) {
            SaleModeIds = REF;////REF
            pApp = "";
            pShow = "REF_Show";
            pEn = "REF_KPI";
            appOrLeads = "Leads";
            ShowToAppOrLeads = "ShowToLeads";
        } else if ("7".equals(channelType)) {
            SaleModeIds = TIN + "," + TOUT + "," + WalkIN + "," + REF + "," +MC;//MKT+REF
            pApp = "TIN_AND_TOUT_APP";
            pShow = "MKT_AND_REF_Show";
            pEn = "MKT_AND_REF_KPI";
            appOrLeads = "app";
            ShowToAppOrLeads = "ShowToApp";
        }else if("8".equals(channelType)){
            SaleModeIds = WalkIN + "," + REF;//WalkIn+REF
            pApp = "";
            pShow = "WALKIN_AND_REF_Show";
            pEn = "WALKIN_AND_REF_KPI";
            appOrLeads = "Leads";
            ShowToAppOrLeads = "ShowToLeads";
        } else {
            SaleModeIds = TIN;//TIN
            pApp = "TIN_APP";
            pShow = "TIN_Show";
            pEn = "TIN_KPI";
            appOrLeads = "app";
            ShowToAppOrLeads = "ShowToApp";
        }

        DecimalFormat df = new DecimalFormat("######0");
        DecimalFormat df1 = new DecimalFormat("######0.00");

        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(DateUtil.dayMap());//当前月

        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", dateMap.get("AutoID"));

        String beninDate;
        String endDate;

        BigDecimal app = new BigDecimal(0);
        BigDecimal Show = new BigDecimal(0);
        BigDecimal En = new BigDecimal(0);

        BigDecimal tApp = new BigDecimal(0);
        BigDecimal tShow = new BigDecimal(0);
        BigDecimal tEn = new BigDecimal(0);

        List refs = new ArrayList();
        List allRefS = new ArrayList();

        List SHList = new ArrayList();

        if (!StringUtil.isEmpty(AutoID)) {
            Map beginDateAndEndDate = campusDSRService.queryNaturalMonth(Integer.parseInt(AutoID));//财务月开始和结束时间

            beninDate = beginDateAndEndDate.get("BeginDate").toString();
            endDate = beginDateAndEndDate.get("EndDate").toString();

            Map map = new HashMap();
            map.put("startTime", beninDate.substring(0,10));
            map.put("endTime", endDate.substring(0,10));
            map.put("text", endDate.substring(0, 7));
            map.put("SaleModeIds", SaleModeIds);
            map.put("startDate", DateUtil.getNowBefoerDate(beninDate, -1).substring(0, 10)+" 23:59:59");
            map.put("endDate", endDate);

            //查leads,show,指标...等
            refs = dmrService.queryDmr(map);

            request.setAttribute("AutoID", AutoID);

            if (refs.size() > 0) {
                for (Object object : refs) {
                    Map objMap = (HashMap) object;

                    int appOfCount = (int)objMap.get("appCount");
                    int leadsWalkInCount = (int)objMap.get("leadsWalkInCount");
                    int leadsRefCount = (int)objMap.get("leadsRefCount");
                    int wLeadsCount = (int)objMap.get("walkInAppCount");
                    int rLeadsCount = (int)objMap.get("refAppCount");

                    if("4".equals(channelType)){
//                        objMap.put("appCount",leadsWalkInCount);//walk-in
                    }else if("5".equals(channelType)){
                        objMap.put("appCount",appOfCount+leadsWalkInCount-wLeadsCount);//mkt
                    }else if("6".equals(channelType)){
//                        objMap.put("appCount",leadsRefCount);//erf
                    }else if("7".equals(channelType)){
                        objMap.put("appCount",appOfCount+leadsWalkInCount+leadsRefCount-wLeadsCount-rLeadsCount);//mkt+erf
                    }else if("8".equals(channelType)){
                        objMap.put("appCount",leadsWalkInCount+leadsRefCount);//walk+ref
                    }

                    app = app.add(new BigDecimal(objMap.get("appCount").toString()));
                    Show = Show.add(new BigDecimal(objMap.get("scount").toString()));

//                    map.put("CampusID", objMap.get("cID"));
//                    map.put("SaleModeIds", SaleModeIds);
//                    Map refByCampus = refSituationService.queryEnByCampus(map);//查业绩(ref)
//
//                    if (refByCampus != null) {
//                        objMap.put("ecount", refByCampus.get("Contracts"));
                        En = En.add(new BigDecimal(objMap.get("ecount").toString()));
//                    } else {
//                        objMap.put("ecount", 0.00);
//                    }

                    if(!StringUtil.isEmpty(pApp)){
                        tApp = tApp.add(new BigDecimal(objMap.get(pApp).toString()));
                    }
                    tShow = tShow.add(new BigDecimal(objMap.get(pShow).toString()));
                    tEn = tEn.add(new BigDecimal(objMap.get(pEn).toString()));

                    BigDecimal appCount = new BigDecimal(objMap.get("appCount").toString());
                    BigDecimal scount = new BigDecimal(objMap.get("scount").toString());
                    BigDecimal ecount = new BigDecimal(objMap.get("ecount").toString());
                    //ShowToApp
                    if (StringUtil.divideValue(appCount)) {
                        objMap.put("ShowToApp", df.format(scount.divide(appCount, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
                    } else {
                        objMap.put("ShowToApp", 0);
                    }
                    //ShowToEn
//                    if (StringUtil.divideValue(ecount) && refByCampus != null) {
                    if (StringUtil.divideValue(ecount)) {
                        objMap.put("ShowToEn",df1.format(scount.divide(ecount, 4, BigDecimal.ROUND_HALF_UP)));
                    } else {
                        objMap.put("ShowToEn", 0.00);
                    }

                    BigDecimal regionShow = new BigDecimal(objMap.get(pShow).toString());
                    BigDecimal regionAEn = new BigDecimal(objMap.get(pEn).toString());

                    objMap.put("regionShow",regionShow);
                    objMap.put("regionAEn",df.format(regionAEn));

                    //app/Tapp
                    if (StringUtil.isEmpty(pApp)) {
                        objMap.put("regionApp",0);
                        objMap.put("aApp", 0);
                    } else {
                        BigDecimal regionApp = new BigDecimal(objMap.get(pApp).toString());
                        objMap.put("regionApp",regionApp);
                        if (!StringUtil.divideValue(regionApp)) {
                            objMap.put("aApp", 0);
                        }else {
                            objMap.put("aApp", df.format(appCount.divide(regionApp, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
                        }
                    }

                    //show/Tshow
                    if (!StringUtil.divideValue(regionShow)) {
                        objMap.put("aShow", 0);
                    } else {
                        objMap.put("aShow", df.format(scount.divide(regionShow, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
                    }

                    //en/Ten
                    if (!StringUtil.divideValue(regionAEn)) {
                        objMap.put("aEn", 0);
                    } else {
                        objMap.put("aEn", df.format(ecount.divide(regionAEn, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
                    }

                    allRefS.add(objMap);
                }

                SHList.add(regionList("SH1", allRefS, pApp, pShow, pEn));
                SHList.add(regionList("SH2", allRefS, pApp, pShow, pEn));
                SHList.add(regionList("SH3", allRefS, pApp, pShow, pEn));
                SHList.add(regionList("SH4", allRefS, pApp, pShow, pEn));
                SHList.add(regionList("SH5", allRefS, pApp, pShow, pEn));
                //SHList.add(rc.regionList("SH6", allRefS, pApp, pShow, pEn));
            }
        }

        Map map = new HashMap();
        map.put("app", app);
        map.put("Show", Show);
        map.put("En", En);

        map.put("tApp", tApp);
        map.put("tShow", tShow);
        map.put("tEn", df.format(tEn));

        if (!StringUtil.divideValue(app)) {
            map.put("showToApp", 0);
        } else {
            map.put("showToApp", df.format(Show.divide(app, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        }

        if (!StringUtil.divideValue(En)) {
            map.put("showToEn", 0.00);
        } else {
            map.put("showToEn", Show.divide(En, 2, BigDecimal.ROUND_HALF_UP));
        }

        if (!StringUtil.divideValue(tApp)) {
            map.put("aApp", 0);
        } else {
            map.put("aApp", df.format(app.divide(tApp, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        }

        if (!StringUtil.divideValue(tShow)) {
            map.put("aShow", 0);
        } else {
            map.put("aShow", df.format(Show.divide(tShow, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        }

        if (!StringUtil.divideValue(tEn)) {
            map.put("aEn", 0);
        } else {
            map.put("aEn", df.format(En.divide(tEn, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        }

        request.setAttribute("map", map);
        request.setAttribute("SHList", SHList);

        request.setAttribute("FinanceMonths", FinanceMonths);
        request.setAttribute("channelType", channelType);

        request.setAttribute("appOrLeads", appOrLeads);
        request.setAttribute("ShowToAppOrLeads", ShowToAppOrLeads);

        return "dmr/list";
    }

    public Map regionList(String region, List refs, String pApp, String pShow, String pEn) {
        DecimalFormat df = new DecimalFormat("######0");

        Map regionMap = new HashMap();
        List regions = new ArrayList();

        BigDecimal sumApp = new BigDecimal(0);
        BigDecimal sumShow = new BigDecimal(0);
        BigDecimal sumEn = new BigDecimal(0);

        BigDecimal sumTApp = new BigDecimal(0);
        BigDecimal sumTShow = new BigDecimal(0);
        BigDecimal sumTEn = new BigDecimal(0);

        for (Object obj : refs) {
            Map refMap = (HashMap) obj;

            if (refMap.get("cTel").equals(region)) {
                regions.add(refMap);
                sumApp = sumApp.add(new BigDecimal(refMap.get("appCount").toString()));
                sumShow = sumShow.add(new BigDecimal(refMap.get("scount").toString()));
                sumEn = sumEn.add(new BigDecimal(refMap.get("ecount").toString()));

                if(!StringUtil.isEmpty(pApp)){
                    sumTApp = sumTApp.add(new BigDecimal(refMap.get(pApp).toString()));
                }
                sumTShow = sumTShow.add(new BigDecimal(refMap.get(pShow).toString()));
                sumTEn = sumTEn.add(new BigDecimal(refMap.get(pEn).toString()));
            }
        }

        regionMap.put("app", sumApp);
        regionMap.put("Show", sumShow);
        regionMap.put("En", sumEn);
        regionMap.put("tApp", sumTApp);
        regionMap.put("tShow", sumTShow);
        regionMap.put("tEn", df.format(sumTEn));

        if (!StringUtil.divideValue(sumApp)) {
            regionMap.put("showToApp", 0);
        } else {
            regionMap.put("showToApp", df.format(sumShow.divide(sumApp, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        }

        if (!StringUtil.divideValue(sumEn)) {
            regionMap.put("showToEn", 0.00);
        } else {
            regionMap.put("showToEn", sumShow.divide(sumEn, 2, BigDecimal.ROUND_HALF_UP));
        }

        if (StringUtil.divideValue(sumTApp)) {
            regionMap.put("RAApp", df.format(sumApp.divide(sumTApp, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        } else {
            regionMap.put("RAApp", 0);
        }

        if (StringUtil.divideValue(sumTShow)) {
            regionMap.put("RAShow", df.format(sumShow.divide(sumTShow, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        } else {
            regionMap.put("RAShow", 0);
        }

        if (StringUtil.divideValue(sumTEn)) {
            regionMap.put("RAEn", df.format(sumEn.divide(sumTEn, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        } else {
            regionMap.put("RAEn", 0);
        }
        regionMap.put("regionName", region);
        regionMap.put("regions", regions);
        return regionMap;
    }
}
