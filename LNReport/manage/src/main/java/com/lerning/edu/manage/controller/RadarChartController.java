package com.lerning.edu.manage.controller;

import com.alibaba.fastjson.JSONObject;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.services.CampusDSRService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/17
 */
@Controller
@RequestMapping("radar")
public class RadarChartController {

    @Autowired
    private CampusDSRService campusDSRService;

    @RequestMapping("/chart")
    @RequiresPermissions("radarChart:view")
    public String chart(HttpServletRequest request){

        List departs = campusDSRService.queryDepartList();//校区

        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(DateUtil.dayMap());//当前月

        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", dateMap.get("AutoID"));

        request.setAttribute("FinanceMonths", FinanceMonths);
        request.setAttribute("departs", departs);

        return "radar/radarChart";
    }

    @RequestMapping("/queryDepart")
    @ResponseBody
    public JSONObject queryDepart(String CampusID){
        Map map = new HashMap();
        map.put("CampusID", "|"+CampusID+"|");
        List departs = campusDSRService.queryDepart(map);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", departs);
        return jsonObject;
    }

    @RequestMapping("/queryEmployee")
    @ResponseBody
    public JSONObject queryEmployee(String departID){
        Map map = new HashMap();
        map.put("departID", departID);
        List departs = campusDSRService.queryEmployee(map);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", departs);
        return jsonObject;
    }
}
