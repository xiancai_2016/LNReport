package com.lerning.edu.manage.controller;

import com.lerning.edu.beans.SysOpenid;
import com.lerning.edu.commons.getopenid.WxUtils;
import com.lerning.edu.services.SysOpenidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * @author jiangwei
 * @since 18/6/4
 */
@Controller
@RequestMapping("/")
public class LoginController {

    @Autowired
    private SysOpenidService sysOpenidService;

    @RequestMapping(value = "/wechatLogin", method = RequestMethod.GET)
    public String login(HttpServletRequest request) {
        String code = request.getParameter("code");
        if (code == null) {
            return "weCharErroe";
        }

        String openid = WxUtils.getopendid(code);

        SysOpenid sysOpenid = sysOpenidService.queryByOpenid(openid);
        if (sysOpenid == null) {
            request.setAttribute("openid", openid);
            return "auth/login2";
        } else {
            return "weCharIndex";
        }
    }

    @RequestMapping(value = "/wechatLogin/test", method = RequestMethod.GET)
    public String loginText(HttpServletRequest request) {
        return "auth/login2";

    }
}
