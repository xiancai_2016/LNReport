package com.lerning.edu.manage.controller;

import com.lerning.edu.services.FullAttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/6
 */
@Controller
@RequestMapping("/fullAttendance")
public class FullAttendanceController extends BaseController{

    @Autowired
    public FullAttendanceService fullAttendanceService;

    @RequestMapping("/attendanceTime")
    public String pageData(HttpServletRequest request){
        List attendanceTime = fullAttendanceService.fullAttendance();

        Map totalTime = fullAttendanceService.fullAttendanceTotal();

        request.setAttribute("attendanceTime", attendanceTime);
        request.setAttribute("totalTime", totalTime);

        return "Attendance/attendanceTime";
    }
}
