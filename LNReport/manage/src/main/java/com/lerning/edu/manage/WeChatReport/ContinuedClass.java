package com.lerning.edu.manage.WeChatReport;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.manage.controller.CampusDSRController;
import com.lerning.edu.services.CampusDSRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/13
 */
@Controller
@RequestMapping("wechat")
public class ContinuedClass {

    @Autowired
    private CampusDSRService campusDSRService;

    CampusDSRController ccr = new CampusDSRController();

    @RequestMapping("/continuedClass")
    public String queryClassCount(HttpServletRequest request,
                                  @RequestParam(value = "type", required = false) String type,
                                  @RequestParam(value = "CampusID", required = false) String CampusID,
                                  @RequestParam(value = "dateTime", required = false) String dateTime) {
        List departs = campusDSRService.queryDepartList();//校区

        String beginDate = null;
        String endDate = null;

        if (type.equals("1")) {
            beginDate = dateTime + " 00:00:00.0";
            endDate = dateTime + " 23:59:59.0";
        } else if (type.equals("2")) {
            beginDate = dateTime + " 00:00:00.0";
            endDate = DateUtil.getBeforeDateToString(Constants.DATE_FORMART, 6, DateUtil.formateStringToDate(dateTime)) + " 23:59:59.0";
        } else {
            Map dateMap = new HashMap();
            dateMap.put("dqDay", dateTime.subSequence(0, 7));
            List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dateMap);//财务月列表
            Map map = (HashMap) FinanceMonthOfDq.get(0);
            beginDate = map.get("NBeginDate").toString();
            endDate = map.get("NEndDate").toString();
        }

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("Grade", "");
        map.put("BeginDate", beginDate);
        map.put("EndDate", endDate);

        List queryStudentCountByK = new ArrayList();
        List queryStudentCountByC = new ArrayList();
        List queryStudentCountByY = new ArrayList();

        //List queryStudentCount = campusDSRService.queryStudentCount(map);

        Map lockMap = new HashMap();
        lockMap.put("CampusID",CampusID);
        lockMap.put("text", endDate.subSequence(0, 7));

        List queryStudentCount = new ArrayList();
        if(StringUtil.isEmpty(CampusID)){
            queryStudentCount = campusDSRService.queryStudentCountOfLockAll(lockMap);
        }else{
            queryStudentCount = campusDSRService.queryStudentCountOfLock(lockMap);
        }

        if (queryStudentCount.size() > 0) {
            for (int i = 0; i < queryStudentCount.size(); i++) {
                Map mapKItem = (HashMap) queryStudentCount.get(i);
                String grade = mapKItem.get("grade").toString();
                if (grade.equals("K1") || grade.equals("K2") || grade.equals("K3") || grade.equals("K4")) {
                    queryStudentCountByK.add(mapKItem);
                } else if (grade.equals("Y1") || grade.equals("Y2") || grade.equals("Y3") || grade.equals("Y4") || grade.equals("Y5") || grade.equals("Y6")) {
                    queryStudentCountByY.add(mapKItem);
                } else {
                    queryStudentCountByC.add(mapKItem);
                }
            }
        }

        Map mapK = ccr.stu(queryStudentCountByK);
        Map mapC = ccr.stu(queryStudentCountByC);
        Map mapY = ccr.stu(queryStudentCountByY);

        Map mapT = new HashMap();

        int tkdCount = Integer.parseInt(mapK.get("kdCount").toString()) + Integer.parseInt(mapC.get("kdCount").toString()) + Integer.parseInt(mapY.get("kdCount").toString());
        mapT.put("tkdCount", tkdCount);
        BigDecimal ssCountK = new BigDecimal(mapK.get("ssCount").toString());
        BigDecimal ssCountC = new BigDecimal(mapC.get("ssCount").toString());
        BigDecimal ssCountY = new BigDecimal(mapY.get("ssCount").toString());
        mapT.put("tssCount", ssCountK.add(ssCountC).add(ssCountY).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

        BigDecimal mbCountK = new BigDecimal(mapK.get("mbCount").toString());
        BigDecimal mbCountC = new BigDecimal(mapC.get("mbCount").toString());
        BigDecimal mbCountY = new BigDecimal(mapY.get("mbCount").toString());

        int totalCount = Integer.parseInt(mapK.get("totalCount").toString())+Integer.parseInt(mapC.get("totalCount").toString())+Integer.parseInt(mapY.get("totalCount").toString());

        if (tkdCount == 0) {
            mapT.put("tagvXb", 0);
        } else {
            mapT.put("tagvXb", (ssCountK.add(ssCountC).add(ssCountY)).divide(new BigDecimal(tkdCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
        }

        if(totalCount == 0){
            mapT.put("ttmbCount", 0);
        }else {
            mapT.put("ttmbCount", (mbCountK.add(mbCountC).add(mbCountY)).divide(new BigDecimal(totalCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
        }
        request.setAttribute("departs", departs);

        request.setAttribute("CampusID", CampusID);
        request.setAttribute("type", type);
        request.setAttribute("dateTime", dateTime);

        request.setAttribute("queryStudentCountByK", queryStudentCountByK);
        request.setAttribute("queryStudentCountByC", queryStudentCountByC);
        request.setAttribute("queryStudentCountByY", queryStudentCountByY);

        request.setAttribute("mapK", mapK);
        request.setAttribute("mapC", mapC);
        request.setAttribute("mapY", mapY);
        request.setAttribute("mapT", mapT);

        request.setAttribute("reportInfo", "KPI Summary-续班统计");
        request.setAttribute("date", beginDate.substring(0,10)+"到"+endDate.substring(0,10));

        return "wechar/queryStudentCount";
    }
}
