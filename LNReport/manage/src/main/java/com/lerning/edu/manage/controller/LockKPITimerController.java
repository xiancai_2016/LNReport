package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.services.CampusDSRService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/26
 */
@Component
@EnableScheduling
public class LockKPITimerController {
    private static Logger log = LoggerFactory.getLogger(LockKPITimerController.class);

    @Autowired
    private CampusDSRService campusDSRService;

    /***
     * KPI Summary-报名退费
     * KPI Summary-开班概况
     * 每天凌晨0点10分执行(财务月月报表)
     */
    @Scheduled(cron = "0 10 0 * * ?")
    public void KPITotalCAR() {
        //获取前一天年月日
        String lastDate = DateUtil.getLastDateStr1();

        //根据 前一天年月 查询当前财务月开始可结束时间
        Map dqd = new HashMap();
        dqd.put("dqDay", lastDate.subSequence(0, 7));
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dqd);//当前月
        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);

        //前一天是否财务月最后一天
        String EenDate = dateMap.get("EndDate").toString().substring(0, 10);
        if (lastDate.equals(EenDate)) {
            //查询校区
            List departs = campusDSRService.queryDepartList();//校区

            //循环查询个校区数据,并存入数据库
            if (departs.size() > 0) {
                for (Object obj : departs) {
                    Map depart = (HashMap) obj;
                    dateMap.put("CampusID", depart.get("cID"));
                    dateMap.put("Grade", "");

                    //报名退费
                    List chargeAndRefund = new ArrayList();
                    List list = campusDSRService.chargeAndRefund(dateMap);

                    for (Object o : list) {
                        Map omap = (HashMap) o;
                        omap.put("text", lastDate.subSequence(0, 7));
                        chargeAndRefund.add(omap);
                    }

                    campusDSRService.insertChargeAndRefund(chargeAndRefund);

//                    dateMap.put("cFlag", 1);
//                    Map mapSKC = campusDSRService.querySCKCount(dateMap);
//                    dateMap.put("cFlag", -1);
//                    Map maprSKC = campusDSRService.querySCKCount(dateMap);
//
//                    maprSKC.put("rtcost", maprSKC.get("skcrFact").toString().replace("-", ""));//退费金额
//                    maprSKC.put("tcost", mapSKC.get("skcrFact").toString());//报名金额
//                    maprSKC.put("grade", "暑假班");
//                    maprSKC.put("cCampusID", depart.get("cID"));
//                    maprSKC.put("text", lastDate.subSequence(0, 7));
//
//                    campusDSRService.insertSKC(maprSKC);

                    //开班概况
                    Map classDateMap = new HashMap();
                    String endDate = dateMap.get("EndDate").toString();

                    classDateMap.put("BeginDate", DateUtil.getBeforeDateToString(Constants.DATE_FORMART, 6, DateUtil.formateStringToDate(endDate)) + " 00:00:00.0");
                    classDateMap.put("EndDate", endDate);
                    classDateMap.put("BeginDateFoWKB", dateMap.get("BeginDate"));
                    classDateMap.put("CampusID", depart.get("cID"));
                    classDateMap.put("Grade", "");

                    List queryClassCount = campusDSRService.queryClassCountByK(classDateMap);
                    campusDSRService.insertClassCount(queryClassCount);
                }
            }
            log.info("锁定成功!!!");
        }
    }

    /***
     * KPI Summary-续班统计
     * 每个月八号凌晨0点15分执行(自然月月报表)
     */
    @Scheduled(cron = "0 15 0 8 * ?")
    public void KPIFoStudentCunt() {
        //获取上月当天的日期 格式:yyyy-MM-dd
        String lastDate = DateUtil.getLastMonthDateStr2();

        //根据 前一天年月 查询当前财务月开始可结束时间
        Map dqd = new HashMap();
        dqd.put("dqDay", lastDate.subSequence(0, 7));
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dqd);//当前月
        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);

        List departs = campusDSRService.queryDepartList();//校区

        //循环查询个校区数据,并存入数据库
        if (departs.size() > 0) {
            for (Object obj : departs) {
                Map depart = (HashMap) obj;
                dateMap.put("CampusID", depart.get("cID"));
                dateMap.put("Grade", "");
                dateMap.put("BeginDate", dateMap.get("NBeginDate").toString());
                dateMap.put("EndDate", dateMap.get("NEndDate").toString());

                //续班统计
                List queryStudentCount = campusDSRService.queryStudentCount(dateMap);
                campusDSRService.insertStudentCount(queryStudentCount);
            }
        }
        log.info("锁定成功!!!");
    }
}
