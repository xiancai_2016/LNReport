package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.RevenueService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/5
 */
@Controller
@RequestMapping("revenue")
public class RevenueController {
    @Autowired
    private RevenueService revenueService;
    @Autowired
    private CampusDSRService campusDSRService;

    CampusDSRController cdc = new CampusDSRController();

    @RequestMapping("/list")
    @RequiresPermissions("view:revenue")
    public String revenue(HttpServletRequest request,
                          @RequestParam(value = "AutoID", required = false) String AutoID) {
        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(cdc.dayMap());//当前月

        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", dateMap.get("AutoID"));

        BigDecimal wFst = new BigDecimal(0);
        BigDecimal totalKpi = new BigDecimal(0);

        List revenues = new ArrayList();

        if (!StringUtil.isEmpty(AutoID)) {
            Map beginDateAndEndDate = campusDSRService.queryNaturalMonth(Integer.parseInt(AutoID));//财务月开始和结束时间

            String beninDate = beginDateAndEndDate.get("BeginDate").toString();
            String endDate = beginDateAndEndDate.get("EndDate").toString();

            Map map = new HashMap();
            map.put("startTime", beninDate);
            map.put("endTime", endDate);
            map.put("nowYear", endDate.substring(0, 4));
            map.put("nowMonth", endDate.substring(5, 7));
            map.put("text", endDate.substring(0, 10));

            revenues = revenueService.queryRevenueByWeek(map);

            request.setAttribute("AutoID", AutoID);

            if (revenues.size() > 0) {
                for (Object object : revenues) {
                    Map objMap = (HashMap) object;
                    wFst = wFst.add(new BigDecimal(objMap.get("fstWeek").toString()));
                    totalKpi = totalKpi.add(new BigDecimal(objMap.get("kpi").toString()));
                }
            }
        }

        revenues = StringUtil.theFirstThree(revenues, "kpiRate");
        StringUtil.sortByWhatAsc(revenues,"cName");

        request.setAttribute("revenues", revenues);
        request.setAttribute("FinanceMonths", FinanceMonths);

        Map totalMap = new HashMap();
        totalMap.put("wFst", wFst);
        totalMap.put("totalKpi", totalKpi);

        if (!StringUtil.divideValue(totalKpi)) {
            totalMap.put("kpiRate", 0.00);
        } else {
            totalMap.put("kpiRate", wFst.multiply(new BigDecimal(100)).divide(totalKpi, 2, BigDecimal.ROUND_HALF_UP));
        }

        request.setAttribute("totalMap", totalMap);

        return "DRS/revenue";
    }
}
