package com.lerning.edu.manage.test;

import com.lerning.edu.commons.util.DateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author jiangwei
 * @since 18/9/27
 */
public class dateTest {
    public static void main(String[] args) {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date=null;
        try {
            date = sdf.parse("2012-07-25 21:23:12");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar ca=Calendar.getInstance();
        ca.setTime(date);
        ca.add(Calendar.HOUR_OF_DAY, -2);
        //System.out.println(sdf.format(ca.getTime()));


       // System.out.println(DateUtil.getLastDateStrN(0));

        //获取前一天年月日
        String lastDate = DateUtil.getLastDateStr1() + " 23:59:59";

        //获取钱三天的时间
        String lastTheDate = DateUtil.getLastDateStrN(3) + " 23:59:59";

        System.out.println(lastDate + lastTheDate);
    }
}
