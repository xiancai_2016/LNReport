package com.lerning.edu.manage.controller;

import com.lerning.edu.beans.SysReport;
import com.lerning.edu.beans.page.PageList;
import com.lerning.edu.manage.util.UserUtils;
import com.lerning.edu.services.SysReportService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author jiangwei
 * @since 18/6/5
 */
@Controller
@RequestMapping(value = "report")
public class ReportController extends BaseController{
    @Autowired
    private SysReportService sysReportService;

    /**
     * 查询列表  不做查询数据处
     * @param request
     * @return
     */
    @RequiresPermissions("sys:report:view")
    @RequestMapping(value = {"","/","list"})
    public String list(SysReport sysReport, HttpServletRequest request){
        request.setAttribute("sysReport",sysReport);

        return "sys/report/list";
    }

    /**
     * 异步获取list资源
     * @param pageList
     * @param sysReport
     * @return
     */
    @RequiresPermissions("sys:report:view")
    @RequestMapping("asynList")
    @ResponseBody
    public PageList asynList(PageList pageList, SysReport sysReport){
        return sysReportService.queryPage(pageList,sysReport);
    }

    /**
     * 新增修改
     * @param id
     * @return
     */
    @RequiresPermissions("sys:report:edit")
    @RequestMapping(value = {"add","edit"},method = RequestMethod.GET)
    public String edit(Integer id, HttpServletRequest request){
        SysReport sysReport;
        if(id == null|| id < 1l ){ //add
            sysReport = new SysReport();
        }else{//edit
            sysReport = sysReportService.queryReportById(id);
        }

        SysReport stype = new SysReport();
        stype.setReportType("1");
        List reportsByMenus = sysReportService.queryListByReport(stype);
        request.setAttribute("reportsByMenus", reportsByMenus);
        request.setAttribute("sysReport",sysReport);
        return "sys/report/edit";
    }

    @RequiresPermissions("sys:report:edit")
    @RequestMapping(value = "save",method = RequestMethod.POST )
    public String save(SysReport sysReport,HttpServletRequest request){
        int result = -1;
        if(StringUtils.isEmpty(sysReport.getPid())){
            sysReport.setPid("0");
        }
        if(StringUtils.isEmpty(sysReport.getUrl())){
            Date date = new Date();
            sysReport.setUrl(String.valueOf(date.getTime()));
        }
        if( sysReport.getId() == null ){ //add
            result = sysReportService.add(sysReport);
        }else{//edit
            result = sysReportService.update(sysReport);
        }

        // 清除用户菜单缓存
        UserUtils.removeCache(UserUtils.CACHE_REPORT_LIST);
        return "redirect:list";
    }

    @RequiresPermissions("sys:report:delete")
    @RequestMapping("delete")
    @ResponseBody
    public Object delete(Long id,HttpServletRequest request){
        SysReport sysReport = new SysReport();
        sysReport.setId(id);
        sysReport.setOperator(UserUtils.getUser().getId());
        sysReportService.delete(sysReport);

        // 清除用户菜单缓存
        UserUtils.removeCache(UserUtils.CACHE_REPORT_LIST);
        return  "报表删除成功";
    }

    /**
     * 不存在  返回true 存在返回false,条件为空返回false
     * @param oldUrl
     * @param url
     * @return
     */
    @RequestMapping("notExists")
    @ResponseBody
    public Boolean exists(String oldUrl, String url){
        SysReport sysReport = new SysReport();
        sysReport.setUrl(url);
            if (url !=null && url.equals(oldUrl)) {
                return Boolean.TRUE;
            } else if (url !=null && sysReportService.exists(sysReport) < 1) {
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        }
}
