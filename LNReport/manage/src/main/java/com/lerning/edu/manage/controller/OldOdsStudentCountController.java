package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/12
 */
@Controller
@RequestMapping("oldOds")
public class OldOdsStudentCountController {

    @Autowired
    private CampusDSRService campusDSRService;

    CampusDSRController crc = new CampusDSRController();

    @RequestMapping("/continued")
    public String Continued(HttpServletRequest request,
                            @RequestParam(value = "CampusID", required = false) String CampusID,
                            @RequestParam(value = "key", required = false) String key) {

        if (StringUtil.isEmpty(key)) {
            return "oldods";
        } else {
            key = URLDecoder.decode(key);
            double minute = DateUtil.getDistanceOfTwoMinute(DateUtil.formateStringToDate(key.replace("/", "-"), Constants.DATE_FORMART2), new Date());
            if (minute > 15 || minute < 0) {
                return "oldods";
            }
        }

        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(crc.dayMap());//当前月

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("Grade", "");

        Map dmap = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", dmap.get("AutoID"));
        request.setAttribute("departs", departs);

        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("queryStudentCountByK", "");
        request.setAttribute("queryStudentCountByC", "");
        request.setAttribute("queryStudentCountByY", "");

        request.setAttribute("mapK", "");
        request.setAttribute("mapC", "");
        request.setAttribute("mapY", "");
        request.setAttribute("mapT", "");
        request.setAttribute("key", key);

        return "oldOds/queryStudentCount";
    }

    @RequestMapping("/continuedList")
    public String continuedList(HttpServletRequest request,
                                @RequestParam(value = "CampusID", required = false) String CampusID,
                                @RequestParam(value = "AutoID", required = false) int AutoID,
                                @RequestParam(value = "key", required = false) String key) {

        if (StringUtil.isEmpty(key)) {
            return "oldods";
        } else {
            key = URLDecoder.decode(key);
            double minute = DateUtil.getDistanceOfTwoMinute(DateUtil.formateStringToDate(key.replace("/", "-"), Constants.DATE_FORMART2), new Date());
            if (minute > 15 || minute < 0) {
                return "oldods";
            }
        }

        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        Map beginDateAndEndDate = campusDSRService.queryNaturalMonth(AutoID);//财务月开始和结束时间

        List CampusIDS = new ArrayList();

        if (!StringUtil.isEmpty(CampusID)) {
            if (CampusID.substring(0, 1).equals(",")) {
                CampusID = CampusID.substring(1, CampusID.length());
            }
            String[] cIDs = CampusID.split(",");

            for (int i = 0; i < cIDs.length; i++) {
                CampusIDS.add(cIDs[i]);
            }
        } else {
            CampusID = "";
        }

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("Grade", "");
        map.put("BeginDate", beginDateAndEndDate.get("NBeginDate").toString());
        map.put("EndDate", beginDateAndEndDate.get("NEndDate").toString());

        List queryStudentCountByK = new ArrayList();
        List queryStudentCountByC = new ArrayList();
        List queryStudentCountByY = new ArrayList();

        List queryStudentCount = campusDSRService.queryStudentCount(map);

        if (queryStudentCount.size() > 0) {
            for (int i = 0; i < queryStudentCount.size(); i++) {
                Map mapKItem = (HashMap) queryStudentCount.get(i);
                String grade = mapKItem.get("grade").toString();
                if (grade.equals("K1") || grade.equals("K2") || grade.equals("K3") || grade.equals("K4")) {
                    queryStudentCountByK.add(mapKItem);
                } else if (grade.equals("Y1") || grade.equals("Y2") || grade.equals("Y3") || grade.equals("Y4") || grade.equals("Y5") || grade.equals("Y6")) {
                    queryStudentCountByY.add(mapKItem);
                } else {
                    queryStudentCountByC.add(mapKItem);
                }
            }
        }

        Map mapK = crc.stu(queryStudentCountByK);
        Map mapC = crc.stu(queryStudentCountByC);
        Map mapY = crc.stu(queryStudentCountByY);

        Map mapT = new HashMap();

        int tkdCount = Integer.parseInt(mapK.get("kdCount").toString()) + Integer.parseInt(mapC.get("kdCount").toString()) + Integer.parseInt(mapY.get("kdCount").toString());
        mapT.put("tkdCount", tkdCount);
        BigDecimal ssCountK = new BigDecimal(mapK.get("ssCount").toString());
        BigDecimal ssCountC = new BigDecimal(mapC.get("ssCount").toString());
        BigDecimal ssCountY = new BigDecimal(mapY.get("ssCount").toString());
        mapT.put("tssCount", ssCountK.add(ssCountC).add(ssCountY).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

        BigDecimal mbCountK = new BigDecimal(mapK.get("mbCount").toString());
        BigDecimal mbCountC = new BigDecimal(mapC.get("mbCount").toString());
        BigDecimal mbCountY = new BigDecimal(mapY.get("mbCount").toString());

        int totalCount = Integer.parseInt(mapK.get("totalCount").toString()) + Integer.parseInt(mapC.get("totalCount").toString()) + Integer.parseInt(mapY.get("totalCount").toString());

        if (tkdCount == 0) {
            mapT.put("tagvXb", 0);
        } else {
            mapT.put("tagvXb", (ssCountK.add(ssCountC).add(ssCountY)).divide(new BigDecimal(tkdCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
        }

        if (totalCount == 0) {
            mapT.put("ttmbCount", 0);
        } else {
            mapT.put("ttmbCount", (mbCountK.add(mbCountC).add(mbCountY)).divide(new BigDecimal(totalCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
        }

        request.setAttribute("AutoID", AutoID);
        request.setAttribute("departs", departs);

        request.setAttribute("FinanceMonths", FinanceMonths);
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("queryStudentCountByK", queryStudentCountByK);
        request.setAttribute("queryStudentCountByC", queryStudentCountByC);
        request.setAttribute("queryStudentCountByY", queryStudentCountByY);

        request.setAttribute("mapK", mapK);
        request.setAttribute("mapC", mapC);
        request.setAttribute("mapY", mapY);
        request.setAttribute("mapT", mapT);
        request.setAttribute("CampusIDS", CampusIDS);
        request.setAttribute("key", key);

        return "oldOds/queryStudentCount";
    }

}
