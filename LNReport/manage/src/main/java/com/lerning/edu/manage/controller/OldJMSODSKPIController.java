package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.JMSODSKPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/11/2
 */
@Controller
@RequestMapping("oldOds")
public class OldJMSODSKPIController {

    @Autowired
    private JMSODSKPIService jmsodskpiService;

    @Autowired
    private CampusDSRService campusDSRService;

    @RequestMapping("/kpiByCC")
    public String kpi(HttpServletRequest request,
                      @RequestParam(value = "AutoID", required = false) String AutoID,
                      @RequestParam(value = "campusID", required = false)String campusID,
                      @RequestParam(value = "key", required = false) String key) {

        if (StringUtil.isEmpty(key)) {
            return "oldods";
        } else {
            key = URLDecoder.decode(key);
            double minute = DateUtil.getDistanceOfTwoMinute(DateUtil.formateStringToDate(key.replace("/","-"), Constants.DATE_FORMART2), new Date());
            if (minute > 15 || minute < 0) {
                return "oldods";
            }
        }

        if(StringUtil.isEmpty(campusID)){
            campusID = "D4AA9269-0B2C-4992-8B1B-769B46080BFF";
        }

        Map dateMap = new HashMap();
        List months = campusDSRService.queryFinanceMonths();

        if (StringUtil.isEmpty(AutoID)) {
            List FinanceMonthOfDq = campusDSRService.queryFinanceMonthOfDq(DateUtil.dayMap());//当前月

            dateMap = (HashMap) FinanceMonthOfDq.get(0);
            AutoID = dateMap.get("AutoID").toString();
        } else {
            dateMap = campusDSRService.queryFinanceMonth(Integer.parseInt(AutoID));//财务月开始和结束时间
        }

        dateMap.put("Text", dateMap.get("EndDate").toString().substring(0,7));
        dateMap.put("campusID", '|'+campusID+'|');

        List KPIByCC = jmsodskpiService.queryKPIByCC(dateMap);

        request.setAttribute("FinanceMonths", months);
        request.setAttribute("AutoID", AutoID);
        request.setAttribute("KPIByCC", KPIByCC);
        request.setAttribute("listSize", KPIByCC.size());

        request.setAttribute("campusID", campusID);

        request.setAttribute("key", key);

        return "oldOds/KpiOfCC";
    }

    @RequestMapping("/saveUpdateByCC")
    public String saveUpdateByCC(HttpServletRequest request,
                                 @RequestParam(value = "campusID", required = false)String campusID,
                                 @RequestParam(value = "AutoID", required = false) String AutoID,
                                 @RequestParam(value = "cID", required = false) String cID,
                                 @RequestParam(value = "cName", required = false) String cName,
                                 @RequestParam(value = "reNewKpi", required = false) String reNewKpi,
                                 @RequestParam(value = "refKpi", required = false) String refKpi,
                                 @RequestParam(value = "mktKpi", required = false) String mktKpi){
        Map dateMap = campusDSRService.queryFinanceMonth(Integer.parseInt(AutoID));//财务月开始和结束时间

        String[] ids = cID.split(",");
        String[] cNames = cName.split(",");
        String[] renNews = reNewKpi.split(",");
        String[] refs = refKpi.split(",");
        String[] mkts = mktKpi.split(",");

        List renNewList = new ArrayList();
        List refList = new ArrayList();
        List mktList = new ArrayList();

        for (int i = 0; i < ids.length; i++) {

            Map reNewMap = new HashMap();

            reNewMap.put("cID", ids[i].replace("|", ""));
            reNewMap.put("cName", cNames[i]);
            reNewMap.put("reNewKpiValue", renNews[i]);
            reNewMap.put("refKpiValue", refs[i]);
            reNewMap.put("mktKpiValue", mkts[i]);

            renNewList.add(reNewMap);
            refList.add(reNewMap);
            mktList.add(reNewMap);
        }

        Map map = new HashMap();
        map.put("AutoID",AutoID );

        map.put("reNewKpi", renNewList);
        map.put("refKpi", refList);
        map.put("mktKpi", mktList);

        map.put("Text", dateMap.get("EndDate").toString().substring(0,7));

        jmsodskpiService.updateAddByCC(map);

        String str = "";
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        try {
            str = URLEncoder.encode(sdf.format(new Date()),"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "redirect:/oldOds/kpiByCC?AutoID=" + AutoID + "&campusID=" + campusID+"&key="+str;
    }
}
