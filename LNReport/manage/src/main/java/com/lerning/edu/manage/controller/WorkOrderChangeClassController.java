package com.lerning.edu.manage.controller;

import com.lerning.edu.beans.SysUser;
import com.lerning.edu.beans.WorkOrder;
import com.lerning.edu.beans.WorkOrderAuditRecord;
import com.lerning.edu.beans.page.PageList;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.manage.util.UserUtils;
import com.lerning.edu.services.StudentService;
import com.lerning.edu.services.WorkOrderAuditRecordService;
import com.lerning.edu.services.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/3
 */
@Controller
@RequestMapping("/workOrderChangeClasses")
public class WorkOrderChangeClassController extends BaseController {
    @Autowired
    private WorkOrderService workOrderService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private WorkOrderAuditRecordService workOrderAuditRecordService;

    @RequestMapping("/apply")
    public String apply(HttpServletRequest request, String employeeId) {

        HttpSession session = request.getSession();

        if(session.getValue(employeeId) != null){
            String value = session.getValue(employeeId).toString();

            if(session.getValue(employeeId) != null && !("").equals(session.getValue(employeeId)) && (employeeId).equals(value)){
                request.setAttribute("isLogin", "N");
            }else{
                request.setAttribute("isLogin", "Y");
            }
        }else {
            request.setAttribute("isLogin", "Y");
        }

        List employees = new ArrayList();
        Map employee = new HashMap();

        if (StringUtil.isEmpty(employeeId)) {
            SysUser user = UserUtils.getUser();
            String account = user.getAccount();
            employees = workOrderService.queryEmployeeByCfield(account);
        } else {
            employees = workOrderService.queryEmployeeByUserID(employeeId);
        }

        if (employees.size() > 0) {
            employee = (HashMap)employees.get(0);
            if (!employee.get("cValue").equals("CC_SA")) {
                return "/WorkOrderChangeClasses/view";
            }
        } else {
            return "/WorkOrderChangeClasses/view";
        }

        request.setAttribute("tEmployeeID", employeeId);
        return "/WorkOrderChangeClasses/apply";
    }

    @RequestMapping("/save")
    public String save(HttpServletRequest request,
                       @RequestParam(value = "tEmployeeID", required = false) String tEmployeeID,
                       @RequestParam(value = "userId", required = false) String userId,
                       @RequestParam(value = "classID", required = false) String classID,
                       @RequestParam(value = "creason", required = false) String cReason,
                       @RequestParam(value = "cstopclassdate", required = false) String cStopClassDate,
                       @RequestParam(value = "crecoverydate", required = false) String cRecoveryDate,
                       @RequestParam(value = "cisoriginal", required = false) int cIsOriginal,
                       @RequestParam(value = "cremarks", required = false) String cRemarks,
                       @RequestParam(value = "resumeClassID", required = false) String resumeClassID) {

        WorkOrder workOrder = new WorkOrder();

        if(StringUtil.isEmpty(tEmployeeID)){
            SysUser user = UserUtils.getUser();
            List employees = workOrderService.queryEmployeeByCfield(user.getAccount());

            if (employees.size() > 0) {
                Map employee = (HashMap)employees.get(0);
                tEmployeeID = employee.get("cUserID").toString();
            }else{
                tEmployeeID = "3378BBC1-8313-4023-9D15-5C8C58BD66A6";
            }
        }

        workOrder.setCcreateuser(tEmployeeID);

        workOrder.setCuserid(userId);
        workOrder.setCclassid(classID.substring(1,classID.length()));
        workOrder.setCstatus(1);
        workOrder.setCtype(3);//转班
        workOrder.setCreason(cReason);
        workOrder.setCstopclassdate(DateUtil.formateStringToDate(cStopClassDate));
        workOrder.setCrecoverydate(DateUtil.formateStringToDate(cRecoveryDate));
        workOrder.setCisoriginal(cIsOriginal);
        workOrder.setCremarks(cRemarks);
        workOrder.setCisprint(2);
        workOrder.setCcreatedate(new Date());
        workOrder.setResumeClassID(resumeClassID);

        Map map = new HashMap();
        map.put("userId", userId);
        Map users = workOrderService.queryByUserId(map);
        workOrder.setCampus(users.get("cName").toString());

        map.put("campus", workOrder.getCampus());
        Map maxOrderNo = workOrderService.queryMaxOrderNo(map);

        String orderNo = DateUtil.getYear().substring(2, 4) + users.get("cAddress").toString();
        if (maxOrderNo == null) {
            workOrder.setCorderno(orderNo + "0001");
        } else {
            String oNo = maxOrderNo.get("cOrderNo").toString();
            workOrder.setCorderno(orderNo + StringUtil.autoGenericCode(oNo.substring(oNo.length() - 4, oNo.length()), 4));
        }

        workOrderService.add(workOrder);
        return "redirect:/workOrderInterrupt/listPage?employeeId="+tEmployeeID;
    }

    //审核任务
    @RequestMapping("/taskList")
    public String list(HttpServletRequest request, WorkOrder workOrder){
        request.setAttribute("workOrder",workOrder);
        return "/WorkOrderChangeClasses/taskList";
    }
    //审核任务
    @RequestMapping("/asynTaskList")
    @ResponseBody
    public PageList asynTaskList(PageList pageList, WorkOrder workOrder) {
        workOrder.setCstatus(1);
        workOrder.setCtype(3);//转班
        return workOrderService.queryPage(pageList, workOrder);
    }

    //操作任务
    @RequestMapping("/operationTaskList")
    public String operationTaskList(HttpServletRequest request, WorkOrder workOrder){
        request.setAttribute("workOrder",workOrder);
        return "/WorkOrderChangeClasses/operationTaskList";
    }

    //操作任务
    @RequestMapping("/asynOperationTaskList")
    @ResponseBody
    public PageList asynOperationTaskList(PageList pageList, WorkOrder workOrder) {
        workOrder.setCstatus(2);
        workOrder.setCtype(3);//停课
        return workOrderService.queryPage(pageList, workOrder);
    }

    //打印停课工单列表
    @RequestMapping("/printList")
    public String printList(HttpServletRequest request, WorkOrder workOrder){
        request.setAttribute("workOrder",workOrder);
        return "/WorkOrderChangeClasses/printList";
    }

    //打印停课工单列表
    @RequestMapping("/asnyPrintList")
    @ResponseBody
    public PageList asnPrintList(PageList pageList, WorkOrder workOrder) {
        workOrder.setCstatus(4);
        workOrder.setCtype(3);//停课
        return workOrderService.queryPage(pageList, workOrder);
    }

    @RequestMapping("/detail")
    public String detail(HttpServletRequest request, String orderNo, String userID, String flag) {

        String returnValue = "WorkOrderChangeClasses/detail";

        Map map = new HashMap();
        map.put("orderNo", orderNo);
        Map workOrderDetail = workOrderService.queryDetail(map);

        if (workOrderDetail == null) {
            returnValue = "WorkOrderChangeClasses/auditPage";
            workOrderDetail = workOrderService.queryDetail1(map);
        } else if (workOrderDetail.get("cStatus").toString().equals("2")) {
            returnValue = "WorkOrderChangeClasses/operation";
        } else if (workOrderDetail.get("cStatus").toString().equals("4")) {
            returnValue = "WorkOrderChangeClasses/printPage";
        }

        map.put("userID", workOrderDetail.get("cUserID"));
        map.put("userId", workOrderDetail.get("cUserID"));

        Map student = studentService.findStudentOne(map);

        String cID = workOrderDetail.get("cClassID").toString();
        String newCID = workOrderDetail.get("resumeClassID").toString();

        map.put("classID", cID.trim());
        map.put("newClassID", newCID.trim());

        //查询详情(班级)
        List myClass = studentService.findClassByResume(map);

        WorkOrderAuditRecord woar = new WorkOrderAuditRecord();
        woar.setCworkorderid(workOrderDetail.get("cID").toString());
        List record = workOrderAuditRecordService.queryList(woar);

        request.setAttribute("workOrderDetail", workOrderDetail);
        request.setAttribute("student", student);
        request.setAttribute("classes", myClass);

        request.setAttribute("record", record);

        request.setAttribute("flag", flag);
        request.setAttribute("userID", userID);

        return returnValue;
    }
}
