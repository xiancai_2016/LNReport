package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/5/17
 */
@Controller
@RequestMapping("campusDSR")
public class CampusDSRController extends BaseController {

    @Autowired
    private CampusDSRService campusDSRService;

    @RequestMapping("")
    @RequiresPermissions("view:campusDSR")
    public String compusDSR(HttpServletRequest request,
                            @RequestParam(value = "CampusID", defaultValue = "CBD07D30-909D-467A-B5AB-ED8C11532FB0") String CampusID,
                            @RequestParam(value = "AutoID", defaultValue = "3") int AutoID) {

        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryFinanceMonths();//财务月列表
        Map beginDateAndEndDate = campusDSRService.queryFinanceMonth(AutoID);//财务月开始和结束时间

        int appSumTIN = 0;
        int FreshPresSumTIN = 0;
        BigDecimal ShowRatioSumTIN = new BigDecimal(0.00);
        BigDecimal RatioSumTIN = new BigDecimal(0.00);
        BigDecimal ocSumTIN = new BigDecimal(0.00);
        BigDecimal ContractsSumTIN = new BigDecimal(0.00);
        BigDecimal CashInSumTIN = new BigDecimal(0.00);

        int appSumTOUT = 0;
        int FreshPresSumTOUT = 0;
        BigDecimal ShowRatioSumTOUT = new BigDecimal(0.00);
        BigDecimal RatioSumTOUT = new BigDecimal(0.00);
        BigDecimal ocSumTOUT = new BigDecimal(0.00);
        BigDecimal ContractsSumTOUT = new BigDecimal(0.00);
        BigDecimal CashInSumTOUT = new BigDecimal(0.00);

        int appSumWKIN = 0;
        int FreshPresSumWKIN = 0;
        BigDecimal ShowRatioSumWKIN = new BigDecimal(0.00);
        BigDecimal RatioSumWKIN = new BigDecimal(0.00);
        BigDecimal ocSumWKIN = new BigDecimal(0.00);
        BigDecimal ContractsSumWKIN = new BigDecimal(0.00);
        BigDecimal CashInSumWKIN = new BigDecimal(0.00);

        int appSumREF = 0;
        int FreshPresSumREF = 0;
        BigDecimal ShowRatioSumREF = new BigDecimal(0.00);
        BigDecimal RatioSumREF = new BigDecimal(0.00);
        BigDecimal ocSumREF = new BigDecimal(0.00);
        BigDecimal ContractsSumREF = new BigDecimal(0.00);
        BigDecimal CashInSumREF = new BigDecimal(0.00);

        BigDecimal ContractsSumRENEW = new BigDecimal(0.00);
        BigDecimal CashInSumRENEW = new BigDecimal(0.00);

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("BeginDate", beginDateAndEndDate.get("BeginDate").toString());
        map.put("EndDate", beginDateAndEndDate.get("EndDate").toString());
        map.put("SaleMode", "27F05A82-5601-45B2-83A5-8360A641F41F','65C4B6C0-465E-427B-96BB-85225704BA9D"); //TIN
        List TIN = campusDSRService.queryTIN(map);
        for (int i = 0; i < TIN.size(); i++) {
            Map mapTIN = (HashMap) TIN.get(i);
            appSumTIN = appSumTIN + Integer.parseInt(mapTIN.get("APPNum").toString());
            FreshPresSumTIN = FreshPresSumTIN + Integer.parseInt(mapTIN.get("FreshPres").toString());

            ShowRatioSumTIN = ShowRatioSumTIN.add(new BigDecimal(mapTIN.get("Show").toString()));
            RatioSumTIN = RatioSumTIN.add(new BigDecimal(mapTIN.get("Ratio").toString()));
            ocSumTIN = ocSumTIN.add(new BigDecimal(mapTIN.get("OC").toString()));
            ContractsSumTIN = ContractsSumTIN.add(new BigDecimal(mapTIN.get("Contracts").toString()));
            CashInSumTIN = CashInSumTIN.add(new BigDecimal(mapTIN.get("CashIn").toString()));
        }

        map.put("SaleMode", "FBE4B6F5-E5C9-4D36-BBB2-F31EE975F076");//TOUT
        List TOUT = campusDSRService.queryTIN(map);
        for (int i = 0; i < TOUT.size(); i++) {
            Map mapTOUT = (HashMap) TOUT.get(i);
            appSumTOUT = appSumTOUT + Integer.parseInt(mapTOUT.get("APPNum").toString());
            FreshPresSumTOUT = FreshPresSumTOUT + Integer.parseInt(mapTOUT.get("FreshPres").toString());

            ShowRatioSumTOUT = ShowRatioSumTOUT.add(new BigDecimal(mapTOUT.get("Show").toString()));
            RatioSumTOUT = RatioSumTOUT.add(new BigDecimal(mapTOUT.get("Ratio").toString()));
            ocSumTOUT = ocSumTOUT.add(new BigDecimal(mapTOUT.get("OC").toString()));
            ContractsSumTOUT = ContractsSumTOUT.add(new BigDecimal(mapTOUT.get("Contracts").toString()));
            CashInSumTOUT = CashInSumTOUT.add(new BigDecimal(mapTOUT.get("CashIn").toString()));
        }

        map.put("SaleMode", "A0D6D64E-8018-4766-82A3-1A41578A7F38"); //REF
        List REF = campusDSRService.queryREF(map);
        for (int i = 0; i < REF.size(); i++) {
            Map mapREF = (HashMap) REF.get(i);
            appSumREF = appSumREF + Integer.parseInt(mapREF.get("Leads").toString());
            FreshPresSumREF = FreshPresSumREF + Integer.parseInt(mapREF.get("FreshPres").toString());

            ShowRatioSumREF = ShowRatioSumREF.add(new BigDecimal(mapREF.get("Show").toString()));
            RatioSumREF = RatioSumREF.add(new BigDecimal(mapREF.get("Ratio").toString()));
            ocSumREF = ocSumREF.add(new BigDecimal(mapREF.get("OC").toString()));
            ContractsSumREF = ContractsSumREF.add(new BigDecimal(mapREF.get("Contracts").toString()));
            CashInSumREF = CashInSumREF.add(new BigDecimal(mapREF.get("CashIn").toString()));
        }
        map.put("SaleMode", "EADC6B6D-4E9F-4DB2-B8DC-0739F72E2507"); //WOIK IN
        List WKIN = campusDSRService.queryREF(map);
        for (int i = 0; i < WKIN.size(); i++) {
            Map mapWKIN = (HashMap) WKIN.get(i);
            appSumWKIN = appSumWKIN + Integer.parseInt(mapWKIN.get("Leads").toString());
            FreshPresSumWKIN = FreshPresSumWKIN + Integer.parseInt(mapWKIN.get("FreshPres").toString());

            ShowRatioSumWKIN = ShowRatioSumWKIN.add(new BigDecimal(mapWKIN.get("Show").toString()));
            RatioSumWKIN = RatioSumWKIN.add(new BigDecimal(mapWKIN.get("Ratio").toString()));
            ocSumWKIN = ocSumWKIN.add(new BigDecimal(mapWKIN.get("OC").toString()));
            ContractsSumWKIN = ContractsSumWKIN.add(new BigDecimal(mapWKIN.get("Contracts").toString()));
            CashInSumWKIN = CashInSumTIN.add(new BigDecimal(mapWKIN.get("CashIn").toString()));
        }

        List RENEW = campusDSRService.queryRENEW(map);
        for (int i = 0; i < RENEW.size(); i++) {
            Map mapRENEW = (HashMap) RENEW.get(i);

            ContractsSumRENEW = ContractsSumRENEW.add(new BigDecimal(mapRENEW.get("Contracts").toString()));
            CashInSumRENEW = CashInSumRENEW.add(new BigDecimal(mapRENEW.get("CashIn").toString()));
        }

        request.setAttribute("AutoID", AutoID);
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("departs", departs);
        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("TIN", TIN);
        request.setAttribute("appSumTIN", appSumTIN);
        request.setAttribute("FreshPresSumTIN", FreshPresSumTIN);
        request.setAttribute("ShowRatioSumTIN", ShowRatioSumTIN);
        request.setAttribute("RatioSumTIN", RatioSumTIN);
        request.setAttribute("ocSumTIN", ocSumTIN);
        request.setAttribute("ContractsSumTIN", ContractsSumTIN);
        request.setAttribute("CashInSumTIN", CashInSumTIN);

        request.setAttribute("TOUT", TOUT);
        request.setAttribute("appSumTOUT", appSumTOUT);
        request.setAttribute("FreshPresSumTOUT", FreshPresSumTOUT);
        request.setAttribute("ShowRatioSumTOUT", ShowRatioSumTOUT);
        request.setAttribute("RatioSumTOUT", RatioSumTOUT);
        request.setAttribute("ocSumTOUT", ocSumTOUT);
        request.setAttribute("ContractsSumTOUT", ContractsSumTOUT);
        request.setAttribute("CashInSumTOUT", CashInSumTOUT);

        request.setAttribute("REF", REF);
        request.setAttribute("appSumREF", appSumREF);
        request.setAttribute("FreshPresSumREF", FreshPresSumREF);
        request.setAttribute("ShowRatioSumREF", ShowRatioSumREF);
        request.setAttribute("RatioSumREF", RatioSumREF);
        request.setAttribute("ocSumREF", ocSumREF);
        request.setAttribute("ContractsSumREF", ContractsSumREF);
        request.setAttribute("CashInSumREF", CashInSumREF);

        request.setAttribute("WKIN", WKIN);
        request.setAttribute("appSumWKIN", appSumWKIN);
        request.setAttribute("FreshPresSumWKIN", FreshPresSumWKIN);
        request.setAttribute("ShowRatioSumWKIN", ShowRatioSumWKIN);
        request.setAttribute("RatioSumWKIN", RatioSumWKIN);
        request.setAttribute("ocSumWKIN", ocSumWKIN);
        request.setAttribute("ContractsSumWKIN", ContractsSumWKIN);
        request.setAttribute("CashInSumWKIN", CashInSumWKIN);

        request.setAttribute("RENEW", RENEW);
        request.setAttribute("ContractsSumRENEW", ContractsSumRENEW);
        request.setAttribute("CashInSumRENEW", CashInSumRENEW);

        return "Campus/dsr";
    }

    /***
     * 到报名退费页面
     *
     * @param request
     * @param CampusID
     * @param key
     * @return
     */
    @RequestMapping("totalCAR")
    @RequiresPermissions("view:totalCAR")
    public String totalCARIndex(HttpServletRequest request,
                                @RequestParam(value = "CampusID", required = false) String CampusID,
                                @RequestParam(value = "key", required = false) String key) {

        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryFinanceMonths();//财务月列表

        List FinanceMonthOfDq = campusDSRService.queryFinanceMonthOfDq(dayMap());//财务月列表
        Map map = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", map.get("AutoID"));
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("departs", departs);
        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("chargeAndRefundByK", "");
        request.setAttribute("chargeAndRefundByC", "");
        request.setAttribute("chargeAndRefundByY", "");

        request.setAttribute("mapK", "");
        request.setAttribute("mapC", "");
        request.setAttribute("mapY", "");

        request.setAttribute("totalChargeAndRefund", "");
        request.setAttribute("key", key);

        return "Campus/totalCAR";
    }

    /***
     * 报名退费
     *
     * @param request
     * @param CampusID
     * @param AutoID
     * @return
     */
    @RequestMapping("totalCARList")
    @RequiresPermissions("view:totalCAR")
    public String totalCAR(HttpServletRequest request,
                           @RequestParam(value = "CampusID", required = false) String CampusID,
                           @RequestParam(value = "AutoID", required = false) int AutoID) {

        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryFinanceMonths();//财务月列表
        Map beginDateAndEndDate = campusDSRService.queryFinanceMonth(AutoID);//财务月开始和结束时间

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("BeginDate", beginDateAndEndDate.get("BeginDate").toString());
        map.put("EndDate", beginDateAndEndDate.get("EndDate").toString());
        map.put("Grade", "");

        NumberFormat nf = new DecimalFormat("￥,###.##");

        List chargeAndRefund = campusDSRService.chargeAndRefund(map);

        List chargeAndRefundByK = new ArrayList();
        List chargeAndRefundByC = new ArrayList();
        List chargeAndRefundByY = new ArrayList();

        if (chargeAndRefund.size() > 0) {
            //汇总
            for (int i = 0; i < chargeAndRefund.size(); i++) {
                Map mapKItem = (HashMap) chargeAndRefund.get(i);
                String grade = mapKItem.get("tgrade").toString();
                if (grade.equals("K1") || grade.equals("K2") || grade.equals("K3") || grade.equals("K4")) {
                    chargeAndRefundByK.add(mapKItem);
                } else if (grade.equals("Y1") || grade.equals("Y2") || grade.equals("Y3") || grade.equals("Y4") || grade.equals("Y5") || grade.equals("Y6")) {
                    chargeAndRefundByY.add(mapKItem);
                } else {
                    chargeAndRefundByC.add(mapKItem);
                }
            }
        }

        Map mapK = Amount(chargeAndRefundByK);
        Map mapC = Amount(chargeAndRefundByC);
        Map mapY = Amount(chargeAndRefundByY);

        map.put("cFlag", 1);
        Map mapSKC = campusDSRService.querySCKCount(map);
        map.put("cFlag", -1);
        Map maprSKC = campusDSRService.querySCKCount(map);
        maprSKC.put("skcrFact", maprSKC.get("skcrFact").toString().replace("-", ""));

        Map SKC = new HashMap();
        SKC.put("skcFact", nf.format(Double.valueOf(mapSKC.get("skcrFact").toString())));
        SKC.put("skcrFact", nf.format(Double.valueOf(maprSKC.get("skcrFact").toString())));

        Map totalChargeAndRefund = campusDSRService.totalChargeAndRefund(map);
        totalChargeAndRefund.put("sumtcost", nf.format(Double.valueOf(totalChargeAndRefund.get("sumtcost").toString()) + Double.valueOf(mapSKC.get("skcrFact").toString())));
        totalChargeAndRefund.put("sumrtcost", nf.format(Double.valueOf(totalChargeAndRefund.get("sumrtcost").toString()) + Double.valueOf(maprSKC.get("skcrFact").toString())));


        request.setAttribute("AutoID", AutoID);
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("departs", departs);
        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("chargeAndRefundByK", changeType(chargeAndRefundByK));
        request.setAttribute("chargeAndRefundByC", changeType(chargeAndRefundByC));
        request.setAttribute("chargeAndRefundByY", changeType(chargeAndRefundByY));

        request.setAttribute("mapK", mapK);
        request.setAttribute("mapC", mapC);
        request.setAttribute("mapY", mapY);

        request.setAttribute("SKC", SKC);

        request.setAttribute("totalChargeAndRefund", totalChargeAndRefund);

        return "Campus/totalCAR";
    }

    /***
     * 到开班概况页面
     *
     * @param request
     * @param CampusID
     * @param key
     * @return
     */
    @RequestMapping("queryClassCount")
    @RequiresPermissions("view:queryClassCount")
    public String queryClassCountIndex(HttpServletRequest request,
                                       @RequestParam(value = "CampusID", required = false) String CampusID,
                                       @RequestParam(value = "key", required = false) String key) {

        List departs = campusDSRService.queryDepartList();//校区

        List FinanceMonths = campusDSRService.queryFinanceMonths();//财务月列表

        List FinanceMonthOfDq = campusDSRService.queryFinanceMonthOfDq(dayMap());//财务月列表
        Map map = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", map.get("AutoID"));

        request.setAttribute("CampusID", CampusID);

        request.setAttribute("departs", departs);

        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("queryClassCountByK", "");
        request.setAttribute("queryClassCountByC", "");
        request.setAttribute("queryClassCountByY", "");

        request.setAttribute("mapK", "");
        request.setAttribute("mapC", "");
        request.setAttribute("mapY", "");
        request.setAttribute("mapT", "");

        return "Campus/queryClassCount";
    }


    /***
     * 开班概况
     *
     * @param request
     * @param CampusID
     * @param AutoID
     * @return
     */
    @RequestMapping("queryClassCountList")
    @RequiresPermissions("view:queryClassCount")
    public String queryClassCount(HttpServletRequest request,
                                  @RequestParam(value = "CampusID", required = false) String CampusID,
                                  @RequestParam(value = "AutoID", required = false) int AutoID) {
        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryFinanceMonths();//财务月列表
        Map beginDateAndEndDate = campusDSRService.queryFinanceMonth(AutoID);//财务月开始和结束时间

        String endDate = beginDateAndEndDate.get("EndDate").toString();

        //如果不是当前财务月,则返回null
        String newDate = DateUtil.formatDate(new Date(), Constants.DATE_FORMART2);
        if (DateUtil.compare_date(endDate, newDate)) {
            request.setAttribute("AutoID", AutoID);
            request.setAttribute("departs", departs);

            request.setAttribute("FinanceMonths", FinanceMonths);
            request.setAttribute("CampusID", CampusID);

            request.setAttribute("queryClassCountByK", "");
            request.setAttribute("queryClassCountByC", "");
            request.setAttribute("queryClassCountByY", "");

            request.setAttribute("mapK", "");
            request.setAttribute("mapC", "");
            request.setAttribute("mapY", "");
            request.setAttribute("mapT", "");

            request.setAttribute("dataInfo", "暂无数据");

            return "Campus/queryClassCount";
        }

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("Grade", "");
        map.put("BeginDate", DateUtil.getBeforeDateToString(Constants.DATE_FORMART, 6, DateUtil.formateStringToDate(endDate)) + " 00:00:00.0");
        map.put("EndDate", endDate);
        map.put("BeginDateFoWKB", beginDateAndEndDate.get("BeginDate").toString());

        List queryClassCountByK = new ArrayList();
        List queryClassCountByC = new ArrayList();
        List queryClassCountByY = new ArrayList();

        List queryClassCount = campusDSRService.queryClassCountByK(map);

        if (queryClassCount.size() > 0) {
            for (int i = 0; i < queryClassCount.size(); i++) {
                Map mapKItem = (HashMap) queryClassCount.get(i);
                String grade = mapKItem.get("grade").toString();
                if (grade.equals("K1") || grade.equals("K2") || grade.equals("K3") || grade.equals("K4")) {
                    queryClassCountByK.add(mapKItem);
                } else if (grade.equals("Y1") || grade.equals("Y2") || grade.equals("Y3") || grade.equals("Y4") || grade.equals("Y5") || grade.equals("Y6")) {
                    queryClassCountByY.add(mapKItem);
                } else {
                    queryClassCountByC.add(mapKItem);
                }
            }
        }

        Map mapK = cls(queryClassCountByK);
        Map mapC = cls(queryClassCountByC);
        Map mapY = cls(queryClassCountByY);

        Map mapT = new HashMap();

        mapT.put("tacc", (int) mapK.get("acc") + (int) mapC.get("acc") + (int) mapY.get("acc"));
        mapT.put("tacc1", (int) mapK.get("acc1") + (int) mapC.get("acc1") + (int) mapY.get("acc1"));
        mapT.put("tacc2", (int) mapK.get("acc2") + (int) mapC.get("acc2") + (int) mapY.get("acc2"));
        mapT.put("tacc3", (int) mapK.get("acc3") + (int) mapC.get("acc3") + (int) mapY.get("acc3"));
        mapT.put("tacc4", (int) mapK.get("acc4") + (int) mapC.get("acc4") + (int) mapY.get("acc4"));
        mapT.put("tclsCount", (int) mapK.get("clsCount") + (int) mapC.get("clsCount") + (int) mapY.get("clsCount"));
        mapT.put("tsumcc", (int) mapK.get("sumcc") + (int) mapC.get("sumcc") + (int) mapY.get("sumcc"));
        mapT.put("twkbCount", (int) mapK.get("wkbCount") + (int) mapC.get("wkbCount") + (int) mapY.get("wkbCount"));
        mapT.put("twkbClsCount", (int) mapK.get("wkbClsCount") + (int) mapC.get("wkbClsCount") + (int) mapY.get("wkbClsCount"));
        mapT.put("txuCount", (int) mapK.get("xuCount") + (int) mapC.get("xuCount") + (int) mapY.get("xuCount"));
        mapT.put("thJCount", (int) mapK.get("hJCount") + (int) mapC.get("hJCount") + (int) mapY.get("hJCount"));
        DecimalFormat df = new DecimalFormat("#.00");
        int tclsCount = (int) mapK.get("clsCount") + (int) mapC.get("clsCount") + (int) mapY.get("clsCount");
        int sumcc = (int) mapK.get("sumcc") + (int) mapC.get("sumcc") + (int) mapY.get("sumcc");

        if (tclsCount == 0 && sumcc == 0) {
            mapT.put("tagvStu", df.format(0));

        } else {
            tclsCount = tclsCount == 0 ? 1 : tclsCount;
            sumcc = sumcc == 0 ? 1 : sumcc;

            BigDecimal a = new BigDecimal(sumcc);
            BigDecimal b = new BigDecimal(tclsCount);
            mapT.put("tagvStu", df.format(a.divide(b, 2, BigDecimal.ROUND_HALF_UP).doubleValue()));

        }

        request.setAttribute("AutoID", AutoID);
        request.setAttribute("departs", departs);

        request.setAttribute("FinanceMonths", FinanceMonths);
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("queryClassCountByK", queryClassCountByK);
        request.setAttribute("queryClassCountByC", queryClassCountByC);
        request.setAttribute("queryClassCountByY", queryClassCountByY);

        request.setAttribute("mapK", mapK);
        request.setAttribute("mapC", mapC);
        request.setAttribute("mapY", mapY);
        request.setAttribute("mapT", mapT);

        request.setAttribute("dataInfo", "");

        return "Campus/queryClassCount";
    }

    /**
     * 到续班统计页面
     *
     * @param request
     * @param CampusID
     * @param key
     * @return
     */
    @RequestMapping("queryStudentCount")
    @RequiresPermissions("view:queryStudentCount")
    public String queryStudentCountIndex(HttpServletRequest request,
                                         @RequestParam(value = "CampusID", required = false) String CampusID,
                                         @RequestParam(value = "key", required = false) String key) {
        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dayMap());//当前月

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("Grade", "");

        Map dmap = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", dmap.get("AutoID"));
        request.setAttribute("departs", departs);

        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("queryStudentCountByK", "");
        request.setAttribute("queryStudentCountByC", "");
        request.setAttribute("queryStudentCountByY", "");

        request.setAttribute("mapK", "");
        request.setAttribute("mapC", "");
        request.setAttribute("mapY", "");
        request.setAttribute("mapT", "");
        request.setAttribute("key", key);

        return "Campus/queryStudentCount";
    }

    /***
     * 续班统计
     *
     * @param request
     * @param CampusID
     * @param AutoID
     * @return
     */
    @RequestMapping("queryStudentCountList")
    @RequiresPermissions("view:queryStudentCount")
    public String queryStudentCount(HttpServletRequest request,
                                    @RequestParam(value = "CampusID", required = false) String CampusID,
                                    @RequestParam(value = "AutoID", required = false) int AutoID) {
        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        Map beginDateAndEndDate = campusDSRService.queryNaturalMonth(AutoID);//财务月开始和结束时间

        List CampusIDS = new ArrayList();

        if (!StringUtil.isEmpty(CampusID)) {
            if (CampusID.substring(0, 1).equals(",")) {
                CampusID = CampusID.substring(1, CampusID.length());
            }
            String[] cIDs = CampusID.split(",");

            for (int i = 0; i < cIDs.length; i++) {
                CampusIDS.add(cIDs[i]);
            }
        } else {
            CampusID = "";
        }

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("Grade", "");
        map.put("BeginDate", beginDateAndEndDate.get("NBeginDate").toString());
        map.put("EndDate", beginDateAndEndDate.get("NEndDate").toString());

        List queryStudentCountByK = new ArrayList();
        List queryStudentCountByC = new ArrayList();
        List queryStudentCountByY = new ArrayList();

        List queryStudentCount = campusDSRService.queryStudentCount(map);

        if (queryStudentCount.size() > 0) {
            for (int i = 0; i < queryStudentCount.size(); i++) {
                Map mapKItem = (HashMap) queryStudentCount.get(i);
                String grade = mapKItem.get("grade").toString();
                if (grade.equals("K1") || grade.equals("K2") || grade.equals("K3") || grade.equals("K4")) {
                    queryStudentCountByK.add(mapKItem);
                } else if (grade.equals("Y1") || grade.equals("Y2") || grade.equals("Y3") || grade.equals("Y4") || grade.equals("Y5") || grade.equals("Y6")) {
                    queryStudentCountByY.add(mapKItem);
                } else {
                    queryStudentCountByC.add(mapKItem);
                }
            }
        }

        Map mapK = stu(queryStudentCountByK);
        Map mapC = stu(queryStudentCountByC);
        Map mapY = stu(queryStudentCountByY);

        Map mapT = new HashMap();

        int tkdCount = Integer.parseInt(mapK.get("kdCount").toString()) + Integer.parseInt(mapC.get("kdCount").toString()) + Integer.parseInt(mapY.get("kdCount").toString());
        mapT.put("tkdCount", tkdCount);
        BigDecimal ssCountK = new BigDecimal(mapK.get("ssCount").toString());
        BigDecimal ssCountC = new BigDecimal(mapC.get("ssCount").toString());
        BigDecimal ssCountY = new BigDecimal(mapY.get("ssCount").toString());
        mapT.put("tssCount", ssCountK.add(ssCountC).add(ssCountY).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

        BigDecimal mbCountK = new BigDecimal(mapK.get("mbCount").toString());
        BigDecimal mbCountC = new BigDecimal(mapC.get("mbCount").toString());
        BigDecimal mbCountY = new BigDecimal(mapY.get("mbCount").toString());

        int totalCount = Integer.parseInt(mapK.get("totalCount").toString()) + Integer.parseInt(mapC.get("totalCount").toString()) + Integer.parseInt(mapY.get("totalCount").toString());

        if (tkdCount == 0) {
            mapT.put("tagvXb", 0);
        } else {
            mapT.put("tagvXb", (ssCountK.add(ssCountC).add(ssCountY)).divide(new BigDecimal(tkdCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
        }

        if (totalCount == 0) {
            mapT.put("ttmbCount", 0);
        } else {
            mapT.put("ttmbCount", (mbCountK.add(mbCountC).add(mbCountY)).divide(new BigDecimal(totalCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
        }

        request.setAttribute("AutoID", AutoID);
        request.setAttribute("departs", departs);

        request.setAttribute("FinanceMonths", FinanceMonths);
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("queryStudentCountByK", queryStudentCountByK);
        request.setAttribute("queryStudentCountByC", queryStudentCountByC);
        request.setAttribute("queryStudentCountByY", queryStudentCountByY);

        request.setAttribute("mapK", mapK);
        request.setAttribute("mapC", mapC);
        request.setAttribute("mapY", mapY);
        request.setAttribute("mapT", mapT);
        request.setAttribute("CampusIDS", CampusIDS);

        return "Campus/queryStudentCount";
    }

    public Map cls(List queryClassCount) {
        int acc = 0;
        int acc1 = 0;
        int acc2 = 0;
        int acc3 = 0;
        int acc4 = 0;
        int clsCount = 0;
        int sumcc = 0;
        int wkbCount = 0;
        int wkbClsCount = 0;
        int xuCount = 0;
        int hJCount = 0;

        Map mapK = new HashMap();

        for (int i = 0; i < queryClassCount.size(); i++) {
            Map mapKItem = (HashMap) queryClassCount.get(i);
            acc = acc + Integer.parseInt(mapKItem.get("acc").toString());
            acc1 = acc1 + Integer.parseInt(mapKItem.get("acc1").toString());
            acc2 = acc2 + Integer.parseInt(mapKItem.get("acc2").toString());
            acc3 = acc3 + Integer.parseInt(mapKItem.get("acc3").toString());
            acc4 = acc4 + Integer.parseInt(mapKItem.get("acc4").toString());
            clsCount = clsCount + Integer.parseInt(mapKItem.get("clsCount").toString());
            sumcc = sumcc + Integer.parseInt(mapKItem.get("sumcc").toString());
            wkbCount = wkbCount + Integer.parseInt(mapKItem.get("wkbCount").toString());
            wkbClsCount = wkbClsCount + Integer.parseInt(mapKItem.get("wkbClsCount").toString());
            xuCount = xuCount + Integer.parseInt(mapKItem.get("xuCount").toString());
            hJCount = hJCount + Integer.parseInt(mapKItem.get("hJCount").toString());
        }
        mapK.put("acc", acc);
        mapK.put("acc1", acc1);
        mapK.put("acc2", acc2);
        mapK.put("acc3", acc3);
        mapK.put("acc4", acc4);
        mapK.put("clsCount", clsCount);
        mapK.put("sumcc", sumcc);
        mapK.put("wkbCount", wkbCount);
        mapK.put("wkbClsCount", wkbClsCount);
        mapK.put("xuCount", xuCount);
        mapK.put("hJCount", hJCount);
        DecimalFormat df = new DecimalFormat("#.00");

        if (clsCount == 0 && sumcc == 0) {
            mapK.put("agvStu", df.format(0));
        } else {
            clsCount = clsCount == 0 ? 1 : clsCount;
            sumcc = sumcc == 0 ? 1 : sumcc;

            BigDecimal c = new BigDecimal(sumcc);
            BigDecimal d = new BigDecimal(clsCount);
            mapK.put("agvStu", df.format(c.divide(d, 2, BigDecimal.ROUND_HALF_UP).doubleValue()));
        }
        return mapK;
    }

    public Map stu(List queryStudentCount) {
        int kdCount = 0;
        BigDecimal ssCount = new BigDecimal(0.00);
        BigDecimal mbCount = new BigDecimal(0.00);
        int totalCount = 0;
        BigDecimal tmbCount = new BigDecimal(0.00);

        Map map = new HashMap();

        for (int i = 0; i < queryStudentCount.size(); i++) {
            Map mapKItem = (HashMap) queryStudentCount.get(i);
            kdCount = kdCount + Integer.parseInt(mapKItem.get("kdCount").toString());
            ssCount = ssCount.add(new BigDecimal(mapKItem.get("ssCount").toString()));
            mbCount = mbCount.add(new BigDecimal(mapKItem.get("mbCount").toString()));
            totalCount = totalCount + Integer.parseInt(mapKItem.get("totalCount").toString());
            tmbCount = tmbCount.add(new BigDecimal(mapKItem.get("tmbCount").toString()));
        }

        map.put("kdCount", kdCount);
        map.put("ssCount", ssCount);
        map.put("totalCount", totalCount);
        map.put("mbCount", mbCount);

        if (kdCount == 0) {
            map.put("agvXb", 0);
        } else {
            map.put("agvXb", ssCount.divide(new BigDecimal(kdCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
        }

        if (totalCount == 0) {
            map.put("tmbCount", 0);
        } else {
            map.put("tmbCount", mbCount.divide(new BigDecimal(totalCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
        }

        return map;
    }

    public Map Amount(List queryStudentCount) {

        BigDecimal stucount = new BigDecimal(0.00);
        BigDecimal tcost = new BigDecimal(0.00);
        int rstucount = 0;
        BigDecimal rtcost = new BigDecimal(0.00);

        Map map = new HashMap();

        for (int i = 0; i < queryStudentCount.size(); i++) {
            Map mapKItem = (HashMap) queryStudentCount.get(i);
            stucount = stucount.add(new BigDecimal(mapKItem.get("stucount").toString()));
            tcost = tcost.add(new BigDecimal(mapKItem.get("tcost").toString()));
            rstucount = rstucount + Integer.parseInt(mapKItem.get("rstucount").toString());
            rtcost = rtcost.add(new BigDecimal(mapKItem.get("rtcost").toString()));
        }

        NumberFormat nf = new DecimalFormat("￥,###.##");
        map.put("stucount", stucount);
        map.put("tcost", nf.format(tcost));
        map.put("rstucount", rstucount);
        map.put("rtcost", nf.format(rtcost));

        return map;
    }

    public List changeType(List queryStudentCount) {
        NumberFormat nf = new DecimalFormat("￥,###.##");

        List newChangeType = new ArrayList();
        if (queryStudentCount.size() > 0) {
            for (int i = 0; i < queryStudentCount.size(); i++) {
                Map mapKItem = (HashMap) queryStudentCount.get(i);
                mapKItem.put("tcost", nf.format(Double.valueOf(mapKItem.get("tcost").toString())));
                mapKItem.put("rtcost", nf.format(Double.valueOf(mapKItem.get("rtcost").toString())));
                newChangeType.add(mapKItem);
            }
        }
        return newChangeType;
    }

    /***
     * 返回当前年月
     */
    public Map dayMap() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String m;
        Map dqd = new HashMap();
        if (month < 10) {
            m = "0" + month;
        } else {
            m = month + "";
        }
        dqd.put("dqDay", year + "-" + m);

        return dqd;
    }
}
