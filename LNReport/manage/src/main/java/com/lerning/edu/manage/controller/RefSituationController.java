package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.RefSituationService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/9
 */
@Controller
@RequestMapping("refSituation")
public class RefSituationController {

    @Autowired
    private RefSituationService refSituationService;
    @Autowired
    private CampusDSRService campusDSRService;


    @RequestMapping("/list")
    @RequiresPermissions("view:refSituation")
    public String list(HttpServletRequest request,
                       @RequestParam(value = "AutoID", required = false) String AutoID) {
        DecimalFormat df = new DecimalFormat("######0.00");
        DecimalFormat df1 = new DecimalFormat("######0");

        String SaleModeIds = "A0D6D64E-8018-4766-82A3-1A41578A7F38";//REF渠道

        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(DateUtil.dayMap());//当前月

        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", dateMap.get("AutoID"));

        String beninDate = "";
        String endDate = "";

        BigDecimal Leads = new BigDecimal(0);
        BigDecimal Show = new BigDecimal(0);
        BigDecimal En = new BigDecimal(0);

        BigDecimal tLeads = new BigDecimal(0);
        BigDecimal tShow = new BigDecimal(0);
        BigDecimal tEn = new BigDecimal(0);

        List refs = new ArrayList();
        List allRefS = new ArrayList();

        Map SH1 = new HashMap();
        Map SH2 = new HashMap();
        Map SH3 = new HashMap();
        Map SH4 = new HashMap();
        Map SH5 = new HashMap();
        Map SH6 = new HashMap();

        List SHList = new ArrayList();

        //数据表现
        Map data = new HashMap();

        if (!StringUtil.isEmpty(AutoID)) {
            Map beginDateAndEndDate = campusDSRService.queryNaturalMonth(Integer.parseInt(AutoID));//财务月开始和结束时间

            beninDate = beginDateAndEndDate.get("BeginDate").toString();
            endDate = beginDateAndEndDate.get("EndDate").toString();

            Map map = new HashMap();
            map.put("startTime", beninDate);
            map.put("endTime", endDate);
            map.put("text", endDate.substring(0, 7));

            refs = refSituationService.queryRef(map);

            request.setAttribute("AutoID", AutoID);

            if (refs.size() > 0) {
                for (Object object : refs) {
                    Map objMap = (HashMap) object;
                    Leads = Leads.add(new BigDecimal(objMap.get("lcount").toString()));
                    Show = Show.add(new BigDecimal(objMap.get("scount").toString()));

                    map.put("CampusID", objMap.get("cID"));
                    map.put("SaleModeIds", SaleModeIds);
                    Map refByCampus = refSituationService.queryEnByCampus(map);

                    if (refByCampus != null) {
                        objMap.put("ecount", refByCampus.get("Contracts"));
                        En = En.add(new BigDecimal(refByCampus.get("Contracts").toString()));
                    } else {
                        objMap.put("ecount", 0.00);
                    }

                    tLeads = tLeads.add(new BigDecimal(objMap.get("Tleads").toString()));
                    tShow = tShow.add(new BigDecimal(objMap.get("Tshow").toString()));
                    tEn = tEn.add(new BigDecimal(objMap.get("kValue").toString()));

                    BigDecimal kValue = new BigDecimal(objMap.get("kValue").toString());

                    if (StringUtil.divideValue(kValue) && refByCampus != null) {

                        objMap.put("AchiEn", Double.valueOf(df.format(new BigDecimal(refByCampus.get("Contracts").toString()).divide(kValue, 4, BigDecimal.ROUND_HALF_UP).doubleValue() * 100)));
                    } else {
                        objMap.put("AchiEn", 0.00);
                    }

                    allRefS.add(objMap);
                }
                SH1 = regionList("SH1", allRefS);
                SH2 = regionList("SH2", allRefS);
                SH3 = regionList("SH3", allRefS);
                SH4 = regionList("SH4", allRefS);
                SH5 = regionList("SH5", allRefS);
                SH6 = regionList("SH6", allRefS);

                SHList.add(SH1);
                SHList.add(SH2);
                SHList.add(SH3);
                SHList.add(SH4);
                //SHList.add(SH5);
                //SHList.add(SH6);
            }

            //财务月的总天数
            double totalMonthDay = DateUtil.getDistanceOfTwoDate(DateUtil.formateStringToDate(beninDate, Constants.DATE_FORMART2), DateUtil.formateStringToDate(endDate, Constants.DATE_FORMART2));

            //财务月到今天的总天数
            double totalMonthDayOfNew = DateUtil.getDistanceOfTwoDate(DateUtil.formateStringToDate(beninDate, Constants.DATE_FORMART2), new Date());


            data.put("year", endDate.substring(0, 4));
            data.put("month", endDate.substring(5, 7));

            data.put("CWDate", beninDate.substring(0, 10).replace("-", ".") + "-" + endDate.substring(5, 10).replace("-", "."));
            data.put("dateJD", df1.format(totalMonthDay));

            if (DateUtil.compare_date(endDate, DateUtil.formatDate(new Date(), Constants.DATE_FORMART2))) {
                data.put("TJData", beninDate.substring(0, 10).replace("-", ".") + "-" + DateUtil.formatDate(new Date()).substring(5, 10).replace("-", "."));
                data.put("dataJD", df1.format(totalMonthDayOfNew / totalMonthDay * 100) + "%");
            } else {
                data.put("TJData", beninDate.substring(0, 10).replace("-", ".") + "-" + endDate.substring(5, 10).replace("-", "."));
                data.put("dataJD", "100%");
            }
        }

        Map map = new HashMap();
        map.put("Leads", Leads);
        map.put("Show", Show);
        map.put("En", En);

        map.put("tLeads", tLeads);
        map.put("tShow", tShow);
        map.put("tEn", tEn);

        if (!StringUtil.divideValue(tLeads)) {
            map.put("aLeads", 0);
        } else {
            map.put("aLeads", df.format(Leads.divide(tLeads, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        }

        if (!StringUtil.divideValue(tShow)) {
            map.put("aShow", 0);
        } else {
            map.put("aShow", df.format(Show.divide(tShow, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        }

        if (!StringUtil.divideValue(tEn)) {
            map.put("aEn", 0);
        } else {
            map.put("aEn", df.format(En.divide(tEn, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        }

        request.setAttribute("map", map);
        request.setAttribute("data", data);

        request.setAttribute("SH1", SH1);
        request.setAttribute("SH2", SH2);
        request.setAttribute("SH3", SH3);
        request.setAttribute("SH4", SH4);
        request.setAttribute("SH5", SH5);
        request.setAttribute("SH6", SH6);

        request.setAttribute("SHList", SHList);

        Map map1 = new HashMap();
        Map map2 = new HashMap();
        Map map3 = new HashMap();
        Map map4 = new HashMap();
        Map map5 = new HashMap();
        Map map6 = new HashMap();

        List mapList = new ArrayList();

        if (allRefS.size() > 0) {
            List sortRefs = new ArrayList();
            for (Object obj : allRefS) {
                Map sortMap = (HashMap) obj;
                if (Integer.valueOf(sortMap.get("kValue").toString()) > 0) {
                    sortRefs.add(obj);
                }
            }
            //
            if (sortRefs.size() > 6) {
                StringUtil.sortByWhat(sortRefs, "AchiEn");
                map1.put("Ranking", "第1名");
                map1.put("EnName", ((HashMap) sortRefs.get(0)).get("cName"));
                map1.put("AchiEn", ((HashMap) sortRefs.get(0)).get("AchiEn"));

                map2.put("Ranking", "第2名");
                map2.put("EnName", ((HashMap) sortRefs.get(1)).get("cName"));
                map2.put("AchiEn", ((HashMap) sortRefs.get(1)).get("AchiEn"));

                map3.put("Ranking", "第3名");
                map3.put("EnName", ((HashMap) sortRefs.get(2)).get("cName"));
                map3.put("AchiEn", ((HashMap) sortRefs.get(2)).get("AchiEn"));

                map4.put("Ranking", "最后第3名");
                map4.put("EnName", ((HashMap) sortRefs.get(sortRefs.size() - 3)).get("cName"));
                map4.put("AchiEn", ((HashMap) sortRefs.get((sortRefs.size() - 3))).get("AchiEn"));

                map5.put("Ranking", "最后第2名");
                map5.put("EnName", ((HashMap) sortRefs.get(sortRefs.size() - 2)).get("cName"));
                map5.put("AchiEn", ((HashMap) sortRefs.get((sortRefs.size() - 2))).get("AchiEn"));

                map6.put("Ranking", "最后第1名");
                map6.put("EnName", ((HashMap) sortRefs.get(sortRefs.size() - 1)).get("cName"));
                map6.put("AchiEn", ((HashMap) sortRefs.get((sortRefs.size() - 1))).get("AchiEn"));

                StringUtil.sortByWhat(sortRefs, "AchiShow");
                map1.put("ShowName", ((HashMap) sortRefs.get(0)).get("cName"));
                map1.put("AchiShow", ((HashMap) sortRefs.get(0)).get("AchiShow"));

                map2.put("ShowName", ((HashMap) sortRefs.get(1)).get("cName"));
                map2.put("AchiShow", ((HashMap) sortRefs.get(1)).get("AchiShow"));

                map3.put("ShowName", ((HashMap) sortRefs.get(2)).get("cName"));
                map3.put("AchiShow", ((HashMap) sortRefs.get(2)).get("AchiShow"));

                map4.put("ShowName", ((HashMap) sortRefs.get(sortRefs.size() - 3)).get("cName"));
                map4.put("AchiShow", ((HashMap) sortRefs.get((sortRefs.size() - 3))).get("AchiShow"));

                map5.put("ShowName", ((HashMap) sortRefs.get(sortRefs.size() - 2)).get("cName"));
                map5.put("AchiShow", ((HashMap) sortRefs.get((sortRefs.size() - 2))).get("AchiShow"));

                map6.put("ShowName", ((HashMap) sortRefs.get(sortRefs.size() - 1)).get("cName"));
                map6.put("AchiShow", ((HashMap) sortRefs.get((sortRefs.size() - 1))).get("AchiShow"));


                StringUtil.sortByWhat(sortRefs, "AchiLeads");
                map1.put("LeadsName", ((HashMap) sortRefs.get(0)).get("cName"));
                map1.put("AchiLeads", ((HashMap) sortRefs.get(0)).get("AchiLeads"));

                map2.put("LeadsName", ((HashMap) sortRefs.get(1)).get("cName"));
                map2.put("AchiLeads", ((HashMap) sortRefs.get(1)).get("AchiLeads"));

                map3.put("LeadsName", ((HashMap) sortRefs.get(2)).get("cName"));
                map3.put("AchiLeads", ((HashMap) sortRefs.get(2)).get("AchiLeads"));

                map4.put("LeadsName", ((HashMap) sortRefs.get(sortRefs.size() - 3)).get("cName"));
                map4.put("AchiLeads", ((HashMap) sortRefs.get((sortRefs.size() - 3))).get("AchiLeads"));

                map5.put("LeadsName", ((HashMap) sortRefs.get(sortRefs.size() - 2)).get("cName"));
                map5.put("AchiLeads", ((HashMap) sortRefs.get((sortRefs.size() - 2))).get("AchiLeads"));

                map6.put("LeadsName", ((HashMap) sortRefs.get(sortRefs.size() - 1)).get("cName"));
                map6.put("AchiLeads", ((HashMap) sortRefs.get((sortRefs.size() - 1))).get("AchiLeads"));

                mapList.add(map1);
                mapList.add(map2);
                mapList.add(map3);
                mapList.add(map4);
                mapList.add(map5);
                mapList.add(map6);
            }
        }
        request.setAttribute("mapList", mapList);

        StringUtil.sortByWhatAsc(allRefS, "cName");
        request.setAttribute("FinanceMonths", FinanceMonths);
        request.setAttribute("refs", allRefS);

        return "ref/situation";
    }

    @RequestMapping("/statistics")
    @RequiresPermissions("view:statistics")
    public String statistics(HttpServletRequest request,
                             @RequestParam(value = "AutoID", required = false) String AutoID,
                             @RequestParam(value = "type", required = false) String type) {

        DecimalFormat df = new DecimalFormat("######0.00");
        String SaleModeIds = "A0D6D64E-8018-4766-82A3-1A41578A7F38";//REF渠道

        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        List refs = new ArrayList();

        Map refByCampus = new HashMap();

        List allRefS = new ArrayList();

        String sort = "AchiEn";

        if (("leads").equals(type)) {
            sort = "AchiLeads";
        } else if (("show").equals(type)) {
            sort = "AchiShow";
        }

        if (!StringUtil.isEmpty(AutoID)) {
            Map beginDateAndEndDate = campusDSRService.queryNaturalMonth(Integer.parseInt(AutoID));//财务月开始和结束时间

            String beninDate = beginDateAndEndDate.get("BeginDate").toString();
            String endDate = beginDateAndEndDate.get("EndDate").toString();

            Map map = new HashMap();
            map.put("startTime", beninDate);
            map.put("endTime", endDate);
            map.put("text", endDate.substring(0, 7));

            refs = refSituationService.queryRef(map);

            if (("en").equals(type) || type == null) {
                for (Object obj : refs) {
                    Map objMap = (HashMap) obj;
                    map.put("CampusID", objMap.get("cID"));
                    map.put("SaleModeIds", SaleModeIds);
                    refByCampus = refSituationService.queryEnByCampus(map);

                    BigDecimal kValue = new BigDecimal(objMap.get("kValue").toString());

                    if (StringUtil.divideValue(kValue) && refByCampus != null) {
                        objMap.put("AchiEn", Double.valueOf(df.format(new BigDecimal(refByCampus.get("Contracts").toString()).divide(kValue, 4, BigDecimal.ROUND_HALF_UP).doubleValue() * 100)));
                    } else {
                        objMap.put("AchiEn", 0.00);
                    }

                    allRefS.add(objMap);
                }
            } else {
                allRefS = refs;
            }
            request.setAttribute("AutoID", AutoID);
        } else {
            List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(DateUtil.dayMap());//当前月
            Map dateMap = (HashMap) FinanceMonthOfDq.get(0);

            String beninDate = dateMap.get("BeginDate").toString();
            String endDate = dateMap.get("EndDate").toString();

            Map map = new HashMap();
            map.put("startTime", beninDate);
            map.put("endTime", endDate);
            map.put("text", endDate.substring(0, 7));

            refs = refSituationService.queryRef(map);

            if (("en").equals(type) || type == null) {
                for (Object obj : refs) {
                    Map objMap = (HashMap) obj;
                    map.put("CampusID", objMap.get("cID"));
                    map.put("SaleModeIds", SaleModeIds);
                    refByCampus = refSituationService.queryEnByCampus(map);

                    BigDecimal kValue = new BigDecimal(objMap.get("kValue").toString());

                    if (StringUtil.divideValue(kValue) && refByCampus != null) {
                        objMap.put("AchiEn", Double.valueOf(df.format(new BigDecimal(refByCampus.get("Contracts").toString()).divide(kValue, 4, BigDecimal.ROUND_HALF_UP).doubleValue() * 100)));
                    } else {
                        objMap.put("AchiEn", 0.00);
                    }

                    allRefS.add(objMap);
                }
            } else {
                allRefS = refs;
            }
            request.setAttribute("AutoID", dateMap.get("AutoID"));
        }

        List cNames = new ArrayList();
        List values = new ArrayList();

        StringUtil.sortByWhat(allRefS, sort);

        if (allRefS.size() > 0) {
            for (Object obj : allRefS) {
                Map mapObj = (HashMap) obj;
                cNames.add("'" + mapObj.get("cName") + "'");
                values.add(mapObj.get(sort));
            }
        }

        request.setAttribute("cNames", cNames);
        request.setAttribute("values", values);
        request.setAttribute("type", sort);

        request.setAttribute("FinanceMonths", FinanceMonths);

        return "ref/situationStatistics";
    }

    public Map regionList(String region, List refs) {
        DecimalFormat df = new DecimalFormat("######0.00");

        Map regionMap = new HashMap();
        List regions = new ArrayList();

        BigDecimal sumLeads = new BigDecimal(0);
        BigDecimal sumShow = new BigDecimal(0);
        BigDecimal sumEn = new BigDecimal(0);

        BigDecimal sumTLeads = new BigDecimal(0);
        BigDecimal sumTShow = new BigDecimal(0);
        BigDecimal sumTEn = new BigDecimal(0);

        for (Object obj : refs) {
            Map refMap = (HashMap) obj;

            if (refMap.get("cTel").equals(region)) {
                regions.add(refMap);
                sumLeads = sumLeads.add(new BigDecimal(refMap.get("lcount").toString()));
                sumShow = sumShow.add(new BigDecimal(refMap.get("scount").toString()));
                sumEn = sumEn.add(new BigDecimal(refMap.get("ecount").toString()));

                sumTLeads = sumTLeads.add(new BigDecimal(refMap.get("Tleads").toString()));
                sumTShow = sumTShow.add(new BigDecimal(refMap.get("Tshow").toString()));
                sumTEn = sumTEn.add(new BigDecimal(refMap.get("kValue").toString()));
            }
        }
        regionMap.put("Leads", sumLeads);
        regionMap.put("Show", sumShow);
        regionMap.put("En", sumEn);
        regionMap.put("tLeads", sumTLeads);
        regionMap.put("tShow", sumTShow);
        regionMap.put("tEn", sumTEn);

        if (StringUtil.divideValue(sumTLeads)) {
            regionMap.put("RALeads", df.format(sumLeads.divide(sumTLeads, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        } else {
            regionMap.put("RALeads", 0.0);
        }

        if (StringUtil.divideValue(sumTShow)) {
            regionMap.put("RAShow", df.format(sumShow.divide(sumTShow, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        } else {
            regionMap.put("RAShow", 0.0);
        }

        if (StringUtil.divideValue(sumTEn)) {
            regionMap.put("RAEn", df.format(sumEn.divide(sumTEn, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
        } else {
            regionMap.put("RAEn", 0.0);
        }
        regionMap.put("regionName", region);
        regionMap.put("regions", regions);
        return regionMap;
    }
}
