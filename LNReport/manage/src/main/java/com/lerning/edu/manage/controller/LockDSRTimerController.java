package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.ChannelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/26
 */
@Component
@EnableScheduling
public class LockDSRTimerController {
    private static Logger log = LoggerFactory.getLogger(LockDSRTimerController.class);

    @Autowired
    private ChannelService channelService;
    @Autowired
    private CampusDSRService campusDSRService;

    /***
     * DSR Updated
     * 每天凌晨0点20分执行(财务月月报表)
     */
    @Scheduled(cron = "0 20 0 * * ?")
    public void dsrByCampus() {
        //获取前一天年月日
        String lastDate = DateUtil.getLastDateStr1();

        //根据 前一天年月 查询当前财务月开始可结束时间
        Map dqd = new HashMap();
        dqd.put("dqDay", lastDate.subSequence(0, 7));
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dqd);//当前月
        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);

        //前一天是否财务月最后一天
        String EenDate = dateMap.get("EndDate").toString().substring(0, 10);
        if (lastDate.equals(EenDate)) {
            //查询校区
            List departs = campusDSRService.queryDepartList();//校区

            //
            NumberFormat nf = new DecimalFormat("￥,###.##");

            //
            Map totalMap = new HashMap();
            BigDecimal emkt2018 = new BigDecimal("0.00");
            BigDecimal eref2018 = new BigDecimal("0.00");
            BigDecimal erenew2018 = new BigDecimal("0.00");
            BigDecimal etotal2018 = new BigDecimal("0.00");

            BigDecimal cmkt2018 = new BigDecimal("0.00");
            BigDecimal cref2018 = new BigDecimal("0.00");
            BigDecimal crenew2018 = new BigDecimal("0.00");
            BigDecimal ctotal2018 = new BigDecimal("0.00");

            BigDecimal emkt2017 = new BigDecimal("0.00");
            BigDecimal eref2017 = new BigDecimal("0.00");
            BigDecimal erenew2017 = new BigDecimal("0.00");
            BigDecimal etotal2017 = new BigDecimal("0.00");

            BigDecimal cmkt2017 = new BigDecimal("0.00");
            BigDecimal cerf2017 = new BigDecimal("0.00");
            BigDecimal crenew2017 = new BigDecimal("0.00");
            BigDecimal ctotal2017 = new BigDecimal("0.00");

            BigDecimal e18 = new BigDecimal("0.00");

            BigDecimal c18 = new BigDecimal("0.00");

            List channels = new ArrayList();

            //循环查询个校区数据,并存入数据库
            if (departs.size() > 0) {
                for (Object obj : departs) {
                    Map depart = (HashMap) obj;
                    dateMap.put("CampusID", depart.get("cID"));
                    dateMap.put("Grade", "");

                    dateMap.put("BeginDate", DateUtil.getYear() + "-01-01 00:00:00");
                    dateMap.put("EndDate", dateMap.get("EndDate").toString());

                    Map channel = channelService.queryAll(dateMap);

                    channel.put("text", lastDate.subSequence(0, 7));
                    channel.put("cCampusID", depart.get("cID"));

                    Map map2018 = new HashMap();
                    map2018.put("text", dateMap.get("Text").toString());
                    map2018.put("cAddress", depart.get("cAddress"));
                    Map odsReNew18 = channelService.queryByOdsReNew18(map2018);

                    if (odsReNew18 != null) {
                        e18 = new BigDecimal(odsReNew18.get("enrollmint").toString());
                        c18 = new BigDecimal(odsReNew18.get("cashin").toString());
                    }

                    emkt2018 = emkt2018.add(new BigDecimal(channel.get("mash").toString()));
                    eref2018 = eref2018.add(new BigDecimal(channel.get("fash").toString()));
                    erenew2018 = erenew2018.add(new BigDecimal(channel.get("tash").toString())).add(e18);
                    etotal2018 = etotal2018.add(new BigDecimal(channel.get("allash").toString())).add(e18);

                    channel.put("tashf", new BigDecimal(channel.get("tash").toString()).add(e18));
                    channel.put("allashf", new BigDecimal(channel.get("allash").toString()).add(e18));

                    cmkt2018 = cmkt2018.add(new BigDecimal(channel.get("mcost").toString()));
                    cref2018 = cref2018.add(new BigDecimal(channel.get("fcost").toString()));
                    crenew2018 = crenew2018.add(new BigDecimal(channel.get("tcost").toString())).add(c18);
                    ctotal2018 = ctotal2018.add(new BigDecimal(channel.get("allccost").toString())).add(c18);

                    dateMap.put("cAddress", depart.get("cAddress"));

                    //2018Cash In
                    channel.put("mcostf", nf.format(new BigDecimal(channel.get("mcost").toString())));
                    channel.put("fcostf", nf.format(new BigDecimal(channel.get("fcost").toString())));
                    channel.put("tcostf", nf.format(new BigDecimal(channel.get("tcost").toString()).add(c18)));
                    channel.put("allccostf", nf.format(new BigDecimal(channel.get("allccost").toString()).add(c18)));

                    Map map2017 = new HashMap();
                    map2017.put("text", dateMap.get("Text").toString().replace("2018", "2017"));
                    map2017.put("cAddress", dateMap.get("cAddress"));

                    Map odsChannle2017 = channelService.queryAllByChannel2017(map2017);

                    //校区简称
                    channel.put("cAddress", dateMap.get("cAddress"));
                    if (odsChannle2017 == null) {
                        //2017Enrollment
                        channel.put("emkt", 0);//mkt
                        channel.put("eref", 0);//ref
                        channel.put("erenew", 0);//renew
                        channel.put("etotal", 0);//total
                        //增长数量(2018Enrollment - 2017Enrollment)
                        channel.put("emktInCount", 0);//mkt
                        channel.put("erefInCount", 0);//ref
                        channel.put("erenewInCount", 0);//renew
                        channel.put("etotalInCount", 0);//total
                        //增长率(2018Enrollment - 2017Enrollment)/2017Enrollment
                        channel.put("emktInRate", 0);//mkt
                        channel.put("erefInRate", 0);//ref
                        channel.put("erenewInRate", 0);//renew
                        channel.put("etotalInRate", 0);//total

                        //2017Cash in
                        channel.put("cmkt", 0);//mkt
                        channel.put("cref", 0);//ref
                        channel.put("crenew", 0);//renew
                        channel.put("ctotal", 0);//total
                        //增长数量(2018Cash in - 2017Cash in)
                        channel.put("cmktCashInCount", 0);//mkt
                        channel.put("crefCashInCount", 0);//ref
                        channel.put("crenewCashInCount", 0);//renew
                        channel.put("ctotalCashInCount", 0);//total
                        //增长率(2018Cash in - 2017Cash in)/2017Cash in
                        channel.put("cmktCashInRate", 0);//mkt
                        channel.put("crefCashInRate", 0);//ref
                        channel.put("crenewCashInRate", 0);//renew
                        channel.put("ctotalCashInRate", 0);//total
                    } else {

                        emkt2017 = emkt2017.add(new BigDecimal(odsChannle2017.get("emkt").toString()));
                        eref2017 = eref2017.add(new BigDecimal(odsChannle2017.get("eref").toString()));
                        erenew2017 = erenew2017.add(new BigDecimal(odsChannle2017.get("erenew").toString()));
                        etotal2017 = etotal2017.add(new BigDecimal(odsChannle2017.get("etotal").toString()));

                        cmkt2017 = cmkt2017.add(new BigDecimal(odsChannle2017.get("cmkt").toString()));
                        cerf2017 = cerf2017.add(new BigDecimal(odsChannle2017.get("cref").toString()));
                        crenew2017 = crenew2017.add(new BigDecimal(odsChannle2017.get("crenew").toString()));
                        ctotal2017 = ctotal2017.add(new BigDecimal(odsChannle2017.get("ctotal").toString()));

                        //2017Enrollment
                        channel.put("emkt", odsChannle2017.get("emkt").toString());//mkt
                        channel.put("eref", odsChannle2017.get("eref").toString());//ref
                        channel.put("erenew", odsChannle2017.get("erenew").toString());//renew
                        channel.put("etotal", odsChannle2017.get("etotal").toString());//total

                        //增长数量(2018Enrollment - 2017Enrollment)
                        BigDecimal emktInCount = new BigDecimal(channel.get("mash").toString()).subtract(new BigDecimal(odsChannle2017.get("emkt").toString()));
                        channel.put("emktInCount", emktInCount);//mkt

                        BigDecimal erefInCount = new BigDecimal(channel.get("fash").toString()).subtract(new BigDecimal(odsChannle2017.get("eref").toString()));
                        channel.put("erefInCount", erefInCount);//ref

                        BigDecimal erenewInCount = new BigDecimal(channel.get("tash").toString()).subtract(new BigDecimal(odsChannle2017.get("erenew").toString())).add(e18);
                        channel.put("erenewInCount", erenewInCount);//renew

                        BigDecimal etotalInCount = new BigDecimal(channel.get("allash").toString()).subtract(new BigDecimal(odsChannle2017.get("etotal").toString())).add(e18);
                        channel.put("etotalInCount", etotalInCount);//total

                        //增长率(2018Enrollment - 2017Enrollment)/2017Enrollment
                        channel.put("emktInRate", divideValue(emktInCount, new BigDecimal(odsChannle2017.get("emkt").toString())));//mkt
                        channel.put("erefInRate", divideValue(erefInCount, new BigDecimal(odsChannle2017.get("eref").toString())));//ref
                        channel.put("erenewInRate", divideValue(erenewInCount, new BigDecimal(odsChannle2017.get("erenew").toString())));//renew
                        channel.put("etotalInRate", divideValue(etotalInCount, new BigDecimal(odsChannle2017.get("etotal").toString())));//total

                        //2017Cash in
                        channel.put("cmkt", nf.format(new BigDecimal(odsChannle2017.get("cmkt").toString())));//mkt
                        channel.put("cref", nf.format(new BigDecimal(odsChannle2017.get("cref").toString())));//ref
                        channel.put("crenew", nf.format(new BigDecimal(odsChannle2017.get("crenew").toString())));//renew
                        channel.put("ctotal", nf.format(new BigDecimal(odsChannle2017.get("ctotal").toString())));//total

                        //增长金额(2018Cash in - 2017Cash in)
                        BigDecimal cmktCashInCount = new BigDecimal(channel.get("mcost").toString()).subtract(new BigDecimal(odsChannle2017.get("cmkt").toString()));
                        channel.put("cmktCashInCount", nf.format(cmktCashInCount));//mkt

                        BigDecimal crefCashInCount = new BigDecimal(channel.get("fcost").toString()).subtract(new BigDecimal(odsChannle2017.get("cref").toString()));
                        channel.put("crefCashInCount", nf.format(crefCashInCount));//ref

                        BigDecimal crenewCashInCount = new BigDecimal(channel.get("tcost").toString()).subtract(new BigDecimal(odsChannle2017.get("crenew").toString())).add(c18);
                        channel.put("crenewCashInCount", nf.format(crenewCashInCount));//renew

                        BigDecimal ctotalCashInCount = new BigDecimal(channel.get("allccost").toString()).subtract(new BigDecimal(odsChannle2017.get("ctotal").toString())).add(c18);
                        channel.put("ctotalCashInCount", nf.format(ctotalCashInCount));//total

                        //增长率(2018Cash in - 2017Cash in)/2017Cash in
                        channel.put("cmktCashInRate", divideValue(cmktCashInCount, new BigDecimal(odsChannle2017.get("cmkt").toString())));//mkt
                        channel.put("crefCashInRate", divideValue(crefCashInCount, new BigDecimal(odsChannle2017.get("cref").toString())));//ref
                        channel.put("crenewCashInRate", divideValue(crenewCashInCount, new BigDecimal(odsChannle2017.get("crenew").toString())));//renew
                        channel.put("ctotalCashInRate", divideValue(ctotalCashInCount, new BigDecimal(odsChannle2017.get("ctotal").toString())));//total

                    }
                    channels.add(channel);
                    channelService.insertDsr(channel);
                }
            }

            //有问题,有空再调试
            //channelService.insertDsrByCampus(channels);

            totalMap.put("emkt2018", emkt2018);
            totalMap.put("eref2018", eref2018);
            totalMap.put("erenew2018", erenew2018);
            totalMap.put("etotal2018", etotal2018);

            totalMap.put("cmkt2018", nf.format(cmkt2018));
            totalMap.put("cref2018", nf.format(cref2018));
            totalMap.put("crenew2018", nf.format(crenew2018));
            totalMap.put("ctotal2018", nf.format(ctotal2018));

            totalMap.put("emkt2017", emkt2017);
            totalMap.put("eref2017", eref2017);
            totalMap.put("erenew2017", erenew2017);
            totalMap.put("etotal2017", etotal2017);

            totalMap.put("cmkt2017", nf.format(cmkt2017));
            totalMap.put("cerf2017", nf.format(cerf2017));
            totalMap.put("crenew2017", nf.format(crenew2017));
            totalMap.put("ctotal2017", nf.format(ctotal2017));

            //Enrollment 增长数据量
            BigDecimal emkttatolCount = emkt2018.subtract(emkt2017);
            totalMap.put("emkttatolCount", emkttatolCount);

            BigDecimal ereftatolCount = eref2018.subtract(eref2017);
            totalMap.put("ereftatolCount", ereftatolCount);

            BigDecimal erenewtatolCount = erenew2018.subtract(erenew2017);
            totalMap.put("erenewtatolCount", erenewtatolCount);

            BigDecimal ettatolCount = etotal2018.subtract(etotal2017);
            totalMap.put("ettatolCount", ettatolCount);

            //Enrollment 增长率
            totalMap.put("emkttatolrate", divideValue(emkttatolCount, emkt2017));
            totalMap.put("ereftatolrate", divideValue(ereftatolCount, eref2017));
            totalMap.put("erenewtatolrate", divideValue(erenewtatolCount, erenew2017));
            totalMap.put("ettatolrate", divideValue(ettatolCount, etotal2017));

            //Cash In 增长数量
            BigDecimal cmkttatolCount = cmkt2018.subtract(cmkt2017);
            totalMap.put("cmkttatolCount", nf.format(cmkttatolCount));

            BigDecimal creftatolCount = cref2018.subtract(cerf2017);
            totalMap.put("creftatolCount", nf.format(creftatolCount));

            BigDecimal crenewtatolCount = crenew2018.subtract(crenew2017);
            totalMap.put("crenewtatolCount", nf.format(crenewtatolCount));

            BigDecimal cttatolCount = ctotal2018.subtract(ctotal2017);
            totalMap.put("cttatolCount", nf.format(cttatolCount));

            //Cash In 增长率
            totalMap.put("cmkttatolrate", divideValue(cmkttatolCount, cmkt2017));
            totalMap.put("creftatolrate", divideValue(creftatolCount, cerf2017));
            totalMap.put("crenewtatolrate", divideValue(crenewtatolCount, crenew2017));
            totalMap.put("cttatolrate", divideValue(cttatolCount, ctotal2017));
            totalMap.put("text", lastDate.subSequence(0, 7));

            //
            channelService.insertDsrByCampusSum(totalMap);

            log.info("锁定成功!!!" + new Date());
        }
    }

    public BigDecimal divideValue(BigDecimal a, BigDecimal b) {
        double aa = a.byteValue();
        double bb = b.byteValue();
        if (bb == 0) {
            return new BigDecimal("0");
        } else if (aa > 0) {
            return a.divide(b, 2, RoundingMode.HALF_UP);
        } else {
            return a.divide(b, 2, RoundingMode.HALF_DOWN);
        }
    }
}
