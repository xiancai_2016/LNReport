package com.lerning.edu.manage.controller;

import com.alibaba.fastjson.JSONObject;
import com.lerning.edu.beans.Student;
import com.lerning.edu.beans.SysUser;
import com.lerning.edu.beans.WorkOrder;
import com.lerning.edu.beans.WorkOrderAuditRecord;
import com.lerning.edu.beans.page.PageList;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.manage.util.UserUtils;
import com.lerning.edu.services.StudentService;
import com.lerning.edu.services.WorkOrderAuditRecordService;
import com.lerning.edu.services.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/21
 */
@Controller
@RequestMapping("workOrderOfResume")
public class WorkOrderOfResume extends BaseController {

    @Autowired
    private WorkOrderService workOrderService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private WorkOrderAuditRecordService workOrderAuditRecordService;

    @RequestMapping("/apply")
    public String apply(HttpServletRequest request, String employeeId) {

        HttpSession session = request.getSession();

        if(session.getValue(employeeId) != null){
            String value = session.getValue(employeeId).toString();

            if(session.getValue(employeeId) != null && !("").equals(session.getValue(employeeId)) && (employeeId).equals(value)){
                request.setAttribute("isLogin", "N");
            }else{
                request.setAttribute("isLogin", "Y");
            }
        }else {
            request.setAttribute("isLogin", "Y");
        }

        List employees = new ArrayList();
        Map employee = new HashMap();

        if (StringUtil.isEmpty(employeeId)) {
            SysUser user = UserUtils.getUser();
            String account = user.getAccount();
            employees = workOrderService.queryEmployeeByCfield(account);
        } else {
            employees = workOrderService.queryEmployeeByUserID(employeeId);
        }

        if (employees.size() > 0) {
            employee = (HashMap)employees.get(0);
            if (!employee.get("cValue").equals("CC_SA")) {
                return "/WorkOrderResume/view";
            }
        } else {
            return "/WorkOrderResume/view";
        }

        request.setAttribute("tEmployeeID", employeeId);
        return "WorkOrderResume/apply";
    }

    @RequestMapping("/save")
    public String save(HttpServletRequest request,
                       @RequestParam(value = "tEmployeeID", required = false) String tEmployeeID,
                       @RequestParam(value = "userId", required = false) String userId,
                       @RequestParam(value = "classID", required = false) String classID,
                       @RequestParam(value = "creason", required = false) String cReason,
                       @RequestParam(value = "cstopclassdate", required = false) String cStopClassDate,
                       @RequestParam(value = "crecoverydate", required = false) String cRecoveryDate,
                       @RequestParam(value = "cremarks", required = false) String cRemarks,
                       @RequestParam(value = "resumeClassID", required = false) String resumeClassID) {

        WorkOrder workOrder = new WorkOrder();

        if (StringUtil.isEmpty(resumeClassID)) {
            resumeClassID = classID;
        }

        if(StringUtil.isEmpty(tEmployeeID)){
            SysUser user = UserUtils.getUser();
            List employees = workOrderService.queryEmployeeByCfield(user.getAccount());

            if (employees.size() > 0) {
                Map employee = (HashMap)employees.get(0);
                tEmployeeID = employee.get("cUserID").toString();
            }else{
                tEmployeeID = "3378BBC1-8313-4023-9D15-5C8C58BD66A6";
            }
        }

        workOrder.setCuserid(userId);
        workOrder.setCclassid(classID.substring(1,classID.length()));
        workOrder.setCstatus(1);
        workOrder.setCtype(2);//复课
        workOrder.setCreason(cReason);
        workOrder.setCstopclassdate(DateUtil.formateStringToDate(cStopClassDate));
        workOrder.setCrecoverydate(DateUtil.formateStringToDate(cRecoveryDate));
        workOrder.setCisoriginal(0);
        workOrder.setCremarks(cRemarks);
        workOrder.setCcreateuser(tEmployeeID);
        workOrder.setCisprint(2);
        workOrder.setCcreatedate(new Date());
        workOrder.setResumeClassID(resumeClassID);

        Map map = new HashMap();
        map.put("userId", userId);
        Map users = workOrderService.queryByUserId(map);
        workOrder.setCampus(users.get("cName").toString());

        map.put("campus", workOrder.getCampus());
        Map maxOrderNo = workOrderService.queryMaxOrderNo(map);

        String orderNo = DateUtil.getYear().substring(2, 4) + users.get("cAddress").toString();
        if (maxOrderNo == null) {
            workOrder.setCorderno(orderNo + "0001");
        } else {
            String oNo = maxOrderNo.get("cOrderNo").toString();
            workOrder.setCorderno(orderNo + StringUtil.autoGenericCode(oNo.substring(oNo.length() - 4, oNo.length()), 4));
        }

        workOrderService.add(workOrder);

        return "redirect:/workOrderInterrupt/listPage?employeeId="+tEmployeeID;
    }

    @RequestMapping("/findClass")
    @ResponseBody
    public JSONObject findClass(
            @RequestParam(value = "userId", required = false) String userId) {

        Map map = new HashMap();
        map.put("userId", userId);

        Map student = studentService.findStudentOne(map);
        List classes = studentService.findClassByStop(map);

        List classList = new ArrayList();

        if (classes.size() > 0) {
            for (Object obj : classes) {
                Map objMap = (HashMap) obj;


                classList.add(objMap);
            }
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", classList);
        jsonObject.put("student", student);

        return jsonObject;
    }

    //审核任务
    @RequestMapping("/taskList")
    public String list(HttpServletRequest request, WorkOrder workOrder) {
        request.setAttribute("workOrder", workOrder);
        return "/WorkOrderResume/taskList";
    }

    //审核任务
    @RequestMapping("/asynTaskList")
    @ResponseBody
    public PageList asynTaskList(PageList pageList, WorkOrder workOrder) {
        workOrder.setCstatus(1);
        workOrder.setCtype(2);//复课
        return workOrderService.queryPage(pageList, workOrder);
    }

    //操作任务
    @RequestMapping("/operationTaskList")
    public String operationTaskList(HttpServletRequest request, WorkOrder workOrder){
        request.setAttribute("workOrder",workOrder);
        return "/WorkOrderResume/operationTaskList";
    }

    //操作任务
    @RequestMapping("/asynOperationTaskList")
    @ResponseBody
    public PageList asynOperationTaskList(PageList pageList, WorkOrder workOrder) {
        workOrder.setCstatus(2);
        workOrder.setCtype(2);//复课
        return workOrderService.queryPage(pageList, workOrder);
    }

    //打印复课工单列表
    @RequestMapping("/printList")
    public String printList(HttpServletRequest request, WorkOrder workOrder){
        request.setAttribute("workOrder",workOrder);
        return "/WorkOrderResume/printList";
    }

    //打印复课工单列表
    @RequestMapping("/asnyPrintList")
    @ResponseBody
    public PageList asnPrintList(PageList pageList, WorkOrder workOrder) {
        workOrder.setCstatus(4);
        workOrder.setCtype(2);//复课
        return workOrderService.queryPage(pageList, workOrder);
    }

    @RequestMapping("/findClassToPage")
    @ResponseBody
    public JSONObject findClassToPage(
            @RequestParam(value = "classID", required = false) String classID,
            @RequestParam(value = "userId", required = false) String userId,
            @RequestParam(value = "newClassID", required = false) String newClassID) {

        Map map = new HashMap();
        map.put("classID", classID);
        map.put("userId", userId);
        if (StringUtil.isEmpty(newClassID)) {
            newClassID = classID.replace(",", "");
        }
        map.put("newClassID", newClassID);

        List tClass = studentService.findClassByResume(map);

        Map objMap = (HashMap) tClass.get(0);
        String cTime = objMap.get("cCreateTime").toString();
        objMap.put("sDate", DateUtil.dateToWeek(cTime) + " " + cTime.substring(11, 16));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("tClass", objMap);

        return jsonObject;
    }

    @RequestMapping("/findResumeClass")
    @ResponseBody
    public JSONObject findResumeClass(
            @RequestParam(value = "userId", required = false) String userId,
            @RequestParam(value = "className", required = false) String className,
            @RequestParam(value = "campusID", required = false) String campusID,
            @RequestParam(value = "page", required = false) int page) {

        Map map = new HashMap();
        map.put("userId", userId);
        map.put("Start", page * 10);
        map.put("End", page * 10 + 10);

        if(!StringUtil.isEmpty(className)){
            map.put("className", className.trim());
        }else {
            map.put("className", className);
        }

        if(StringUtil.isEmpty(userId)){
            map.put("CampusID", campusID);
        }else {
            Student student = studentService.queryByUserId(userId);
            map.put("CampusID", student.getCcampusid());
        }

        List classList = new ArrayList();
        List classes = studentService.findClassByCampus(map);

        if (classes.size() > 0) {
            for (Object obj : classes) {
                Map objMap = (HashMap) obj;

                List resumeList = studentService.queryResumeList(objMap);

                if (resumeList.size() > 1) {
                    objMap.put("sDate", "");
                } else if (resumeList.size() > 0) {
                    Map rMap = (HashMap) resumeList.get(0);

                    String week = "周日";

                    if (rMap.get("cWeekday").toString().equals("1")) {
                        week = "周一";
                    } else if (rMap.get("cWeekday").toString().equals("2")) {
                        week = "周二";
                    } else if (rMap.get("cWeekday").toString().equals("3")) {
                        week = "周三";
                    } else if (rMap.get("cWeekday").toString().equals("4")) {
                        week = "周四";
                    } else if (rMap.get("cWeekday").toString().equals("5")) {
                        week = "周五";
                    } else if (rMap.get("cWeekday").toString().equals("6")) {
                        week = "周六";
                    }
                    if (objMap.get("cStartTime").toString().equals("1900-01-01 00:00:00.0")) {
                        objMap.put("sDate", "未排课");
                        objMap.put("onClassTime", "未排课");
                    } else {
                        objMap.put("sDate", week + objMap.get("cStartTime").toString().substring(11, 16) + "-" + objMap.get("cEndTime").toString().substring(11, 16));
                        objMap.put("onClassTime", objMap.get("cStartTime").toString().substring(0, 10));
                    }
                } else {
                    objMap.put("sDate", "");
                }

                classList.add(objMap);
            }
        }

        int classCount = studentService.findClassByCampusCount(map);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", classList);
        double sc = (double) classCount - 10;
        sc = sc / 10;
        classCount = (int) Math.ceil(sc);
        jsonObject.put("count", classCount);//向上取整

        return jsonObject;
    }

    @RequestMapping("/detail")
    public String detail(HttpServletRequest request, String orderNo, String userID, String flag) {

            String returnValue = "WorkOrderResume/detail";

        Map map = new HashMap();
        map.put("orderNo", orderNo);
        Map workOrderDetail = workOrderService.queryDetail(map);

        if (workOrderDetail == null) {
            returnValue = "WorkOrderResume/auditPage";
            workOrderDetail = workOrderService.queryDetail1(map);
        } else if (workOrderDetail.get("cStatus").toString().equals("2")) {
            returnValue = "WorkOrderResume/operation";
        } else if (workOrderDetail.get("cStatus").toString().equals("4")) {
            returnValue = "WorkOrderResume/printPage";
        }

        map.put("userID", workOrderDetail.get("cUserID"));
        map.put("userId", workOrderDetail.get("cUserID"));

        Map student = studentService.findStudentOne(map);

        String cID = workOrderDetail.get("cClassID").toString();
        String newCID = workOrderDetail.get("resumeClassID").toString();

        if(StringUtil.isEmpty(newCID)){
            newCID = cID;
        }

        map.put("classID", cID.trim());
        if(newCID.substring(0,1).equals(",")){
            newCID = newCID.substring(1,newCID.length());
        }
        map.put("newClassID", newCID.trim());

        //查询详情(班级)
        List myClass = studentService.findClassByResume(map);

        WorkOrderAuditRecord woar = new WorkOrderAuditRecord();
        woar.setCworkorderid(workOrderDetail.get("cID").toString());
        List record = workOrderAuditRecordService.queryList(woar);

        request.setAttribute("workOrderDetail", workOrderDetail);
        request.setAttribute("student", student);
        request.setAttribute("classes", myClass);

        request.setAttribute("record", record);

        request.setAttribute("flag", flag);
        request.setAttribute("userID", userID);

        return returnValue;
    }
}
