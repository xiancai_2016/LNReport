package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.UntrustedService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/3
 */
@Controller
@RequestMapping("untreatedApp")
public class UntreatedAppController {

    @Autowired
    private UntrustedService untrustedService;

    @RequestMapping("/list")
    @RequiresPermissions("view:untreatedApp")
    public String appPage(HttpServletRequest request,
                          @RequestParam(value = "startTime", required = false) String startTime,
                          @RequestParam(value = "endTime", required = false) String endTime,
                          @RequestParam(value = "campusID", required = false) String campusID,
                          @RequestParam(value = "saleType", required = false) String saleType) {
        Integer totalCount = 0;

        Map map = new HashMap();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        map.put("saleType", saleType);
        map.put("campusID", campusID);

        List campusAndCount = untrustedService.queryCampusAndCount(map);
        List detail = new ArrayList();
        if (!StringUtil.isEmpty(campusID)) {
            detail = untrustedService.queryDetail(map);
        }

        if(campusAndCount.size()>0){
            for (Object obj : campusAndCount){
                Map objMap = (HashMap)obj;
                totalCount = totalCount+Integer.parseInt(objMap.get("totalCount").toString());
            }
        }

        request.setAttribute("startTime", startTime);
        request.setAttribute("endTime", endTime);
        request.setAttribute("campusID", campusID);
        request.setAttribute("saleType", saleType);
        request.setAttribute("campusAndCount", campusAndCount);
        request.setAttribute("detail", detail);
        request.setAttribute("totalCount",totalCount);

        return "app/untreated";
    }

    @RequestMapping("/exportApp")
    public void exportApp(HttpServletResponse response,
                          @RequestParam(value = "exportStartTime", required = false) String exportStartTime,
                          @RequestParam(value = "exportEndTime", required = false) String exportEndTime,
                          @RequestParam(value = "exportSaleType", required = false) String exportSaleType,
                          @RequestParam(value = "exportCampus", required = false) String exportCampus) throws Exception {

        Map map = new HashMap();
        map.put("startTime", exportStartTime);
        map.put("endTime", exportEndTime);
        map.put("saleType", exportSaleType);
        map.put("campusID", exportCampus);

        HSSFWorkbook workbook = new HSSFWorkbook();
        // 生成一个表格
        HSSFSheet sheet = workbook.createSheet("未完成APP");
        // 设置表格默认列宽度为15个字节
        sheet.setDefaultColumnWidth((short) 15);
        // 产生表格标题行
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell(0);
        HSSFCell cell1 = row.createCell(1);
        HSSFCell cell2 = row.createCell(2);
        HSSFCell cell3 = row.createCell(3);
        HSSFCell cell4 = row.createCell(4);
        HSSFCell cell5 = row.createCell(5);
        HSSFCell cell6 = row.createCell(6);
        HSSFCell cell7 = row.createCell(7);
        HSSFCell cell8 = row.createCell(8);

        cell.setCellValue("校区");
        cell1.setCellValue("姓名");
        cell2.setCellValue("手机号");
        cell3.setCellValue("渠道");
        cell4.setCellValue("主责任人");
        cell5.setCellValue("最后沟通内容");
        cell6.setCellValue("线索产生人");
        cell7.setCellValue("市场邀约时间");
        cell8.setCellValue("线索所有者");

        String cName = "";

        List detail = untrustedService.queryDetail(map);

        if (detail.size() > 0) {
            for (int i = 0; i < detail.size(); i++) {
                HSSFRow rowValue = sheet.createRow(1 + i);
                HSSFCell cellDate = rowValue.createCell(0);
                HSSFCell cellDate1 = rowValue.createCell(1);
                HSSFCell cellDate2 = rowValue.createCell(2);
                HSSFCell cellDate3 = rowValue.createCell(3);
                HSSFCell cellDate4 = rowValue.createCell(4);
                HSSFCell cellDate5 = rowValue.createCell(5);
                HSSFCell cellDate6 = rowValue.createCell(6);
                HSSFCell cellDate7 = rowValue.createCell(7);
                HSSFCell cellDate8 = rowValue.createCell(8);

                Map appMap = (HashMap) detail.get(i);
                cName = appMap.get("campus").toString();

                cellDate.setCellValue(appMap.get("campus").toString());
                cellDate1.setCellValue(appMap.get("name").toString());
                cellDate2.setCellValue(appMap.get("cSMSTel").toString());
                cellDate3.setCellValue(appMap.get("sale").toString());
                cellDate4.setCellValue(appMap.get("cName").toString());
                cellDate5.setCellValue(appMap.get("cContent").toString());
                cellDate6.setCellValue(appMap.get("cField2").toString());
                cellDate7.setCellValue(appMap.get("cField6").toString());
                cellDate8.setCellValue(appMap.get("cField5").toString());
            }
        }

        if(StringUtil.isEmpty(exportCampus)){
            cName = "";
        }else{
            cName = cName+"-";
        }

        ServletOutputStream outputStream = null;// 创建一个输出流对象
        try {
            response.setContentType("application/binary;charset=utf-8");
            String headerStr = new String((cName + exportSaleType + "渠道").getBytes("utf-8"), "ISO8859-1");// headerString为中文时转码
            response.setHeader("Content-disposition", "attachment; filename=" + headerStr + ".xls");// filename是下载的xls的名
            outputStream = response.getOutputStream();
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
