package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.ExceptionRenewService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/13
 */
@Controller
@RequestMapping("exceptionRenew")
public class ExceptionRenewController extends BaseController {
    @Autowired
    private ExceptionRenewService exceptionRenewService;
    @Autowired
    private CampusDSRService campusDSRService;

    @RequestMapping("/renew")
    @RequiresPermissions("view:exception:renew")
    public String exceptionClass(HttpServletRequest request,
                                 @RequestParam(value = "CampusID", required = false) String CampusID,
                                 @RequestParam(value = "AutoID", required = false) String AutoID) {

        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表

        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(DateUtil.dayMap());//当前月
        Map dateMap = new HashMap();

        List exceptionRenew = new ArrayList();

        int exceptionCount = 0;

        List departs = campusDSRService.queryDepartList();//校区

        if(!StringUtil.isEmpty(AutoID)){
            dateMap = campusDSRService.queryNaturalMonth(Integer.parseInt(AutoID));//财务月开始和结束时间
            request.setAttribute("AutoID", AutoID);

            Map map = new HashMap();
            map.put("BeginDate", dateMap.get("BeginDate").toString().substring(0,10));
            map.put("EndDate", dateMap.get("EndDate").toString().substring(0,10));

            map.put("CampusID", CampusID);
            exceptionRenew = exceptionRenewService.queryExceptionRenew(map);

            exceptionCount = exceptionRenewService.queryExceptionRenewCount(map);

        }else {
            dateMap = (HashMap) FinanceMonthOfDq.get(0);
            request.setAttribute("AutoID", dateMap.get("AutoID"));
        }


        request.setAttribute("CampusID", CampusID);
        request.setAttribute("departs", departs);

        request.setAttribute("exceptionRenew", exceptionRenew);

        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("exceptionCount", exceptionCount);

        return "exception/renew";
    }

    /***
     * 导出
     *
     * @param response
     * @throws Exception
     */
    @RequestMapping("/exportInfo")
    public void exportInfo(HttpServletResponse response,
                           @RequestParam(value = "CampusID", required = false) String CampusID,
                           @RequestParam(value = "AutoID", required = false) String AutoID) throws Exception {

        Map map = new HashMap();
        map.put("CampusID", CampusID);

        Map dateMap = campusDSRService.queryNaturalMonth(Integer.parseInt(AutoID));//财务月开始和结束时间

        map.put("BeginDate", dateMap.get("BeginDate").toString().substring(0,10));
        map.put("EndDate", dateMap.get("EndDate").toString().substring(0,10));

        String campsName = "";

        HSSFWorkbook workbook = new HSSFWorkbook();
        // 生成一个表格
        HSSFSheet sheet = workbook.createSheet("异常续费");
        // 设置表格默认列宽度为15个字节
        sheet.setDefaultColumnWidth((short) 15);
        // 产生表格标题行
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell(0);
        HSSFCell cell1 = row.createCell(1);
        HSSFCell cell2 = row.createCell(2);
        HSSFCell cell3 = row.createCell(3);
        HSSFCell cell4 = row.createCell(4);
        HSSFCell cell5 = row.createCell(5);
        HSSFCell cell6 = row.createCell(6);
        HSSFCell cell7 = row.createCell(7);
        HSSFCell cell8 = row.createCell(8);
        HSSFCell cell9 = row.createCell(9);

        cell.setCellValue("校区");
        cell1.setCellValue("CC/AS姓名");
        cell2.setCellValue("CC/AS工号");
        cell3.setCellValue("合同编号");
        cell4.setCellValue("缴费日期");
        cell5.setCellValue("业绩归属日期");
        cell6.setCellValue("学生姓名");
        cell7.setCellValue("学生电话");
        cell8.setCellValue("当前班级名");
        cell9.setCellValue("排课结算日");

        List exceptionRenew = exceptionRenewService.queryExceptionRenew(map);

        if (exceptionRenew.size() > 0) {
            for (int i = 0; i < exceptionRenew.size(); i++) {
                HSSFRow rowValue = sheet.createRow(1 + i);
                HSSFCell cellDate = rowValue.createCell(0);
                HSSFCell cellDate1 = rowValue.createCell(1);
                HSSFCell cellDate2 = rowValue.createCell(2);
                HSSFCell cellDate3 = rowValue.createCell(3);
                HSSFCell cellDate4 = rowValue.createCell(4);
                HSSFCell cellDate5 = rowValue.createCell(5);
                HSSFCell cellDate6 = rowValue.createCell(6);
                HSSFCell cellDate7 = rowValue.createCell(7);
                HSSFCell cellDate8 = rowValue.createCell(8);
                HSSFCell cellDate9 = rowValue.createCell(9);

                Map appMap = (HashMap) exceptionRenew.get(i);
                campsName = appMap.get("campusName").toString();

                cellDate.setCellValue(appMap.get("campusName").toString());
                cellDate1.setCellValue(appMap.get("cName").toString());
                cellDate2.setCellValue(appMap.get("cField1").toString());
                cellDate3.setCellValue(appMap.get("cReceiptNo").toString());
                cellDate4.setCellValue(appMap.get("cCreateTime").toString());
                cellDate5.setCellValue(appMap.get("cEffectDate").toString());
                cellDate6.setCellValue(appMap.get("studentUser").toString());
                cellDate7.setCellValue(appMap.get("cSMSTel").toString());
                cellDate8.setCellValue(appMap.get("className").toString());
                cellDate9.setCellValue(appMap.get("endTime").toString());
            }
        }

        ServletOutputStream outputStream = null;// 创建一个输出流对象
        try {
            response.setContentType("application/binary;charset=utf-8");
            String headerStr = new String((campsName + "-" +  dateMap.get("EndDate").toString().substring(0,7)).getBytes("utf-8"), "ISO8859-1");// headerString为中文时转码
            response.setHeader("Content-disposition", "attachment; filename=" + headerStr + ".xls");// filename是下载的xls的名
            outputStream = response.getOutputStream();
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
