package com.lerning.edu.manage.WeChatReport;

import com.lerning.edu.services.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/26
 */
@Controller
@RequestMapping("wechat")
public class DSRChannel {

    @Autowired
    private ChannelService channelService;

    @RequestMapping("/dsrChannel")
    public String dsrChannel(HttpServletRequest request,
                             @RequestParam(value = "dateTime", required = false) String dateTime) {

        Map dateMap = new HashMap();
        dateMap.put("dqDay", dateTime.subSequence(0, 7));
        //锁定数据
        List channels = channelService.queryChannelsLockData(dateMap);
        List channelsSum = channelService.queryChannelsLockDataSum(dateMap);
        Map totalMap = (HashMap) channelsSum.get(0);

        request.setAttribute("channels", channels);
        request.setAttribute("totalMap", totalMap);
        request.setAttribute("dateTime", dateTime);

        request.setAttribute("reportInfo", "DSR Updated");
        request.setAttribute("date", "截止到" + dateTime);

        return "wechar/channel";
    }
}
