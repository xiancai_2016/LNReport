package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRByJMSService;
import com.lerning.edu.services.CampusDSRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/11/1
 */
@Controller
@RequestMapping("oldOds")
public class OldJMSDSRByUserController {

    @Autowired
    private CampusDSRService campusDSRService;
    @Autowired
    private CampusDSRByJMSService campusDSRByJMSService;

    CampusDsrNewController cnc = new CampusDsrNewController();

    @RequestMapping("/dsrByUserByJMS")
    public String dsrByUserByJMS(HttpServletRequest request,
                                 @RequestParam(value = "CampusID", required = false) String CampusID,
                                 @RequestParam(value = "companyID", required = false) String companyID,
                                 @RequestParam(value = "AutoID", required = false) String AutoID,
                                 @RequestParam(value = "key", required = false) String key) {


        if (StringUtil.isEmpty(key)) {
            return "oldods";
        } else {
            key = URLDecoder.decode(key);
            double minute = DateUtil.getDistanceOfTwoMinute(DateUtil.formateStringToDate(key.replace("/","-"), Constants.DATE_FORMART2), new Date());
            if (minute > 15 || minute < 0) {
                return "oldods";
            }
        }

        Map campusMap = new HashMap();
        campusMap.put("CampusID", CampusID);
        campusMap.put("companyID", companyID);

        List saleMode = campusDSRByJMSService.querySaleModeByCampus(campusMap);

        String TIN = "";
        String TOUT = "";
        String WalkIN = "";
        String REF = "";

        if (saleMode.size() > 0) {
            for (Object obj : saleMode) {
                Map objMap = (HashMap) obj;
                if (objMap.get("cName").equals("Tin")) {
                    TIN = objMap.get("cID").toString();
                } else if (objMap.get("cName").equals("T-out")) {
                    TOUT = objMap.get("cID").toString();
                } else if (objMap.get("cName").equals("Ref")) {
                    REF = objMap.get("cID").toString();
                } else if (objMap.get("cName").equals("Walk-in")) {
                    if (!StringUtil.isEmpty(WalkIN)) {
                        WalkIN = WalkIN + "," + objMap.get("cID").toString();
                    } else {
                        WalkIN = objMap.get("cID").toString();

                    }
                }
            }
        }

        String BeginDate = "";
        String EndDate = "";

        List tinList = new ArrayList();
        List toutList = new ArrayList();
        List winList = new ArrayList();
        List refList = new ArrayList();

        List tinByUser = new ArrayList();
        List toutByUser = new ArrayList();
        List winByUser = new ArrayList();
        List refByUser = new ArrayList();

        List tinByUserFPShow = new ArrayList();
        List tinByUserOCShow = new ArrayList();

        List toutByUserFPShow = new ArrayList();
        List toutByUserOCShow = new ArrayList();

        List winByUserFPShow = new ArrayList();
        List winByUserOCShow = new ArrayList();

        List refByUserFPShow = new ArrayList();
        List refByUserOCShow = new ArrayList();

        List tinEnByUser = new ArrayList();
        List toutEnByUser = new ArrayList();
        List winEnByUser = new ArrayList();
        List refEnByUser = new ArrayList();

        List enByUserSummary = new ArrayList();

        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表

        if (StringUtil.isEmpty(AutoID)) {
            List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(DateUtil.dayMap());//当前月

            Map dateMap = (HashMap) FinanceMonthOfDq.get(0);
            AutoID = dateMap.get("AutoID").toString();
        } else {
            Map beginDateAndEndDate = campusDSRService.queryNaturalMonth(Integer.parseInt(AutoID));//财务月开始和结束时间

            BeginDate = beginDateAndEndDate.get("BeginDate").toString().substring(0, 10);
            EndDate = beginDateAndEndDate.get("EndDate").toString().substring(0, 10);
            Map map = new HashMap();
            map.put("CampusID", CampusID);
            map.put("BeginDate", BeginDate);
            map.put("EndDate", EndDate);

            map.put("FBeginDate", beginDateAndEndDate.get("BeginDate").toString());
            map.put("FEndDate", beginDateAndEndDate.get("EndDate").toString());
            map.put("companyID", companyID);

            map.put("SaleModeIds", TIN);
            tinByUser = campusDSRByJMSService.queryByUserForApp(map);
            tinByUserFPShow = campusDSRByJMSService.queryByUserOfFPShowByTinTout(map);
            tinByUserOCShow = campusDSRByJMSService.queryByUserOfOCShowByTinTout(map);

            if (Integer.valueOf(AutoID) > 8) {
                tinEnByUser = campusDSRByJMSService.queryEnByUserNew(map);
            } else {
                tinEnByUser = campusDSRByJMSService.queryEnByUser(map);
            }

            map.put("SaleModeIds", TOUT);
            toutByUser = campusDSRByJMSService.queryByUserForApp(map);
            toutByUserFPShow = campusDSRByJMSService.queryByUserOfFPShowByTinTout(map);
            toutByUserOCShow = campusDSRByJMSService.queryByUserOfOCShowByTinTout(map);

            if (Integer.valueOf(AutoID) > 8) {
                toutEnByUser = campusDSRByJMSService.queryEnByUserNew(map);
            } else {
                toutEnByUser = campusDSRByJMSService.queryEnByUser(map);
            }

            map.put("SaleModeIds", WalkIN);
            winByUser = campusDSRByJMSService.queryByUser(map);
            winByUserFPShow = campusDSRByJMSService.queryByUserOfFPShowByWalkInRef(map);
            winByUserOCShow = campusDSRByJMSService.queryByUserOfOCShowByWalkInRef(map);

            if (Integer.valueOf(AutoID) > 8) {
                winEnByUser = campusDSRByJMSService.queryEnByUserNew(map);
            } else {
                winEnByUser = campusDSRByJMSService.queryEnByUser(map);
            }

            map.put("SaleModeIds", REF);
            refByUser = campusDSRByJMSService.queryByUser(map);
            refByUserFPShow = campusDSRByJMSService.queryByUserOfFPShowByWalkInRef(map);
            refByUserOCShow = campusDSRByJMSService.queryByUserOfOCShowByWalkInRef(map);

            if (Integer.valueOf(AutoID) > 8) {
                refEnByUser = campusDSRByJMSService.queryEnByUserNew(map);
            } else {
                refEnByUser = campusDSRByJMSService.queryEnByUser(map);
            }

            //
            Map ebsMap = new HashMap();
            ebsMap.put("BeginDate", beginDateAndEndDate.get("BeginDate").toString());
            ebsMap.put("EndDate", beginDateAndEndDate.get("EndDate").toString());
            ebsMap.put("CampusID", CampusID);
            ebsMap.put("text", EndDate.substring(0, 7));
            ebsMap.put("companyID", companyID);


            if (Integer.valueOf(AutoID) > 8) {
                enByUserSummary = campusDSRByJMSService.queryEnByUserSummaryNew(ebsMap);
            } else {
                enByUserSummary = campusDSRByJMSService.queryEnByUserSummary(ebsMap);
            }

            List ebs = new ArrayList();
            if (enByUserSummary.size() > 0) {
                for (Object obj : enByUserSummary) {
                    Map objMap = (HashMap) obj;
                    double totalEn = Double.valueOf(objMap.get("totalEn").toString());
                    if (totalEn > 0) {
                        ebs.add(objMap);
                    }
                }
            }

            enByUserSummary = ebs;

            //合并app ,FreshPresShow,OCShow和En
            tinList = getCampusOfUserJMS(tinByUser, CampusID, TIN, BeginDate, EndDate, "app", tinByUserFPShow, tinByUserOCShow, tinEnByUser);
            toutList = getCampusOfUserJMS(toutByUser, CampusID, TOUT, BeginDate, EndDate, "app", toutByUserFPShow, toutByUserOCShow, toutEnByUser);
            winList = getCampusOfUserJMS(winByUser, CampusID, WalkIN, BeginDate, EndDate, "leads", winByUserFPShow, winByUserOCShow, winEnByUser);
            refList = getCampusOfUserJMS(refByUser, CampusID, REF, BeginDate, EndDate, "leads", refByUserFPShow, refByUserOCShow, refEnByUser);
        }

        request.setAttribute("AutoID", AutoID);
        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("CampusID", CampusID);
        request.setAttribute("companyID", companyID);

        StringUtil.sortByWhatAsc(tinList, "cField1");
        StringUtil.sortByWhatAsc(toutList, "cField1");
        StringUtil.sortByWhatAsc(winList, "cField1");
        StringUtil.sortByWhatAsc(refList, "cField1");

        request.setAttribute("tinByUser", cnc.ratio(tinList));
        request.setAttribute("toutByUser", cnc.ratio(toutList));
        request.setAttribute("winByUser", cnc.ratio(winList));
        request.setAttribute("refByUser", cnc.ratio(refList));
        request.setAttribute("renewByUser", refEnByUser);

        List ebsList = new ArrayList();
        if (refEnByUser.size() > 0) {
            for (Object obj : refEnByUser) {
                Map ebsMap = (HashMap) obj;
                ebsMap.put("CashIn", Double.valueOf(ebsMap.get("TCashIn").toString()));
                ebsMap.put("CashIn1", cnc.nf.format(Double.valueOf(ebsMap.get("TCashIn").toString())));
                ebsMap.put("CashIn2", cnc.nf.format(Double.valueOf(ebsMap.get("TCashIn2").toString())));
                ebsList.add(ebsMap);
            }
        }

        refEnByUser = ebsList;

        request.setAttribute("enByUserSummary", enByUserSummary);

        request.setAttribute("appSumTIN", cnc.sumJMS(tinList, CampusID, TIN, BeginDate, EndDate, "app"));
        request.setAttribute("appSumTOUT", cnc.sumJMS(toutList, CampusID, TOUT, BeginDate, EndDate, "app"));
        request.setAttribute("appSumWKIN", cnc.sumJMS(winList, CampusID, WalkIN, BeginDate, EndDate, "leads"));
        request.setAttribute("appSumRef", cnc.sumJMS(refList, CampusID, REF, BeginDate, EndDate, "leads"));
        request.setAttribute("renewByUserTotal", cnc.renewByUserTotal(refEnByUser));

        request.setAttribute("ebsTotal", cnc.ebsTotal(enByUserSummary));

        request.setAttribute("key", key);

        return "oldOds/byUserByJMS";
    }

    public List getCampusOfUserJMS(List list, String cID, String SaleModeIds, String BeginDate, String EndDate, String type, List fpShow, List ocShow, List enByUser) {
        List returnList = new ArrayList();

        List fpList = new ArrayList();
        List ocList = new ArrayList();

        if (list.size() > 0) {
            for (Object obj : list) {
                Map map = (HashMap) obj;

                map.put("CashInValue", cnc.nf.format(map.get("CashIn")));
                map.put("oc1", map.get("oc"));
                map.put("Contracts1", map.get("Contracts"));

                BigDecimal fp = new BigDecimal(map.get("fp").toString());
                BigDecimal app = new BigDecimal(map.get("app").toString());

                String CampusID = map.get("cLevelString").toString();
                CampusID = CampusID.replace("|", "");

                if (!StringUtil.isEmpty(CampusID)) {
                    Map tDepart = campusDSRByJMSService.queryCampus(CampusID);
                    map.put("userCampus", tDepart.get("cName"));
                    map.put("userCampusID", tDepart.get("cID"));

                    if (!StringUtil.divideValue(app)) {
                        map.put("ShowRatio", 0);
                    } else {
                        map.put("ShowRatio", cnc.df.format(fp.divide(app, 2, BigDecimal.ROUND_HALF_UP).doubleValue() * 100));
                    }

                    if (cID.equals(CampusID)) {
                        returnList.add(map);
                        fpList.add(map);
                    } else {
                        Map countMap = new HashMap();
                        countMap.put("cField1", map.get("cField1"));
                        countMap.put("CampusID", cID);
                        countMap.put("SaleModeIds", SaleModeIds);
                        countMap.put("BeginDate", BeginDate);
                        countMap.put("EndDate", EndDate);

                        int countApp = 0;

                        if (type.equals("app")) {
                            countApp = campusDSRByJMSService.queryByUserForAppCount(countMap);
                        } else {
                            countApp = campusDSRByJMSService.queryByUserForLeadsCount(countMap);
                        }

                        int customerOfCampusCount = campusDSRByJMSService.queryCustomerOfCampusCount(countMap);

                        if (countApp > 0 && customerOfCampusCount == 1) {
                            returnList.add(map);
                            fpList.add(map);
                        }
                    }
                }

                //不在app里的员工,并且fp show不为0 (FP)
                if (fpShow.size() > 0) {
                    String cName = map.get("cField1").toString();//app
                    for (Object objApp : fpShow) {
                        Map mapApp = (HashMap) objApp;
                        String cNameFP = mapApp.get("cField1").toString();//pfShow
                        String fpCampusID = mapApp.get("cLevelString").toString();
                        fpCampusID = fpCampusID.replace("|", "");

                        int fpShowCount = (int) mapApp.get("fp");

                        if (!cName.equals(cNameFP) && cID.equals(fpCampusID) && fpShowCount > 0) {
                            mapApp.put("app", 0);
                            mapApp.put("oc", 0);
                            mapApp.put("oc1", 0);
                            mapApp.put("Contracts1", 0);
                            mapApp.put("Contracts", 0.00);
                            mapApp.put("CashIn", 0.00);
                            mapApp.put("CashInValue", cnc.nf.format(0));
                            mapApp.put("ShowRatio", 0);
                            returnList.add(mapApp);
                            fpList.add(mapApp);
                        }
                    }
                }
            }

            if (fpList.size() == 0) {
                fpList = list;
            }
            //不在app里的员工,并且OC show不为0 (OC)
            if (fpList.size() > 0 && ocShow.size() > 0) {
                for (Object objFP : fpList) {
                    Map fpMap = (HashMap) objFP;
                    String cName = fpMap.get("cField1").toString();//fp
                    for (Object objOC : ocShow) {
                        Map mapOC = (HashMap) objOC;
                        String cNameFP = mapOC.get("cField1").toString();//pfShow

                        String ocCampusID = mapOC.get("cLevelString").toString();
                        ocCampusID = ocCampusID.replace("|", "");

                        int oc = (int) mapOC.get("oc");

                        if (!cName.equals(cNameFP) && cID.equals(ocCampusID) && oc > 0) {
                            mapOC.put("app", 0);
                            mapOC.put("fp", 0);
                            mapOC.put("oc1", oc);
                            mapOC.put("Contracts1", 0);
                            mapOC.put("Contracts", 0.00);
                            mapOC.put("CashIn", 0.00);
                            mapOC.put("CashInValue", cnc.nf.format(0));
                            mapOC.put("ShowRatio", 0);
                            returnList.add(mapOC);
                            ocList.add(mapOC);
                        }
                    }
                }
            }

            if (ocList.size() == 0) {
                ocList = fpList;
            }
            //不在app里的员工,并且有En (En)
            if (ocList.size() > 0) {
                for (Object objOC : ocList) {
                    Map ocMap = (HashMap) objOC;
                    String cName = ocMap.get("cField1").toString();//oc
                    for (Object objEn : enByUser) {
                        Map mapEn = (HashMap) objEn;
                        String cNameEn = mapEn.get("cField1").toString();//En

                        String enCampusID = mapEn.get("cLevelString").toString();
                        enCampusID = enCampusID.replace("|", "");

                        Double Contracts = Double.valueOf(mapEn.get("Contracts").toString());

                        if (!cName.equals(cNameEn) && Contracts > 0 && cID.equals(enCampusID)) {
                            mapEn.put("app", 0);
                            mapEn.put("fp", 0);
                            mapEn.put("oc", 0);
                            mapEn.put("oc1", 0);
                            mapEn.put("ShowRatio", 0);
                            mapEn.put("Contracts1", Contracts);
                            mapEn.put("CashIn1", mapEn.get("CashIn"));
                            mapEn.put("CashInValue", cnc.nf.format(mapEn.get("CashIn")));
                            returnList.add(mapEn);
                        }
                    }
                }
            }
        }

        //去重
        HashSet h = new HashSet(returnList);
        returnList.clear();
        returnList.addAll(h);

        return cnc.removeDuplicate(returnList);
    }
}
