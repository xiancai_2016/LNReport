package com.lerning.edu.manage.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by FCHEN on 2017/1/12.
 */
@Controller
@RequestMapping("druid1")
public class DruidController extends BaseController {
    @RequiresPermissions("user")
    @RequestMapping(value = {""})
    public String druid(){
        return "druid/druid";
    }
}
