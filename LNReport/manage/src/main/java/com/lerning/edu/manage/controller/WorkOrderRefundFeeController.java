package com.lerning.edu.manage.controller;

import com.lerning.edu.services.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/27
 */
@Controller
@RequestMapping("refundFee")
public class WorkOrderRefundFeeController {

    @Autowired
    public WorkOrderService workOrderService;

    @RequestMapping("/detail")
    public String detail(HttpServletRequest request, String orderNo) {

        List shiftInfo = new ArrayList();
        Map refundFee = new HashMap();

        Map map = new HashMap<>();
        map.put("orderNo", orderNo);

        List refundFees = workOrderService.queryByRefundFee(map);

        if (refundFees.size() > 0) {
            refundFee = (HashMap)refundFees.get(0);
            map.put("cChargeID", refundFee.get("cChargeID"));
            shiftInfo = workOrderService.queryByShiftInfo(map);
        }

        request.setAttribute("refundFee", refundFee);
        request.setAttribute("shiftInfo", shiftInfo);

        return "WorkRefundFee/printPage";
    }
}
