package com.lerning.edu.manage.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("monitor")
public class MonitoringController extends BaseController {

    @RequiresPermissions("user")
    @RequestMapping(value = {""})
    public String monitor1(){
        return "monitor/monitor";
    }
}
