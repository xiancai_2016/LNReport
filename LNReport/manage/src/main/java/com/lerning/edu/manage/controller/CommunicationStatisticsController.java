package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.CommunicationStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/10/22
 */
@Controller
@RequestMapping("communicationStatistics")
public class CommunicationStatisticsController {
    @Autowired
    public CommunicationStatisticsService communicationStatisticsService;
    @Autowired
    public CampusDSRService campusDSRService;

    @RequestMapping("/statistics")
    public String statistics(HttpServletRequest request,
                             @RequestParam(value = "startDate", required = false) String startDate,
                             @RequestParam(value = "endDate", required = false) String endDate,
                             @RequestParam(value = "campusID", required = false)String campusID){

        if(StringUtil.isEmpty(startDate) || StringUtil.isEmpty(endDate)){
            Map dateMap = DateUtil.getTimeInterval(new Date());

            startDate = dateMap.get("start").toString();
            endDate = dateMap.get("end").toString();
        }

        Map map = new HashMap();
        map.put("startDate", startDate + " 00:00:00");
        map.put("endDate", endDate + " 23:59:59");

        map.put("campusID", campusID);

        List communicationStatistics = communicationStatisticsService.queryComStatis(map);
        List communicationStatisticsInfo = new ArrayList();

        if(!StringUtil.isEmpty(campusID)){
            communicationStatisticsInfo = communicationStatisticsService.queryComStatisInfo(map);
        }

        request.setAttribute("startDate", startDate);
        request.setAttribute("endDate", endDate);

        request.setAttribute("data", communicationStatistics);
        request.setAttribute("comStatisInfo", communicationStatisticsInfo);

        request.setAttribute("campusID", campusID);

        return "communication/statistics";
    }
}
