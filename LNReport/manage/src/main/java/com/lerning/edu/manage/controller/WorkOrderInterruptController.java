package com.lerning.edu.manage.controller;

import com.alibaba.fastjson.JSONObject;
import com.lerning.edu.beans.Student;
import com.lerning.edu.beans.SysUser;
import com.lerning.edu.beans.WorkOrder;
import com.lerning.edu.beans.WorkOrderAuditRecord;
import com.lerning.edu.beans.page.PageList;
import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.manage.common.EmailSend;
import com.lerning.edu.manage.context.GlobalContext;
import com.lerning.edu.manage.util.UserUtils;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.StudentService;
import com.lerning.edu.services.WorkOrderAuditRecordService;
import com.lerning.edu.services.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/6
 */
@Controller
@RequestMapping("workOrderInterrupt")
public class WorkOrderInterruptController extends BaseController {

    @Autowired
    private StudentService studentService;
    @Autowired
    private WorkOrderService workOrderService;
    @Autowired
    private WorkOrderAuditRecordService workOrderAuditRecordService;

    @RequestMapping("/listPage")
    public String listPage(HttpServletRequest request, WorkOrder workOrder,
                           @RequestParam(value = "employeeId", required = false) String userID) {

        HttpSession session = request.getSession();

        if(session.getValue(userID) != null){
            String value = session.getValue(userID).toString();

            if(session.getValue(userID) != null && !("").equals(session.getValue(userID)) && (userID).equals(value)){
                request.setAttribute("isLogin", "N");
            }else{
                request.setAttribute("isLogin", "Y");
            }
        }else {
            request.setAttribute("isLogin", "Y");
        }

        request.setAttribute("workOrder", workOrder);
        request.setAttribute("userID", userID);

        return "/interrupt/list";
    }

    //工单列表
    @RequestMapping("/asynList")
    @ResponseBody
    public PageList asynList(PageList pageList, WorkOrder workOrder) {
        if (workOrder.getCstopclassdate() != null) {
            workOrder.setStartDate(DateUtil.dateToString(workOrder.getCstopclassdate(), Constants.DATE_FORMART));
        }
        if (workOrder.getCrecoverydate() != null) {
            workOrder.setEndDate(DateUtil.dateToString(workOrder.getCrecoverydate(), Constants.DATE_FORMART));
        }

        return workOrderService.queryPage(pageList, workOrder);
    }

    //审核任务
    @RequestMapping("/taskList")
    public String list(HttpServletRequest request, String employeeId) {

        HttpSession session = request.getSession();

        if(session.getValue(employeeId) != null){
            String value = session.getValue(employeeId).toString();

            if(session.getValue(employeeId) != null && !("").equals(session.getValue(employeeId)) && (employeeId).equals(value)){
                request.setAttribute("isLogin", "N");
            }else{
                request.setAttribute("isLogin", "Y");
            }
        }else {
            request.setAttribute("isLogin", "Y");
        }

        String ctstaus = "";

        List employees = new ArrayList();
        List employeesOfLN = new ArrayList();
        Map employee = new HashMap();

        if (StringUtil.isEmpty(employeeId)) {
            SysUser user = UserUtils.getUser();
            String account = user.getAccount();
            employees = workOrderService.queryEmployeeByCfield(account);
            employeesOfLN = workOrderService.queryEmployeeByCfieldOfLN(account);
            if (employees.size() > 0) {
                Map empMap = (HashMap) employees.get(0);
                employeeId = empMap.get("cUserID").toString();
            } else if (employeesOfLN.size() > 0) {
                Map empMap = (HashMap) employeesOfLN.get(0);
                employeeId = empMap.get("cUserID").toString();
            }
        } else {
            employees = workOrderService.queryEmployeeByUserID(employeeId);
            employeesOfLN = workOrderService.queryEmployeeByUserIDOfLN(employeeId);
        }

        String campusIDs = "";
        if (employees.size() > 0) {
            for (Object obj : employees) {
                Map mapObj = (HashMap) obj;
                campusIDs = mapObj.get("cLevelString").toString().replace("|", "") + "," + campusIDs;
            }
            employee = (HashMap) employees.get(0);
        }
        //审核 type: 1 , status: 1 cd
        //操作 type: 1 , status: 2 总部
        //打印 type: 1 , status: 4 OA_OAS

        if (employee.size() > 0) {
            employeeId = employee.get("cUserID").toString();
            if (employee.get("cValue").equals("CD")) {
                ctstaus = "1";
            } else if (employee.get("cValue").equals("0")) {
                ctstaus = "2";
            } else if (employee.get("cValue").equals("OA_OAS")) {
                ctstaus = "2";
            } else {
                ctstaus = "4";
            }
        } else if (employeesOfLN.size() > 0) {
            ctstaus = "2";
        } else {
            ctstaus = "4";
        }

        String u1 = "57E4E1B7-775D-4471-856E-7228EC539DCF";//将秀琴

        if (u1.equals(employeeId)) {
            request.setAttribute("CampusID", "changeFee");
        } else {
            request.setAttribute("CampusID", campusIDs);
        }

        request.setAttribute("ctstaus", ctstaus);
        request.setAttribute("userID", employeeId);

        return "/interrupt/myTaskList";
    }

    //审核任务
    @RequestMapping("/asynTaskList")
    @ResponseBody
    public PageList asynTaskList(PageList pageList, WorkOrder workOrder) {
        workOrder.setCstatus(1);
//        workOrder.setCtype(1);//停课
        return workOrderService.queryPage(pageList, workOrder);
    }

    //操作任务
    @RequestMapping("/operationTaskList")
    public String operationTaskList(HttpServletRequest request, WorkOrder workOrder) {
        request.setAttribute("workOrder", workOrder);
        return "/interrupt/operationTaskList";
    }

    //操作任务
    @RequestMapping("/asynOperationTaskList")
    @ResponseBody
    public PageList asynOperationTaskList(PageList pageList, WorkOrder workOrder) {
        workOrder.setCstatus(2);
//        workOrder.setCtype(1);//停课
        return workOrderService.queryPage(pageList, workOrder);
    }

    //打印停课工单列表
    @RequestMapping("/printList")
    public String printList(HttpServletRequest request, WorkOrder workOrder) {
        request.setAttribute("workOrder", workOrder);
        return "/interrupt/printList";
    }

    //打印停课工单列表
    @RequestMapping("/asnyPrintList")
    @ResponseBody
    public PageList asnPrintList(PageList pageList, WorkOrder workOrder) {
        workOrder.setCstatus(4);
//        workOrder.setCtype(1);//停课
        return workOrderService.queryPage(pageList, workOrder);
    }

    @RequestMapping("/apply")
    public String apply(HttpServletRequest request, String employeeId) {

        HttpSession session = request.getSession();

        if(session.getValue(employeeId) != null){
            String value = session.getValue(employeeId).toString();

            if(session.getValue(employeeId) != null && !("").equals(session.getValue(employeeId)) && (employeeId).equals(value)){
                request.setAttribute("isLogin", "N");
            }else{
                request.setAttribute("isLogin", "Y");
            }
        }else {
            request.setAttribute("isLogin", "Y");
        }

        List employees = new ArrayList();
        Map employee = new HashMap();

        if (StringUtil.isEmpty(employeeId)) {
            SysUser user = UserUtils.getUser();
            String account = user.getAccount();
            employees = workOrderService.queryEmployeeByCfield(account);
        } else {
            employees = workOrderService.queryEmployeeByUserID(employeeId);
        }

        if (employees.size() > 0) {
            employee = (HashMap) employees.get(0);
            employeeId = employee.get("cUserID").toString();
        }

        request.setAttribute("tEmployeeID", employeeId);

        if (employee.size() > 0) {
            if (!employee.get("cValue").equals("CC_SA")) {
                return "/interrupt/view";
            }
        } else {
            return "/interrupt/view";
        }

        return "interrupt/apply";
    }

    @RequestMapping("/findStudent")
    @ResponseBody
    public JSONObject findStudent(
            @RequestParam(value = "cName", required = false) String cName,
            @RequestParam(value = "userID", required = false) String userID,
            @RequestParam(value = "page", required = false) int page) {
        Map map = new HashMap();
        map.put("cName", cName);

        List employee = new ArrayList();

        if (StringUtil.isEmpty(userID)) {
            SysUser user = UserUtils.getUser();
            employee = workOrderService.queryEmployeeByCfield(user.getAccount());
        } else {
            employee = workOrderService.queryEmployeeByUserID(userID);
        }
        String campusIDs = "";
        if (employee.size() > 0) {
            for (Object obj : employee) {
                Map mapObj = (HashMap) obj;
                campusIDs = mapObj.get("cLevelString").toString().replace("|", "") + "," + campusIDs;
            }
            map.put("campusID", campusIDs);
        } else {
            map.put("campusID", "");
        }

        map.put("Start", page * 10);
        map.put("End", page * 10 + 10);

        List students = studentService.findStudent(map);
        int stuCount = studentService.findStudentCount(map);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", students);
        double sc = (double) stuCount - 10;
        sc = sc / 10;
        stuCount = (int) Math.ceil(sc);
        jsonObject.put("count", stuCount);//向上取整

        return jsonObject;
    }

    @RequestMapping("/findStudentAndReceipt")
    @ResponseBody
    public JSONObject findStudentAndReceipt(
            @RequestParam(value = "userId", required = false) String userId) {

        Map map = new HashMap();
        map.put("userId", userId);

        BigDecimal balance = new BigDecimal("0.00");

        Map student = studentService.findStudentOne(map);
        List receipts = studentService.queryReceipt(map);

        Map walletMoney = studentService.queryElectronicBalance(map);

        if (receipts.size() > 0) {
            for (Object obj : receipts) {
                Map receiptsMap = (HashMap) obj;
                balance = balance.add(new BigDecimal(receiptsMap.get("cRemainMoney").toString()));
            }
        }

        if (walletMoney != null) {
            student.put("walletMoney", walletMoney.get("cMoney"));
            balance = balance.add(new BigDecimal(walletMoney.get("cMoney").toString()));
        } else {
            student.put("walletMoney", 0);
        }

        student.put("totalMoney", balance);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", receipts);
        jsonObject.put("student", student);

        return jsonObject;
    }

    @RequestMapping("/findClass")
    @ResponseBody
    public JSONObject findClass(
            @RequestParam(value = "userId", required = false) String userId) {

        Map map = new HashMap();
        map.put("userId", userId);

        Map student = studentService.findStudentOne(map);
        List classes = studentService.findClass(map);

        List classList = new ArrayList();

        if (classes.size() > 0) {
            for (Object obj : classes) {
                Map objMap = (HashMap) obj;

                List resumeList = studentService.queryResumeList(objMap);

                if (resumeList.size() > 1) {
                    objMap.put("sDate", "");
                } else if (resumeList.size() > 0) {
                    Map rMap = (HashMap) resumeList.get(0);

                    String week = "周日";

                    if (rMap.get("cWeekday").toString().equals("1")) {
                        week = "周一";
                    } else if (rMap.get("cWeekday").toString().equals("2")) {
                        week = "周二";
                    } else if (rMap.get("cWeekday").toString().equals("3")) {
                        week = "周三";
                    } else if (rMap.get("cWeekday").toString().equals("4")) {
                        week = "周四";
                    } else if (rMap.get("cWeekday").toString().equals("5")) {
                        week = "周五";
                    } else if (rMap.get("cWeekday").toString().equals("6")) {
                        week = "周六";
                    }
                    objMap.put("sDate", week + rMap.get("cStartTime").toString().substring(11, 16) + "-" + rMap.get("cEndTime").toString().substring(11, 16));
                } else {
                    objMap.put("sDate", "");
                }

                classList.add(objMap);
            }
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", classList);
        jsonObject.put("student", student);

        return jsonObject;
    }

    @RequestMapping("/findClassToPage")
    @ResponseBody
    public JSONObject findClassToPage(
            @RequestParam(value = "classID", required = false) String classID,
            @RequestParam(value = "userId", required = false) String userId) {

        Map map = new HashMap();
        map.put("classID", classID);
        map.put("userId", userId);

        List tClass = studentService.findClass(map);

        Map objMap = (HashMap) tClass.get(0);
        String cTime = objMap.get("cCreateTime").toString();
        objMap.put("sDate", DateUtil.dateToWeek(cTime) + " " + cTime.substring(11, 16));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("tClass", objMap);

        return jsonObject;
    }

    @RequestMapping("/save")
    public String save(HttpServletRequest request,
                       @RequestParam(value = "tEmployeeID", required = false) String tEmployeeID,
                       @RequestParam(value = "userId", required = false) String userId,
                       @RequestParam(value = "classID", required = false) String classID,
                       @RequestParam(value = "creason", required = false) String cReason,
                       @RequestParam(value = "cstopclassdate", required = false) String cStopClassDate,
                       @RequestParam(value = "crecoverydate", required = false) String cRecoveryDate,
                       @RequestParam(value = "cisoriginal", required = false) int cIsOriginal,
                       @RequestParam(value = "cremarks", required = false) String cRemarks) {

        WorkOrder workOrder = new WorkOrder();

        if (StringUtil.isEmpty(tEmployeeID)) {
            SysUser user = UserUtils.getUser();
            List employees = workOrderService.queryEmployeeByCfield(user.getAccount());

            if (employees.size() > 0) {
                Map employee = (HashMap) employees.get(0);
                tEmployeeID = employee.get("cUserID").toString();
            } else {
                tEmployeeID = "3378BBC1-8313-4023-9D15-5C8C58BD66A6";
            }
        }

        workOrder.setCcreateuser(tEmployeeID);
        workOrder.setCuserid(userId);
        workOrder.setCclassid(classID.substring(1, classID.length()));
        workOrder.setCstatus(1);
        workOrder.setCtype(1);//停课
        workOrder.setCreason(cReason);
        workOrder.setCstopclassdate(DateUtil.formateStringToDate(cStopClassDate));
        workOrder.setCrecoverydate(DateUtil.formateStringToDate(cRecoveryDate));
        workOrder.setCisoriginal(cIsOriginal);
        workOrder.setCremarks(cRemarks);
        workOrder.setCisprint(2);
        workOrder.setCcreatedate(new Date());

        Map map = new HashMap();
        map.put("userId", userId);
        Map users = workOrderService.queryByUserId(map);
        workOrder.setCampus(users.get("cName").toString());

        map.put("campus", workOrder.getCampus());
        Map maxOrderNo = workOrderService.queryMaxOrderNo(map);

        String orderNo = DateUtil.getYear().substring(2, 4) + users.get("cAddress").toString();
        if (maxOrderNo == null) {
            workOrder.setCorderno(orderNo + "0001");
        } else {
            String oNo = maxOrderNo.get("cOrderNo").toString();
            workOrder.setCorderno(orderNo + StringUtil.autoGenericCode(oNo.substring(oNo.length() - 4, oNo.length()), 4));
        }

        workOrderService.add(workOrder);

        return "redirect:/workOrderInterrupt/listPage?employeeId=" + tEmployeeID;
    }

    @RequestMapping("/detail")
    public String detail(HttpServletRequest request, String orderNo, String flag, String userID) {

        String pageValue = "interrupt";

        String returnValue = pageValue + "/detail";

        List classes = new ArrayList();

        Map map = new HashMap();
        map.put("orderNo", orderNo);
        Map workOrderDetail = workOrderService.queryDetail(map);
        // 1:停课, 2:,复课, 3:,转班, 4:转校, 5:退费，6:转费
        if (workOrderDetail != null) {
            if (workOrderDetail.get("cType").toString().equals("2")) {
                return "redirect:/workOrderOfResume/detail?orderNo=" + orderNo + "&userID=" + userID + "&flag=" + flag;
            } else if (workOrderDetail.get("cType").toString().equals("3")) {
                return "redirect:/workOrderChangeClasses/detail?orderNo=" + orderNo + "&userID=" + userID + "&flag=" + flag;
            } else if (workOrderDetail.get("cType").toString().equals("4")) {
                return "redirect:/workOrderChangeSchool/detail?orderNo=" + orderNo + "&userID=" + userID + "&flag=" + flag;
            } else if (workOrderDetail.get("cType").toString().equals("6")) {
                return "redirect:/workOrderCost/detail?orderNo=" + orderNo + "&userID=" + userID + "&flag=" + flag;
            } else if (workOrderDetail.get("cType").toString().equals("5")) {
                return "redirect:/refundFee/detail?orderNo=" + orderNo;
            }
        }

        if (workOrderDetail == null) {
            returnValue = pageValue + "/auditPage";
            workOrderDetail = workOrderService.queryDetail1(map);

            // 1:停课, 2:,复课, 3:,转班, 4:转校, 5:退费，6:转费
            if (workOrderDetail != null) {
                if (workOrderDetail.get("cType").toString().equals("2")) {
                    return "redirect:/workOrderOfResume/detail?orderNo=" + orderNo + "&userID=" + userID + "&flag=" + flag;
                } else if (workOrderDetail.get("cType").toString().equals("3")) {
                    return "redirect:/workOrderChangeClasses/detail?orderNo=" + orderNo + "&userID=" + userID + "&flag=" + flag;
                } else if (workOrderDetail.get("cType").toString().equals("4")) {
                    return "redirect:/workOrderChangeSchool/detail?orderNo=" + orderNo + "&userID=" + userID + "&flag=" + flag;
                } else if (workOrderDetail.get("cType").toString().equals("6")) {
                    return "redirect:/workOrderCost/detail?orderNo=" + orderNo + "&userID=" + userID + "&flag=" + flag;
                }
            }
        } else if (workOrderDetail.get("cStatus").toString().equals("2")) {
            returnValue = pageValue + "/operation";
        } else if (workOrderDetail.get("cStatus").toString().equals("4")) {
            returnValue = pageValue + "/printPage";
        }

        map.put("userID", workOrderDetail.get("cUserID"));
        map.put("userId", workOrderDetail.get("cUserID"));

        Map student = studentService.findStudentOne(map);

        String cID = workOrderDetail.get("cClassID").toString();
        String[] classIds = cID.split(",");

        for (int i = 0; i < classIds.length; i++) {

            if (!StringUtil.isEmpty(classIds[i])) {
                map.put("classID", classIds[i].toString().trim());
                map.put("cID", classIds[i].toString().trim());

                List myClass = studentService.findClass(map);
                if (myClass.size() > 0) {
                    Map classMap = (HashMap) myClass.get(0);

                    List resumeList = studentService.queryResumeList(map);

                    if (resumeList.size() > 1) {
                        classMap.put("sDate", "");
                    } else if (resumeList.size() > 0) {
                        Map rMap = (HashMap) resumeList.get(0);

                        String week = "周日";

                        if (rMap.get("cWeekday").toString().equals("1")) {
                            week = "周一";
                        } else if (rMap.get("cWeekday").toString().equals("2")) {
                            week = "周二";
                        } else if (rMap.get("cWeekday").toString().equals("3")) {
                            week = "周三";
                        } else if (rMap.get("cWeekday").toString().equals("4")) {
                            week = "周四";
                        } else if (rMap.get("cWeekday").toString().equals("5")) {
                            week = "周五";
                        } else if (rMap.get("cWeekday").toString().equals("6")) {
                            week = "周六";
                        }
                        classMap.put("sDate", week + rMap.get("cStartTime").toString().substring(11, 16) + "-" + rMap.get("cEndTime").toString().substring(11, 16));
                    } else {
                        classMap.put("sDate", "");
                    }

                    classes.add(classMap);
                }
            }
        }

        WorkOrderAuditRecord woar = new WorkOrderAuditRecord();
        woar.setCworkorderid(workOrderDetail.get("cID").toString());
        List record = workOrderAuditRecordService.queryList(woar);

        request.setAttribute("workOrderDetail", workOrderDetail);
        request.setAttribute("student", student);
        request.setAttribute("classes", classes);

        request.setAttribute("record", record);
        request.setAttribute("flag", flag);
        request.setAttribute("userID", userID);

        return returnValue;
    }

    @RequestMapping("/audit")
    public String audit(@RequestParam(value = "cUserID", required = false) String cUserID,
                        @RequestParam(value = "workOrderID", required = false) String workOrderID,
                        @RequestParam(value = "woType", required = false) String woType,
                        @RequestParam(value = "adoptReason", required = false) String adoptReason,
                        @RequestParam(value = "unAdoptReason", required = false) String unAdoptReason) throws Exception {

        WorkOrder workOrder = new WorkOrder();

        if (StringUtil.isEmpty(cUserID)) {
            SysUser user = UserUtils.getUser();
            List employees = workOrderService.queryEmployeeByCfield(user.getAccount());

            if (employees.size() > 0) {
                Map employee = new HashMap();

                if (employee.size() > 0) {
                    cUserID = employee.get("cUserID").toString();
                } else {
                    cUserID = "59355E3A-62C6-41B1-8B74-6F29E697CC53";
                }
            } else {
                cUserID = "59355E3A-62C6-41B1-8B74-6F29E697CC53";
            }
        }

        List sendEmailUser = new ArrayList();

        WorkOrderAuditRecord wrar = new WorkOrderAuditRecord();
        if (woType.equals("1")) {//通过
            wrar.setCstatus(2);
            wrar.setCauditreason(adoptReason);
            workOrder.setCstatus(2);

        } else if (woType.equals("2")) {//驳回
            wrar.setCstatus(3);
            wrar.setCauditreason(unAdoptReason);
            workOrder.setCstatus(3);
        } else if (woType.equals("3")) {//校管家操作成功
            wrar.setCstatus(4);
            wrar.setCauditreason(adoptReason);
            workOrder.setCstatus(4);

            sendEmailUser.add(GlobalContext.getProperties("EmailUser"));
            sendEmailUser.add(GlobalContext.getProperties("EmailUser1"));

            WorkOrder wo = workOrderService.queryById(Long.valueOf(workOrderID));
            Student student = studentService.queryByUserId(wo.getCuserid());

            String content = "";

            Map map = new HashMap();
            map.put("classID", wo.getCclassid().trim());
            Map oldClass = studentService.queryClassByCost(map);

            String recoveryDate = DateUtil.dateToString(wo.getCrecoverydate(), Constants.DATE_FORMART);

            // 1:停课, 2:,复课, 3:,转班, 4:转校, 5:退费，6:转费
            if (wo.getCtype().toString().equals("1")) {
                content = "学生:" + student.getCname() + "(电话:" + student.getCsmstel() + "), 在" + recoveryDate + "日, 从" + oldClass.get("className") + "班停课";
            } else if (wo.getCtype().toString().equals("2")) {
                content = "学生:" + student.getCname() + "(电话:" + student.getCsmstel() + "), 在" + recoveryDate + "日, 复课到" + oldClass.get("className") + "班";
            } else if (wo.getCtype().toString().equals("3")) {
                map.put("classID", wo.getResumeClassID());
                Map newClass = studentService.queryClassByCost(map);
                content = "学生:" + student.getCname() + "(电话:" + student.getCsmstel() + "), 在" + recoveryDate + "日, 从" + oldClass.get("className") + "班转到" + newClass.get("className") + "班";
            } else if (wo.getCtype().toString().equals("4")) {
                map.put("classID", wo.getResumeClassID());
                Map newClass = studentService.queryClassByCost(map);
                content = "学生:" + student.getCname() + "(电话:" + student.getCsmstel() + "), 在" + recoveryDate + "日, 从" + oldClass.get("cName") + "校区的" + oldClass.get("className") + "班转入到" + newClass.get("cName") + "校区的" + newClass.get("className") + "班";
            } else if (wo.getCtype().toString().equals("6")) {
                Student student1 = studentService.queryByUserId(wo.getCreason());
                sendEmailUser.clear();
                sendEmailUser.add(GlobalContext.getProperties("EmailUserOfCost"));
                content = "学生:" + student.getCname() + "(电话:" + student.getCsmstel() + "), 在" + recoveryDate + "日, 转费给" + student1.getCname();
            }

            StringBuffer sendContent = new StringBuffer("您好! <br> ");
            sendContent.append(content);

            String from = "特况单任务提醒";
            String title = wo.getCorderno() + "特况处理完成通知！";

            //发送邮件
            EmailSend.emailSend(sendEmailUser, from, title, sendContent.toString());
        } else if (woType.equals("4")) {//校管家操作失败
            wrar.setCstatus(5);
            wrar.setCauditreason(unAdoptReason);
            workOrder.setCstatus(5);
        }

        wrar.setCuserid(cUserID);
        wrar.setCworkorderid(workOrderID);

        workOrder.setCid(Integer.valueOf(workOrderID));

        workOrderService.update(workOrder);

        workOrderAuditRecordService.add(wrar);

        return "redirect:/workOrderInterrupt/taskList?employeeId=" + cUserID;
    }

    @RequestMapping("/print")
    @ResponseBody
    private JSONObject printWorkOrder(@RequestParam(value = "workOrderID", required = false) String workOrderID) {
        WorkOrder workOrder = new WorkOrder();

        workOrder.setCid(Integer.valueOf(workOrderID));
        workOrder.setCisprint(1);

        workOrderService.update(workOrder);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", 1);
        return jsonObject;
    }

    @RequestMapping("/findWorkOrder")
    @ResponseBody
    public JSONObject findWorkOrder(
            @RequestParam(value = "txtBeginDay", required = false) String txtBeginDay,
            @RequestParam(value = "txtEndDay", required = false) String txtEndDay,
            @RequestParam(value = "cstatus", required = false) String cstatus,
            @RequestParam(value = "ctype", required = false) String ctype,
            @RequestParam(value = "userName", required = false) String userName,
            @RequestParam(value = "userID", required = false) String userID,
            @RequestParam(value = "page", required = false) int page) {

        //String campusID = "";
        String cValue = "";

        Map employee = new HashMap();

        String campusIDs = "";

        if (StringUtil.isEmpty(userID)) {
            SysUser user = UserUtils.getUser();
            String account = user.getAccount();
            List employees = workOrderService.queryEmployeeByCfield(account);

            if (employees.size() > 0) {
                for (Object obj : employees) {
                    Map mapObj = (HashMap) obj;
                    campusIDs = mapObj.get("cLevelString").toString().replace("|", "") + "," + campusIDs;
                }
                employee = (HashMap) employees.get(0);
            }

            if (employee.size() > 0) {
                cValue = employee.get("cValue").toString();
            }
        } else {
            List employees = workOrderService.queryEmployeeByUserID(userID);

            if (employees.size() > 0) {
                for (Object obj : employees) {
                    Map mapObj = (HashMap) obj;
                    campusIDs = mapObj.get("cLevelString").toString().replace("|", "") + "," + campusIDs;
                }
                employee = (HashMap) employees.get(0);
            }

            if (employee.size() > 0) {
                cValue = employee.get("cValue").toString();
            }
        }

        if (campusIDs.contains(",")) {
            campusIDs = campusIDs.substring(0, campusIDs.length() - 1);
        }

        Map map = new HashMap();
        if (StringUtil.isEmpty(txtBeginDay)) {
            map.put("startDate", txtBeginDay);
        } else {
            map.put("startDate", txtBeginDay + " 00:00:00");
        }

        if (StringUtil.isEmpty(txtBeginDay)) {
            map.put("endDate", txtEndDay);
        } else {
            map.put("endDate", txtEndDay + " 23:59:59");
        }
        map.put("cstatus", cstatus);
        map.put("ctype", ctype);
        map.put("userName", userName);
        map.put("campusID", campusIDs);
        map.put("Start", (page - 1) * 10);
        map.put("End", page * 10);

        List workOrders = new ArrayList();
        int wkCount = 0;

        if (!cValue.equals("CC_SA")) {
//            if(ctype.equals("5")){
//                workOrders = workOrderService.findWorkOrderOfFundFee(map);
//                wkCount = workOrderService.findWorkOrderCountOfFundFee(map);
//            }else{
            workOrders = workOrderService.findWorkOrder(map);
            wkCount = workOrderService.findWorkOrderCount(map);
//            }
        } else {
            List workOrderList = workOrderService.findWorkOrderByAdd(map);

            if (workOrderList.size() > 0) {
                for (Object obj : workOrderList) {
                    Map mapWorkOrder = (HashMap) obj;
                    if (!mapWorkOrder.get("cstatus").equals("1")) {
                        Map workOrderRecord = workOrderService.queryWorkOrderRecordById(mapWorkOrder.get("cID").toString());
                        if (workOrderRecord != null) {
                            mapWorkOrder.put("employName", workOrderRecord.get("employName"));
                        }
                    }
                    workOrders.add(mapWorkOrder);
                }
            }

            wkCount = workOrderService.findWorkOrderCountByAdd(map);
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", workOrders);
        double sc = (double) wkCount;
        sc = sc / 10;
        wkCount = (int) Math.ceil(sc);
        jsonObject.put("count", wkCount);//向上取整

        return jsonObject;
    }

    @RequestMapping("/myTaskList")
    @ResponseBody
    public JSONObject myTaskList(
            @RequestParam(value = "CampusID", required = false) String CampusID,
            @RequestParam(value = "cstatus", required = false) String cstatus,
            @RequestParam(value = "page", required = false) int page) {

        String cIsPrint = "";
        String cType = "";

        if (cstatus.equals("4")) {
            cIsPrint = "2";
        }

        if (CampusID.contains(",")) {
            CampusID = CampusID.substring(0, CampusID.length() - 1);
        }

        Map map = new HashMap();
        if (cstatus.equals("2")) {
            map.put("CampusID", "");
        } else {
            map.put("CampusID", CampusID);
        }

        if ("changeFee".equals(CampusID)) {
            cType = "6";
        }

        map.put("cType", cType);
        map.put("cstatus", cstatus);
        map.put("cIsPrint", cIsPrint);
        map.put("Start", (page - 1) * 10);
        map.put("End", page * 10);

        List workOrders = workOrderService.findMyTaskList(map);
        int wkCount = workOrderService.findMyTaskListCount(map);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", workOrders);
        double sc = (double) wkCount;
        sc = sc / 10;
        wkCount = (int) Math.ceil(sc);
        jsonObject.put("count", wkCount);//向上取整

        return jsonObject;
    }

    @RequestMapping("/findClassByCost")
    @ResponseBody
    public JSONObject findClass(@RequestParam(value = "userId", required = false) String userId,
                                @RequestParam(value = "classID", required = false) String classID) {

        Map map = new HashMap();
        map.put("userId", userId);
        map.put("classID", classID);

        Map student = studentService.findStudentOne(map);

        Map classByCost = studentService.queryClassByCost(map);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", classByCost);
        jsonObject.put("student", student);

        return jsonObject;
    }

    @RequestMapping("/odsLogin")
    @ResponseBody
    public JSONObject odsLogin(HttpServletRequest request,
                               @RequestParam(value = "odsUserName", required = false) String odsUserName,
                               @RequestParam(value = "odsPassword", required = false) String odsPassword,
                               @RequestParam(value = "userID", required = false)String userID){

        Map map = new HashMap();
        map.put("odsUserName", odsUserName);
        map.put("odsPassword", odsPassword);

        List odsUsers = workOrderService.queryOdsUser(map);

        if(odsUsers.size() > 0){
            HttpSession session = request.getSession();
            session.setAttribute(userID,userID);
            session.setMaxInactiveInterval(1000*60*30);
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", odsUsers.size());

        return jsonObject;
    }
}
