package com.lerning.edu.manage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 错误管理
 *
 * @date 2016-01-31
 * @author FCHEN
 * @version 1.0
 *
 */
@Controller
@RequestMapping("error")
public class ErrorController extends BaseController {

    @RequestMapping(value = {""})
    public String error(){
        return "error";
    }

}
