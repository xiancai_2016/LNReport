package com.lerning.edu.manage.WeChatReport;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.manage.controller.CampusDSRController;
import com.lerning.edu.services.CampusDSRService;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * @author jiangwei
 * @since 18/6/13
 */
@Controller
@RequestMapping("wechat")
public class EnrollAndRefund {

    @Autowired
    private CampusDSRService campusDSRService;

    CampusDSRController ccr = new CampusDSRController();

    @RequestMapping("/enrollAndRefund")
    public String enrollAndRefund(HttpServletRequest request,
                                  @RequestParam(value = "type", required = false) String type,
                                  @RequestParam(value = "CampusID", required = false) String CampusID,
                                  @RequestParam(value = "dateTime", required = false)String dateTime) {

        List departs = campusDSRService.queryDepartList();//校区

        String benginDate = null;
        String endDate = null;

        if(type.equals("1")){
            benginDate = dateTime +" 00:00:00.0";
            endDate = dateTime +" 23:59:59.0";
        }else if (type.equals("2")){
            benginDate = dateTime +" 00:00:00.0";
            endDate = DateUtil.getBeforeDateToString(Constants.DATE_FORMART,6, DateUtil.formateStringToDate(dateTime))+" 23:59:59.0";
        }else {
            Map dateMap = new HashMap();
            dateMap.put("dqDay",dateTime.subSequence(0,7));
            List FinanceMonthOfDq = campusDSRService.queryFinanceMonthOfDq(dateMap);//财务月列表
            Map map = (HashMap) FinanceMonthOfDq.get(0);
            benginDate = map.get("BeginDate").toString();
            endDate = map.get("EndDate").toString();
        }

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("BeginDate", benginDate);
        map.put("EndDate", endDate);
        map.put("Grade", "");

        NumberFormat nf = new DecimalFormat("￥,###.##");

        //List chargeAndRefund = campusDSRService.chargeAndRefund(map);

        Map lockMap = new HashMap();
        lockMap.put("CampusID",CampusID);
        lockMap.put("text", endDate.subSequence(0, 7));

        List chargeAndRefund = new ArrayList();
        if(StringUtil.isEmpty(CampusID)){
            chargeAndRefund = campusDSRService.chargeAndRefundOfLockAll(lockMap);
        }else{
            chargeAndRefund = campusDSRService.chargeAndRefundOfLock(lockMap);
        }

            List chargeAndRefundByK = new ArrayList();
        List chargeAndRefundByC = new ArrayList();
        List chargeAndRefundByY = new ArrayList();

        if (chargeAndRefund.size() > 0) {
            //汇总
            for (int i = 0; i < chargeAndRefund.size(); i++) {
                Map mapKItem = (HashMap) chargeAndRefund.get(i);
                String grade = mapKItem.get("grade").toString();
                if (grade.equals("K1") || grade.equals("K2") || grade.equals("K3") || grade.equals("K4")) {
                    chargeAndRefundByK.add(mapKItem);
                } else if (grade.equals("Y1") || grade.equals("Y2") || grade.equals("Y3") || grade.equals("Y4") || grade.equals("Y5") || grade.equals("Y6")) {
                    chargeAndRefundByY.add(mapKItem);
                } else {
                    chargeAndRefundByC.add(mapKItem);
                }
            }
        }

        Map mapK = ccr.Amount(chargeAndRefundByK);
        Map mapC = ccr.Amount(chargeAndRefundByC);
        Map mapY = ccr.Amount(chargeAndRefundByY);

        map.put("cFlag", 1);
        Map mapSKC = campusDSRService.querySCKCount(map);
        map.put("cFlag", -1);
        Map maprSKC = campusDSRService.querySCKCount(map);
        maprSKC.put("skcrFact", maprSKC.get("skcrFact").toString().replace("-",""));

        Map SKC = new HashMap();
        SKC.put("skcFact", nf.format(Double.valueOf(mapSKC.get("skcrFact").toString())));
        SKC.put("skcrFact", nf.format(Double.valueOf(maprSKC.get("skcrFact").toString())));

//        Map totalChargeAndRefund = campusDSRService.totalChargeAndRefund(map);
        Map totalChargeAndRefund =  campusDSRService.totalChargeAndRefundLock(lockMap);

        totalChargeAndRefund.put("sumtcost", nf.format(Double.valueOf(totalChargeAndRefund.get("sumtcost").toString()) + Double.valueOf(mapSKC.get("skcrFact").toString())));
        totalChargeAndRefund.put("sumrtcost", nf.format(Double.valueOf(totalChargeAndRefund.get("sumrtcost").toString()) + Double.valueOf(maprSKC.get("skcrFact").toString())));

        request.setAttribute("CampusID", CampusID);
        request.setAttribute("type", type);
        request.setAttribute("dateTime", dateTime);

        request.setAttribute("departs", departs);

        request.setAttribute("chargeAndRefundByK", ccr.changeType(chargeAndRefundByK));
        request.setAttribute("chargeAndRefundByC", ccr.changeType(chargeAndRefundByC));
        request.setAttribute("chargeAndRefundByY", ccr.changeType(chargeAndRefundByY));

        request.setAttribute("mapK", mapK);
        request.setAttribute("mapC", mapC);
        request.setAttribute("mapY", mapY);

        request.setAttribute("SKC", SKC);

        request.setAttribute("reportInfo", "KPI Summary-报名退费");

        request.setAttribute("totalChargeAndRefund", totalChargeAndRefund);
        request.setAttribute("date", benginDate.substring(0,10)+"到"+endDate.substring(0,10));

        return "wechar/totalCAR";
    }

}
