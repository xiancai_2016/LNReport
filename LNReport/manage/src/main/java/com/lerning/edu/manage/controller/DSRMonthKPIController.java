package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.DSRMonthKPIService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/29
 */
@Controller
@RequestMapping("dsr")
public class DSRMonthKPIController {

    @Autowired
    private DSRMonthKPIService dsRMonthKPIService;

    @Autowired
    private CampusDSRService campusDSRService;

    CampusDSRController cdc = new CampusDSRController();

    @RequestMapping("/monthKpi")
    @RequiresPermissions("view:monthKpi")
    public String queryMonthKpiIndex(HttpServletRequest request) {
        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表
        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(cdc.dayMap());//当前月

        Map dateMap = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", dateMap.get("AutoID"));

        request.setAttribute("FinanceMonths", FinanceMonths);
        return "DRS/monthKpi";
    }

    @RequestMapping("/monthKpiList")
    @RequiresPermissions("view:monthKpi")
    public String monthKpi(HttpServletRequest request,
                           @RequestParam(value = "AutoID", required = false) int AutoID) {

        List FinanceMonths = campusDSRService.queryNaturalMonths();//财务月列表

        Map beginDateAndEndDate = campusDSRService.queryNaturalMonth(AutoID);//财务月开始和结束时间

        Map map = new HashMap();

        map.put("BeginDate", beginDateAndEndDate.get("BeginDate").toString());
        map.put("EndDate", beginDateAndEndDate.get("EndDate").toString());

        List depars = campusDSRService.queryDepartList();
        List monthKpis = new ArrayList();

        BigDecimal totalMkt = new BigDecimal("0");//mkt渠道业绩汇总
        BigDecimal totalKPIMkt = new BigDecimal("0");// mkt渠道KPI汇总

        BigDecimal totalRef = new BigDecimal("0");//ref渠道业绩汇总
        BigDecimal totalKPIRef = new BigDecimal("0");// ref渠道KPI汇总

        BigDecimal totalRenew = new BigDecimal("0");//renew渠道业绩汇总
        BigDecimal totalKPIRenew = new BigDecimal("0");// renew渠道KPI汇总

        Map totalMap = new HashMap();

        if (depars.size() > 0) {
            for (Object obj : depars) {
                Map dmap = (HashMap) obj;
                map.put("CampusID", dmap.get("cID"));
                map.put("text", beginDateAndEndDate.get("EndDate").toString().substring(0, 7));
                map.put("cAddress", dmap.get("cAddress"));

                BigDecimal mktKpi = new BigDecimal("0");//mkt渠道pki
                BigDecimal refKpi = new BigDecimal("0");//ref渠道pki
                BigDecimal renewKpi = new BigDecimal("0");//renew渠道pki

                //查询完成业绩
                Map monthKpi = dsRMonthKPIService.queryAll(map);
                monthKpi.put("cAddress", dmap.get("cAddress"));
                monthKpi.put("cName", dmap.get("cNAME"));
                //查询18年提前续费的业绩
                Map odsReNewByMonth = dsRMonthKPIService.queryRenewByCmpus(map);

                BigDecimal enrollmint = new BigDecimal("0");
                if(odsReNewByMonth != null){
                    enrollmint = new BigDecimal(odsReNewByMonth.get("enrollmint").toString());
                }

                //查询指标
                List odsCampusKpi = dsRMonthKPIService.queryCampusKpi(map);

                //加上18年提前分摊的业绩
                BigDecimal renew = new BigDecimal(monthKpi.get("tash").toString()).add(enrollmint);
                monthKpi.put("renew", renew);

                //取到对应渠道的指标
                if (odsCampusKpi.size() > 0) {
                    for (Object campusKpi : odsCampusKpi) {
                        Map mapKpi = (HashMap) campusKpi;
                        if (mapKpi.get("KpiType").equals("MKT")) {
                            mktKpi = new BigDecimal(mapKpi.get("KpiValue").toString());
                            monthKpi.put("mktKpi", mktKpi);
                        } else if (mapKpi.get("KpiType").equals("REF")) {
                            refKpi = new BigDecimal(mapKpi.get("KpiValue").toString());
                            monthKpi.put("refKpi", refKpi);
                        } else if (mapKpi.get("KpiType").equals("ReNew")) {
                            renewKpi = new BigDecimal(mapKpi.get("KpiValue").toString());
                            monthKpi.put("renewKpi", renewKpi);
                        }
                    }
                }else{
                    monthKpi.put("mktKpi", 0.00);
                    monthKpi.put("refKpi", 0.00);
                    monthKpi.put("renewKpi", 0.00);
                }
                //汇总渠道业绩(按校区)
                BigDecimal totalEnrollmentByCampus = new BigDecimal(monthKpi.get("allash").toString()).add(enrollmint);
                monthKpi.put("totalEnrollmentByCampus", totalEnrollmentByCampus);
                //汇总渠道指标(按校区)
                BigDecimal totalKPIByCampus = mktKpi.add(refKpi).add(renewKpi);
                monthKpi.put("totalKPIByCampus", totalKPIByCampus);

                //计算达标率(mkt)
                if(StringUtil.divideValue(mktKpi)){
                    monthKpi.put("mktRate", new BigDecimal(monthKpi.get("mash").toString()).divide(mktKpi, 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")));
                }else {
                    monthKpi.put("mktRate", 0.00);
                }
                //ref
                if(StringUtil.divideValue(refKpi)){
                    monthKpi.put("refRate", new BigDecimal(monthKpi.get("fash").toString()).divide(refKpi, 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")));
                }else {
                    monthKpi.put("refRate", 0.00);
                }
                //renew
                if(StringUtil.divideValue(renewKpi)){
                    monthKpi.put("renewRate", renew.divide(renewKpi, 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")));
                }else {
                    monthKpi.put("renewRate", 0.00);
                }
                //
                if(StringUtil.divideValue(totalKPIByCampus)){
                    monthKpi.put("totalRate", totalEnrollmentByCampus.divide(totalKPIByCampus, 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")));
                }else {
                    monthKpi.put("totalRate", 0.00);
                }
                //汇总渠道目标值
                totalKPIMkt = totalKPIMkt.add(mktKpi);
                totalKPIRef = totalKPIRef.add(refKpi);
                totalKPIRenew = totalKPIRenew.add(renewKpi);
                //汇总渠道业绩值
                totalMkt = totalMkt.add(new BigDecimal(monthKpi.get("mash").toString()));
                totalRef = totalRef.add(new BigDecimal(monthKpi.get("fash").toString()));
                totalRenew = totalRenew.add(renew);

                monthKpis.add(monthKpi);


            }
        }

        BigDecimal totalEnrollment = totalMkt.add(totalRef).add(totalRenew);
        BigDecimal totalKpi = totalKPIMkt.add(totalKPIRef).add(totalKPIRenew);

        totalMap.put("totalKPIMkt", totalKPIMkt);
        totalMap.put("totalMkt", totalMkt);
        //
        if(StringUtil.divideValue(totalKpi)){
            totalMap.put("totalMktRate", totalMkt.divide(totalKPIMkt, 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")));
        }else {
            totalMap.put("totalMktRate", 0.00);
        }

        totalMap.put("totalKPIRef", totalKPIRef);
        totalMap.put("totalRef", totalRef);
        //
        if(StringUtil.divideValue(totalKpi)){
            totalMap.put("totalRefRate", totalRef.divide(totalKPIRef, 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")));
        }else {
            totalMap.put("totalRefRate", 0.00);
        }

        totalMap.put("totalKPIRenew", totalKPIRenew);
        totalMap.put("totalRenew", totalRenew);
        //
        if(StringUtil.divideValue(totalKpi)){
            totalMap.put("totalRenewRate", totalRenew.divide(totalKPIRenew, 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")));
        }else {
            totalMap.put("totalRenewRate", 0.00);
        }

        totalMap.put("totalEnrollment", totalEnrollment);
        totalMap.put("totalKpi", totalKpi);
        //
        if(StringUtil.divideValue(totalKpi)){
            totalMap.put("totalRate", totalEnrollment.divide(totalKpi, 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")));
        }else {
            totalMap.put("totalRate", 0.00);
        }

        request.setAttribute("FinanceMonths", FinanceMonths);
        request.setAttribute("AutoID", AutoID);

        monthKpis =  StringUtil.theFirstThree(monthKpis, "renewRate");
        monthKpis =  StringUtil.theFirstThree(monthKpis, "mktRate");
        monthKpis =  StringUtil.theFirstThree(monthKpis, "refRate");
        monthKpis =  StringUtil.theFirstThree(monthKpis, "totalRate");

        StringUtil.sortByWhatAsc(monthKpis,"cName");

        request.setAttribute("monthKpis", monthKpis);
        request.setAttribute("totalMap", totalMap);

        return "DRS/monthKpi";
    }
}
