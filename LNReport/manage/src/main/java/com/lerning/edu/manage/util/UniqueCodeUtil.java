package com.lerning.edu.manage.util;

import com.lerning.edu.commons.SequnceUtil;
import com.lerning.edu.manage.common.GlobalVariableNames;
import com.lerning.edu.manage.context.GlobalContext;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Created by FCHEN on 2016/10/18.
 */
public class UniqueCodeUtil {

    /**
     * 指定位数唯一码
     * @param uniqueType 当flag为true时有效，指定生成的唯一码以某个字符开头代表该唯一码的象征意义
     * @param flag 是否拼接uniqueType在首位
     * @return
     */
    public static String getUniqueCode(String uniqueType, boolean flag){
        Integer machineId = NumberUtils.toInt(GlobalContext.getProperties(GlobalVariableNames.INGOREAUTH));
        return SequnceUtil.getUniqueNo(uniqueType, machineId, flag);
    }
}
