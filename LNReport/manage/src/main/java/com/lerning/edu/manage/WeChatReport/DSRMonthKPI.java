package com.lerning.edu.manage.WeChatReport;

import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.DSRMonthKPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/26
 */
@Controller
@RequestMapping("wechat")
public class DSRMonthKPI {

    @Autowired
    private DSRMonthKPIService dsrMonthKPIService;
    @Autowired
    private CampusDSRService campusDSRService;

    @RequestMapping("/dsrMonthKPI")
    public String dsrChannel(HttpServletRequest request,
                             @RequestParam(value = "dateTime", required = false) String dateTime) {

        Map dateMap = new HashMap();
        dateMap.put("dqDay", dateTime.subSequence(0, 7));

        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dateMap);//当前月
        Map map = (HashMap) FinanceMonthOfDq.get(0);

        //财务月的后一个周五跟财务月最后一天在同一个月,则查询下个财务月的数据
        String lastDate = dateTime + " 23:59:59";
        if (!DateUtil.compare_date(map.get("EndDate").toString(), lastDate)) {
            dateMap.put("dqDay", DateUtil.addMonth(dateTime).substring(0,7));
            FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dateMap);//当前月
            map = (HashMap) FinanceMonthOfDq.get(0);
        }

        dateMap.put("BeginDate", map.get("BeginDate").toString());
        dateMap.put("EndDate",dateTime);
        //财务月的后一个周五跟财务月最后一天在同一个月,则查询下个财务月的数据

        //锁定数据
        List dsrMonthKPIs = dsrMonthKPIService.queryDsrMonthKpiLockData(dateMap);

        dsrMonthKPIs =  StringUtil.theFirstThree(dsrMonthKPIs, "renewRate");
        dsrMonthKPIs =  StringUtil.theFirstThree(dsrMonthKPIs, "mktRate");
        dsrMonthKPIs =  StringUtil.theFirstThree(dsrMonthKPIs, "refRate");
        dsrMonthKPIs =  StringUtil.theFirstThree(dsrMonthKPIs, "totalRate");

        StringUtil.sortByWhatAsc(dsrMonthKPIs,"cAddress");

        Map dsrMonthKPI = dsrMonthKPIService.queryDsrMonthKpiLockDataSum(dateMap);

        request.setAttribute("dsrMonthKPIs", dsrMonthKPIs);
        request.setAttribute("dsrMonthKPI", dsrMonthKPI);

        request.setAttribute("dateTime", dateTime);

        request.setAttribute("reportInfo", "DSR月度KPI达成");
        request.setAttribute("date", map.get("BeginDate").toString().substring(0,10) + "到" + dateTime);

        return "wechar/dsrMonthKpi";
    }
}
