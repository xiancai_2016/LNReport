/*
 * For com.royal.art
 * Copyright [2015/11/12] By FCHEN
 */
package com.lerning.edu.manage.context;

import com.google.common.base.Charsets;
import com.lerning.edu.manage.common.GlobalVariableNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * GlobalContext
 * 系统公共资源文件
 * @author FCHEN
 * @date 2015/11/12
 */
public class GlobalContext implements ApplicationContextAware {

    private static Logger LOGGER = LoggerFactory.getLogger(GlobalContext.class);

    /**
     * 资源文件
     */
    private Resource resource;

    private WebApplicationContext applicationContext;

    public static Properties global_p = new Properties();

    boolean propInit = false;
    boolean acInit = false;
    boolean setGlobal = false;

    /**
     * 是/否
     */
    public static final String YES = "1";
    public static final String NO = "0";

    /**
     * 对/错
     */
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    /**
     * 获取指定的key
     * @param key
     * @return
     */
    public static String getProperties(String key){
        return global_p.getProperty(key);
    }

    /**
     * 获取管理端根路径
     */
    public static String getAdminPath() {
        return getProperties("adminPath");
    }

    /**
     * 页面获取常量
     *
     * @see ${fns:getConst('YES')}
     */
    public static Object getConst(String field) {
        try {
            return GlobalContext.class.getField(field).get(null);
        } catch (Exception e) {
            // 异常代表无配置，这里什么也不做
        }
        return null;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
        init();
        propInit = true;
        setGlobalInApplcation();
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = (WebApplicationContext)applicationContext;
        acInit = true;
        setGlobalInApplcation();
    }

    /**
     * 初始化资源
     */
    void init(){
        LOGGER.info(" starting to load global resource ...");
        if (resource != null && resource.exists()) {
            if(resource.isReadable()){
                try {
                    InputStreamReader reader = new InputStreamReader(resource.getInputStream(), Charsets.UTF_8);
                    global_p.load(reader);
                    LOGGER.info(" loading global file success ...");
                } catch (IOException e) {
                    LOGGER.error(" had a exception when loading global file ",e);
                    e.printStackTrace();
                }
            }else{
                LOGGER.warn(" no readable resource to be loaded,please check you file ...");
            }
        }else{
            LOGGER.warn(" there is no global file to be load ");
        }
    }

    /**
     * resource 和 applicationContext 的初始化顺序为 resource > applicationContext
     * 此方法必须在两个变量全初始化完成后才能执行
     * 为避免会存在的初始化顺序和上面理解的不一致的情况，这里加入了初始化标识
     * 避免重复加入 setGlobal 标识
     */
    void setGlobalInApplcation(){
        if(setGlobal == false && propInit && acInit ){
            setGlobal = true;
            applicationContext.getServletContext().setAttribute(GlobalVariableNames.GLOBAL,global_p);
        }
    }


}
