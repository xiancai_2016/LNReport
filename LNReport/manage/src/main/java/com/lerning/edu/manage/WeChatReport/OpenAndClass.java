package com.lerning.edu.manage.WeChatReport;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.manage.controller.CampusDSRController;
import com.lerning.edu.services.CampusDSRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author jiangwei
 * @since 18/6/13
 */
@Controller
@RequestMapping("wechat")
public class OpenAndClass {

    @Autowired
    private CampusDSRService campusDSRService;

    CampusDSRController ccr = new CampusDSRController();

    @RequestMapping("/openAndClass")
    public String queryClassCount(HttpServletRequest request,
                                  @RequestParam(value = "type", required = false) String type,
                                  @RequestParam(value = "CampusID", required = false) String CampusID,
                                  @RequestParam(value = "dateTime", required = false)String dateTime) {
        List departs = campusDSRService.queryDepartList();//校区

        String beginDate = null;
        String beginDateFowbk = null;
        String endDate = null;
        String monthBeginDate = null;

        if(type.equals("1")){
            beginDate = dateTime +" 00:00:00.0";
            beginDateFowbk = beginDate;
            endDate = dateTime +" 23:59:59.0";
            monthBeginDate =  dateTime +" 00:00:00.0";
        }else if (type.equals("2")){
            endDate = dateTime +" 23:59:59.0 ";
            beginDate = DateUtil.getBeforeDateToString(Constants.DATE_FORMART,6, DateUtil.formateStringToDate(dateTime))+" 00:00:00.0";
            beginDateFowbk = beginDate;
            monthBeginDate = beginDate;
        }else {
            Map dateMap = new HashMap();
            dateMap.put("dqDay",dateTime.subSequence(0,7));
            List FinanceMonthOfDq = campusDSRService.queryFinanceMonthOfDq(dateMap);//财务月列表
            Map map = (HashMap) FinanceMonthOfDq.get(0);
            endDate = map.get("EndDate").toString();
            beginDateFowbk = map.get("BeginDate").toString();
            beginDate = DateUtil.getBeforeDateToString(Constants.DATE_FORMART, 6, DateUtil.formateStringToDate(endDate)) + " 00:00:00.0";

            monthBeginDate =  map.get("BeginDate").toString();;
        }

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("Grade", "");
        map.put("BeginDate", beginDate);
        map.put("BeginDateFoWKB",beginDateFowbk);
        map.put("EndDate", endDate);

        List queryClassCountByK = new ArrayList();
        List queryClassCountByC = new ArrayList();
        List queryClassCountByY = new ArrayList();

        //List queryClassCount = campusDSRService.queryClassCountByK(map);

        Map lockMap = new HashMap();
        lockMap.put("CampusID",CampusID);
        lockMap.put("text", endDate.subSequence(0, 7));

        List queryClassCount = new ArrayList();
        if(StringUtil.isEmpty(CampusID)){
            queryClassCount = campusDSRService.queryClassCountByKOfLockAll(lockMap);
        }else{
            queryClassCount = campusDSRService.queryClassCountByKOfLock(lockMap);
        }

        if (queryClassCount.size() > 0) {
            for (int i = 0; i < queryClassCount.size(); i++) {
                Map mapKItem = (HashMap) queryClassCount.get(i);
                String grade = mapKItem.get("grade").toString();
                if (grade.equals("K1") || grade.equals("K2") || grade.equals("K3") || grade.equals("K4")) {
                    queryClassCountByK.add(mapKItem);
                } else if (grade.equals("Y1") || grade.equals("Y2") || grade.equals("Y3") || grade.equals("Y4") || grade.equals("Y5") || grade.equals("Y6")) {
                    queryClassCountByY.add(mapKItem);
                } else {
                    queryClassCountByC.add(mapKItem);
                }
            }
        }

        Map mapK = ccr.cls(queryClassCountByK);
        Map mapC = ccr.cls(queryClassCountByC);
        Map mapY = ccr.cls(queryClassCountByY);

        Map mapT = new HashMap();

        mapT.put("tacc", (int) mapK.get("acc") + (int) mapC.get("acc") + (int) mapY.get("acc"));
        mapT.put("tacc1", (int) mapK.get("acc1") + (int) mapC.get("acc1") + (int) mapY.get("acc1"));
        mapT.put("tacc2", (int) mapK.get("acc2") + (int) mapC.get("acc2") + (int) mapY.get("acc2"));
        mapT.put("tacc3", (int) mapK.get("acc3") + (int) mapC.get("acc3") + (int) mapY.get("acc3"));
        mapT.put("tacc4", (int) mapK.get("acc4") + (int) mapC.get("acc4") + (int) mapY.get("acc4"));
        mapT.put("tclsCount", (int) mapK.get("clsCount") + (int) mapC.get("clsCount") + (int) mapY.get("clsCount"));
        mapT.put("tsumcc", (int) mapK.get("sumcc") + (int) mapC.get("sumcc") + (int) mapY.get("sumcc"));
        mapT.put("twkbCount", (int) mapK.get("wkbCount") + (int) mapC.get("wkbCount") + (int) mapY.get("wkbCount"));
        mapT.put("twkbClsCount", (int) mapK.get("wkbClsCount") + (int) mapC.get("wkbClsCount") + (int) mapY.get("wkbClsCount"));
        mapT.put("txuCount", (int) mapK.get("xuCount") + (int) mapC.get("xuCount") + (int) mapY.get("xuCount"));
        mapT.put("thJCount", (int) mapK.get("hJCount") + (int) mapC.get("hJCount") + (int) mapY.get("hJCount"));
        DecimalFormat df = new DecimalFormat("#.00");
        int tclsCount = (int) mapK.get("clsCount") + (int) mapC.get("clsCount") + (int) mapY.get("clsCount");
        int sumcc = (int) mapK.get("sumcc") + (int) mapC.get("sumcc") + (int) mapY.get("sumcc");

        if (tclsCount == 0 && sumcc == 0) {
            mapT.put("tagvStu", df.format(0));

        } else {
            tclsCount = tclsCount == 0 ? 1 : tclsCount;
            sumcc = sumcc == 0 ? 1 : sumcc;

            BigDecimal a = new BigDecimal(sumcc);
            BigDecimal b = new BigDecimal(tclsCount);
            mapT.put("tagvStu", df.format(a.divide(b, 2, BigDecimal.ROUND_HALF_UP).doubleValue()));

        }

        request.setAttribute("departs", departs);

        request.setAttribute("CampusID", CampusID);
        request.setAttribute("type", type);
        request.setAttribute("dateTime", dateTime);

        request.setAttribute("queryClassCountByK", queryClassCountByK);
        request.setAttribute("queryClassCountByC", queryClassCountByC);
        request.setAttribute("queryClassCountByY", queryClassCountByY);

        request.setAttribute("mapK", mapK);
        request.setAttribute("mapC", mapC);
        request.setAttribute("mapY", mapY);
        request.setAttribute("mapT", mapT);

        request.setAttribute("reportInfo", "KPI Summary-开班概况");
        request.setAttribute("date", monthBeginDate.substring(0,10)+"到"+endDate.substring(0,10));

        return "wechar/queryClassCount";
    }
}
