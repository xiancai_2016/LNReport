package com.lerning.edu.manage.controller;

import com.lerning.edu.beans.SysLog;
import com.lerning.edu.beans.SysUser;
import com.lerning.edu.beans.page.PageList;
import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.manage.util.UserUtils;
import com.lerning.edu.services.SysLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by FCHEN on 2016/12/28.
 */
@Controller
@RequestMapping(value = "/sys/log")
public class LogController extends BaseController {

    @Autowired
    private SysLogService logService;

    /**
     * 查询列表
     * @param sysLog
     * @return
     */
    @RequiresPermissions("sys:log:view")
    @RequestMapping(value = {"","list"})
    public String list(SysLog sysLog, HttpServletRequest request){
        request.setAttribute("sysLog",sysLog);
        return "sys/log/list";
    }

    /**
     * 异步获取list资源
     * @param pageList
     * @param sysLog
     * @return
     */
    @RequiresPermissions("sys:log:view")
    @RequestMapping("asynList")
    @ResponseBody
    public PageList asynList(PageList pageList, SysLog sysLog){
        setDate(sysLog);
        SysUser user = UserUtils.getUser();
        if( !user.isAdmin() ) {
            sysLog.setOperator(UserUtils.getUser().getId());
        }
        return logService.queryPage(pageList,sysLog);
    }

    /**
     * 开始结束时间设置
     * @param sysLog
     * @return*/
    private void setDate(SysLog sysLog){
        String serchDate = sysLog.getSerchStratEndDate();
        if ( serchDate != null && !serchDate.isEmpty()) {
            String[] arrays = serchDate.split("-");
            sysLog.setSearchStartTime(DateUtil.dateToString(DateUtil.formateStringToDate(arrays[0].trim(),"MM/dd/yyyy"), Constants.DATE_FORMART));
            sysLog.setSearchEndTime(DateUtil.dateToString(DateUtil.formateStringToDate(arrays[1].trim(),"MM/dd/yyyy"), Constants.DATE_FORMART));
        }
    }
}
