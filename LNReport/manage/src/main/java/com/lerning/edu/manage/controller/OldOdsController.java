package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/2
 */
@Controller
@RequestMapping("oldOds")
public class OldOdsController {

    @Autowired
    private CampusDSRService campusDSRService;

    CampusDSRController cdc = new CampusDSRController();

    /***
     * 到开班概况页面
     *
     * @param request
     * @param CampusID
     * @param key
     * @return
     */
    @RequestMapping("queryClassCount")
    public String queryClassCountIndex(HttpServletRequest request,
                                       @RequestParam(value = "CampusID", required = false) String CampusID,
                                       @RequestParam(value = "key", required = false) String key) {

        if (StringUtil.isEmpty(key)) {
            return "oldods";
        } else {
            key = URLDecoder.decode(key);
            double minute = DateUtil.getDistanceOfTwoMinute(DateUtil.formateStringToDate(key.replace("/","-"), Constants.DATE_FORMART2), new Date());
            if (minute > 15 || minute < 0) {
                return "oldods";
            }
        }

        List departs = campusDSRService.queryDepartList();//校区

        List FinanceMonths = campusDSRService.queryFinanceMonths();//财务月列表

        List FinanceMonthOfDq = campusDSRService.queryFinanceMonthOfDq(cdc.dayMap());//财务月列表
        Map map = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", map.get("AutoID"));

        request.setAttribute("CampusID", CampusID);

        request.setAttribute("departs", departs);

        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("queryClassCountByK", "");
        request.setAttribute("queryClassCountByC", "");
        request.setAttribute("queryClassCountByY", "");

        request.setAttribute("mapK", "");
        request.setAttribute("mapC", "");
        request.setAttribute("mapY", "");
        request.setAttribute("mapT", "");

        request.setAttribute("key", key);

        return "oldOds/queryClassCount";
    }

    /***
     * 开班概况
     *
     * @param request
     * @param CampusID
     * @param AutoID
     * @return
     */
    @RequestMapping("queryClassCountList")
    public String queryClassCount(HttpServletRequest request,
                                  @RequestParam(value = "CampusID", required = false) String CampusID,
                                  @RequestParam(value = "AutoID", required = false) int AutoID,
                                  @RequestParam(value = "key", required = false) String key) {

        if (StringUtil.isEmpty(key)) {
            return "oldods";
        } else {
            key = URLDecoder.decode(key);
            double minute = DateUtil.getDistanceOfTwoMinute(DateUtil.formateStringToDate(key.replace("/","-"), Constants.DATE_FORMART2), new Date());
            if (minute > 15 || minute < 0) {
                return "oldods";
            }
        }

        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryFinanceMonths();//财务月列表
        Map beginDateAndEndDate = campusDSRService.queryFinanceMonth(AutoID);//财务月开始和结束时间

        String endDate = beginDateAndEndDate.get("EndDate").toString();

        //如果不是当前财务月,则返回null
        String newDate = DateUtil.formatDate(new Date(), Constants.DATE_FORMART2);
        if(DateUtil.compare_date(endDate, newDate)){
            request.setAttribute("AutoID", AutoID);
            request.setAttribute("departs", departs);

            request.setAttribute("FinanceMonths", FinanceMonths);
            request.setAttribute("CampusID", CampusID);

            request.setAttribute("queryClassCountByK", "");
            request.setAttribute("queryClassCountByC", "");
            request.setAttribute("queryClassCountByY", "");

            request.setAttribute("mapK", "");
            request.setAttribute("mapC", "");
            request.setAttribute("mapY", "");
            request.setAttribute("mapT", "");

            request.setAttribute("key", key);

            request.setAttribute("dataInfo","暂无数据");

            return "oldOds/queryClassCount";
        }

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("Grade", "");
        map.put("BeginDate", DateUtil.getBeforeDateToString(Constants.DATE_FORMART, 6, DateUtil.formateStringToDate(endDate)) + " 00:00:00.0");
        map.put("EndDate", endDate);
        map.put("BeginDateFoWKB", beginDateAndEndDate.get("BeginDate").toString());

        List queryClassCountByK = new ArrayList();
        List queryClassCountByC = new ArrayList();
        List queryClassCountByY = new ArrayList();

        List queryClassCount = campusDSRService.queryClassCountByK(map);

        if (queryClassCount.size() > 0) {
            for (int i = 0; i < queryClassCount.size(); i++) {
                Map mapKItem = (HashMap) queryClassCount.get(i);
                String grade = mapKItem.get("grade").toString();
                if (grade.equals("K1") || grade.equals("K2") || grade.equals("K3") || grade.equals("K4")) {
                    queryClassCountByK.add(mapKItem);
                } else if (grade.equals("Y1") || grade.equals("Y2") || grade.equals("Y3") || grade.equals("Y4") || grade.equals("Y5") || grade.equals("Y6")) {
                    queryClassCountByY.add(mapKItem);
                } else {
                    queryClassCountByC.add(mapKItem);
                }
            }
        }

        Map mapK = cdc.cls(queryClassCountByK);
        Map mapC = cdc.cls(queryClassCountByC);
        Map mapY = cdc.cls(queryClassCountByY);

        Map mapT = new HashMap();

        mapT.put("tacc", (int) mapK.get("acc") + (int) mapC.get("acc") + (int) mapY.get("acc"));
        mapT.put("tacc1", (int) mapK.get("acc1") + (int) mapC.get("acc1") + (int) mapY.get("acc1"));
        mapT.put("tacc2", (int) mapK.get("acc2") + (int) mapC.get("acc2") + (int) mapY.get("acc2"));
        mapT.put("tacc3", (int) mapK.get("acc3") + (int) mapC.get("acc3") + (int) mapY.get("acc3"));
        mapT.put("tacc4", (int) mapK.get("acc4") + (int) mapC.get("acc4") + (int) mapY.get("acc4"));
        mapT.put("tclsCount", (int) mapK.get("clsCount") + (int) mapC.get("clsCount") + (int) mapY.get("clsCount"));
        mapT.put("tsumcc", (int) mapK.get("sumcc") + (int) mapC.get("sumcc") + (int) mapY.get("sumcc"));
        mapT.put("twkbCount", (int) mapK.get("wkbCount") + (int) mapC.get("wkbCount") + (int) mapY.get("wkbCount"));
        mapT.put("twkbClsCount", (int) mapK.get("wkbClsCount") + (int) mapC.get("wkbClsCount") + (int) mapY.get("wkbClsCount"));
        mapT.put("txuCount", (int) mapK.get("xuCount") + (int) mapC.get("xuCount") + (int) mapY.get("xuCount"));
        mapT.put("thJCount", (int) mapK.get("hJCount") + (int) mapC.get("hJCount") + (int) mapY.get("hJCount"));
        DecimalFormat df = new DecimalFormat("#.00");
        int tclsCount = (int) mapK.get("clsCount") + (int) mapC.get("clsCount") + (int) mapY.get("clsCount");
        int sumcc = (int) mapK.get("sumcc") + (int) mapC.get("sumcc") + (int) mapY.get("sumcc");

        if (tclsCount == 0 && sumcc == 0) {
            mapT.put("tagvStu", df.format(0));

        } else {
            tclsCount = tclsCount == 0 ? 1 : tclsCount;
            sumcc = sumcc == 0 ? 1 : sumcc;

            BigDecimal a = new BigDecimal(sumcc);
            BigDecimal b = new BigDecimal(tclsCount);
            mapT.put("tagvStu", df.format(a.divide(b, 2, BigDecimal.ROUND_HALF_UP).doubleValue()));

        }

        request.setAttribute("AutoID", AutoID);
        request.setAttribute("departs", departs);

        request.setAttribute("FinanceMonths", FinanceMonths);
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("queryClassCountByK", queryClassCountByK);
        request.setAttribute("queryClassCountByC", queryClassCountByC);
        request.setAttribute("queryClassCountByY", queryClassCountByY);

        request.setAttribute("mapK", mapK);
        request.setAttribute("mapC", mapC);
        request.setAttribute("mapY", mapY);
        request.setAttribute("mapT", mapT);

        request.setAttribute("key", key);

        request.setAttribute("dataInfo","");

        return "oldOds/queryClassCount";
    }
}
