/**
 * Copyright &copy; 2012-2014 All rights reserved.
 */
package com.lerning.edu.manage.util;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.lerning.edu.beans.SysLog;
import com.lerning.edu.commons.util.SpringContextHolder;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.SysResourceService;
import com.lerning.edu.beans.SysResource;
import com.lerning.edu.beans.SysUser;
import com.lerning.edu.commons.util.CacheUtils;
import com.lerning.edu.commons.util.Exceptions;
import com.lerning.edu.manage.context.GlobalContext;
import com.lerning.edu.services.SysLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.method.HandlerMethod;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 字典工具类
 * @author FCHEN
 * @version 2014-11-7
 */
public class LogUtils {

	public static final String CACHE_MENU_NAME_PATH_MAP = "menuNamePathMap";

	private static SysLogService logService = SpringContextHolder.getBean(SysLogService.class);
	private static SysResourceService resourceService = SpringContextHolder.getBean(SysResourceService.class);

	/**
	 * 保存日志
	 */
	public static void saveLog(HttpServletRequest request, String title){
		saveLog(request, null, null, title);
	}

	/**
	 * 保存日志
	 */
	public static void saveLog(HttpServletRequest request, Object handler, Exception ex, String title){
		SysUser user = UserUtils.getUser();
		if (user != null && user.getId() != null){
			SysLog log = new SysLog();
			log.setTitle(title);
			log.setOperator(user.getId());
			log.setType(ex == null ? SysLog.TYPE_ACCESS : SysLog.TYPE_EXCEPTION);
			log.setRemoteAddr(StringUtil.getRemoteAddr(request));
			log.setUserAgent(request.getHeader("user-agent"));
			log.setRequestUri(request.getRequestURI());
			log.setParams(request.getParameterMap());
			log.setMethod(request.getMethod());
			// 异步保存日志
			new SaveLogThread(log, handler, ex).start();
		}
	}

	/**
	 * 保存日志线程
	 */
	public static class SaveLogThread extends Thread{

		private SysLog log;
		private Object handler;
		private Exception ex;

		public SaveLogThread(SysLog log, Object handler, Exception ex){
			super(SaveLogThread.class.getSimpleName());
			this.log = log;
			this.handler = handler;
			this.ex = ex;
		}

		@Override
		public void run() {
			// 获取日志标题
			if (StringUtil.isBlank(log.getTitle())){
				String permission = "";
				if (handler instanceof HandlerMethod){
					Method m = ((HandlerMethod)handler).getMethod();
					RequiresPermissions rp = m.getAnnotation(RequiresPermissions.class);
					permission = (rp != null ? StringUtil.join(rp.value(), ",") : "");
				}
				log.setTitle(getMenuNamePath(log.getRequestUri(), permission));
			}
			// 如果有异常，设置异常信息
			log.setException(Exceptions.getStackTraceAsString(ex));
			// 如果无标题并无异常日志，则不保存信息
			if (StringUtil.isBlank(log.getTitle()) && StringUtil.isBlank(log.getException())){
				return;
			}
			// 保存日志信息
			logService.add(log);
		}
	}

	/**
	 * 获取菜单名称路径（如：系统设置-机构用户-用户管理-编辑）
	 */
	public static String getMenuNamePath(String requestUri, String permission){
		String href = StringUtil.substringAfter(requestUri, GlobalContext.getAdminPath());
		Map<String, String> menuMap = (Map<String, String>) CacheUtils.get(CACHE_MENU_NAME_PATH_MAP);
		if (menuMap == null){
			menuMap = Maps.newHashMap();
			List<SysResource> menuList = resourceService.findAllList(new SysResource());
			for (SysResource menu : menuList){
				// 获取菜单名称路径（如：系统设置-机构用户-用户管理-编辑）
				String namePath = "";
				if (menu.getParentIds() != null){
					List<String> namePathList = Lists.newArrayList();
					for (String id : StringUtil.split(menu.getParentIds(), ",")){
						if (SysResource.getRootId().equals(id)){
							continue; // 过滤跟节点
						}
						for (SysResource m : menuList){
							if (m.getId().toString().equals(id)){
								namePathList.add(m.getName());
								break;
							}
						}
					}
					namePathList.add(menu.getName());
					namePath = StringUtil.join(namePathList, "-");
				}
				// 设置菜单名称路径
				if (StringUtil.isNotBlank(menu.getUrl())){
					menuMap.put(menu.getUrl(), namePath);
				}else if (StringUtil.isNotBlank(menu.getPermission())){
					for (String p : StringUtil.split(menu.getPermission())){
						menuMap.put(p, namePath);
					}
				}

			}
			CacheUtils.put(CACHE_MENU_NAME_PATH_MAP, menuMap);
		}
		String menuNamePath = menuMap.get(href);
		if (menuNamePath == null){
			for (String p : StringUtil.split(permission)){
				menuNamePath = menuMap.get(p);
				if (StringUtil.isNotBlank(menuNamePath)){
					break;
				}
			}
			if (menuNamePath == null){
				return "";
			}
		}
		return menuNamePath;
	}
}
