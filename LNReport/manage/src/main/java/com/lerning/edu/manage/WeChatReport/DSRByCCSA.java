package com.lerning.edu.manage.WeChatReport;

import com.alibaba.fastjson.JSON;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import com.lerning.edu.services.DSRByCCSAService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/22
 */
@Controller
@RequestMapping("wechat")
public class DSRByCCSA {

    @Autowired
    public DSRByCCSAService dsrByCCSAService;
    @Autowired
    public CampusDSRService campusDSRService;

    @RequestMapping("/ccsa")
    public String ccsa(HttpServletRequest request,
                       @RequestParam(value = "CampusID", required = false) String CampusID,
                       @RequestParam(value = "dateTime", required = false) String dateTime,
                       @RequestParam(value = "account", required = false) String account){

        List departs = new ArrayList();

        String isAll = "";

        Map dateMap = new HashMap();
        dateMap.put("dqDay", dateTime.subSequence(0, 7));

        dateMap.put("cField1", account);

        if(StringUtil.isEmpty(account)|| account.equals("michelle")|| account.equals("Michael")|| account.equals("admin")){
            departs = campusDSRService.queryDepartList();//校区
            isAll = "Y";
        }else {
            List queryUserOfDeparts = campusDSRService.queryUserOfDeparts(dateMap);

            if(queryUserOfDeparts.size() > 0){
                Map mapUser = (HashMap)queryUserOfDeparts.get(0);
                if(!mapUser.get("cStatus").toString().equals("1")){
                    return "oldods";
                }
            }

            if(queryUserOfDeparts.size() > 0){
                for (Object obj : queryUserOfDeparts){
                    Map ojbMap = (HashMap)obj;
                    if(ojbMap.get("cLevelString").toString().equals("|14D10F0F-0C58-4270-A1C8-5C64B3C5C03E|") || ojbMap.get("cLevelString").toString().equals("|")){
                        departs = campusDSRService.queryDepartList();//校区
                        isAll = "Y";
                        break;
                    }else{
                        Map cmap = new HashMap();
                        cmap.put("campusID", ojbMap.get("cLevelString").toString().replace("|",""));
                        departs.add(campusDSRService.queryDepartByEmployee(cmap));

                        if(StringUtil.isEmpty(CampusID)){
                            CampusID = ojbMap.get("cLevelString").toString().replace("|","");
                        }else {
                            CampusID = CampusID + "," + ojbMap.get("cLevelString").toString().replace("|","");
                        }
                        isAll = "N";
                    }
                }
            }
        }

        List FinanceMonthOfDq = campusDSRService.queryNatureMonthOfDq(dateMap);//当前月
        Map map = (HashMap) FinanceMonthOfDq.get(0);

        dateMap.put("BeginDate", map.get("BeginDate").toString());
        dateMap.put("EndDate",dateTime);

        List CampusIDS = new ArrayList();

        if (!StringUtil.isEmpty(CampusID)) {
            if (CampusID.substring(0, 1).equals(",")) {
                CampusID = CampusID.substring(1, CampusID.length());
            }
            String[] cIDs = CampusID.split(",");

            for (int i = 0; i < cIDs.length; i++) {
                CampusIDS.add(cIDs[i]);
            }
        } else {
            CampusID = "";
        }

        Map param = new HashMap();
        param.put("CampusID", CampusID);
        param.put("dateTime", dateTime);

        List dsrByCCSA = dsrByCCSAService.queryCCSAList(param);
        List cNames = new ArrayList();

        List MKT = new ArrayList();
        List REF = new ArrayList();
        List RENEW = new ArrayList();

        DecimalFormat df = new DecimalFormat("0");
        DecimalFormat df1 = new DecimalFormat("0.0");

        if(dsrByCCSA.size() > 0){

            StringUtil.sortByAscs(dsrByCCSA,"reachRatio","cName", false, false);

            for (Object obj : dsrByCCSA){
                Map objMap = (HashMap)obj;

                String tin = "MKT:"+df1.format(objMap.get("MKT"))+"/"+df1.format(objMap.get("MKTKPI"));
                String tout = "REF:"+df1.format(objMap.get("REF"))+"/"+df1.format(objMap.get("REFKPI"));
                String win = "RENEW:"+df1.format(objMap.get("RENEW"))+"/"+df1.format(objMap.get("RENEWKPI"));

                double reachRatio = Math.ceil(Double.valueOf(objMap.get("reachRatio").toString()));
                cNames.add("'"+objMap.get("cName")+"\\n"+objMap.get("departName")+"\\n"+"总达成:"+df.format(reachRatio)+"%"+"\\n"+tin+"\\n"+tout+"\\n"+win+"'");
                MKT.add(objMap.get("mktReachRatio"));
                REF.add(objMap.get("refReachRatio"));
                RENEW.add(objMap.get("renewReachRatio"));
            }
        }

        int heightValue = dsrByCCSA.size() * 120;

        if(dsrByCCSA.size() < 4){
            heightValue = dsrByCCSA.size() * 140;
        }

        //显示数字
        Map m = new HashMap();

        Map mm = new HashMap();
        mm.put("show",true);
        mm.put("formatter", "{c}%");
        mm.put("position","insideLeft");
        m.put("normal",mm);

//        //颜色Kkt
//        Map colorMkt = new HashMap();
//        colorMkt.put("color","#438EB9");
//        Map color1 = new HashMap();
//        color1.put("normal", colorMkt);
//
//        //颜色
//        Map colorRfe = new HashMap();
//        colorRfe.put("color","#FFCC33");
//        Map ff = new HashMap();
//        ff.put("normal", colorRfe);
//
//        //颜色
//        Map colorRenew = new HashMap();
//        colorRenew.put("color","#66CCCC");
//        Map nn = new HashMap();
//        nn.put("normal", colorRenew);


        Map mkt = new HashMap();
        mkt.put("data",MKT);
        mkt.put("type","bar");
        mkt.put("name", "MKT");
        mkt.put("label",m);
//        mkt.put("itemStyle",color1);

        Map ref = new HashMap();
        ref.put("data",REF);
        ref.put("type","bar");
        ref.put("name", "REF");
        ref.put("label",m);
//        ref.put("itemStyle",ff);

        Map renew = new HashMap();
        renew.put("data",RENEW);
        renew.put("type","bar");
        renew.put("name", "RENEW");
        renew.put("label",m);
//        renew.put("itemStyle",nn);

        List pageDate = new ArrayList();

        pageDate.add(JSON.toJSONString(mkt));
        pageDate.add(JSON.toJSONString(ref));
        pageDate.add(JSON.toJSONString(renew));

        request.setAttribute("departs", departs);

        StringUtil.sortByAscs(dsrByCCSA,"reachRatio","cName", true, true);
        request.setAttribute("dsrByCCSA", dsrByCCSA);

        request.setAttribute("dateTime", dateTime);
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("cNames", cNames);

        request.setAttribute("pageDate", pageDate);

        request.setAttribute("heightValue", heightValue);
        request.setAttribute("isAll", isAll);
        request.setAttribute("account", account);

        request.setAttribute("reportInfo", "CC/SA业绩达成排行");
        request.setAttribute("date", map.get("BeginDate").toString().substring(0,10)+"到"+dateTime);

        return "wechar/ccsa";
    }
}
