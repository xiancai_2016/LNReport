/**
 * Copyright &copy; 2012-2014 All rights reserved.
 */
package com.lerning.edu.manage.security;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.lerning.edu.beans.SysResource;
import com.lerning.edu.beans.SysRole;
import com.lerning.edu.beans.SysUser;
import com.lerning.edu.commons.util.SpringContextHolder;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.manage.common.web.Servlets;
import com.lerning.edu.manage.context.GlobalContext;
import com.lerning.edu.manage.util.LogUtils;
import com.lerning.edu.manage.util.UserUtils;
import com.lerning.edu.manage.util.Utils;
import com.lerning.edu.manage.common.security.shiro.session.CacheSessionDAO;
import com.lerning.edu.manage.common.security.shiro.session.SessionDAO;
import com.lerning.edu.manage.servlet.ValidateCodeServlet;
import com.lerning.edu.services.SysUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * 系统安全认证实现类
 * @author FCHEN
 * @version 2014-7-5
 */
@Service
public class SystemAuthorizingRealm extends AuthorizingRealm {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
//	private SystemService systemService;

	private SysUserService sysUserService;

	private SessionDAO sessionDAO;

	/**
	 * 认证回调函数, 登录时调用
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;

		int activeSessionSize = getSessionDao().getActiveSessions(false).size();
		if (logger.isDebugEnabled()){
			logger.debug("login submit, active session size: {}, username: {}", activeSessionSize, token.getUsername());
		}
		if ( StringUtils.isBlank(token.getUsername()) ) {
			throw new AuthenticationException("msg:用户名为空, 请重试.");
		}
		// 校验登录验证码
		if (Utils.isValidateCodeLogin(token.getUsername(), false, false)){
			Session session = UserUtils.getSession();
			String code = (String)session.getAttribute(ValidateCodeServlet.VALIDATE_CODE);
			if (token.getCaptcha() == null || !token.getCaptcha().toUpperCase().equals(code)){
				throw new AuthenticationException("msg:验证码错误, 请重试.");
			}
		}
		// 校验用户名密码
		SysUser user = getSystemService().queryUserByAccount(token.getUsername());
		if ( user != null ) {
			if ( user.getDeleted() ) {
				throw new AuthenticationException("msg:该已帐号禁止登录.");
			}
			token.setSalt(user.getSalt());
			return new SimpleAuthenticationInfo(new Principal(user, token.isMobileLogin()),
					user.getPwd(), ByteSource.Util.bytes(user.getSalt()), getName());
		} else {
			return null;
		}
	}

	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		Principal principal = (Principal) getAvailablePrincipal(principals);
		// 获取当前已登录的用户
		if (!GlobalContext.TRUE.equals(GlobalContext.getProperties("user.multiAccountLogin"))){
			Collection<Session> sessions = getSessionDao().getActiveSessions(true, principal, UserUtils.getSession());
			if (sessions.size() > 0){
				// 如果是登录进来的，则踢出已在线用户
				if (UserUtils.getSubject().isAuthenticated()){
					for (Session session : sessions){
						getSessionDao().delete(session);
					}
				}
				// 记住我进来的，并且当前用户已登录，则退出当前用户提示信息。
				else{
					UserUtils.getSubject().logout();
					throw new AuthenticationException("msg:账号已在其它地方登录，请重新登录。");
				}
			}
		}
		SysUser user = getSystemService().queryUserByAccount(principal.getLoginName());
		if (user != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			List<SysResource> list = UserUtils.getMenuList();
			for (SysResource resource : list){
				if (StringUtils.isNotBlank(resource.getPermission())){
					// 添加基于Permission的权限信息
					for (String permission : StringUtils.split(resource.getPermission(),",")){
						info.addStringPermission(permission);
					}
				}
			}
//			// 添加用户权限
			info.addStringPermission("user");
//			// 添加用户角色信息
			for (SysRole role : user.getRoleList()){
				info.addRole(role.getName());
			}
			// 更新登录IP和时间
			user.setLoginIp(StringUtil.getRemoteAddr(Servlets.getRequest()));
			getSystemService().updateLoginTimes(user);
//			// 记录登录日志
			LogUtils.saveLog(Servlets.getRequest(), "系统登录");
			return info;
		} else {
			return null;
		}
	}
	
	@Override
	protected void checkPermission(Permission permission, AuthorizationInfo info) {
		authorizationValidate(permission);
		super.checkPermission(permission, info);
	}
	
	@Override
	protected boolean[] isPermitted(List<Permission> permissions, AuthorizationInfo info) {
		if (permissions != null && !permissions.isEmpty()) {
            for (Permission permission : permissions) {
        		authorizationValidate(permission);
            }
        }
		return super.isPermitted(permissions, info);
	}
	
	@Override
	public boolean isPermitted(PrincipalCollection principals, Permission permission) {
		authorizationValidate(permission);
		return super.isPermitted(principals, permission);
	}
	
	@Override
	protected boolean isPermittedAll(Collection<Permission> permissions, AuthorizationInfo info) {
		if (permissions != null && !permissions.isEmpty()) {
            for (Permission permission : permissions) {
            	authorizationValidate(permission);
            }
        }
		return super.isPermittedAll(permissions, info);
	}
	
	/**
	 * 授权验证方法
	 * @param permission
	 */
	private void authorizationValidate(Permission permission){
		// 模块授权预留接口
	}
	
	/**
	 * 设定密码校验的Hash算法与迭代次数
	 */
	@PostConstruct
	public void initCredentialsMatcher() {
		setCredentialsMatcher(new CustomCredentialsMatcher());
	}

	/**
	 * 清空所有关联认证
	 * @Deprecated 不需要清空，授权缓存保存到session中
	 */
	@Deprecated
	public void clearAllCachedAuthorizationInfo() {
//		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
//		if (cache != null) {
//			for (Object key : cache.keys()) {
//				cache.remove(key);
//			}
//		}
	}

	/**
	 * 获取系统业务对象
	 */
	public SysUserService getSystemService() {
		if (sysUserService == null){
			sysUserService = SpringContextHolder.getBean(SysUserService.class);
		}
		return sysUserService;
	}

	public SessionDAO getSessionDao() {
		if (sessionDAO == null){
			sessionDAO = new CacheSessionDAO();
		}
		return sessionDAO;
	}
	
	/**
	 * 授权用户信息
	 */
	public static class Principal implements Serializable {

		private static final long serialVersionUID = 1L;
		
		private Long id; // 编号
		private String loginName; // 登录名
		private String name; // 姓名
		private boolean mobileLogin; // 是否手机登录
		
//		private Map<String, Object> cacheMap;

		public Principal(SysUser user, boolean mobileLogin) {
			this.id = user.getId();
			this.loginName = user.getAccount();
			this.name = user.getName();
			this.mobileLogin = mobileLogin;
		}

		public Long getId() {
			return id;
		}

		public String getLoginName() {
			return loginName;
		}

		public String getName() {
			return name;
		}

		public boolean isMobileLogin() {
			return mobileLogin;
		}

		/**
		 * 获取SESSIONID
		 */
		public String getSessionid() {
			try{
				return (String) UserUtils.getSession().getId();
			}catch (Exception e) {
				return "";
			}
		}
		
		@Override
		public String toString() {
			return id+"";
		}

	}
}
