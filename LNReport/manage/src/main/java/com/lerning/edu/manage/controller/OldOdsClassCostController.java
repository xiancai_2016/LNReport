package com.lerning.edu.manage.controller;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CampusDSRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/6
 */
@Controller
@RequestMapping("oldOds")
public class OldOdsClassCostController {

    @Autowired
    private CampusDSRService campusDSRService;

    CampusDSRController cdc = new CampusDSRController();

    /***
     * 到报名退费页面
     *
     * @param request
     * @param CampusID
     * @param key
     * @return
     */
    @RequestMapping("totalCAR")
    public String totalCARIndex(HttpServletRequest request,
                                @RequestParam(value = "CampusID", required = false) String CampusID,
                                @RequestParam(value = "key", required = false) String key) {

        if (StringUtil.isEmpty(key)) {
            return "oldods";
        } else {
            key = URLDecoder.decode(key);
            double minute = DateUtil.getDistanceOfTwoMinute(DateUtil.formateStringToDate(key.replace("/","-"), Constants.DATE_FORMART2), new Date());
            if (minute > 15 || minute < 0) {
                return "oldods";
            }
        }

        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryFinanceMonths();//财务月列表

        List FinanceMonthOfDq = campusDSRService.queryFinanceMonthOfDq(DateUtil.dayMap());//财务月列表
        Map map = (HashMap) FinanceMonthOfDq.get(0);
        request.setAttribute("AutoID", map.get("AutoID"));
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("departs", departs);
        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("chargeAndRefundByK", "");
        request.setAttribute("chargeAndRefundByC", "");
        request.setAttribute("chargeAndRefundByY", "");

        request.setAttribute("mapK", "");
        request.setAttribute("mapC", "");
        request.setAttribute("mapY", "");

        request.setAttribute("totalChargeAndRefund", "");
        request.setAttribute("key", key);

        return "oldOds/totalCAR";
    }

    /***
     * 报名退费
     *
     * @param request
     * @param CampusID
     * @param AutoID
     * @return
     */
    @RequestMapping("totalCARList")
    public String totalCAR(HttpServletRequest request,
                           @RequestParam(value = "CampusID", required = false) String CampusID,
                           @RequestParam(value = "AutoID", required = false) int AutoID,
                           @RequestParam(value = "key", required = false) String key) {

        if (StringUtil.isEmpty(key)) {
            return "oldods";
        } else {
            key = URLDecoder.decode(key);
            double minute = DateUtil.getDistanceOfTwoMinute(DateUtil.formateStringToDate(key.replace("/","-"), Constants.DATE_FORMART2), new Date());
            if (minute > 15 || minute < 0) {
                return "oldods";
            }
        }

        List departs = campusDSRService.queryDepartList();//校区
        List FinanceMonths = campusDSRService.queryFinanceMonths();//财务月列表
        Map beginDateAndEndDate = campusDSRService.queryFinanceMonth(AutoID);//财务月开始和结束时间

        Map map = new HashMap();
        map.put("CampusID", CampusID);
        map.put("BeginDate", beginDateAndEndDate.get("BeginDate").toString());
        map.put("EndDate", beginDateAndEndDate.get("EndDate").toString());
        map.put("Grade", "");

        NumberFormat nf = new DecimalFormat("￥,###.##");

        List chargeAndRefund = campusDSRService.chargeAndRefund(map);

        List chargeAndRefundByK = new ArrayList();
        List chargeAndRefundByC = new ArrayList();
        List chargeAndRefundByY = new ArrayList();

        if (chargeAndRefund.size() > 0) {
            //汇总
            for (int i = 0; i < chargeAndRefund.size(); i++) {
                Map mapKItem = (HashMap) chargeAndRefund.get(i);
                String grade = mapKItem.get("tgrade").toString();
                if (grade.equals("K1") || grade.equals("K2") || grade.equals("K3") || grade.equals("K4")) {
                    chargeAndRefundByK.add(mapKItem);
                } else if (grade.equals("Y1") || grade.equals("Y2") || grade.equals("Y3") || grade.equals("Y4") || grade.equals("Y5") || grade.equals("Y6")) {
                    chargeAndRefundByY.add(mapKItem);
                } else {
                    chargeAndRefundByC.add(mapKItem);
                }
            }
        }

        Map mapK = cdc.Amount(chargeAndRefundByK);
        Map mapC = cdc.Amount(chargeAndRefundByC);
        Map mapY = cdc.Amount(chargeAndRefundByY);

        map.put("cFlag", 1);
        Map mapSKC = campusDSRService.querySCKCount(map);
        map.put("cFlag", -1);
        Map maprSKC = campusDSRService.querySCKCount(map);
        maprSKC.put("skcrFact", maprSKC.get("skcrFact").toString().replace("-", ""));

        Map SKC = new HashMap();
        SKC.put("skcFact", nf.format(Double.valueOf(mapSKC.get("skcrFact").toString())));
        SKC.put("skcrFact", nf.format(Double.valueOf(maprSKC.get("skcrFact").toString())));

        Map totalChargeAndRefund = campusDSRService.totalChargeAndRefund(map);
        totalChargeAndRefund.put("sumtcost", nf.format(Double.valueOf(totalChargeAndRefund.get("sumtcost").toString()) + Double.valueOf(mapSKC.get("skcrFact").toString())));
        totalChargeAndRefund.put("sumrtcost", nf.format(Double.valueOf(totalChargeAndRefund.get("sumrtcost").toString()) + Double.valueOf(maprSKC.get("skcrFact").toString())));


        request.setAttribute("AutoID", AutoID);
        request.setAttribute("CampusID", CampusID);

        request.setAttribute("departs", departs);
        request.setAttribute("FinanceMonths", FinanceMonths);

        request.setAttribute("chargeAndRefundByK", cdc.changeType(chargeAndRefundByK));
        request.setAttribute("chargeAndRefundByC", cdc.changeType(chargeAndRefundByC));
        request.setAttribute("chargeAndRefundByY", cdc.changeType(chargeAndRefundByY));

        request.setAttribute("mapK", mapK);
        request.setAttribute("mapC", mapC);
        request.setAttribute("mapY", mapY);

        request.setAttribute("SKC", SKC);

        request.setAttribute("totalChargeAndRefund", totalChargeAndRefund);

        request.setAttribute("key", key);

        return "oldOds/totalCAR";
    }

}
