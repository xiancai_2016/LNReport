package com.lerning.edu.manage.API;

import com.alibaba.fastjson.JSONObject;
import com.lerning.edu.commons.util.DateUtil;
import com.lerning.edu.commons.util.StringUtil;
import com.lerning.edu.services.CallTheRollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/27
 */
@Controller
@RequestMapping("api")
public class CallTheRollController {
    @Autowired
    public CallTheRollService callTheRollService;

    @RequestMapping("/callTheRoll")
    @ResponseBody
    public JSONObject CallTheRoll(@RequestParam(value = "cSMSTel", required = false)String cSMSTel,
                                  @RequestParam(value = "recordTime", required = false)String recordTime){

        Map map = new HashMap<>();
        map.put("tel", cSMSTel);
        if(StringUtil.isEmpty(recordTime)){
            recordTime = DateUtil.formatDateTime(new Date());
        }
        map.put("startTime", DateUtil.addHour(recordTime, -1));
        map.put("endTime", DateUtil.addHour(recordTime, 1));

        List studentInfo = callTheRollService.queryStudentInfo(map);

        if(studentInfo.size() > 0){
            for (Object obj : studentInfo){
                Map objMap = (HashMap)obj;
                int addCallTheRoll = callTheRollService.addCallTheRoll(objMap);

                if(addCallTheRoll > 0){
                    //保存
                }
            }
        }

        JSONObject jsonObject = new JSONObject();
        return jsonObject;
    }

    //1.定时扫描时间段,上课的班级
    //2.从摄像头数据库里抓取数据
    //3.
}
