/**
 * For com.football
 * Copyright [2015/11/16] By RICK
 */
jQuery.extend(jQuery.validator.messages, {
    required: "必填字段",
    remote: "字段内容已存在",
    email: "请输入正确格式的电子邮件",
    url: "请输入合法的网址",
    date: "请输入合法的日期",
    dateISO: "请输入合法的日期 (ISO).",
    number: "请输入合法的数字",
    digits: "只能输入整数",
    creditcard: "请输入合法的信用卡号",
    equalTo: "请再次输入相同的值",
    accept: "请输入合法的后缀名",
    maxlength: jQuery.validator.format("输入长度不能超过 {0}"),
    minlength: jQuery.validator.format("输入长度必须大于等于 {0} "),
    rangelength: jQuery.validator.format("输入长度必须介于 {0} 和 {1} 之间"),
    range: jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
    max: jQuery.validator.format("最大值为{0} "),
    min: jQuery.validator.format("最小值为{0} ")
});