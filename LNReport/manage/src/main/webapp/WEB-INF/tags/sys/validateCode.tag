<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/views/common/taglib.jsp"%>
<%@ attribute name="name" type="java.lang.String" required="true" description="验证码输入框名称"%>
<%@ attribute name="placeholder" type="java.lang.String" required="true" description="验证码"%>
<%@ attribute name="inputCssStyle" type="java.lang.String" required="false" description="验证框样式"%>
<%@ attribute name="imageCssStyle" type="java.lang.String" required="false" description="验证码图片样式"%>
<%@ attribute name="buttonCssStyle" type="java.lang.String" required="false" description="看不清按钮样式"%>
<input type="text" id="${name}" name="${name}" placeholder="${placeholder}" maxlength="5" class="${inputCssStyle}" style="width: 150px; height: 30px;"/>
<img src="${GLOBAL.basePath}/servlet/validateCodeServlet" onclick="$('.${name}Refresh').click();" class="mid ${name}" style="${imageCssStyle}"/>
<a href="javascript:" onclick="$('.${name}').attr('src','${GLOBAL.basePath}/servlet/validateCodeServlet?'+new Date().getTime());" class="mid ${name}Refresh" style="${buttonCssStyle}">看不清</a>