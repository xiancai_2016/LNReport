<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/myBase_body_header.jsp"%>
<%@include file="../common/myNavbar.jsp"%>
<%--<%@include file="../common/myPage_content_pre.jsp"%>--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.css"/>


<style type="text/css">
    body {
        font-family: "微软雅黑";
        background: #eee;
    }
</style>

<div class="page-content">
    <div class="row">
        <div>&nbsp;</div>
        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="list-table" class="table table-striped table-hover" width="100%" style="font-size: 14px;">
                    <thead>
                    <tr style="color: #356FBE; background: #E3F0F4">
                        <th>单据号</th>
                        <th>校区</th>
                        <th>学员姓名</th>
                        <th>申请时间</th>
                        <th>申请人</th>
                        <th>申请类型</th>
                        <th>当前操作人</th>
                        <th>当前进度</th>
                        <th>是否打印</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<%@include file="../common/page_content_suf.jsp"%>
<%@include file="../common/myScript.jsp"%>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>

<script type="text/javascript">
    var workOrderListTable;
    jQuery(function($) {
        workOrderListTable = $('#list-table').dataTable( {
            "sAjaxSource": "${GLOBAL.basePath}/workOrderChangeSchool/asnyPrintList",
            "fnServerData":function(sSource, aDataSet, fnCallback){
                $.ajax({
                    "dataType" : 'json',
                    "type" : "POST",
                    "url" : sSource,
                    "data" : aDataSet,
                    "success" : fnCallback
                });
            },
            "fnServerParams": function ( aoData ) {//向服务器传额外的参数
                <c:if test="${not empty aoData}">
                aoData.push( ${aoData});
                </c:if>
            },
            "bSort":false,
            "bFilter":false,
            "bServerSide":true,
            "bProcessing":false,
            "bLengthChange":false,
            "bSearching":false,
            "sProcessing": "${GLOBAL.staticImagePath}/loading.gif' />",
            "oLanguage":{
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条",
                "sInfoEmpty": "No data"
            },
            "aoColumns": [
                { "mDataProp":"corderno","mRender": function (data, type, full) {
                        return "<a href=${GLOBAL.basePath}/workOrderChangeSchool/detail?orderNo="+data+">"+data+"</a>";
                }
                },
                { "mDataProp":"campus", "sWidth": "100px"},
                { "mDataProp":"userName", "sWidth": "100px"},
                { "mDataProp":"ccreatedate", "sWidth": "200px"},
                { "mDataProp":"subName", "sWidth": "100px"},
                {
                    "mDataProp": "ctype", "sWidth": "100px", "mRender": function (data, type, full) {
                    if (data == 1) {
                        return "停课";
                    }else if (data == 2){
                        return "复课";
                    }else if (data == 3){
                        return "转班";
                    }else if (data == 4){
                        return "转校";
                    }else if (data == 5){
                        return "退费";
                    }else if (data == 6){
                        return "转费";
                    }
                }
                },
                { "mDataProp":"employName", "sWidth": "100px"},
                {
                    "mDataProp": "cstatus", "sWidth": "100px", "mRender": function (data, type, full) {
                    if (data == 1) {
                        return "未审核";
                    } else if (data == 2){
                        return "审核通过";
                    }else if (data == 3){
                        return "驳回申请";
                    }else if (data == 4){
                        return "操作成功";
                    }else if (data == 5){
                        return "无法操作";
                    }
                }
                },
                {
                    "mDataProp": "cisprint", "sWidth": "100px", "mRender": function (data, type, full) {
                    if (data == 2) {
                        return "未打印";
                    } else if (data == 1){
                        return "已打印";
                    }
                }
                }
            ]
        });
    });
</script>
