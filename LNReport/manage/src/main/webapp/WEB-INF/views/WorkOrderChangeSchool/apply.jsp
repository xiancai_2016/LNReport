<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/myBase_body_header.jsp" %>
<%@include file="../common/myNavbar.jsp" %>
<%--<%@include file="../common/myPage_content_pre.jsp" %>--%>

<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.css"/>

<style type="text/css">
    .input {
        border-color: #E4E4E4;
        border-style: solid;
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 1px;
        border-left-width: 0px;
        width: 65%
    }

    .fount {
        color: #333333;
        font-size: 16px;
        font-weight: bold;
    }

    input::-webkit-input-placeholder {
        color: #C9C9C9;
    }

    input::-moz-placeholder { /* Mozilla Firefox 19+ */
        color: #C9C9C9;
    }

    input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
        color: #C9C9C9;
    }

    input:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #C9C9C9;
    }

    * {
        margin: 0;
        padding: 0;
    }

    body {
        font-family: "微软雅黑";
        background: #eee;
    }

    button {
        display: inline-block;
        padding: 6px 12px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        border: 1px solid transparent;
        border-radius: 4px;
        border-color: #28a4c9;
        color: #fff;
        background-color: #5bc0de;
        margin: 20px 20px 0 0;
    }

    .page {
        width: 700px;
    }

    .fl {
        float: left;
    }
</style>

<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<div class="page-content">

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header" style="color: #737373">
                    <div class="form-group">
                        <div class="col-sm-3"><span></span></div>

                        <div class="col-sm-3"><span id="proposer"></span></div>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form:form class="form-horizontal" modelAttribute="workOrderOfResume"
                                   action="${GLOBAL.basePath}/workOrderChangeSchool/save" method="post"
                                   id="workOrderOfResumeForm">
                            <div>&nbsp;</div>
                            <input type="hidden" name="classID" id = "classID">
                            <input type="hidden" name="userId" id="userById">
                            <input type="hidden" name="crecoverydate" id="crecoverydate">
                            <input type="hidden" name="resumeClassID" id="resumeClassID">
                            <input type="hidden" name = "tEmployeeID" id = "tEmployeeID" value="${tEmployeeID}">

                            <div id="classAndSchoolDate"></div>

                            <fieldset>
                                <p>
                                    <label class="fount">转校学员</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <span class="help-inline"><font color="red">*</font> </span>学员姓名:
                                        <input class="input" data-toggle="modal" id="studentName"
                                               readonly=readonly
                                               data-target="#chooseStudent" placeholder="请选择学员"/>
                                    </div>

                                    <div class="col-sm-3">
                                        学号:
                                        <input class="input" readonly=readonly id="studentNo"/>
                                    </div>

                                    <div class="col-sm-3">
                                        电话:
                                        <input class="input" readonly=readonly id="Telephone"/>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="col-sm-11">
                                <p>
                                    <label class="fount">课程信息</label>
                                </p>
                                <fieldset>
                                    <table class="table table-bordered" id="changeSchoolTable"
                                           style="font-size: 14px; font-weight:bold;">
                                        <thead>
                                        <tr style="color: #356FBE; background: #e3f0f4">
                                            <th>当前校区</th>
                                            <th>转入校区</th>
                                            <th>当前班级</th>
                                            <th>转入班级</th>
                                            <th>转校日期</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </fieldset>
                            </div>

                            <fieldset>
                                <p>
                                    <label class="fount">备注</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <textarea rows="7" cols="80" name="cremarks" id = "cremarksId" required=required maxlength="100"></textarea>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-actions center">
                                <button type="button" onclick="addOrder()"
                                        style="font-size: 14px; font-weight:bold; width: 100px; background-color: #F97961 !important;border-color: #F97961;"
                                        id="save_btn">
                                    确认提交
                                </button>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="chooseStudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 700px">
                <div class="modal-header" style="height: 34px; padding: 8px; background: #1892D1"><span
                        style="font-size: 14px;color: #FFFFFF">请选择学员</span></div>
                <div class="modal-body" style="width: 100%">
                    <div class="col-sm-5">
                        <input class="input" style="width: 100%" name="cname" id="cNameId"
                               placeholder="请输入学生姓名、学号、电话"/>
                    </div>
                    <button type="submit" class="btn btn-xs btn-success" onclick="queryStu(0)"
                            style="background-color: #3598DB !important;border-color: #3598DB; width: 60px">查询
                    </button>
                    <div>&nbsp;</div>
                    <table class="table table-bordered" id="shiftTable" style="font-size: 14px; font-weight:bold;">
                        <thead>
                        <tr style="color: #356FBE; background: #E4F2FF">
                            <th style="width: 20px"></th>
                            <th style="width: 30px">学号</th>
                            <th style="width: 100px">姓名</th>
                            <th style="width: 40px">手机号</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                    <div style="height: 66px; padding-bottom:0px; top:0px;">
                        <div id="pagination1" class="page fl"></div>
                    </div>
                </div>
                <div class="modal-footer" style="background: #F0F0F0 ;height: 43px">
                    <button type="button" class="btn btn-xs btn-success" onclick="chooseSchool()"
                            style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">确定
                    </button>
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal" onclick="cancelChoose()"
                            style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                            style="color: #666666">取消</span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chooseSchool" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 450px">
                <div class="modal-header" style="height: 34px; padding: 8px; background: #1892D1"><span
                        style="font-size: 14px;color: #FFFFFF">提示</span></div>
                <div class="modal-body" style="width: 100%; ">
                    <span class="help-inline"><font color="red">&nbsp;</font></span><strong>当前校区：</strong> <span
                        id="campusID"></span>

                    <div>&nbsp;</div>
                    <span class="help-inline"><font color="red">*</font></span><strong>转入校区:</strong>
                    <select class="input-xlarge required" id="departs"></select>

                    <div>&nbsp;</div>
                    <strong><span class="help-inline"><font color="red">*</font> </span>转校日期:</strong>
                    <input type="text" required=required name="resumeDate"
                           id="changeSchoolDate" style="padding: 3px 4px; width: 66%;height: 28px"
                           onclick="WdatePicker({dateFmt:'yyyy-MM-dd', minDate:'%y-%M-{%d}'});"/>
                </div>
                <div class="modal-footer" style="background: #F0F0F0 ;">
                    <button type="button" class="btn btn-xs btn-success" onclick="chooseClass()"
                            style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">确定
                    </button>
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal" onclick="cancelCAdopt()"
                            style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                            style="color: #666666">取消</span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chooseClass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 700px">
                <div class="modal-header" style="height: 34px; padding: 8px; background: #1892D1"><span
                        style="font-size: 14px;color: #FFFFFF">请选择班级</span></div>
                <div class="modal-body" style="width: 100%">
                    <div>&nbsp;</div>
                    <table class="table table-bordered" id="classTable" style="font-size: 14px; font-weight:bold;">
                        <thead>
                        <tr style="color: #356FBE; background: #E4F2FF">
                            <th>班级名称</th>
                            <th>上课时间</th>
                            <th>上课教室</th>
                            <th>老师</th>
                            <th>剩余数量</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="modal-footer" style="background: #F0F0F0 ;height: 43px">
                    <button type="button" class="btn btn-xs btn-success" onclick="confirmSchool()"
                            style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">确定
                    </button>

                    <button type="button" class="btn btn-xs btn-default" onclick="closeNewCampusOfClass()"
                            style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                            style="color: #666666">取消</span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="classTableResumeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 700px">
                <div class="modal-header" style="height: 34px; padding: 8px; background: #1892D1"><span
                        style="font-size: 14px;color: #FFFFFF">请选择班级</span></div>
                <div class="modal-body" style="width: 100%">
                    <div class="col-sm-5">
                        <input class="input" style="width: 100%" name="className" id="className"
                               placeholder="请输入班级名称"/>
                    </div>
                    <button type="submit" class="btn btn-xs btn-success" onclick="changeSchoolOfClass('qnc')"
                            style="background-color: #3598DB !important;border-color: #3598DB; width: 60px">查询
                    </button>
                    <div>&nbsp;</div>
                    <table class="table table-bordered" id="classTableResume"
                           style="font-size: 14px; font-weight:bold;">
                        <thead>
                        <tr style="color: #356FBE; background: #E4F2FF">
                            <th style="width: 20px"></th>
                            <th>班级名称</th>
                            <th>课程</th>
                            <th>老师</th>
                            <th>人数</th>
                            <th>开班时间</th>
                            <th>上课时间</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div style="height: 66px; padding-bottom:0px; top:0px;">
                        <div id="pagination2" class="page fl"></div>
                    </div>
                </div>
                <div class="modal-footer" style="background: #F0F0F0 ;height: 43px">
                    <button type="button" class="btn btn-xs btn-success" onclick="confirmClass()"
                            style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">确定
                    </button>
                    <button type="button" class="btn btn-xs btn-default" onclick="closeClassTableResumeModal()"
                            style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                            style="color: #666666">取消</span></button>
                </div>
            </div>
        </div>
    </div>
    <input id = "cscIdValue" type="hidden">
    <input id="isLogin" type="hidden" value="${isLogin}">
</div>

<div class="modal fade" id="adopt" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 550px">
            <div class="modal-header"
                 style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">提示</span></div>
            <div class="modal-body" style="width: 100%">
                <strong><h3>请先认证ODS</h3></strong>

                <div>&nbsp;</div>
                <strong>账号:</strong> <input id="odsUserName"> &nbsp;&nbsp;&nbsp;&nbsp;
                <strong>密码:</strong> <input id="odsPassword">
                <br>
                <span id="info" style="color: red"></span>
            </div>

            <div class="modal-footer" style="background: #F0F0F0 ;">
                <button type="button" class="btn btn-xs btn-success" onclick="adopt()"
                        data-toggle="modal" data-target="#chooseClass"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">
                    确定
                </button>
            </div>
        </div>
    </div>
</div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/myScript.jsp" %>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.min.js"></script>

<script type="text/javascript">
    var studentList;
    var shiftTableList;
    var page = 0;
    var chooleClassOfID;
    var siLogin = $("#isLogin").val();

    jQuery(function ($) {
        queryStu(page);
        $("#pagination1").pagination({
            currentPage: 1,
            totalPage: 3,
            callback: function (current) {
                queryStu(current);
            }
        });

        if (siLogin != 'N') {
            $("#adopt").modal('show');
        }
    });

    //查询学生
    function queryStu(page) {
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findStudent',
            dataType: 'json',
            data: {
                cName: $("#cNameId").val(),
                userID: $("#tEmployeeID").val(),
                page: page
            },
            success: function (data) {
                var values = data.data;
                var count = data.count;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'cidRadio' value=' " + values[i].cid + "'></td>" +
                                "<td>" + values[i].cserial + "</td>" +
                                "<td>" + values[i].cname + "</td>" +
                                "<td>" + values[i].csmstel + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#shiftTable tbody").html(str);

                $("#pagination1").pagination({
                    currentPage: 1,
                    totalPage: count,
                    callback: function (current) {
                        queryPage(current);
                    }
                });
            }
        });
    }

    //学生分页
    function queryPage(current) {
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findStudent',
            dataType: 'json',
            data: {
                cName: $("#cNameId").val(),
                userID: $("#tEmployeeID").val(),
                page: current
            },
            success: function (data) {
                var values = data.data;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'cidRadio' value=' " + values[i].cid + "'></td>" +
                                "<td>" + values[i].cserial + "</td>" +
                                "<td>" + values[i].cname + "</td>" +
                                "<td>" + values[i].csmstel + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#shiftTable tbody").html(str);
            }
        });
    }

    //选择学校
    function chooseSchool() {
        var userId = $("input[name='cidRadio']:checked").val();
        $("#userById").val(userId);
        if (userId == null || userId == '') {
            layer.alert("请选择学员");
        } else {
            $("#chooseStudent").modal('hide');
            $("#chooseSchool").modal('show');
            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderChangeSchool/findDepart',
                dataType: 'json',
                data: {
                    userId: userId
                },
                success: function (data) {
                    var values = data.data;
                    $("#campusID").html(data.campusID);

                    if (values.length > 0) {
                        for (i in values) {
                            $("#departs").append("<option value = '" + values[i].cID + "'>" + values[i].cNAME + "</option>");
                        }
                    }
                }
            });
        }
    }

    //选择班级
    function chooseClass() {
        var userId = $("input[name='cidRadio']:checked").val();
        $("#userById").val(userId);
        var changeSchoolDate = $("#changeSchoolDate").val();

        var str = '';
        if (userId == null || userId == '') {
            layer.alert("请选择学员");
        } else {
            if (changeSchoolDate == null || changeSchoolDate == '') {
                layer.alert("请选择转校时间");
            } else {
                $("#chooseSchool").modal('hide');
                $("#chooseClass").modal('show');
                $.ajax({
                    type: 'post',
                    url: '${GLOBAL.basePath}/workOrderInterrupt/findClass',
                    dataType: 'json',
                    data: {
                        userId: userId
                    },
                    success: function (data) {
                        var values = data.data;
                        var student = data.student;

                        $("#studentName").val(student.cName);
                        $("#studentNo").val(student.cSerial);
                        $("#Telephone").val(student.cSMSTel);

                        if (values.length > 0) {
                            for (i in values) {
                                var status = values[i].cStatus;
                                if (status == 1) {
                                    status = "停课";
                                }
                                var varCid = '"' + values[i].cID.trim() + '"';
                                str += "<tr>" +
                                        "<td>" + values[i].className + "</td>" +
                                        "<td>" + values[i].sDate + "</td>" +
                                        "<td>" + values[i].roomName + "</td>" +
                                        "<td>" + values[i].userName + "</td>" +
                                        "<td>" + values[i].xyAmount + "</td>" +
                                        "<td> <button type='button' class='btn btn-xs btn-success' id= " + values[i].cID + " onclick='changeSchoolOfClass(" + varCid + ")' style='background-color: #3598DB !important;border-color: #3598DB; width: 60px'>转校出班" +
                                        "</button>"
                                "</td>" +
                                "</tr>";
                            }
                        } else {
                            str = "<tr align='left'>" +
                                    "<td colspan='12'><span style='font-size: 16px;color: red'>暂无班级</span></td>" +
                                    "</tr>";
                        }
                        $("#classTable tbody").html(str);
                    }
                });
            }
        }
    }

    //查询转校的班级
    function changeSchoolOfClass(cscId) {
        if (cscId != 'qnc') {
            //$("#" + cscId).attr("disabled", "true");cscIdValue
            $("#cscIdValue").val(cscId);
            chooleClassOfID = cscId;
        }
        var className = $("#className").val();

        var str = '';
        var campusID = $("#departs").val();

        $("#classTableResumeModal").modal('show');
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderOfResume/findResumeClass',
            dataType: 'json',
            data: {
                page: page,
                campusID: campusID,
                className: className
            },
            success: function (data) {
                var values = data.data;
                var count = data.count;
                if (values.length > 0) {
                    //$("#chooseClass").modal('hide');
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'newCampusClassID' value=' " + values[i].cID + "'></td>" +
                                "<td>" + values[i].className + "</td>" +
                                "<td>" + values[i].shiftName + "</td>" +
                                "<td>" + values[i].cName + "</td>" +
                                "<td>" + values[i].onGoingCount + "</td>" +
                                "<td>" + values[i].onClassTime + "</td>" +
                                "<td>" + values[i].sDate + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#classTableResume tbody").html(str);

                $("#pagination2").pagination({
                    currentPage: 1,
                    totalPage: count,
                    callback: function (current) {
                        queryNewSchoolOfClass(current, campusID);
                    }
                });
            }
        });
    }

    //查询新校区班级
    function queryNewSchoolOfClass(current, campusID) {
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderOfResume/findResumeClass',
            dataType: 'json',
            data: {
                campusID: campusID,
                page: current
            },
            success: function (data) {
                var values = data.data;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'resumeRadioNew' value=' " + values[i].cID + "'></td>" +
                                "<td>" + values[i].className + "</td>" +
                                "<td>" + values[i].shiftName + "</td>" +
                                "<td>" + values[i].cName + "</td>" +
                                "<td>" + values[i].onGoingCount + "</td>" +
                                "<td>" + values[i].onClassTime + "</td>" +
                                "<td>" + values[i].sDate + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#classTableResume tbody").html(str);
            }
        });
    }

    function confirmClass() {
        var cscId = $("#cscIdValue").val();
        $("#" + cscId).attr("disabled", "true");

        var newCampusClassID = $("input[name='newCampusClassID']:checked").val();
        if (newCampusClassID != null) {
            $("#classAndSchoolDate").append("<input name = 'ncClassID'  id='" + newCampusClassID + "' value=" + newCampusClassID + ">");
            $("#classAndSchoolDate").append("<input name = 'cClassID'  id='" + chooleClassOfID + "' value=" + chooleClassOfID + ">");

            closeClassTableResumeModal();
        } else {
            layer.alert("请选择班级");
        }
    }

    function confirmSchool() {
        var oclassID = "";
        var nclassID = "";

        var changeSchoolDate = $("#changeSchoolDate").val();
        $("#crecoverydate").val(changeSchoolDate);

        var str = '';

        $("input[name='ncClassID']").each(function (index, item) {
            if (nclassID == "") {
                nclassID = $(this).val();
            } else {
                nclassID = nclassID + "," + $(this).val();
            }
        });

        $("input[name='cClassID']").each(function (index, item) {
            if (oclassID == "") {
                oclassID = $(this).val();
            } else {
                oclassID = oclassID + "," + $(this).val();
            }
        });

        var campusID = $("#departs").val();

        $("#classID").val(oclassID);
        $("#resumeClassID").val(nclassID);

        if (oclassID != null && oclassID != '') {
            if (nclassID != null && nclassID != '') {
                $("#classAndSchoolDate").empty();
                $.ajax({
                    type: 'post',
                    url: '${GLOBAL.basePath}/workOrderChangeSchool/findClassToPage',
                    dataType: 'json',
                    data: {
                        classID: oclassID,
                        campusID: campusID,
                        newClassID: nclassID
                    },

                    success: function (data) {
                        var values = data.data;
                        console.log(values);
                        if (values.length > 0) {
                            for (i in values) {
                                str += "<tr>" +
                                        "<td>" + values[i].campusName + "</td>" +
                                        "<td>" + values[i].newCampusName + "</td>" +
                                        "<td>" + values[i].className + "</td>" +
                                        "<td>" + values[i].newName + "</td>" +
                                        "<td>" + changeSchoolDate + "</td>" +
                                        "</tr>";
                            }
                        }

                        $("#changeSchoolTable tbody").html(str);

                        cancelChoose();
                        $("#chooseClass").modal("hide");
                        $("#changeSchoolDate").val("");
                    }
                });
            } else {
                layer.alert("请选择转入班级");
            }
        } else {
            layer.alert("请先转校出班");
        }
    }

    function cancelChoose() {
        $("#cNameId").val('');
        queryStu(page);
    }

    function closeClassTableResumeModal() {
        $("#classTableResumeModal").modal("hide");
    }

    function closeNewCampusOfClass() {
        $("#chooseClass").modal("hide");
        $("#classAndSchoolDate").empty();
    }

    function addOrder(){
        var classIDS = $("#classIDS").val();
        var userById = $("#userById").val();
        var resumeClassID = $("#resumeClassID").val();
        var cremarksId = $("#cremarksId").val();

        if(userById == null || userById ==''){
            layer.alert("请选择转校学员");
        }else if(resumeClassID == null || resumeClassID ==''){
            layer.alert("请选择转入校区班级");
        }else if(cremarksId == null || cremarksId ==''){
            layer.alert("备注不能为空");
        }else{
            $("#workOrderOfResumeForm").submit();
        }
    };

    function adopt() {
        var odsUserName = $("#odsUserName").val();
        var odsPassword = $("#odsPassword").val();
        var userID = $("#tEmployeeID").val();
        if (odsUserName == '' || odsPassword == '') {
            $("#info").html("账号或密码不能为空");
        } else {
            $("#info").html("");

            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderInterrupt/odsLogin',
                dataType: 'json',
                data: {
                    odsUserName: odsUserName,
                    odsPassword: odsPassword,
                    userID: userID
                },
                success: function (data) {
                    if(data.data >0){
                        $("#adopt").modal('hide');
                    }else{
                        $("#info").html("账号或密码输入错误");
                    }
                }
            });
        }
    }
</script>
