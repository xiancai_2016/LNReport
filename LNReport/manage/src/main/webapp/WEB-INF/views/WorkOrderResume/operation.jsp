<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/myBase_body_header.jsp" %>
<%@include file="../common/myNavbar.jsp" %>
<%--<%@include file="../common/myPage_content_pre.jsp" %>--%>

<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.css"/>

<style type="text/css">
    .input {
        border-color: #E4E4E4;
        border-style: solid;
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 1px;
        border-left-width: 0px;
        width: 65%
    }

    .fount {
        color: #333333;
        font-size: 16px;
        font-weight: bold;
    }

    input::-webkit-input-placeholder {
        color: #C9C9C9;
    }

    input::-moz-placeholder { /* Mozilla Firefox 19+ */
        color: #C9C9C9;
    }

    input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
        color: #C9C9C9;
    }

    input:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #C9C9C9;
    }

    * {
        margin: 0;
        padding: 0;
    }

    body {
        font-family: "微软雅黑";
        background: #eee;
    }

    button {
        display: inline-block;
        padding: 6px 12px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        border: 1px solid transparent;
        border-radius: 4px;
        border-color: #28a4c9;
        color: #fff;
        background-color: #5bc0de;
        margin: 20px 20px 0 0;
    }
</style>

<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<div class="page-content">

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header" style="color: #737373">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <span class="help-inline"><font color="red">&nbsp;</font> </span>单据号:
                            <span>${workOrderDetail.cOrderNo}</span>
                        </div>

                        <div class="col-sm-3">
                            申请时间: <span>${workOrderDetail.cCreateDate}</span>
                        </div>

                        <div class="col-sm-3">
                            申请人: <span>${workOrderDetail.cName}</span>
                        </div>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form:form class="form-horizontal" modelAttribute="sysReport"
                                   action="${GLOBAL.basePath}/workOrderInterrupt/audit" method="post" id="auditForm">
                            <div>&nbsp;</div>
                            <input type="hidden" name="workOrderID" id="workOrderID" value="${workOrderDetail.cID}">
                            <input type="hidden" name="woType" id="woType">
                            <input type="hidden" name="cUserID" value="${userID}">

                            <fieldset>
                                <p>
                                    <label class="fount">复课学员</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <span class="help-inline"><font color="red">*</font> </span>学员姓名:
                                        <input class="input" readonly=readonly" value="${student.cName}"/>
                                    </div>

                                    <div class="col-sm-3">
                                        学号:
                                        <input class="input" readonly=readonly id="cField1" value="${student.cSerial}"/>
                                    </div>

                                    <div class="col-sm-3">
                                        电话:
                                        <input class="input" readonly=readonly id="Telephone"
                                               value="${student.cSMSTel}"/>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="col-sm-11">
                                <p>
                                    <label class="fount">课程信息</label>
                                </p>
                                <fieldset>
                                    <table class="table table-bordered" id="shiteTable"
                                           style="font-size: 14px; font-weight:bold;">
                                        <thead>
                                        <tr style="color: #356FBE; background: #e3f0f4">
                                            <th>转班日期</th>
                                            <th>原班级名称</th>
                                            <th>现班级名称</th>
                                            <th>转出课次</th>
                                            <th>转出金额</th>
                                        </tr>
                                        </thead>
                                        <c:forEach items="${classes}" var="item">
                                            <tr>
                                                <td>${workOrderDetail.cRecoveryDate}</td>
                                                <td>${item.className}</td>
                                                <td>${item.newClassNaeme}</td>
                                                <td>${item.xyAmount}</td>
                                                <td>${item.tatolRrice}</td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </fieldset>
                            </div>

                            <fieldset>
                                <p>
                                    <label class="fount">备注</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <textarea rows="7" cols="80" name="cremarks"
                                                  disabled=disabled>${workOrderDetail.cRemarks}</textarea>
                                    </div>
                                </div>

                                <div>&nbsp;</div>
                            </fieldset>

                            <div>
                                <table id="list-table" class="table table-striped table-bordered table-hover"
                                       width="100%">
                                    <tr>
                                        <th>时间</th>
                                        <th>操作人</th>
                                        <th>操作</th>
                                        <th>操作原因</th>
                                    </tr>
                                    <c:forEach items="${record}" var="item">
                                        <tr>
                                            <td>${item.ccreatedate}</td>
                                            <td>${item.cName}</td>
                                            <td>
                                                <c:if test="${item.cstatus == 2}">审核通过</c:if>
                                                <c:if test="${item.cstatus == 3}">驳回审核</c:if>
                                                <c:if test="${item.cstatus == 4}">操作成功</c:if>
                                                <c:if test="${item.cstatus == 5}">无法操作</c:if>
                                            </td>
                                            <td>${item.cauditreason}</td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>

                            <div>&nbsp;</div>

                            <c:if test="${flag == 2}">
                                <div class="form-actions center">
                                    <button type="submit" data-toggle="modal" data-target="#adopt"
                                            style="font-size: 14px; font-weight:bold; width: 150px; background-color: rgba(14, 178, 184, 1) !important;border-color: rgba(14, 178, 184, 1);"
                                            id="save_btn">
                                        校管家操作完成
                                    </button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button type="submit" data-toggle="modal" data-target="#unAdopt"
                                            style="font-size: 14px; font-weight:bold; width: 150px; background-color: rgba(146, 131, 219, 1) !important;border-color: rgba(146, 131, 219, 1);"
                                            id="clean_btn">
                                        校管家无法操作
                                    </button>
                                </div>
                            </c:if>

                            <div class="modal fade" id="adopt" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel"
                                 aria-hidden="true" data-backdrop="static" data-keyboard="false">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="width: 450px">
                                        <div class="modal-header"
                                             style="height: 34px; padding: 8px; background: #1892D1"><span
                                                style="font-size: 14px;color: #FFFFFF">提示</span></div>
                                        <div class="modal-body" style="width: 100%">
                                            <strong>确认通过？请说明操作原因：</strong>

                                            <div>&nbsp;</div>
                                            <textarea rows="5" cols="55" name="adoptReason" id="adoptReason"></textarea>
                                        </div>
                                        <div class="modal-footer" style="background: #F0F0F0 ;">
                                            <button type="button" class="btn btn-xs btn-success" onclick="adopt()"
                                                    data-toggle="modal" data-target="#chooseClass"
                                                    style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">
                                                确定
                                            </button>
                                            <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"
                                                    onclick="cancelCAdopt()"
                                                    style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                                                    style="color: #666666">取消</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="unAdopt" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel"
                                 aria-hidden="true" data-backdrop="static" data-keyboard="false">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="width: 450px">
                                        <div class="modal-header"
                                             style="height: 34px; padding: 8px; background: #1892D1"><span
                                                style="font-size: 14px;color: #FFFFFF">提示</span></div>
                                        <div class="modal-body" style="width: 100%">
                                            <strong>确认驳回？请说明操作原因：</strong>

                                            <div>&nbsp;</div>
                                            <textarea rows="5" cols="55" name="unAdoptReason"
                                                      id="unAdoptReason"></textarea>
                                        </div>
                                        <div class="modal-footer" style="background: #F0F0F0 ;">
                                            <button type="button" class="btn btn-xs btn-success" onclick="unAdopt()"
                                                    data-toggle="modal" data-target="#chooseClass"
                                                    style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">
                                                确定
                                            </button>
                                            <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"
                                                    onclick="cancelUnAdopt()"
                                                    style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                                                    style="color: #666666">取消</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/myScript.jsp" %>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>

<script type="text/javascript">
    function adopt() {
        $("#woType").val(3);
        $("#auditForm").submit();
    }

    function cancelCAdopt() {
        $("#adoptReason").val("");
    }

    function unAdopt() {
        $("#woType").val(4);
        $("#auditForm").submit();
    }

    function cancelUnAdopt() {
        $("#unAdoptReason").val("");
    }
</script>
