<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/myBase_body_header.jsp" %>
<%@include file="../common/myNavbar.jsp" %>
<%--<%@include file="../common/myPage_content_pre.jsp" %>--%>
<style>
    button {
        display: inline-block;
        padding: 6px 12px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        border: 1px solid transparent;
        border-radius: 4px;
        border-color: #28a4c9;
        color: #fff;
        background-color: #5bc0de;
        margin: 20px 20px 0 0;
    }
</style>

<div class="page-content" style="margin: 3px;">
    <div class="row">
        <div id="printPage">
            <div class="widget-main no-padding">
                <form:form modelAttribute="sysReport"
                           action="${GLOBAL.basePath}/workOrder/print" method="post" id="auditForm">
                    <input type="hidden" name="workOrderID" id="workOrderID" value="${workOrderDetail.cID}">
                    <div><br><br><br></div>
                    <fieldset>
                        <div style="margin: 0 auto;">
                            <table style=" border-style: dashed; border-width: 1px; border-color: #000000;
                                                   width: 100%; height: 50px; border-radius: 5px; width: 100%; border: 1px solid #ccc;
                                                   border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate">
                                <tr>
                                    <td style="text-align:center;font-size: 12px">申请日期:
                                        : ${workOrderDetail.cCreateDate}</td>
                                    <td style="text-align:center;font-size: 12px">申请校区 : ${workOrderDetail.campus}</td>
                                    <td style="text-align:center;font-size: 12px">审核日期
                                        : ${workOrderDetail.auditTime}</td>
                                    <td style="text-align:center;font-size: 12px">单据号 : ${workOrderDetail.cOrderNo}</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label><strong>复课学员</strong></label>

                        <div style="margin: 0 auto;">
                            <table style=" border-style: dashed; border-width: 1px; border-color: #000000;
                                                   width: 100%; height: 50px; border-radius: 5px; width: 100%; border: 1px solid #ccc;
                                                   border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate">
                                <tr>
                                    <td style="text-align:center;font-size: 12px">学生姓名 : ${student.cName}</td>
                                    <td style="text-align:center;font-size: 12px">学号 : ${student.cSerial}</td>
                                    <td style="text-align:center;font-size: 12px">联系电话 : ${student.cSMSTel}</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label><strong>复课详情</strong></label>

                        <div style="margin: 0 auto;">
                            <table style="text-align:center; border-style: dashed; border-width: 0.5px; border-color: #8c8585; width: 100%; border-radius: 5px;
                                                width: 100%; border: 1px solid #ccc; border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate;"
                                   border: 1px>
                                <tr style="height: 50px;">
                                    <td style="font-size: 12px"><strong>原班级名称</strong></td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>现班级名称</strong>
                                    </td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>停课时间</strong>
                                    </td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>实际复课时间</strong>
                                    </td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>购买数量</strong>
                                    </td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>剩余数量</strong>
                                    </td>
                                </tr>
                                <c:forEach items="${classes}" var="item">
                                    <tr style="height: 50px;">
                                        <td style="border-top:0.5px dashed #8c8585;font-size: 12px">${item.className}</td>
                                        <td style=" border-top: 1px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${item.newClassNaeme}</td>
                                        <td style=" border-top: 0.5px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${item.cStopClassDate}</td>
                                        <td style=" border-top: 0.5px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${workOrderDetail.cRecoveryDate}</td>
                                        <td style=" border-top: 0.5px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${item.totalAmount}</td>
                                        <td style=" border-top: 0.5px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${item.xyAmount}</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </fieldset>


                    <fieldset>
                        <div>
                            <label><strong>备注</strong></label>

                            <div style="margin: 0 auto;">
                                <table style="height: 60px; border-style: dashed; border-width: 0.5px; border-color: #000000;
                                                      width: 100%; height: 50px; border-radius: 5px; width: 100%; border: 1px solid #ccc;
                                                      border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate;">
                                    <tr>
                                        <td style="font-size: 12px;">${workOrderDetail.cRemarks}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </fieldset>

                    <%--<fieldset>--%>
                        <%--<label><strong>冻结说明</strong></label>--%>
                        <%--<div style="margin: 0 auto;">--%>
                            <%--<table style="height: 90px; border-style: dashed; border-width: 0.5px; border-color: #000000;--%>
                                                  <%--width: 100%; height: 50px; border-radius: 5px; width: 100%; border: 1px solid #ccc;--%>
                                                  <%--border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate">--%>
                                <%--<tr>--%>
                                    <%--<td style="font-size: 12px">1.此表不作为后期的学费证明 ；</td>--%>
                                <%--</tr>--%>
                                <%--<tr>--%>
                                    <%--<td style="font-size: 12px">2.复课需要提前1-2周联系班主任，以便班主任帮助孩子安排合适的班级；</td>--%>
                                <%--</tr>--%>
                                <%--<tr>--%>
                                    <%--<td style="font-size: 12px">3.冻结周期最长180天。</td>--%>
                                <%--</tr>--%>
                            <%--</table>--%>
                        <%--</div>--%>
                    <%--</fieldset>--%>
                </form:form>
            </div>
        </div>

        <div>
            <table id="list-table" class="table table-striped table-bordered table-hover"
                   width="100%">
                <%--<thead>--%>
                <tr>
                    <th>时间</th>
                    <th>操作人</th>
                    <th>操作</th>
                    <th>操作原因</th>
                </tr>
                <%--</thead>--%>
                <c:forEach items="${record}" var="item">
                    <tr>
                        <td>${item.ccreatedate}</td>
                        <td>${item.cName}</td>
                        <td>
                            <c:if test="${item.cstatus == 2}">审核通过</c:if>
                            <c:if test="${item.cstatus == 3}">驳回审核</c:if>
                            <c:if test="${item.cstatus == 4}">操作成功</c:if>
                            <c:if test="${item.cstatus == 5}">无法操作</c:if>
                        </td>
                        <td>${item.cauditreason}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>

        <div class="form-actions center">
            <button type="button" onclick="pPrint()"
                    style="font-size: 12px; font-weight:bold; width: 120px; background-color: #3796F1 !important;border-color: #3796F1;">
                打印特况单
            </button>
        </div>
    </div>
</div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/myScript.jsp" %>

<script src="${GLOBAL.staticJsPath}/assets/js/print/jquery.jqprint-0.3.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/print/jquery-migrate-1.2.1.min.js" ;></script>

<script type="text/javascript">
    function pPrint() {
        var workOrderID = $("#workOrderID").val();
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/print',
            dataType: 'json',
            data: {
                workOrderID: workOrderID
            },
            success: function (data) {
                if (data.data == 1) {
                    $("#printPage").jqprint();
                }
            }
        });
    }
</script>
