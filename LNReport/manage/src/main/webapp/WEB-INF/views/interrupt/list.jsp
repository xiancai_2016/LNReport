<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/myBase_body_header.jsp" %>
<%@include file="../common/myNavbar.jsp" %>
<%--<%@include file="../common/myPage_content_pre.jsp" %>--%>

<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.css"/>
<style type="text/css">
    .input {
        border-color: #E4E4E4;
        border-style: solid;
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 1px;
        border-left-width: 0px;
        width: 65%
    }

    input::-webkit-input-placeholder {
        color: #C9C9C9;
    }

    input::-moz-placeholder { /* Mozilla Firefox 19+ */
        color: #C9C9C9;
    }

    input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
        color: #C9C9C9;
    }

    input:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #C9C9C9;
    }

    * {
        margin: 0;
        padding: 0;
    }

    body {
        font-family: "微软雅黑";
        background: #eee;
    }

    button {
        display: inline-block;
        padding: 6px 12px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        border: 1px solid transparent;
        border-radius: 4px;
        border-color: #28a4c9;
        color: #fff;
        background-color: #5bc0de;
        /*margin: 20px 20px 0 0;*/
    }
</style>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <div>&nbsp;</div>
            <form class="form-horizontal" role="form" action="${GLOBAL.basePath}/workOrderInterrupt/listPage"
                  method="post">
                <table>
                    <tr>
                        <td>申请时间：</td>
                        <td><input type="text" class="input" style="width: 100%"
                                   id="txtBeginDay" name="startDate" value="${workOrder.startDate}"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,maxDate:'#F{$dp.$D(txtEndDay)}'});"/>
                        </td>
                        <td> 到
                            <input type="text" class="input" style="width: 80%"
                                   id="txtEndDay" name="endDate" value="${workOrder.endDate}"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,minDate:'#F{$dp.$D(txtBeginDay)}'});"/>
                        </td>
                        <td>进度</td>
                        <td style="width: 150px">
                            <select class="input" name="cstatus" id="cstatus">
                                <option value="">全部</option>
                                <option value="1" <c:if test="${workOrder.cstatus == '1'}">selected</c:if>>未审核</option>
                                <option value="2" <c:if test="${workOrder.cstatus == '2'}">selected</c:if>>审核通过</option>
                                <option value="3" <c:if test="${workOrder.cstatus == '3'}">selected</c:if>>驳回</option>
                                <option value="4" <c:if test="${workOrder.cstatus == '4'}">selected</c:if>>操作成功</option>
                                <option value="5" <c:if test="${workOrder.cstatus == '5'}">selected</c:if>>无法操作</option>
                            </select>
                        </td>
                        <td>申请类型：</td>
                        <td style="width: 150px">
                            <select class="input" name="ctype" id="ctype">
                                <option value="">全部</option>
                                <option value="1" <c:if test="${workOrder.ctype == '1'}">selected</c:if>>停课</option>
                                <option value="2" <c:if test="${workOrder.ctype == '2'}">selected</c:if>>复课</option>
                                <option value="3" <c:if test="${workOrder.ctype == '3'}">selected</c:if>>转班</option>
                                <option value="4" <c:if test="${workOrder.ctype == '4'}">selected</c:if>>转校</option>
                                <option value="5" <c:if test="${workOrder.ctype == '5'}">selected</c:if>>退费</option>
                                <option value="6" <c:if test="${workOrder.ctype == '6'}">selected</c:if>>转费</option>
                            </select>
                        </td>
                        <td style="width: 150px">
                            <input name="userName" id="userName" value="${workOrder.userName}" class="input"
                                   style="height: 30px; width: 130px" placeholder="请输入学员名"/>
                        </td>
                        <td></td>
                        <td>
                            <button class="btn btn-primary btn-sm" type="button" onclick="queryWorkOrder(1)"
                                    style="font-size: 10px; font-weight:bold; width: 70px; background-color: #3796F1 !important;border-color: #3796F1;"
                                    id="save_btn">
                                查询
                            </button>
                        </td>
                    </tr>
                </table>
            </form>
            <input type="hidden" value="${userID}" name="userID" id="userID">
        </div>
        <div>&nbsp;</div>
        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="workOrderTable" class="table table-striped table-hover" width="100%"
                       style="font-size: 14px;">
                    <thead>
                    <tr style="color: #356FBE; background: #E3F0F4">
                        <th>单据号</th>
                        <th>校区</th>
                        <th>学员姓名</th>
                        <th>申请时间</th>
                        <th>申请人</th>
                        <th>申请类型</th>
                        <th>当前操作人</th>
                        <th>当前进度</th>
                        <th>是否打印</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <div style="height: 66px; padding-bottom:0px; top:0px;">
                    <div id="pagination1" class="page fl"></div>
                </div>

            </div>
        </div>
    </div>

    <input id="isLogin" type="hidden" value="${isLogin}">
</div>

<div class="modal fade" id="adopt" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 550px">
            <div class="modal-header"
                 style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">提示</span></div>
            <div class="modal-body" style="width: 100%">
                <strong><h3>请先认证ODS</h3></strong>

                <div>&nbsp;</div>
                <strong>账号:</strong> <input id="odsUserName"> &nbsp;&nbsp;&nbsp;&nbsp;
                <strong>密码:</strong> <input id="odsPassword">
                <br>
                <span id="info" style="color: red"></span>
            </div>

            <div class="modal-footer" style="background: #F0F0F0 ;">
                <button type="button" class="btn btn-xs btn-success" onclick="adopt()"
                        data-toggle="modal" data-target="#chooseClass"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">
                    确定
                </button>
            </div>
        </div>
    </div>
</div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/myScript.jsp" %>

<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.min.js"></script>

<script type="text/javascript">
    var page = 1;
    var siLogin = $("#isLogin").val();

    jQuery(function ($) {
        queryWorkOrder(page);
        $("#pagination1").pagination({
            currentPage: 1,
            totalPage: 3,
            callback: function (current) {
                queryWorkOrder(current);
            }
        });

        if (siLogin != 'N') {
            $("#adopt").modal('show');
        }
    });

    function queryWorkOrder(page) {
        var txtBeginDay = $("#txtBeginDay").val();
        var txtEndDay = $("#txtEndDay").val();
        var cstatus = $("#cstatus").val();
        var ctype = $("#ctype").val();
        var userName = $("#userName").val();
        var userID = $("#userID").val();

        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findWorkOrder',
            dataType: 'json',
            data: {
                txtBeginDay: txtBeginDay,
                txtEndDay: txtEndDay,
                cstatus: cstatus,
                ctype: ctype,
                userName: userName,
                userID: userID,
                page: page
            },
            success: function (data) {
                var values = data.data;
                var count = data.count;
                if (values.length > 0) {
                    for (i in values) {
                        var ctype = values[i].cType;
                        var cTypeValue;
                        var createDate = values[i].cCreateDate;
                        if (ctype == 1) {
                            cTypeValue = "停课";
                        } else if (ctype == 2) {
                            cTypeValue = "复课";
                        } else if (ctype == 3) {
                            cTypeValue = "转班";
                        } else if (ctype == 4) {
                            cTypeValue = "转校";
                        } else if (ctype == 5) {
                            cTypeValue = "退费";
                            createDate = values[i].cRecoveryDate;
                        } else if (ctype == 6) {
                            cTypeValue = "转费";
                        }

                        var cstatus = values[i].cstatus;
                        var cstatusValue;
                        if (cstatus == 1) {
                            cstatusValue = "未审核";
                        } else if (cstatus == 2) {
                            cstatusValue = "审核通过";
                        } else if (cstatus == 3) {
                            cstatusValue = "驳回申请";
                        } else if (cstatus == 4) {
                            cstatusValue = "操作成功";
                        } else if (cstatus == 5) {
                            cstatusValue = "无法操作";
                        }

                        var cisprint = values[i].cIsPrint;
                        var cisprintValue;
                        if (cisprint == 2) {
                            cisprintValue = "未打印";
                        } else if (cisprint == 1) {
                            cisprintValue = "已打印";
                        }

                        var employName;
                        if (values[i].employName == 'undefined' || values[i].employName == null) {
                            employName = "";
                        } else {
                            employName = values[i].employName;
                        }

                        str += "<tr>" +
                                "<td><a href=${GLOBAL.basePath}/workOrderInterrupt/detail?orderNo=" + values[i].cOrderNo + "&flag=1>" + values[i].cOrderNo + "</a></td>" +
                                "<td>" + values[i].campus + "</td>" +
                                "<td>" + values[i].userName + "</td>" +
                                "<td>" + createDate + "</td>" +
                                "<td>" + values[i].subName + "</td>" +
                                "<td>" + cTypeValue + "</td>" +
                                "<td>" + employName + "</td>" +
                                "<td>" + cstatusValue + "</td>" +
                                "<td>" + cisprintValue + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#workOrderTable tbody").html(str);

                $("#pagination1").pagination({
                    currentPage: 1,
                    totalPage: count,
                    callback: function (current) {
                        queryPage(current);
                    }
                });
            }
        });
    };

    function queryPage(current) {
        var txtBeginDay = $("#txtBeginDay").val();
        var txtEndDay = $("#txtEndDay").val();
        var cstatus = $("#cstatus").val();
        var ctype = $("#ctype").val();
        var userName = $("#userName").val();
        var userID = $("#userID").val();

        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findWorkOrder',
            dataType: 'json',
            data: {
                txtBeginDay: txtBeginDay,
                txtEndDay: txtEndDay,
                cstatus: cstatus,
                ctype: ctype,
                userName: userName,
                userID: userID,
                page: current
            },
            success: function (data) {
                var values = data.data;
                if (values.length > 0) {
                    for (i in values) {
                        var ctype = values[i].cType;
                        var cTypeValue;

                        var createDate = values[i].cCreateDate;

                        if (ctype == 1) {
                            cTypeValue = "停课";
                        } else if (ctype == 2) {
                            cTypeValue = "复课";
                        } else if (ctype == 3) {
                            cTypeValue = "转班";
                        } else if (ctype == 4) {
                            cTypeValue = "转校";
                        } else if (ctype == 5) {
                            cTypeValue = "退费";
                            createDate = values[i].cRecoveryDate;
                        } else if (ctype == 6) {
                            cTypeValue = "转费";
                        }

                        var cstatus = values[i].cstatus;
                        var cstatusValue;
                        if (cstatus == 1) {
                            cstatusValue = "未审核";
                        } else if (cstatus == 2) {
                            cstatusValue = "审核通过";
                        } else if (cstatus == 3) {
                            cstatusValue = "驳回申请";
                        } else if (cstatus == 4) {
                            cstatusValue = "操作成功";
                        } else if (cstatus == 5) {
                            cstatusValue = "无法操作";
                        }

                        var cisprint = values[i].cIsPrint;
                        var cisprintValue;
                        if (cisprint == 2) {
                            cisprintValue = "未打印";
                        } else if (cisprint == 1) {
                            cisprintValue = "已打印";
                        }

                        var employName;
                        if (values[i].employName == 'undefined' || values[i].employName == null) {
                            employName = "";
                        } else {
                            employName = values[i].employName;
                        }

                        str += "<tr>" +
                                "<td><a href=${GLOBAL.basePath}/workOrderInterrupt/detail?orderNo=" + values[i].cOrderNo + "&flag=1>" + values[i].cOrderNo + "</a></td>" +
                                "<td>" + values[i].campus + "</td>" +
                                "<td>" + values[i].userName + "</td>" +
                                "<td>" + createDate + "</td>" +
                                "<td>" + values[i].subName + "</td>" +
                                "<td>" + cTypeValue + "</td>" +
                                "<td>" + employName + "</td>" +
                                "<td>" + cstatusValue + "</td>" +
                                "<td>" + cisprintValue + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#workOrderTable tbody").html(str);
            }
        });
    };

    function adopt() {
        var odsUserName = $("#odsUserName").val();
        var odsPassword = $("#odsPassword").val();
        var userID = $("#userID").val();
        if (odsUserName == '' || odsPassword == '') {
            $("#info").html("账号或密码不能为空");
        } else {
            $("#info").html("");

            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderInterrupt/odsLogin',
                dataType: 'json',
                data: {
                    odsUserName: odsUserName,
                    odsPassword: odsPassword,
                    userID: userID
                },
                success: function (data) {
                    if(data.data >0){
                        $("#adopt").modal('hide');
                    }else{
                        $("#info").html("账号或密码输入错误");
                    }
                }
            });
        }
    }
</script>
