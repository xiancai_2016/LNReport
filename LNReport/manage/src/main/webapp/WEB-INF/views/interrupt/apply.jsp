<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/myBase_body_header.jsp" %>
<%@include file="../common/myNavbar.jsp" %>
<%--<%@include file="../common/myPage_content_pre.jsp" %>--%>

<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.css"/>

<style type="text/css">
    .input {
        border-color: #E4E4E4;
        border-style: solid;
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 1px;
        border-left-width: 0px;
        width: 65%
    }

    .fount {
        color: #333333;
        font-size: 16px;
        font-weight: bold;
    }

    input::-webkit-input-placeholder {
        color: #C9C9C9;
    }

    input::-moz-placeholder { /* Mozilla Firefox 19+ */
        color: #C9C9C9;
    }

    input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
        color: #C9C9C9;
    }

    input:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #C9C9C9;
    }

    * {
        margin: 0;
        padding: 0;
    }

    body {
        font-family: "微软雅黑";
        background: #eee;
    }

    button {
        display: inline-block;
        padding: 6px 12px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        border: 1px solid transparent;
        border-radius: 4px;
        border-color: #28a4c9;
        color: #fff;
        background-color: #5bc0de;
        margin: 20px 20px 0 0;
    }

    .page {
        width: 700px;
    }

    .fl {
        float: left;
    }
</style>

<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<div class="page-content">

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header" style="color: #737373">
                    <div class="form-group">
                        <div class="col-sm-3"> <span></span></div>

                        <div class="col-sm-3"><span id="proposer"></span></div>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form:form class="form-horizontal" modelAttribute="sysReport"
                                   action="${GLOBAL.basePath}/workOrderInterrupt/save" method="post" id="sysReportForm">
                            <div>&nbsp;</div>
                            <input type="hidden" name = "classID" id = "classIDS">
                            <input type="hidden" name = "userId" id = "userById">
                            <input type="hidden" name = "tEmployeeID" id = "tEmployeeID" value="${tEmployeeID}">
                            <fieldset>
                                <p>
                                    <label class="fount">冻结学员</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <span class="help-inline"><font color="red">*</font> </span>学员姓名:
                                        <input class="input" data-toggle="modal" id="studentName"
                                               readonly=readonly
                                               data-target="#chooseStudent" placeholder="请选择学员"/>
                                    </div>

                                    <div class="col-sm-3">
                                        学号:
                                        <input class="input" readonly=readonly id="studentNo"/>
                                    </div>

                                    <div class="col-sm-3">
                                        电话:
                                        <input class="input" readonly=readonly id="Telephone"/>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="col-sm-11">
                                <p>
                                    <label class="fount">课程信息</label>
                                </p>
                                <fieldset>
                                    <table class="table table-bordered" id="shiteTable"
                                           style="font-size: 14px; font-weight:bold;">
                                        <thead>
                                        <tr style="color: #356FBE; background: #e3f0f4">
                                            <th>班级名称</th>
                                            <th>课程级别</th>
                                            <th>上课时间</th>
                                            <th>已上/已排课次</th>
                                            <th>购买数量</th>
                                            <th>剩余数量</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </fieldset>
                            </div>

                            <div class="col-sm-11">
                                <p>&nbsp;</p>
                                <fieldset>
                                    <p>
                                        <label class="fount">申请冻结原因</label>
                                    </p>

                                    <div class="form-group">
                                        <div class="col-sm-8">
                                            <select class="input" name = "creason">
                                                <option>申请冻结原因1</option>
                                                <option>申请冻结原因2</option>
                                                <option>申请冻结原因3</option>
                                                <option>申请冻结原因4</option>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <fieldset>
                                <p>
                                    <label class="fount">冻结详情</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <span class="help-inline"><font color="red">*</font> </span>停课日期:

                                        <input type="text" required=required
                                            value="${cstopclassdate}"
                                               id="txtBeginDay" name="cstopclassdate"
                                               onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,minDate:'%y-%M-{%d}'});"/>
                                    </div>

                                    <div class="col-sm-4">
                                        <span class="help-inline"><font color="red">*</font> </span>预计复学日期:

                                        <input type="text" required=required
                                            value="${crecoverydate}"
                                               id="txtEndDay" name="crecoverydate"
                                               onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,minDate:'#F{$dp.$D(txtBeginDay,{d:30})}', maxDate:'#F{$dp.$D(txtBeginDay,{d:180})}'});"/>
                                    </div>
                                </div>
                                <div>&nbsp;</div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <span class="help-inline"><font color="red">*</font> </span>是否复课到班级:&nbsp;

                                        <input type="radio" value="1" checked=checked name="cisoriginal"/>&nbsp;是
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" value="2" name="cisoriginal"/>&nbsp;否
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <p>
                                    <label class="fount">冻结说明</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                       <textarea rows="7" cols="80" disabled = disabled>1.此表不作为后期的学费证明 ；

2.复课需要提前1-2周联系班主任，以便班主任帮助孩子安排合适的班级；

3.冻结周期最长180天。
                                       </textarea>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <p>
                                    <label class="fount">备注</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                       <textarea rows="7" cols="80" name = "cremarks" id = "cremarksId" required = required maxlength="100"></textarea>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-actions center">
                                <button type="button" onclick="addOrder()"
                                        style="font-size: 14px; font-weight:bold; width: 100px; background-color: #F97961 !important;border-color: #F97961;"
                                        id="save_btn">
                                    确认提交
                                </button>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="chooseStudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 700px">
            <div class="modal-header" style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">请选择学员</span></div>
            <div class="modal-body" style="width: 100%">
                <div class="col-sm-5">
                    <input class="input" style="width: 100%" name="cname" id="cNameId"
                           placeholder="请输入学生姓名、学号、电话"/>
                </div>
                <button type="submit" class="btn btn-xs btn-success" onclick="queryStu(0)"
                        style="background-color: #3598DB !important;border-color: #3598DB; width: 60px">查询
                </button>
                <div>&nbsp;</div>
                <table class="table table-bordered" id="shiftTable" style="font-size: 14px; font-weight:bold;">
                    <thead>
                    <tr style="color: #356FBE; background: #E4F2FF">
                        <th style="width: 20px"></th>
                        <th style="width: 30px">学号</th>
                        <th style="width: 100px">姓名</th>
                        <th style="width: 40px">手机号</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <div style="height: 66px; padding-bottom:0px; top:0px;">
                    <div id="pagination1" class="page fl"></div>
                </div>
            </div>
            <div class="modal-footer" style="background: #F0F0F0 ;height: 43px">
                <button type="button" class="btn btn-xs btn-success" onclick="chooseClass()"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">确定
                </button>
                <button type="button" class="btn btn-xs btn-default" data-dismiss="modal" onclick="cancelChoose()"
                        style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                        style="color: #666666">取消</span></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="chooseClass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 700px">
            <div class="modal-header" style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">请选择班级</span></div>
            <div class="modal-body" style="width: 100%">
                <div>&nbsp;</div>
                <table class="table table-bordered" id="classTable" style="font-size: 14px; font-weight:bold;">
                    <thead>
                    <tr style="color: #356FBE; background: #E4F2FF">
                        <th style="width: 20px"></th>
                        <th>班级名称</th>
                        <th>课程级别</th>
                        <th>上课时间</th>
                        <th>已上/已排课次</th>
                        <th>购买数量</th>
                        <th>剩余数量</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer" style="background: #F0F0F0 ;height: 43px">
                <button type="button" class="btn btn-xs btn-success" onclick="addClass()"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">确定
                </button>
                <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"
                        style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                        style="color: #666666">取消</span></button>
            </div>
        </div>
    </div>
    <input id="isLogin" type="hidden" value="${isLogin}">
</div>

<div class="modal fade" id="adopt" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 550px">
            <div class="modal-header"
                 style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">提示</span></div>
            <div class="modal-body" style="width: 100%">
                <strong><h3>请先认证ODS</h3></strong>

                <div>&nbsp;</div>
                <strong>账号:</strong> <input id="odsUserName"> &nbsp;&nbsp;&nbsp;&nbsp;
                <strong>密码:</strong> <input id="odsPassword">
                <br>
                <span id="info" style="color: red"></span>
            </div>

            <div class="modal-footer" style="background: #F0F0F0 ;">
                <button type="button" class="btn btn-xs btn-success" onclick="adopt()"
                        data-toggle="modal" data-target="#chooseClass"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">
                    确定
                </button>
            </div>
        </div>
    </div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/myScript.jsp" %>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.min.js"></script>

<script type="text/javascript">
    var studentList;
    var shiftTableList;
    var page = 0;
    var siLogin = $("#isLogin").val();

    jQuery(function ($) {
        queryStu(page);
        $("#pagination1").pagination({
            currentPage: 1,
            totalPage: 3,
            callback: function (current) {
                queryStu(current);
            }
        });

        if (siLogin != 'N') {
            $("#adopt").modal('show');
        }
    });

    function queryStu(page) {
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findStudent',
            dataType: 'json',
            data: {
                cName: $("#cNameId").val(),
                userID: $("#tEmployeeID").val(),
                page: page
            },
            success: function (data) {
                var values = data.data;
                var count = data.count;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'cidRadio' value=' " + values[i].cid + "'></td>" +
                                "<td>" + values[i].cserial + "</td>" +
                                "<td>" + values[i].cname + "</td>" +
                                "<td>" + values[i].csmstel + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#shiftTable tbody").html(str);

                $("#pagination1").pagination({
                    currentPage: 1,
                    totalPage: count,
                    callback: function (current) {
                        queryPage(current);
                    }
                });
            }
        });
    };

    function queryPage(current) {
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findStudent',
            dataType: 'json',
            data: {
                cName: $("#cNameId").val(),
                userID: $("#tEmployeeID").val(),
                page: current
            },
            success: function (data) {
                var values = data.data;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'cidRadio' value=' " + values[i].cid + "'></td>" +
                                "<td>" + values[i].cserial + "</td>" +
                                "<td>" + values[i].cname + "</td>" +
                                "<td>" + values[i].csmstel + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#shiftTable tbody").html(str);
            }
        });
    };

    function chooseClass() {
        var userId = $("input[name='cidRadio']:checked").val();
        $("#userById").val(userId);

        var str = '';
        if (userId == null || userId == '') {
            layer.alert("请选择班级");
        } else {
            $("#chooseClass").modal('show');
            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderInterrupt/findClass',
                dataType: 'json',
                data: {
                    userId: userId
                },
                success: function (data) {
                    var values = data.data;
                    var student = data.student;

                    $("#studentName").val(student.cName);
                    $("#studentNo").val(student.cSerial);
                    $("#Telephone").val(student.cSMSTel);

                    if (values.length > 0) {
                        for (i in values) {
                            str += "<tr>" +
                                    "<td><input type='checkbox' name = 'cIdCheckbox' value=' " + values[i].cID + "'></td>" +
                                    "<td>" + values[i].className + "</td>" +
                                    "<td>" + values[i].shiftName + "</td>" +
                                    "<td>" + values[i].sDate + "</td>" +
                                    "<td>" + values[i].cnt + "/" + values[i].totalCnt + "</td>" +
                                    "<td>" + values[i].totalAmount + "</td>" +
                                    "<td>" + values[i].xyAmount + "</td>" +
                                    "</tr>";
                        }
                    } else {
                        str = "<tr align='left'>" +
                                "<td colspan='12'><span style='font-size: 16px;color: red'>暂无班级</span></td>" +
                                "</tr>";
                    }
                    $("#classTable tbody").html(str);
                }
            });
        }
    };

    function addClass() {

        var addClass = '';
        var userId = $("input[name='cidRadio']:checked").val();
        if($("input:checkbox[name='cIdCheckbox']:checked").length > 0){
            var claID = '';
            $("input:checkbox[name='cIdCheckbox']:checked").each(function () {
                var classID = $(this).val();
                $.ajax({
                    type: 'post',
                    url: '${GLOBAL.basePath}/workOrderInterrupt/findClassToPage',
                    dataType: 'json',
                    data: {
                        classID: classID,
                        userId: userId
                    },
                    success: function (data) {
                        var values = data.tClass;
                        addClass += "<tr>" +
                                "<td>" + values.className + "</td>" +
                                "<td>" + values.shiftName + "</td>" +
                                "<td>" + values.sDate + "</td>" +
                                "<td>" + values.cnt + "/" + values.totalCnt + "</td>" +
                                "<td>" + values.totalAmount + "</td>" +
                                "<td>" + values.xyAmount + "</td>" +
                                "</tr>";
                        $("#shiteTable tbody").html(addClass);

                        claID = claID+','+classID;
                        $("#classIDS").val(claID);
                        $("#chooseClass").modal('hide');
                        $("#chooseStudent").modal('hide');
                        cancelChoose();
                    }
                });
            });
            $("#classIDS").val(claID);
        }else {
            layer.alert("请选择班级");
        }
    }

    function cancelChoose() {
        $("#cNameId").val('');
        queryStu(page);
    };

    function addOrder(){
        var classIDS = $("#classIDS").val();
        var userById = $("#userById").val();
        var txtEndDay = $("#txtEndDay").val();
        var txtBeginDay = $("#txtBeginDay").val();
        var cremarksId = $("#cremarksId").val();

        if(userById == null || userById ==''){
            layer.alert("请选择停课学员");
        }else if(classIDS == null || classIDS ==''){
            layer.alert("请选择停课学员班级");
        }else if(txtBeginDay == null || txtBeginDay ==''){
            layer.alert("停课日期不能为空");
        }else if(txtEndDay == null || txtEndDay ==''){
            layer.alert("复课日期不能为空");
        }else if(cremarksId == null || cremarksId ==''){
            layer.alert("备注不能为空");
        }else{
           $("#sysReportForm").submit();
        }
    };

    function adopt() {
        var odsUserName = $("#odsUserName").val();
        var odsPassword = $("#odsPassword").val();
        var userID = $("#tEmployeeID").val();
        if (odsUserName == '' || odsPassword == '') {
            $("#info").html("账号或密码不能为空");
        } else {
            $("#info").html("");

            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderInterrupt/odsLogin',
                dataType: 'json',
                data: {
                    odsUserName: odsUserName,
                    odsPassword: odsPassword,
                    userID: userID
                },
                success: function (data) {
                    if(data.data >0){
                        $("#adopt").modal('hide');
                    }else{
                        $("#info").html("账号或密码输入错误");
                    }
                }
            });
        }
    }
</script>
