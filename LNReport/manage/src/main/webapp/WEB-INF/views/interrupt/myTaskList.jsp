<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/myBase_body_header.jsp" %>
<%@include file="../common/myNavbar.jsp" %>
<%--<%@include file="../common/myPage_content_pre.jsp" %>--%>

<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.css"/>

<div class="page-content">
    <div class="row">
        <div>&nbsp;</div>
        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="workOrderTable" class="table table-striped table-hover" width="100%"
                       style="font-size: 14px;">
                    <thead>
                    <tr style="color: #356FBE; background: #E3F0F4">
                        <th>单据号</th>
                        <th>校区</th>
                        <th>学员姓名</th>
                        <th>申请时间</th>
                        <th>申请人</th>
                        <th>申请类型</th>
                        <th>当前操作人</th>
                        <th>当前进度</th>
                        <th>是否打印</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <div style="height: 66px; padding-bottom:0px; top:0px;">
                    <div id="pagination1" class="page fl"></div>
                </div>
            </div>
        </div>
    </div>
    <input id="cstatus" type="hidden" value="${ctstaus}">
    <input id="CampusID" type="hidden" value="${CampusID}">
    <input id="userID" type="hidden" value="${userID}">
    <input id="isLogin" type="hidden" value="${isLogin}">
</div>

<div class="modal fade" id="adopt" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 550px">
            <div class="modal-header"
                 style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">提示</span></div>
            <div class="modal-body" style="width: 100%">
                <strong><h3>请先认证ODS</h3></strong>

                <div>&nbsp;</div>
                <strong>账号:</strong> <input id="odsUserName"> &nbsp;&nbsp;&nbsp;&nbsp;
                <strong>密码:</strong> <input id="odsPassword">
                <br>
                <span id="info" style="color: red"></span>
            </div>

            <div class="modal-footer" style="background: #F0F0F0 ;">
                <button type="button" class="btn btn-xs btn-success" onclick="adopt()"
                        data-toggle="modal" data-target="#chooseClass"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">
                    确定
                </button>
            </div>
        </div>
    </div>
</div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/myScript.jsp" %>

<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.min.js"></script>

<script type="text/javascript">
    var page = 1;
    var siLogin = $("#isLogin").val();

    jQuery(function ($) {
        queryWorkOrder(page);
        $("#pagination1").pagination({
            currentPage: 1,
            totalPage: 3,
            callback: function (current) {
                queryWorkOrder(current);
            }
        });

        if (siLogin != 'N') {
            $("#adopt").modal('show');
        }
    });

    function queryWorkOrder(page) {
        var cstatus = $("#cstatus").val();
        var CampusID = $("#CampusID").val();
        var userID = $("#userID").val();
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/myTaskList',
            dataType: 'json',
            data: {
                cstatus: cstatus,
                CampusID: CampusID,
                page: page
            },
            success: function (data) {
                var values = data.data;
                var count = data.count;
                if (values.length > 0) {
                    for (i in values) {
                        var ctype = values[i].cType;
                        var cTypeValue;
                        if (ctype == 1) {
                            cTypeValue = "停课";
                        } else if (ctype == 2) {
                            cTypeValue = "复课";
                        } else if (ctype == 3) {
                            cTypeValue = "转班";
                        } else if (ctype == 4) {
                            cTypeValue = "转校";
                        } else if (ctype == 5) {
                            cTypeValue = "退费";
                        } else if (ctype == 6) {
                            cTypeValue = "转费";
                        }

                        var cstatus = values[i].cstatus;
                        var cstatusValue;
                        if (cstatus == 1) {
                            cstatusValue = "未审核";
                        } else if (cstatus == 2) {
                            cstatusValue = "审核通过";
                        } else if (cstatus == 3) {
                            cstatusValue = "驳回申请";
                        } else if (cstatus == 4) {
                            cstatusValue = "操作成功";
                        } else if (cstatus == 5) {
                            cstatusValue = "无法操作";
                        }

                        var cisprint = values[i].cIsPrint;
                        var cisprintValue;
                        if (cisprint == 2) {
                            cisprintValue = "未打印";
                        } else if (cisprint == 1) {
                            cisprintValue = "已打印";
                        }

                        var auditTime;
                        if (values[i].auditTime == 'undefined' || values[i].auditTime == null) {
                            auditTime = "";
                        } else {
                            auditTime = values[i].auditTime;
                        }

                        var employName;
                        if (values[i].employName == 'undefined' || values[i].employName == null) {
                            employName = "";
                        } else {
                            employName = values[i].employName;
                        }

                        str += "<tr>" +
                                "<td><a href=${GLOBAL.basePath}/workOrderInterrupt/detail?orderNo=" + values[i].cOrderNo + "&flag=2&userID="+userID+">" + values[i].cOrderNo + "</a></td>" +
                                "<td>" + values[i].campus + "</td>" +
                                "<td>" + values[i].userName + "</td>" +
                                "<td>" + values[i].cCreateDate + "</td>" +
                                "<td>" + values[i].subName + "</td>" +
                                "<td>" + cTypeValue + "</td>" +
                                "<td>" + employName + "</td>" +
                                "<td>" + cstatusValue + "</td>" +
                                "<td>" + cisprintValue + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#workOrderTable tbody").html(str);

                $("#pagination1").pagination({
                    currentPage: 1,
                    totalPage: count,
                    callback: function (current) {
                        queryPage(current);
                    }
                });
            }
        });
    };

    function queryPage(current) {
        var cstatus = $("#cstatus").val();
        var CampusID = $("#CampusID").val();
        var userID = $("#userID").val();

        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/myTaskList',
            dataType: 'json',
            data: {
                cstatus: cstatus,
                CampusID: CampusID,
                page: current
            },
            success: function (data) {
                var values = data.data;
                if (values.length > 0) {
                    for (i in values) {
                        var ctype = values[i].cType;
                        var cTypeValue;
                        if (ctype == 1) {
                            cTypeValue = "停课";
                        } else if (ctype == 2) {
                            cTypeValue = "复课";
                        } else if (ctype == 3) {
                            cTypeValue = "转班";
                        } else if (ctype == 4) {
                            cTypeValue = "转校";
                        } else if (ctype == 5) {
                            cTypeValue = "退费";
                        } else if (ctype == 6) {
                            cTypeValue = "转费";
                        }

                        var cstatus = values[i].cstatus;
                        var cstatusValue;
                        if (cstatus == 1) {
                            cstatusValue = "未审核";
                        } else if (cstatus == 2) {
                            cstatusValue = "审核通过";
                        } else if (cstatus == 3) {
                            cstatusValue = "驳回申请";
                        } else if (cstatus == 4) {
                            cstatusValue = "操作成功";
                        } else if (cstatus == 5) {
                            cstatusValue = "无法操作";
                        }

                        var cisprint = values[i].cIsPrint;
                        var cisprintValue;
                        if (cisprint == 2) {
                            cisprintValue = "未打印";
                        } else if (cisprint == 1) {
                            cisprintValue = "已打印";
                        }

                        var auditTime;
                        if (values[i].auditTime == 'undefined' || values[i].auditTime == null) {
                            auditTime = "";
                        } else {
                            auditTime = values[i].auditTime;
                        }

                        var employName;
                        if (values[i].employName == 'undefined' || values[i].employName == null) {
                            employName = "";
                        } else {
                            employName = values[i].employName;
                        }

                        str += "<tr>" +
                                "<td><a href=${GLOBAL.basePath}/workOrderInterrupt/detail?orderNo=" + values[i].cOrderNo + "&flag=2&userID="+userID+">" + values[i].cOrderNo + "</a></td>" +
                                "<td>" + values[i].campus + "</td>" +
                                "<td>" + values[i].userName + "</td>" +
                                "<td>" + values[i].cCreateDate + "</td>" +
                                "<td>" + values[i].subName + "</td>" +
                                "<td>" + cTypeValue + "</td>" +
                                "<td>" + employName + "</td>" +
                                "<td>" + cstatusValue + "</td>" +
                                "<td>" + cisprintValue + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#workOrderTable tbody").html(str);
            }
        });
    };

    function adopt() {
        var odsUserName = $("#odsUserName").val();
        var odsPassword = $("#odsPassword").val();
        var userID = $("#userID").val();
        if (odsUserName == '' || odsPassword == '') {
            $("#info").html("账号或密码不能为空");
        } else {
            $("#info").html("");

            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderInterrupt/odsLogin',
                dataType: 'json',
                data: {
                    odsUserName: odsUserName,
                    odsPassword: odsPassword,
                    userID: userID
                },
                success: function (data) {
                    if(data.data >0){
                        $("#adopt").modal('hide');
                    }else{
                        $("#info").html("账号或密码输入错误");
                    }
                }
            });
        }
    }
</script>
