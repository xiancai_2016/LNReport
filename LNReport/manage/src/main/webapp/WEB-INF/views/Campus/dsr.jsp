<%--
   For com.football
   Copyright [2015/11/17] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/campusDSR"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <label class="col-sm-1 control-label">校区</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="CampusID">
                                <option value="">全部</option>
                                <c:forEach items="${departs}" var="item">
                                    <option value="${item.cID}"
                                            <c:if test="${item.cID == CampusID}">selected</c:if>>${item.cNAME}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <div class="col-xs-6">
                TIN
                <div class="table-responsive">
                    <table id="table_tin" class="table" width="100%" style="font-size: 8px">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>工号</th>
                            <th>姓名</th>
                            <th>&nbsp;&nbsp;App&nbsp;&nbsp;</th>
                            <th>Fresh
                                Pres
                            </th>
                            <th>Show
                                Ratio
                            </th>
                            <th>签单Ratio</th>
                            <th>OC-Show</th>
                            <th>Contracts</th>
                            <th>Cash-In</th>
                        </tr>
                        </thead>
                        <c:forEach items="${TIN}" var="item">
                            <tr>
                                <td>${item.EmpCode}</td>
                                <td>${item.EmpName}</td>
                                <td>${item.APPNum}</td>
                                <td>${item.FreshPres}</td>
                                <td>${item.Show}</td>
                                <td>${item.Ratio}</td>
                                <td>${item.OC}</td>
                                <td>${item.Contracts}</td>
                                <td>${item.CashIn}</td>
                            </tr>
                        </c:forEach>
                        <tr style="background: #438eb9; color: white">
                            <td></td>
                            <td>总计:</td>
                            <td>${appSumTIN}</td>
                            <td>${FreshPresSumTIN}</td>
                            <td>${ShowRatioSumTIN}</td>
                            <td>${RatioSumTIN}</td>
                            <td>${ocSumTIN}</td>
                            <td>${ContractsSumTIN}</td>
                            <td>¥${CashInSumTIN}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-xs-6">
                TOUT
                <div class="table-responsive">
                    <table id="table_tout" class="table" width="100%" style="font-size: 8px">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>工号</th>
                            <th>姓名</th>
                            <th>&nbsp;App&nbsp;&nbsp;</th>
                            <th>Fresh
                                Pres
                            </th>
                            <th>Show
                                Ratio
                            </th>
                            <th>签单Ratio</th>
                            <th>OC-Show</th>
                            <th>Contracts</th>
                            <th>Cash-In</th>
                        </tr>
                        </thead>
                        <c:forEach items="${TOUT}" var="item">
                            <tr>
                                <td>${item.EmpCode}</td>
                                <td>${item.EmpName}</td>
                                <td>${item.APPNum}</td>
                                <td>${item.FreshPres}</td>
                                <td>${item.Show}</td>
                                <td>${item.Ratio}</td>
                                <td>${item.OC}</td>
                                <td>${item.Contracts}</td>
                                <td>${item.CashIn}</td>
                            </tr>
                        </c:forEach>
                        <tr style="background: #438eb9; color: white">
                            <td></td>
                            <td>总计:</td>
                            <td>${appSumTOUT}</td>
                            <td>${FreshPresSumTOUT}</td>
                            <td>${ShowRatioSumTOUT}</td>
                            <td>${RatioSumTOUT}</td>
                            <td>${ocSumTOUT}</td>
                            <td>${ContractsSumTOUT}</td>
                            <td>¥${CashInSumTOUT}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="col-xs-6">
                WOLK_IN
                <div class="table-responsive">
                    <table id="table_WOLK_IN" class="table" width="100%" style="font-size: 8px">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>工号</th>
                            <th>姓名</th>
                            <th>WalkIn</th>
                            <th>Fresh
                                Pres
                            </th>
                            <th>Show
                                Ratio
                            </th>
                            <th>签单Ratio</th>
                            <th>OC-Show</th>
                            <th>Contracts</th>
                            <th>Cash-In</th>
                            <th></th>
                        </tr>
                        </thead>
                        <c:forEach items="${WKIN}" var="item">
                            <tr>
                                <td>${item.EmpCode}</td>
                                <td>${item.EmpName}</td>
                                <td>${item.Leads}</td>
                                <td>${item.FreshPres}</td>
                                <td>${item.Show}</td>
                                <td>${item.Ratio}</td>
                                <td>${item.OC}</td>
                                <td>${item.Contracts}</td>
                                <td>${item.CashIn}</td>
                            </tr>
                        </c:forEach>
                        <tr style="background: #438eb9; color: white">
                            <td></td>
                            <td>总计:</td>
                            <td>${appSumWKIN}</td>
                            <td>${FreshPresSumWKIN}</td>
                            <td>${ShowRatioSumWKIN}</td>
                            <td>${RatioSumWKIN}</td>
                            <td>${ocSumWKIN}</td>
                            <td>${ContractsSumWKIN}</td>
                            <td>¥${CashInSumWKIN}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col-xs-6">
                REF
                <div class="table-responsive">
                    <table id="table_REF" class="table" width="100%" style="font-size: 8px">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>工号</th>
                            <th>姓名</th>
                            <th>Leads</th>
                            <th>Fresh
                                Pres
                            </th>
                            <th>Show
                                Ratio
                            </th>
                            <th>签单Ratio</th>
                            <th>OC-Show</th>
                            <th>Contracts</th>
                            <th>Cash-In</th>
                        </tr>
                        </thead>
                        <c:forEach items="${REF}" var="item">
                            <tr>
                                <td>${item.EmpCode}</td>
                                <td>${item.EmpName}</td>
                                <td>${item.Leads}</td>
                                <td>${item.FreshPres}</td>
                                <td>${item.Show}</td>
                                <td>${item.Ratio}</td>
                                <td>${item.OC}</td>
                                <td>${item.Contracts}</td>
                                <td>${item.CashIn}</td>
                            </tr>
                        </c:forEach>
                        <tr style="background: #438eb9; color: white">
                            <td></td>
                            <td>总计:</td>
                            <td>${appSumREF}</td>
                            <td>${FreshPresSumREF}</td>
                            <td>${ShowRatioSumREF}</td>
                            <td>${RatioSumREF}</td>
                            <td>${ocSumREF}</td>
                            <td>${ContractsSumREF}</td>
                            <td>¥${CashInSumREF}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-5">
        RENEW
        <div class="table-responsive">
            <table id="table_RENEW" class="table" width="100%" style="font-size: 8px">
                <thead>
                <tr style="background: #438eb9; color: white">
                    <th>工号</th>
                    <th>姓名</th>
                    <th>Contracts</th>
                    <th>Cash-In</th>
                </tr>
                </thead>
                <c:forEach items="${RENEW}" var="item">
                    <tr>
                        <td>${item.EmpCode}</td>
                        <td>${item.EmpName}</td>
                        <td>${item.Contracts}</td>
                        <td>${item.CashIn}</td>
                    </tr>
                </c:forEach>
                <tr style="background: #438eb9; color: white">
                    <td></td>
                    <td>总计:</td>
                    <td>${ContractsSumRENEW}</td>
                    <td>¥${CashInSumRENEW}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div id="mask" class="img"></div>
</div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }
</script>