<%--
   For com.football
   Copyright [2015/11/17] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/campusDSR/totalCARList"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>
                        <input name="key" value="${key}" type="hidden">
                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <label class="col-sm-1 control-label">校区</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="CampusID">
                                <option value="">全部</option>
                                <c:forEach items="${departs}" var="item">
                                    <option value="${item.cID}"
                                            <c:if test="${item.cID == CampusID}">selected</c:if>>${item.cNAME}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="table_REF" class="table" width="100%" style="font-size: 8px">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th></th>
                        <th>课程类型</th>
                        <th>报名人数</th>
                        <th>报名金额</th>
                        <th>退费人数</th>
                        <th>退费金额</th>
                    </tr>
                    </thead>
                    <c:if test="${!empty chargeAndRefundByK}">
                        <c:forEach items="${chargeAndRefundByK}" var="item">
                            <tr>
                                <td></td>
                                <td>${item.tgrade}</td>
                                <td>${item.stucount}</td>
                                <td>${item.tcost}</td>
                                <td>${item.rstucount}</td>
                                <td>${item.rtcost}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapK}">
                        <tr style="background: #438eb9; color: white">
                            <td>小计:</td>
                            <td></td>
                            <td> ${mapK.stucount}</td>
                            <td>${mapK.tcost}</td>
                            <td>${mapK.rstucount}</td>
                            <td>${mapK.rtcost}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty chargeAndRefundByC}">
                        <c:forEach items="${chargeAndRefundByC}" var="item">
                            <tr>
                                <td></td>
                                <td>${item.tgrade}</td>
                                <td>${item.stucount}</td>
                                <td>${item.tcost}</td>
                                <td>${item.rstucount}</td>
                                <td>${item.rtcost}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapC}">
                        <tr style="background: #438eb9; color: white">
                            <td>小计:</td>
                            <td></td>
                            <td>${mapC.stucount}</td>
                            <td>${mapC.tcost}</td>
                            <td>${mapC.rstucount}</td>
                            <td>${mapC.rtcost}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty chargeAndRefundByY}">
                        <c:forEach items="${chargeAndRefundByY}" var="item">
                            <tr>
                                <td></td>
                                <td>${item.tgrade}</td>
                                <td>${item.stucount}</td>
                                <td>${item.tcost}</td>
                                <td>${item.rstucount}</td>
                                <td>${item.rtcost}</td>
                            </tr>
                        </c:forEach>
                    </c:if>


                    <c:if test="${!empty mapY}">
                        <tr style="background: #438eb9; color: white">
                            <td>小计:</td>
                            <td></td>
                            <td>${mapY.stucount}</td>
                            <td>${mapY.tcost}</td>
                            <td>${mapY.rstucount}</td>
                            <td>${mapY.rtcost}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty SKC}">
                        <tr>
                            <td></td>
                            <td>暑假班K/C</td>
                            <td></td>
                            <td>${SKC.skcFact}</td>
                            <td></td>
                            <td>${SKC.skcrFact}</td>
                        </tr>
                    </c:if>

                    <tr></tr>

                    <c:if test="${!empty totalChargeAndRefund}">
                        <tr style="background: #438eb9; color: white">
                            <td>总计:</td>
                            <td></td>
                            <td>${totalChargeAndRefund.sumstucount}</td>
                            <td>${totalChargeAndRefund.sumtcost}</td>
                            <td>${totalChargeAndRefund.sumrstucount}</td>
                            <td>${totalChargeAndRefund.sumrtcost}</td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }
</script>