<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/myBase_body_header.jsp" %>
<%@include file="../common/myNavbar.jsp" %>
<%--<%@include file="../common/myPage_content_pre.jsp" %>--%>

<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.css"/>

<style type="text/css">
    .input {
        border-color: #E4E4E4;
        border-style: solid;
        border-top-width: 0px;
        border-right-width: 0px;
        border-bottom-width: 1px;
        border-left-width: 0px;
        width: 65%
    }

    .fount {
        color: #333333;
        font-size: 16px;
        font-weight: bold;
    }

    input::-webkit-input-placeholder {
        color: #C9C9C9;
    }

    input::-moz-placeholder { /* Mozilla Firefox 19+ */
        color: #C9C9C9;
    }

    input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
        color: #C9C9C9;
    }

    input:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #C9C9C9;
    }

    * {
        margin: 0;
        padding: 0;
    }

    body {
        font-family: "微软雅黑";
        background: #eee;
    }

    button {
        display: inline-block;
        padding: 6px 12px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        border: 1px solid transparent;
        border-radius: 4px;
        border-color: #28a4c9;
        color: #fff;
        background-color: #5bc0de;
        margin: 20px 20px 0 0;
    }

    .page {
        width: 700px;
    }

    .fl {
        float: left;
    }
</style>

<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<div class="page-content">

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header" style="color: #737373">
                    <div class="form-group">
                        <div class="col-sm-3"><span></span></div>

                        <div class="col-sm-3"><span id="proposer"></span></div>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form:form class="form-horizontal" modelAttribute="workOrderOfResume"
                                   action="${GLOBAL.basePath}/workOrderCost/save" method="post"
                                   id="workOrderOfResumeForm">
                            <div>&nbsp;</div>
                            <input type="hidden" name="classID" id="classIDS">
                            <input type="hidden" name="userId" id="userById">
                            <input type="hidden" name="userIdReceive" id="userIdReceive">

                            <input type="hidden" name="crecoverydate" id="crecoverydate">

                            <input type="hidden" name="courseTime" id="courseTimeValue">
                            <input type="hidden" name="courseMoney" id="courseMoneyValue">
                            <input type="hidden" name = "tEmployeeID" id = "tEmployeeID" value="${tEmployeeID}">

                            <fieldset>
                                <p>
                                    <label class="fount">转让学员</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <span class="help-inline"><font color="red">*</font> </span>学员姓名:
                                        <input class="input" data-toggle="modal" id="studentName"
                                               readonly=readonly required = required
                                               data-target="#chooseStudent" placeholder="请选择转让学员"/>
                                    </div>

                                    <div class="col-sm-3">
                                        学号:
                                        <input class="input" readonly=readonly id="studentNo" required = required/>
                                    </div>

                                    <div class="col-sm-3">
                                        电话:
                                        <input class="input" readonly=readonly id="Telephone" required = required/>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="col-sm-11">
                                <p>
                                    <label class="fount">课程信息</label>
                                </p>
                                <fieldset>
                                    <table class="table table-bordered" id="shiftTableByPage"
                                           style="font-size: 14px; font-weight:bold;">
                                        <thead>
                                        <tr style="color: #356FBE; background: #e3f0f4">
                                            <th>课程级别</th>
                                            <th>购买课程</th>
                                            <th>消耗课次</th>
                                            <th>剩余金额</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </fieldset>
                            </div>

                            <div class="col-sm-11">
                                <fieldset>
                                    <p>
                                        <label class="fount"></label>
                                    </p>

                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            电子钱包余额:
                                            <input class="input" data-toggle="modal" id="walletMoney"
                                                   readonly=readonly required = required />
                                        </div>

                                        <div class="col-sm-2">

                                        </div>

                                        <div class="col-sm-4">
                                            转让金额总计:
                                            <input class="input" readonly=readonly id="totalMoney" required = required />
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-sm-11">
                                <fieldset>
                                    <p>
                                        <label class="fount"></label>
                                    </p>

                                    <p>
                                        <label class="fount">接收转让学员</label>
                                    </p>

                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <span class="help-inline"><font color="red">*</font> </span>学员姓名:
                                            <input class="input" data-toggle="modal" id="studentNameNew"
                                                   readonly=readonly required = required
                                                   data-target="#chooseStudentByClass" placeholder="请选择接收学员"/>
                                        </div>

                                        <div class="col-sm-3">
                                            学号:
                                            <input class="input" readonly=readonly required = required id="studentNoNew"/>
                                        </div>

                                        <div class="col-sm-3">
                                            电话:
                                            <input class="input" readonly=readonly required = required id="TelephoneNew"/>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-sm-11">
                                <fieldset>
                                    <p>
                                        <label class="fount"></label>
                                    </p>
                                    <table class="table table-bordered" id="studentBTable"
                                           style="font-size: 14px; font-weight:bold;">
                                        <thead>
                                        <tr style="color: #356FBE; background: #e3f0f4">
                                            <th>课程级别</th>
                                            <th>班级编号</th>
                                            <th>入班日期</th>
                                            <th>转让课单价</th>
                                            <th>接受转让课次</th>
                                            <th>预存电子钱包</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </fieldset>
                            </div>

                            <fieldset>
                                <p>
                                    <label class="fount"></label>
                                </p>

                                <p>
                                    <label class="fount" >备注</label>
                                </p>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <textarea rows="5" cols="128" name="cremarks" id = "cremarksId" maxlength="100"></textarea>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-actions center">
                                <button type="button" onclick="addOrder()"
                                        style="font-size: 14px; font-weight:bold; width: 100px; background-color: #F97961 !important;border-color: #F97961;"
                                        id="save_btn">
                                    确认提交
                                </button>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="chooseStudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 700px">
            <div class="modal-header" style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">请选择学员</span></div>
            <div class="modal-body" style="width: 100%">
                <div class="col-sm-5">
                    <input class="input" style="width: 100%" name="cname" id="cNameId"
                           placeholder="请输入学生姓名、学号、电话"/>
                </div>
                <button type="submit" class="btn btn-xs btn-success" onclick="queryStu(0)"
                        style="background-color: #3598DB !important;border-color: #3598DB; width: 60px">查询
                </button>
                <div>&nbsp;</div>
                <table class="table table-bordered" id="shiftTable" style="font-size: 14px; font-weight:bold;">
                    <thead>
                    <tr style="color: #356FBE; background: #E4F2FF">
                        <th style="width: 20px"></th>
                        <th style="width: 30px">学号</th>
                        <th style="width: 100px">姓名</th>
                        <th style="width: 40px">手机号</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <div style="height: 66px; padding-bottom:0px; top:0px;">
                    <div id="pagination1" class="page fl"></div>
                </div>
            </div>
            <div class="modal-footer" style="background: #F0F0F0 ;height: 43px">
                <button type="button" class="btn btn-xs btn-success" onclick="transferStudent()"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">确定
                </button>
                <button type="button" class="btn btn-xs btn-default" data-dismiss="modal" onclick="cancelChoose()"
                        style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                        style="color: #666666">取消</span></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="chooseStudentByClass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 700px">
            <div class="modal-header" style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">请选择学员</span></div>
            <div class="modal-body" style="width: 100%">
                <div class="col-sm-5">
                    <input class="input" style="width: 100%" name="cname" id="cNameIdByClass"
                           placeholder="请输入学生姓名、学号、电话"/>
                </div>
                <button type="submit" class="btn btn-xs btn-success" onclick="queryStuByClass(0)"
                        style="background-color: #3598DB !important;border-color: #3598DB; width: 60px">查询
                </button>
                <div>&nbsp;</div>
                <table class="table table-bordered" id="shiftTableClass" style="font-size: 14px; font-weight:bold;">
                    <thead>
                    <tr style="color: #356FBE; background: #E4F2FF">
                        <th style="width: 20px"></th>
                        <th style="width: 30px">学号</th>
                        <th style="width: 100px">姓名</th>
                        <th style="width: 40px">手机号</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <div style="height: 66px; padding-bottom:0px; top:0px;">
                    <div id="pagination11" class="page fl"></div>
                </div>
            </div>
            <div class="modal-footer" style="background: #F0F0F0 ;height: 43px">
                <button type="button" class="btn btn-xs btn-success" onclick="chooseClass(0)"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">确定
                </button>
                <button type="button" class="btn btn-xs btn-default" data-dismiss="modal" onclick="cancelChoose()"
                        style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                        style="color: #666666">取消</span></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="classTableResumeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 700px">
            <div class="modal-header" style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">请选择班级</span></div>
            <div class="modal-body" style="width: 100%">
                <div class="col-sm-5">
                    <input class="input" style="width: 100%" name="className" id="className"
                           placeholder="请输入班级名称"/>
                </div>
                <button type="submit" class="btn btn-xs btn-success" onclick="chooseClass(0)"
                        style="background-color: #3598DB !important;border-color: #3598DB; width: 60px">查询
                </button>
                <div>&nbsp;</div>
                <table class="table table-bordered" id="classTableResume" style="font-size: 14px; font-weight:bold;">
                    <thead>
                    <tr style="color: #356FBE; background: #E4F2FF">
                        <th style="width: 20px"></th>
                        <th>班级名称</th>
                        <th>课程</th>
                        <th>老师</th>
                        <th>人数</th>
                        <th>开班时间</th>
                        <th>上课时间</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div style="height: 66px; padding-bottom:0px; top:0px;">
                    <div id="pagination2" class="page fl"></div>
                </div>
            </div>
            <div class="modal-footer" style="background: #F0F0F0 ;height: 43px">
                <span class="help-inline"><font color="red">*</font> </span>转让日期:
                <input type="text" required=required name="resumeDate"
                       id="resumeDate" style="padding: 3px 4px; width: 18%"
                       onclick="WdatePicker({dateFmt:'yyyy-MM-dd', minDate:'%y-%M-{%d}'});"/>
                <span class="help-inline"><font color="red">*</font> </span>购买次数:
                <input type="number" required=required name="Time" id="courseTime" style="padding: 3px 4px; width: 13%"/>

                <span class="help-inline"><font color="red">*</font> </span>课单价:
                <input type="number" required=required name="courseDate" id="courseMoney" style="padding: 3px 4px; width: 13%"/>

                <span class="help-inline"><font color="red">&nbsp;&nbsp;</font> </span>

                <button type="button" class="btn btn-xs btn-success" onclick="changeCost()"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">确定
                </button>
                <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"
                        style="background-color: #E5E6E6 !important;border-color: #E5E6E6; width: 60px"><span
                        style="color: #666666">取消</span></button>
            </div>
        </div>
    </div>
    <input id="isLogin" type="hidden" value="${isLogin}">
</div>

<div class="modal fade" id="adopt" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 550px">
            <div class="modal-header"
                 style="height: 34px; padding: 8px; background: #1892D1"><span
                    style="font-size: 14px;color: #FFFFFF">提示</span></div>
            <div class="modal-body" style="width: 100%">
                <strong><h3>请先认证ODS</h3></strong>

                <div>&nbsp;</div>
                <strong>账号:</strong> <input id="odsUserName"> &nbsp;&nbsp;&nbsp;&nbsp;
                <strong>密码:</strong> <input id="odsPassword">
                <br>
                <span id="info" style="color: red"></span>
            </div>

            <div class="modal-footer" style="background: #F0F0F0 ;">
                <button type="button" class="btn btn-xs btn-success" onclick="adopt()"
                        data-toggle="modal" data-target="#chooseClass"
                        style="background-color: #4CAF50 !important;border-color: #4CAF50; width: 60px">
                    确定
                </button>
            </div>
        </div>
    </div>
</div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/myScript.jsp" %>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/pagination/jquery.pagination.min.js"></script>

<script type="text/javascript">
    var studentList;
    var shiftTableList;
    var page = 0;
    var siLogin = $("#isLogin").val();

    jQuery(function ($) {
        queryStu(page);
        queryStuByClass(page);
        $("#pagination1").pagination({
            currentPage: 1,
            totalPage: 3,
            callback: function (current) {
                queryStu(current);
            }
        });

        $("#pagination11").pagination({
            currentPage: 1,
            totalPage: 3,
            callback: function (current) {
                queryStuByClass(current);
            }
        });

        if (siLogin != 'N') {
            $("#adopt").modal('show');
        }
    });

    //查询学生
    function queryStu(page) {
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findStudent',
            dataType: 'json',
            data: {
                cName: $("#cNameId").val(),
                userID: $("#tEmployeeID").val(),
                page: page
            },
            success: function (data) {
                var values = data.data;
                var count = data.count;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'cidRadio' value=' " + values[i].cid + "'></td>" +
                                "<td>" + values[i].cserial + "</td>" +
                                "<td>" + values[i].cname + "</td>" +
                                "<td>" + values[i].csmstel + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#shiftTable tbody").html(str);

                $("#pagination1").pagination({
                    currentPage: 1,
                    totalPage: count,
                    callback: function (current) {
                        queryPage(current);
                    }
                });
            }
        });
    };

    //学生分页
    function queryPage(current) {
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findStudent',
            dataType: 'json',
            data: {
                cName: $("#cNameId").val(),
                userID: $("#tEmployeeID").val(),
                page: current
            },
            success: function (data) {
                var values = data.data;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'cidRadio' value=' " + values[i].cid + "'></td>" +
                                "<td>" + values[i].cserial + "</td>" +
                                "<td>" + values[i].cname + "</td>" +
                                "<td>" + values[i].csmstel + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#shiftTable tbody").html(str);
            }
        });
    };

    //查询学生
    function queryStuByClass(page) {
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findStudent',
            dataType: 'json',
            data: {
                cName: $("#cNameIdByClass").val(),
                userID: $("#tEmployeeID").val(),
                page: page
            },
            success: function (data) {
                var values = data.data;
                var count = data.count;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'cidRadioByClass' value=' " + values[i].cid + "'></td>" +
                                "<td>" + values[i].cserial + "</td>" +
                                "<td>" + values[i].cname + "</td>" +
                                "<td>" + values[i].csmstel + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#shiftTableClass tbody").html(str);

                $("#pagination11").pagination({
                    currentPage: 1,
                    totalPage: count,
                    callback: function (current) {
                        queryPageByClass(current);
                    }
                });
            }
        });
    };

    //学生分页
    function queryPageByClass(current) {
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/findStudent',
            dataType: 'json',
            data: {
                cName: $("#cNameId").val(),
                userID: $("#tEmployeeID").val(),
                page: current
            },
            success: function (data) {
                var values = data.data;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'cidRadioByClass' value=' " + values[i].cid + "'></td>" +
                                "<td>" + values[i].cserial + "</td>" +
                                "<td>" + values[i].cname + "</td>" +
                                "<td>" + values[i].csmstel + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#shiftTableClass tbody").html(str);
            }
        });
    };

    function transferStudent() {
        var str = '';
        var userId = $("input[name='cidRadio']:checked").val();
        $("#userById").val(userId);

        $("#classIDS").val("");
        $("#userIdReceive").val("");
        $("#courseTimeValue").val("");
        $("#courseMoneyValue").val("");

        $("#studentNameNew").val("");
        $("#studentNoNew").val("");
        $("#TelephoneNew").val("");

        $("#studentBTable tbody").html("")

        if (userId == null || userId == '') {
            layer.alert("请选择学员");
        } else {
            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderInterrupt/findStudentAndReceipt',
                dataType: 'json',
                data: {
                    userId: userId
                },
                success: function (data) {
                    var values = data.data;
                    var student = data.student;

                    $("#studentName").val(student.cName);
                    $("#studentNo").val(student.cSerial);
                    $("#Telephone").val(student.cSMSTel);
                    $("#totalMoney").val(student.totalMoney);
                    $("#walletMoney").val(student.walletMoney);

                    if (values.length > 0) {
                        for (i in values) {
                            str += "<tr>" +
                                    "<td>" + values[i].cName + "</td>" +
                                    "<td>" + values[i].byTime + "</td>" +
                                    "<td>" + values[i].syTime + "</td>" +
                                    "<td>" + values[i].cRemainMoney + "</td>" +
                                    "</tr>";
                        }
                    } else {
                        str = "<tr align='left'>" +
                                "<td colspan='12'><span style='font-size: 16px;color: red'>暂无课程</span></td>" +
                                "</tr>";
                    }
                    $("#shiftTableByPage tbody").html(str);

                    $("#chooseStudent").modal('hide');
                    cancelChoose();
                }
            });
        }
    };

    //选择接收学员班级
    function chooseClass(page) {
        //清除原來选择
        var str = '';
        var userId = $("input[name='cidRadioByClass']:checked").val();

        var className = $("#className").val();//班级名称

        var resumeDate = $("#resumeDate").val();//复课时间
        $("#crecoverydate").val(resumeDate);

        if (userId != null) {
            $("#chooseStudentByClass").modal('hide');
            $("#classTableResumeModal").modal('show');
            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderOfResume/findResumeClass',
                dataType: 'json',
                data: {
                    userId: userId,
                    page: page,
                    className: className
                },
                success: function (data) {
                    var values = data.data;
                    var count = data.count;
                    if (values.length > 0) {
                        $("#chooseClass").modal('hide');
                        for (i in values) {
                            str += "<tr>" +
                                    "<td><input type='radio' name = 'resumeRadioNew' value=' " + values[i].cID + "'></td>" +
                                    "<td>" + values[i].className + "</td>" +
                                    "<td>" + values[i].shiftName + "</td>" +
                                    "<td>" + values[i].cName + "</td>" +
                                    "<td>" + values[i].onGoingCount + "</td>" +
                                    "<td>" + values[i].onClassTime + "</td>" +
                                    "<td>" + values[i].sDate + "</td>" +
                                    "</tr>";
                        }
                    } else {
                        str = "<tr align='left'>" +
                                "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                                "</tr>";
                    }
                    $("#classTableResume tbody").html(str);

                    $("#pagination2").pagination({
                        currentPage: 1,
                        totalPage: count,
                        callback: function (current) {
                            choosePageClass(current, userId);
                        }
                    });
                }
            });

        } else {
            layer.alert("请选择学员");
        }
    };

    //查询复课到新班的班级
    function choosePageClass(current) {
        var userId = $("input[name='cidRadioByClass']:checked").val();
        var str = '';
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderOfResume/findResumeClass',
            dataType: 'json',
            data: {
                userId: userId,
                page: current
            },
            success: function (data) {
                var values = data.data;
                if (values.length > 0) {
                    for (i in values) {
                        str += "<tr>" +
                                "<td><input type='radio' name = 'resumeRadioNew' value=' " + values[i].cID + "'></td>" +
                                "<td>" + values[i].className + "</td>" +
                                "<td>" + values[i].shiftName + "</td>" +
                                "<td>" + values[i].cName + "</td>" +
                                "<td>" + values[i].onGoingCount + "</td>" +
                                "<td>" + values[i].onClassTime + "</td>" +
                                "<td>" + values[i].sDate + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>" +
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>" +
                            "</tr>";
                }
                $("#classTableResume tbody").html(str);
            }
        });
    };

    function changeCost(){
        var classID = $("input[name='resumeRadioNew']:checked").val();
        var userIdReceive = $("input[name='cidRadioByClass']:checked").val();

        var resumeDate = $("#resumeDate").val();
        var courseTime = $("#courseTime").val();
        var courseMoney = $("#courseMoney").val();

        var totalMoney = $("#totalMoney").val();
        var walletMoney = $("#walletMoney").val();

        $("#crecoverydate").val(resumeDate);
        $("#classIDS").val(classID);

        $("#courseTimeValue").val(courseTime);
        $("#courseMoneyValue").val(courseMoney);
        $("#userIdReceive").val(userIdReceive);

        var total = Number(totalMoney);

        var balance = total - courseTime*courseMoney;

        if(classID == null){
            layer.alert("请选择班级");
        }else if(resumeDate == null || resumeDate == ''){
            layer.alert("请输入转让日期");
            $("#resumeDate").focus()
        }else if(courseTime == null || courseTime == ''){
            $("#courseTime").focus()
            layer.alert("请输入课次");
        }else if(courseMoney == null || courseMoney == ''){
            $("#courseMoney").focus()
            layer.alert("请输入课单价");
        }else if(courseMoney == 0){
            $("#courseMoney").focus()
            layer.alert("课单价不能为0");
        }else if(balance < 0){
            layer.alert("购买的课次超额");
        }else{
            var str = '';
            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderInterrupt/findClassByCost',
                dataType: 'json',
                data: {
                    userId: userIdReceive,
                    classID: classID
                },
                success: function (data) {
                    var values = data.data;
                    var student = data.student;

                    $("#studentNameNew").val(student.cName);
                    $("#studentNoNew").val(student.cSerial);
                    $("#TelephoneNew").val(student.cSMSTel);

                    str += "<tr>" +
                                "<td>" + values.shiftName + "</td>" +
                                "<td>" + values.className + "</td>" +
                                "<td>" + resumeDate + "</td>" +
                                "<td>" + courseMoney + "</td>" +
                                "<td>" + courseTime + "</td>" +
                                "<td>" + balance + "</td>" +
                            "</tr>";

                    $("#classTableResumeModal").modal('hide');
                    $("#studentBTable tbody").html(str);

                    $("#resumeDate").val("");
                    $("#courseTime").val("");
                    $("#courseMoney").val("");
                    cancelChoose();
                }
            });
        }
    };

    function cancelChoose() {
        $("#cNameId").val('');
        queryStu(page);
        queryStuByClass(page);
    }

    function addOrder(){
        var classIDS = $("#classIDS").val();
        var userById = $("#userById").val();

        var userIdReceive = $("#userIdReceive").val();
        var courseTimeValue = $("#courseTimeValue").val();
        var courseMoneyValue = $("#courseMoneyValue").val();
        var cremarksId = $("#cremarksId").val();

        if(userById == null || userById ==''){
            layer.alert("请选择转费学员");
        }else if(userIdReceive == null || userIdReceive ==''){
            layer.alert("请选择接收转费学员");
        }else if(classIDS == null || classIDS ==''){
            layer.alert("请选择接收转让班级");
        }else if(courseTimeValue == null || courseTimeValue =='' || courseTimeValue <= 0){
            layer.alert("购买课次必须大于0");
        }else if(courseMoneyValue == null || courseMoneyValue =='' || courseMoneyValue <= 0){
            layer.alert("购买课单价必须大于0");
        }else if(cremarksId == null || cremarksId ==''){
            layer.alert("备注不能为空");
        }else{
            $("#workOrderOfResumeForm").submit();
        }
    };

    function adopt() {
        var odsUserName = $("#odsUserName").val();
        var odsPassword = $("#odsPassword").val();
        var userID = $("#tEmployeeID").val();
        if (odsUserName == '' || odsPassword == '') {
            $("#info").html("账号或密码不能为空");
        } else {
            $("#info").html("");

            $.ajax({
                type: 'post',
                url: '${GLOBAL.basePath}/workOrderInterrupt/odsLogin',
                dataType: 'json',
                data: {
                    odsUserName: odsUserName,
                    odsPassword: odsPassword,
                    userID: userID
                },
                success: function (data) {
                    if(data.data >0){
                        $("#adopt").modal('hide');
                    }else{
                        $("#info").html("账号或密码输入错误");
                    }
                }
            });
        }
    }
</script>
