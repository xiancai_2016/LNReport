<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbarByReport.jsp" %>

<div class="page-content">
    <input type="hidden" name="dateTime" value="${dateTime}">

    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="table_revenue" class="table" width="100%">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th>校区</th>
                        <th>目标</th>
                        <th>累计</th>
                        <th>达成比</th>
                    </tr>
                    </thead>
                    <c:if test="${!empty revenues}">
                        <c:forEach items="${revenues}" var="item">
                            <tr>
                                <td>${item.cName}</td>
                                <td>${item.kpiValue}</td>
                                <td>${item.fstWeek}</td>
                                <td>${item.kpiRate}%</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty revenueSum}">
                        <tr style="background: #438eb9; color: white">
                            <td>总计</td>
                            <td>${revenueSum.totalKpi}</td>
                            <td>${revenueSum.wFst}</td>
                            <td>${revenueSum.kpiRate}%</td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </div>
    </div>
    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    $(function () {
        var useragent = navigator.userAgent;
        if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
            // 以下代码是用javascript强行关闭当前页面
            var opened = window.open('${GLOBAL.basePath}/wechat/page', '_self');
            //            opened.opener = null;
            //            opened.close();
        }
    });

    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }
</script>