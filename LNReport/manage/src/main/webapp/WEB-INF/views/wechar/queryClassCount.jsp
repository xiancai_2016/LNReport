<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbarByReport.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/wechat/openAndClass"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <input type="hidden" name = "type" value="${type}">
                        <input type="hidden" name = "dateTime" value="${dateTime}">
                        <div class="col-sm-4">
                            <br/>
                            <select class="form-control" name="CampusID">
                                <option value="">全部</option>
                                <c:forEach items="${departs}" var="item">
                                    <option value="${item.cID}"
                                            <c:if test="${item.cID == CampusID}">selected</c:if>>${item.cNAME}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <br>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <font size="4px">单位:班级数</font>
            <div class="table-responsive">
                <table id="table_REF" class="table" width="100%" style="font-size: 13px">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th>课程</th>
                        <th>>11人</th>
                        <th>11人</th>
                        <th>9-10人</th>
                        <th>8人</th>
                        <th><8人</th>
                        <th>合计</th>
                    </tr>
                    </thead>
                    <c:if test="${!empty queryClassCountByK}">
                        <c:forEach items="${queryClassCountByK}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.acc}</td>
                                <td>${item.acc1}</td>
                                <td>${item.acc2}</td>
                                <td>${item.acc3}</td>
                                <td>${item.acc4}</td>
                                <td>${item.clsCount}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapK}">
                        <tr style="background: #438eb9; color: white">
                            <td>LK小计:</td>
                            <td>${mapK.acc}</td>
                            <td>${mapK.acc1}</td>
                            <td>${mapK.acc2}</td>
                            <td>${mapK.acc3}</td>
                            <td>${mapK.acc4}</td>
                            <td>${mapK.clsCount}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty queryClassCountByC}">
                        <c:forEach items="${queryClassCountByC}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.acc}</td>
                                <td>${item.acc1}</td>
                                <td>${item.acc2}</td>
                                <td>${item.acc3}</td>
                                <td>${item.acc4}</td>
                                <td>${item.clsCount}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapC}">
                        <tr style="background: #438eb9; color: white">
                            <td>LC小计:</td>
                            <td>${mapC.acc}</td>
                            <td>${mapC.acc1}</td>
                            <td>${mapC.acc2}</td>
                            <td>${mapC.acc3}</td>
                            <td>${mapC.acc4}</td>
                            <td>${mapC.clsCount}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty queryClassCountByY}">
                        <c:forEach items="${queryClassCountByY}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.acc}</td>
                                <td>${item.acc1}</td>
                                <td>${item.acc2}</td>
                                <td>${item.acc3}</td>
                                <td>${item.acc4}</td>
                                <td>${item.clsCount}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapY}">
                        <tr style="background: #438eb9; color: white">
                            <td>LY小计:</td>
                            <td>${mapY.acc}</td>
                            <td>${mapY.acc1}</td>
                            <td>${mapY.acc2}</td>
                            <td>${mapY.acc3}</td>
                            <td>${mapY.acc4}</td>
                            <td>${mapY.clsCount}</td>
                        </tr>
                    </c:if>
                    <tr></tr>
                    <c:if test="${!empty mapT}">
                        <tr style="background: #438eb9; color: white">
                        <td>总计:</td>
                        <td>${mapT.tacc}</td>
                        <td>${mapT.tacc1}</td>
                        <td>${mapT.tacc2}</td>
                        <td>${mapT.tacc3}</td>
                        <td>${mapT.tacc4}</td>
                        <td>${mapT.tclsCount}</td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </div>
        <div class="col-xs-12">

            <font size="4px">单位:班级人数</font>
            <div class="table-responsive">
                <table id="table_REF1" class="table" width="100%" style="font-size: 13px">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th>课程</th>
                        <th>开班平均</th>
                        <th>开班总计</th>
                        <th>等待开班</th>
                        <th>休学</th>
                        <th>合计</th>
                    </tr>
                    </thead>
                    <c:if test="${!empty queryClassCountByK}">
                        <c:forEach items="${queryClassCountByK}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.agvStu}</td>
                                <td>${item.sumcc}</td>
                                <td>${item.wkbCount}</td>
                                <td>${item.xuCount}</td>
                                <td>${item.hJCount}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapK}">
                        <tr style="background: #438eb9; color: white">
                            <td>LK小计:</td>
                            <td>${mapK.agvStu}</td>
                            <td>${mapK.sumcc}</td>
                            <td>${mapK.wkbCount}</td>
                            <td>${mapK.xuCount}</td>
                            <td>${mapK.hJCount}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty queryClassCountByC}">
                        <c:forEach items="${queryClassCountByC}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.agvStu}</td>
                                <td>${item.sumcc}</td>
                                <td>${item.wkbCount}</td>
                                <td>${item.xuCount}</td>
                                <td>${item.hJCount}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapC}">
                        <tr style="background: #438eb9; color: white">
                            <td>LC小计:</td>
                            <td>${mapC.agvStu}</td>
                            <td>${mapC.sumcc}</td>
                            <td>${mapC.wkbCount}</td>
                            <td>${mapC.xuCount}</td>
                            <td>${mapC.hJCount}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty queryClassCountByY}">
                        <c:forEach items="${queryClassCountByY}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.agvStu}</td>
                                <td>${item.sumcc}</td>
                                <td>${item.wkbCount}</td>
                                <td>${item.xuCount}</td>
                                <td>${item.hJCount}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapY}">
                        <tr style="background: #438eb9; color: white">
                            <td>LY小计:</td>
                            <td>${mapY.agvStu}</td>
                            <td>${mapY.sumcc}</td>
                            <td>${mapY.wkbCount}</td>
                            <td>${mapY.xuCount}</td>
                            <td>${mapY.hJCount}</td>
                        </tr>
                    </c:if>
                    <tr></tr>
                    <c:if test="${!empty mapT}">
                        <tr style="background: #438eb9; color: white">
                            <td>总计:</td>
                            <td>${mapT.tagvStu}</td>
                            <td>${mapT.tsumcc}</td>
                            <td>${mapT.twkbCount}</td>
                            <td>${mapT.txuCount}</td>
                            <td>${mapT.thJCount}</td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    $(function () {
        var useragent = navigator.userAgent;
        if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
            // 以下代码是用javascript强行关闭当前页面
            var opened = window.open('${GLOBAL.basePath}/wechat/page', '_self');
//            opened.opener = null;
//            opened.close();
        }
    });

    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }
</script>