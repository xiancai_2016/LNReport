<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbarByReport.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/wechat/continuedClass"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <input type="hidden" name = "type" value="${type}">
                        <input type="hidden" name = "dateTime" value="${dateTime}">
                        <div class="col-sm-4">
                            <br/>
                            <select class="form-control" name="CampusID">
                                <option value="">全部</option>
                                <c:forEach items="${departs}" var="item">
                                    <option value="${item.cID}"
                                            <c:if test="${item.cID == CampusID}">selected</c:if>>${item.cNAME}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <br>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="table_REF" class="table" width="100%" style="font-size: 13px">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th>课程</th>
                        <th>可续班人数</th>
                        <th>已续班人数</th>
                        <th>续班率</th>
                        <th>满班率</th>
                    </tr>
                    </thead>
                    <c:if test="${!empty queryStudentCountByK}">
                        <c:forEach items="${queryStudentCountByK}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.kdCount}</td>
                                <td>${item.ssCount}</td>
                                <td>${item.agvXb}%</td>
                                <td>${item.tmbCount}%</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapK}">
                        <tr style="background: #438eb9; color: white">
                            <td>LK小计:</td>
                            <td>${mapK.kdCount}</td>
                            <td>${mapK.ssCount}</td>
                            <td>${mapK.agvXb}%</td>
                            <td>${mapK.tmbCount}%</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty queryStudentCountByC}">
                        <c:forEach items="${queryStudentCountByC}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.kdCount}</td>
                                <td>${item.ssCount}</td>
                                <td>${item.agvXb}%</td>
                                <td>${item.tmbCount}%</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapC}">
                        <tr style="background: #438eb9; color: white">
                            <td>LC小计:</td>
                            <td>${mapC.kdCount}</td>
                            <td>${mapC.ssCount}</td>
                            <td>${mapC.agvXb}%</td>
                            <td>${mapC.tmbCount}%</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty queryStudentCountByY}">
                        <c:forEach items="${queryStudentCountByY}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.kdCount}</td>
                                <td>${item.ssCount}</td>
                                <td>${item.agvXb}%</td>
                                <td>${item.tmbCount}%</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapY}">
                        <tr style="background: #438eb9; color: white">
                            <td>LY小计:</td>
                            <td>${mapY.kdCount}</td>
                            <td>${mapY.ssCount}</td>
                            <td>${mapY.agvXb}%</td>
                            <td>${mapY.tmbCount}%</td>
                        </tr>
                    </c:if>
                    <tr></tr>
                    <c:if test="${!empty mapT}">
                        <tr style="background: #438eb9; color: white">
                            <td>总计:</td>
                            <td>${mapT.tkdCount}</td>
                            <td>${mapT.tssCount}</td>
                            <td>${mapT.tagvXb}%</td>
                            <td>${mapT.ttmbCount}%</td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    $(function () {
        var useragent = navigator.userAgent;
        if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
            // 以下代码是用javascript强行关闭当前页面
            var opened = window.open('${GLOBAL.basePath}/wechat/page', '_self');
//            opened.opener = null;
//            opened.close();
        }
    });

    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }
</script>