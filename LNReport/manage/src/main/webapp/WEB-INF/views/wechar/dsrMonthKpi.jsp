<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbarByReport.jsp" %>

<link rel='stylesheet' href='${GLOBAL.staticJsPath}/tab/style.css'>
<link rel='stylesheet' href='${GLOBAL.staticJsPath}/tab//swiper.min.css'>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/wechat/dsrChannel"
                  method="post">
                <input type="hidden" name="dateTime" value="${dateTime}">
            </form>
        </div>

        <div class="col-xs-12">
            <!-- header -->
            <header class="favor-header-bar">
                <ul class="tabs">
                    <li class="default"><a href="javascript:void(0);" id="btn11" hidefocus="true">MKT</a></li>
                    <li><a href="javascript:void(0);" id="btn21" hidefocus="true">Ref</a></li>
                    <li><a href="javascript:void(0);" id="btn31" hidefocus="true">Renew</a></li>
                    <li><a href="javascript:void(0);" id="btn41" hidefocus="true">Total</a></li>
                </ul>
            </header>
            <!-- slideTo tab -->
            <div class="swiper-container favor-list">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <table id="table_E2018" class="table" width="100%" style="font-size: 13px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">MKT</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>目标</th>
                                <th>完成</th>
                                <th>完成率</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty dsrMonthKPIs}">
                                <c:forEach items="${dsrMonthKPIs}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.mktKpi}</td>
                                        <td>${item.mash}</td>
                                        <c:if test="${item.mktRateP == 'Y'}">
                                            <td><span style="color: red">${item.mktRate}%</span></td>
                                        </c:if>
                                        <c:if test="${item.mktRateP == 'N'}">
                                            <td>${item.mktRate}%</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty dsrMonthKPI}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${dsrMonthKPI.totalKPIMkt}</td>
                                    <td>${dsrMonthKPI.totalMkt}</td>
                                    <td>${dsrMonthKPI.totalMktRate}%</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                    <div class="swiper-slide margin19">
                        <table id="table_E2017" class="table" width="100%" style="font-size: 13px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Ref</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>目标</th>
                                <th>完成</th>
                                <th>完成率</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty dsrMonthKPIs}">
                                <c:forEach items="${dsrMonthKPIs}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.refKpi}</td>
                                        <td>${item.fash}</td>
                                        <c:if test="${item.refRateP == 'Y'}">
                                            <td><span style="color: red">${item.refRate}%</span></td>
                                        </c:if>
                                        <c:if test="${item.refRateP == 'N'}">
                                            <td>${item.refRate}%</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty dsrMonthKPI}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${dsrMonthKPI.totalKPIRef}</td>
                                    <td>${dsrMonthKPI.totalRef}</td>
                                    <td>${dsrMonthKPI.totalRefRate}%</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                    <div class="swiper-slide">
                        <table id="table_EC" class="table" width="100%" style="font-size: 13px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Renew</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>目标</th>
                                <th>完成</th>
                                <th>完成率</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty dsrMonthKPIs}">
                                <c:forEach items="${dsrMonthKPIs}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.renewKpi}</td>
                                        <td>${item.renew}</td>
                                        <c:if test="${item.renewRateP == 'Y'}">
                                            <td><span style="color: red">${item.renewRate}%</span></td>
                                        </c:if>
                                        <c:if test="${item.renewRateP == 'N'}">
                                            <td>${item.renewRate}%</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty dsrMonthKPI}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${dsrMonthKPI.totalKPIRenew}</td>
                                    <td>${dsrMonthKPI.totalRenew}</td>
                                    <td>${dsrMonthKPI.totalRenewRate}%</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                    <div class="swiper-slide">
                        <table id="table_CC" class="table" width="100%" style="font-size: 13px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Total</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>目标</th>
                                <th>完成</th>
                                <th>完成率</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty dsrMonthKPIs}">
                                <c:forEach items="${dsrMonthKPIs}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.totalKPIByCampus}</td>
                                        <td>${item.totalEnrollment}</td>
                                        <c:if test="${item.totalRateP == 'Y'}">
                                            <td><span style="color: red">${item.totalRate}%</span></td>
                                        </c:if>
                                        <c:if test="${item.totalRateP == 'N'}">
                                            <td>${item.totalRate}%</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty dsrMonthKPI}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${dsrMonthKPI.totalEnrollment}</td>
                                    <td>${dsrMonthKPI.totalKpi}</td>
                                    <td>${dsrMonthKPI.totalRate}%</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>
<script src="${GLOBAL.staticJsPath}/tab/swiper.min.js"></script>
<script src="${GLOBAL.staticJsPath}/tab/idangerous.swiper.min.js"></script>

<script>
    $(function () {
    var useragent = navigator.userAgent;
    if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
    // 以下代码是用javascript强行关闭当前页面
    var opened = window.open('${GLOBAL.basePath}/wechat/page', '_self');
    //            opened.opener = null;
    //            opened.close();
    }
    });

    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }

    var mySwiper = new Swiper('.swiper-container', {
        autoHeight: true,
        onSlideChangeStart: function () {
            $(".tabs .default").removeClass('default');
            $(".tabs li").eq(mySwiper.activeIndex).addClass('default');
        }
    });
    $(".tabs li").on('click', function (e) {
        e.preventDefault();
        $(".tabs .default").removeClass('default');
        $(this).addClass('default');
        mySwiper.swipeTo($(this).index());
    });
    $(".tabs li").click(function (e) {
        e.preventDefault();
    });

</script>