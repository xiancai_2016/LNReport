<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbarByReport.jsp" %>

<link rel='stylesheet' href='${GLOBAL.staticJsPath}/tab/style.css'>
<link rel='stylesheet' href='${GLOBAL.staticJsPath}/tab//swiper.min.css'>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/wechat/dsrChannel"
                  method="post">
                <input type="hidden" name="dateTime" value="${dateTime}">
            </form>
        </div>

        <div class="col-xs-12">
            <!-- header -->
            <header class="favor-header-bar">
                <ul class="tabs">
                    <li class="default"><a href="javascript:void(0);" id="btn11" hidefocus="true">DSR2018</a></li>
                    <li><a href="javascript:void(0);" id="btn21" hidefocus="true">DSR2017</a></li>
                    <li><a href="javascript:void(0);" id="btn31" hidefocus="true">Enrollment</a></li>
                    <li><a href="javascript:void(0);" id="btn41" hidefocus="true">Cash In</a></li>
                </ul>
            </header>
            <!-- slideTo tab -->
            <div class="swiper-container favor-list">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <table id="table_E2018" class="table" width="100%" style="font-size: 13px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">2018 Enrollment</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.mash}</td>
                                        <td>${item.fash}</td>
                                        <td>${item.tash}</td>
                                        <td>${item.allash}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.emkt2018}</td>
                                    <td>${totalMap.eref2018}</td>
                                    <td>${totalMap.erenew2018}</td>
                                    <td>${totalMap.etotal2018}</td>
                                </tr>
                            </c:if>
                            <tr>
                                <th colSpan="5" style="background: white">2018 Cash In</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.mcostf}</td>
                                        <td>${item.fcostf}</td>
                                        <td>${item.tcostf}</td>
                                        <td>${item.allccostf}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.cmkt2018}</td>
                                    <td>${totalMap.cref2018}</td>
                                    <td>${totalMap.crenew2018}</td>
                                    <td>${totalMap.ctotal2018}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                    <div class="swiper-slide margin19">
                        <table id="table_E2017" class="table" width="100%" style="font-size: 13px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">2017 Enrollment</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>

                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.emkt}</td>
                                        <td>${item.eref}</td>
                                        <td>${item.erenew}</td>
                                        <td>${item.erenew}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.emkt2017}</td>
                                    <td>${totalMap.eref2017}</td>
                                    <td>${totalMap.erenew2017}</td>
                                    <td>${totalMap.etotal2017}</td>
                                </tr>
                            </c:if>
                            <tr>
                                <th colSpan="5" style="background: white">2017 Cash In</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>

                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.cmkt}</td>
                                        <td>${item.cref}</td>
                                        <td>${item.crenew}</td>
                                        <td>${item.ctotal}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.cmkt2017}</td>
                                    <td>${totalMap.cerf2017}</td>
                                    <td>${totalMap.crenew2017}</td>
                                    <td>${totalMap.ctotal2017}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                    <div class="swiper-slide">
                        <table id="table_EC" class="table" width="100%" style="font-size: 13px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Enrollment增长数量</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.emktInCount}</td>
                                        <td>${item.erefInCount}</td>
                                        <td>${item.erenewInCount}</td>
                                        <td>${item.etotalInCount}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.emkttatolCount}</td>
                                    <td>${totalMap.ereftatolCount}</td>
                                    <td>${totalMap.erenewtatolCount}</td>
                                    <td>${totalMap.ettatolCount}</td>
                                </tr>
                            </c:if>
                            <tr>
                                <th colSpan="5" style="background: white">Enrollment增长率</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>

                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.emktInRate}</td>
                                        <td>${item.erefInRate}</td>
                                        <td>${item.erenewInRate}</td>
                                        <td>${item.etotalInRate}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.emkttatolrate}</td>
                                    <td>${totalMap.ereftatolrate}</td>
                                    <td>${totalMap.erenewtatolrate}</td>
                                    <td>${totalMap.ettatolrate}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                    <div class="swiper-slide">
                        <table id="table_CC" class="table" width="100%" style="font-size: 13px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Cash In增长数量</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.cmktCashInCount}</td>
                                        <td>${item.crefCashInCount}</td>
                                        <td>${item.crenewCashInCount}</td>
                                        <td>${item.ctotalCashInCount}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.cmkttatolCount}</td>
                                    <td>${totalMap.creftatolCount}</td>
                                    <td>${totalMap.crenewtatolCount}</td>
                                    <td>${totalMap.cttatolCount}</td>
                                </tr>
                            </c:if>
                            <tr>
                                <th colSpan="5" style="background: white">Cash In增长率</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>

                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.cmktCashInRate}</td>
                                        <td>${item.crefCashInRate}</td>
                                        <td>${item.crenewCashInRate}</td>
                                        <td>${item.ctotalCashInRate}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.cmkttatolrate}</td>
                                    <td>${totalMap.creftatolrate}</td>
                                    <td>${totalMap.crenewtatolrate}</td>
                                    <td>${totalMap.cttatolrate}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>
<script src="${GLOBAL.staticJsPath}/tab/swiper.min.js"></script>
<script src="${GLOBAL.staticJsPath}/tab/idangerous.swiper.min.js"></script>

<script>
    $(function () {
        var useragent = navigator.userAgent;
        if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
            // 以下代码是用javascript强行关闭当前页面
            var opened = window.open('${GLOBAL.basePath}/wechat/page', '_self');
//            opened.opener = null;
//            opened.close();
        }
    });

    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }

    var mySwiper = new Swiper('.swiper-container', {
        autoHeight: true,
        onSlideChangeStart: function () {
            $(".tabs .default").removeClass('default');
            $(".tabs li").eq(mySwiper.activeIndex).addClass('default');
        }
    });
    $(".tabs li").on('click', function (e) {
        e.preventDefault();
        $(".tabs .default").removeClass('default');
        $(this).addClass('default');
        mySwiper.swipeTo($(this).index());
    });
    $(".tabs li").click(function (e) {
        e.preventDefault();
    });

</script>