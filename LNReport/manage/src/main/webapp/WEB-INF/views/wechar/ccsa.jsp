<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbarByReport.jsp" %>
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/dmaku/dmaku.css"/>
<link rel="stylesheet" type="text/css" href="${GLOBAL.staticJsPath}/jquery-select2/3.4/select2.min.css"/>

<style>
    .table tbody>tr>td, .table tfoot>tr>td {
        padding: 4px;
        line-height: 1.428571429;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
</style>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12" style="margin:0 auto;">
            <form class="form-horizontal" id="campusdsr" role="form"
                  action="${GLOBAL.basePath}/wechat/ccsa"
                  method="post">

                <input type="hidden" name="dateTime" value="${dateTime}">
                <input type="hidden" name="account" value="${account}">

                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">校区</label>

                        <div class="col-sm-4">
                            <select id="CampusIDID" name="CampusID" class="js-example-basic-single required"
                                    multiple="multiple" data-live-search="true" style="width:80%;">
                                <c:if test="${isAll == 'Y'}">
                                    <option value="" <c:if test="${empty CampusID}">selected</c:if>>全部</option>
                                </c:if>
                                <c:forEach items="${departs}" var="item">
                                    <c:choose>
                                        <c:when test="${!empty CampusID}">
                                            <option value="${item.cID}"
                                                    <c:forEach items="${CampusID}" var="item1">
                                                        <c:if test="${item.cID == item1}">selected</c:if>
                                                    </c:forEach>
                                            >${item.cNAME}
                                            </option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${item.cID}">${item.cNAME}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>

                            <button class="btn btn-primary btn-xs" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div><br><br><br><br><br></div>

        <div class="tab-menu tworow" style="margin:0 auto;">
            <span id="thirdbg"></span>
            <ul id="hourlist" class="ul1">
                <li class="active li1" type="1">达成率</li>
                <li class="li1" type="2">转化率</li>
            </ul>
        </div>

        <div><br><br></div>

        <div class="col-xs-12">
            <div id="Reach">
                <div id="container" style="height: ${heightValue}%"></div>
            </div>

            <div class="table-responsive" id = "conversion" style="display: none;">
                <table id="table_REF" class="table" width="100%">
                    <thead>
                    <%--<tr style="background: #438eb9; color: white;" align="center">--%>
                        <%--<td></td>--%>
                        <%--<td colspan="2">TIN</td>--%>
                        <%--<td colspan="2">TOUT</td>--%>
                        <%--<td >WIN</td>--%>
                    <%--</tr>--%>

                    <tr style="background: #438eb9; color: white">
                        <th>姓名</th>
                        <th>TIN</th>
                        <%--<th>SHOW/EN</th>--%>
                        <th>TOUT</th>
                        <%--<th>SHOW/EN</th>--%>
                        <th>WIN</th>
                    </tr>
                    </thead>
                    <c:forEach items="${dsrByCCSA}" var="item">
                        <tr>
                            <td>${item.cName}</td>
                            <%--<td>--%>
                                <%--<span <c:if test="${item.appToEn > 2.5}"> style="color: red" </c:if>>${item.appToEn}</span>--%>
                            <%--</td>--%>
                            <td>
                                <span <c:if test="${item.showToEn > 2}"> style="color: red" </c:if>>${item.showToEn}</span>
                            </td>
                            <%--<td>--%>
                                <%--<span <c:if test="${item.appToEn2 > 14}"> style="color: red" </c:if>>${item.appToEn2}</span>--%>
                            <%--</td>--%>
                            <td>
                                <span <c:if test="${item.showToEn2 > 5}"> style="color: red" </c:if>>${item.showToEn2}</span>
                            </td>
                            <td>
                                <span <c:if test="${item.showToEn3 > 1.67}"> style="color: red" </c:if>>${item.showToEn3}</span>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>
<script src="${GLOBAL.staticJsPath}/dmaku/dmaku.js"></script>
<script src="${GLOBAL.staticJsPath}/jquery-select2/3.4/select2.min.js"></script>

<script type="text/javascript" src="${GLOBAL.staticJsPath}/echarts/echarts.min.js"></script>

<script>
    $(function () {
        var useragent = navigator.userAgent;
        if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
            // 以下代码是用javascript强行关闭当前页面
            var opened = window.open('${GLOBAL.basePath}/wechat/page', '_self');
        }
    });

    $(window).on('load', function () {
        $(".js-example-basic-single").select2(); //
    });

    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }

    new Slideicon($("#hourlist"), {
        index: 0,
        cover: $("#thirdbg"),
        callback: function (data) {
            if (data == 2) {
                $("#Reach").hide();
                $("#conversion").show();
            } else {
                $("#Reach").show();
                $("#conversion").hide();
            }
        }
    });

    var dom = document.getElementById("container");
    var myChart = echarts.init(dom);
    myChart.on('click', function (params) {
        //图形点击事件
    });
    option = null;
    option = {
        color: ['#438EB9', '#FFCC33', '#66CCCC'],
//        tooltip: {
//            trigger: 'axis',
//            axisPointer: {
//                type: 'shadow'
//            }
//        },
        legend: {
            data: ['MKT', 'REF' , 'RENEW']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            type: 'value',
            boundaryGap: [0, 0.01],
            max: 200,
            silent: false,
            selectedMode:false,
        },
        yAxis: {
            type: 'category',
            axisLable: {
                formatter:
                        function(params) {
                            return  params.name+"\n"+params.value
                        },
            },
            data: ${cNames}
        },
        series : ${pageDate}
    };

    if (option && typeof option === "object") {
        myChart.setOption(option);
    }
</script>