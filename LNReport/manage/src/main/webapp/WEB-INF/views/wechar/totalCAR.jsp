<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbarByReport.jsp" %>
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/wechat/enrollAndRefund"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <input type="hidden" name = "type" value="${type}">
                        <input type="hidden" name = "dateTime" value="${dateTime}">
                        <div class="col-sm-4">
                            <br>
                            <select class="form-control" name="CampusID">
                                <option value="">全部</option>
                                <c:forEach items="${departs}" var="item">
                                    <option value="${item.cID}"
                                            <c:if test="${item.cID == CampusID}">selected</c:if>>${item.cNAME}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <br>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="table_REF" class="table" width="100%" style="font-size: 13px">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th>课程</th>
                        <th>报名</th>
                        <th>报名金额</th>
                        <th>退费</th>
                        <th>退费金额</th>
                    </tr>
                    </thead>
                    <c:if test="${!empty chargeAndRefundByK}">
                        <c:forEach items="${chargeAndRefundByK}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.stucount}</td>
                                <td>${item.tcost}</td>
                                <td>${item.rstucount}</td>
                                <td>${item.rtcost}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapK}">
                        <tr style="background: #438eb9; color: white">
                            <td>LK小计</td>
                            <td> ${mapK.stucount}</td>
                            <td>${mapK.tcost}</td>
                            <td>${mapK.rstucount}</td>
                            <td>${mapK.rtcost}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty chargeAndRefundByC}">
                        <c:forEach items="${chargeAndRefundByC}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.stucount}</td>
                                <td>${item.tcost}</td>
                                <td>${item.rstucount}</td>
                                <td>${item.rtcost}</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapC}">
                        <tr style="background: #438eb9; color: white">
                            <td>LC小计</td>
                            <td>${mapC.stucount}</td>
                            <td>${mapC.tcost}</td>
                            <td>${mapC.rstucount}</td>
                            <td>${mapC.rtcost}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty chargeAndRefundByY}">
                        <c:forEach items="${chargeAndRefundByY}" var="item">
                            <tr>
                                <td>${item.grade}</td>
                                <td>${item.stucount}</td>
                                <td>${item.tcost}</td>
                                <td>${item.rstucount}</td>
                                <td>${item.rtcost}</td>
                            </tr>
                        </c:forEach>
                    </c:if>


                    <c:if test="${!empty mapY}">
                        <tr style="background: #438eb9; color: white">
                            <td>LY小计</td>
                            <td>${mapY.stucount}</td>
                            <td>${mapY.tcost}</td>
                            <td>${mapY.rstucount}</td>
                            <td>${mapY.rtcost}</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty SKC}">
                        <tr>
                            <td>暑假班</td>
                            <td></td>
                            <td>${SKC.skcFact}</td>
                            <td></td>
                            <td>${SKC.skcrFact}</td>
                        </tr>
                    </c:if>

                    <tr></tr>

                    <c:if test="${!empty totalChargeAndRefund}">
                        <tr style="background: #438eb9; color: white">
                            <td>总计</td>
                            <td>${totalChargeAndRefund.sumstucount}</td>
                            <td>${totalChargeAndRefund.sumtcost}</td>
                            <td>${totalChargeAndRefund.sumrstucount}</td>
                            <td>${totalChargeAndRefund.sumrtcost}</td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    $(function () {
        var useragent = navigator.userAgent;
        if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
            // 以下代码是用javascript强行关闭当前页面
            var opened = window.open('${GLOBAL.basePath}/wechat/page', '_self');
//            opened.opener = null;
//            opened.close();
        }
    });

    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }
</script>