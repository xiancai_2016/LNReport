<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp" %>
<%@include file="../../common/navbar.jsp" %>
<%@include file="../../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <shiro:hasPermission name="sys:report:edit">
            <div class="col-sm-12">
                <button class="btn btn-xs btn-success" id="add_btn">
                    添加
                    <i class="icon-plus align-top bigger-125 icon-on-right"></i>
                </button>
            </div>
        </shiro:hasPermission>
        <div class="col-xs-12">
            <div class="table-responsive">

                <table id="list-table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>报表名称</th>
                        <%--<th>排序</th>--%>
                        <th>推送类型</th>
                        <th>推送</th>
                        <th>报表链接</th>
                        <th>模板类型</th>
                        <th>
                            <i class="icon-time bigger-110 hidden-480"></i>
                            创建时间
                        </th>
                        <th>
                            <i class="icon-time bigger-110 hidden-480 "></i>
                            最后修改时间
                        </th>
                        <th>操作</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.page-content -->


<%@include file="../../common/page_content_suf.jsp" %>
<%@include file="../../common/script.jsp" %>

<script type="text/javascript">

    var reportListTable;

    jQuery(function ($) {
        $("#add_btn").on("click", function () {
            addClick();
        });

        $('.date-picker').datepicker({autoclose: true}).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        reportListTable = $('#list-table').dataTable({
            "sAjaxSource": "${GLOBAL.basePath}/report/asynList",
            "fnServerData": function (sSource, aDataSet, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aDataSet,
                    "success": fnCallback
                });
            },
            "fnServerParams": function (aoData) {//向服务器传额外的参数
                <c:if test="${not empty aoData}">
                aoData.push(${aoData});
                </c:if>
            },
            "bSort": false,
            "paging": false,
            'bPaginate': true,
            "bFilter": false,
            "bServerSide": true,
            "bProcessing": false,
            "bLengthChange": false,
            "bSearching": false,
            'bJQueryUI': true,
            "sProcessing": "${GLOBAL.staticImagePath}/loading.gif' />",
            "oLanguage": {
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条",
                "sInfoEmpty": "No data"
            },
            "aoColumns": [
                {"mDataProp": "name", "sWidth": "100px"},
                {
                    "mDataProp": "pushType", "sWidth": "100px", "mRender": function (data, type, full) {
                    if (data == 1) {
                        return "天";
                    } else if (data == 2) {
                        return "周五";
                    } else if (data == 3) {
                        return "财务月";
                    } else if (data == 4) {
                        return "自然月";
                    } else {
                        return "周一";
                    }
                }
                },
                {
                    "mDataProp": "state", "sWidth": "70px", "mRender": function (data, type, full) {
                    if (data == 0) {
                        return "否";
                    } else {
                        return "是";
                    }
                }
                },
                {
                    "mDataProp": "url", "sWidth": "200px", "mRender": function (data, type, full) {
//                    return '<a href='+data+' target =_blank>'+data+'</a>'
                    return data
                }
                },
                {
                    "mDataProp": "sysReportType", "sWidth": "100px", "mRender": function (data, type, full) {
                    if (data == 1) {
                        return "事件模板";
                    } else {
                        return "提醒模板";
                    }
                }
                },
                {"mDataProp": "createddatetime", "sWidth": "180px"},
                {"mDataProp": "updateddatetime", "sWidth": "180px"},
                {
                    "mDataProp": "id", "sWidth": "120px",
                    "mRender": function (data, type, full) {
                        return "<div class='visible-md visible-lg hidden-sm hidden-xs btn-group'>"
                                + "<shiro:hasPermission name='sys:report:edit'><button class='football-edit btn btn-xs btn-info' pkId='" + data + "' onclick='editClick(this)'>"
                                + "<i class='icon-edit bigger-120'></i>"
                                + "</button></shiro:hasPermission>"
                                + "<shiro:hasPermission name='sys:report:delete'><button class='football-del btn btn-xs btn-danger' pkId='" + data + "' onclick='delClick(this)'>"
                                + "<i class='icon-trash bigger-120'></i>"
                                + "</button></shiro:hasPermission>"
                                + "</div>";
                    }
                }
            ]
        });

    });


    var addClick = function () {
        document.location.href = "${GLOBAL.basePath}/report/add";
    };

    var editClick = function (obj) {
        document.location.href = "${GLOBAL.basePath}/report/edit?id=" + $(obj).attr("pkId");
    };

    var delClick = function (obj) {
        layer.confirm("确定要禁用此报表？", {
            title: '提示',
            btn: ['确认', '取消'], //按钮
            area: ['auto', 'auto'] //宽高
        }, function () {
            var result = ART.mAjax("${GLOBAL.basePath}/report/delete", "POST", {id: $(obj).attr("pkId")});
            layer.alert(result, {icon: 4, title: '提示', btn: ['OK']});
            reportListTable.fnDraw();
        })
    }
</script>
