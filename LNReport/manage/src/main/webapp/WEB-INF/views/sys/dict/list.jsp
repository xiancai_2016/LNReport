<%--
   For com.football
   Copyright [2015/11/17] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp"%>
<%@include file="../../common/navbar.jsp"%>
<%@include file="../../common/page_content_pre.jsp"%>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" role="form" action="${GLOBAL.basePath}/dict/list" method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >字典值</label>
                        <div class="col-sm-2">
                            <input class="form-control" id="s_itemName" type="text" name="itemValue" placeholder="请输入值" value="${dict.itemValue}"/>
                        </div>
                        <label class="col-sm-2 control-label" >父字典值</label>
                        <div class="col-sm-2">
                            <select id="s_pItemKey" name="pItemKey" class="form-control" def_value="${dict.pItemKey}"></select>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" >
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <shiro:hasPermission name="sys:dict:edit">
        <div class="col-sm-12">
            <button class="btn btn-xs btn-success" id="add_btn" >
                添加
                <i class="icon-plus align-top bigger-125 icon-on-right"></i>
            </button>
        </div>
        </shiro:hasPermission>
        <div class="col-xs-12">
            <div class="table-responsive">

                <table id="list-table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>
                            <label>
                                <input type="checkbox" class="ace" />
                                <span class="lbl"></span>
                            </label>
                        </th>
                        <th>字典键</th>
                        <th>字典值</th>
                        <th>父字典键</th>
                        <th>父字典值</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>


</div><!-- /.page-content -->


<%@include file="../../common/page_content_suf.jsp"%>
<%@include file="../../common/script.jsp"%>

<script type="text/javascript">

    var categoryListTable;

    jQuery(function($) {

        ART.mLoadSelectOption("${GLOBAL.basePath}/dict/dictSelList?pItemKey=ROOT",{},$("#s_pItemKey"),"itemKey","itemValue",true);

        $("#add_btn").on("click",function(){
            addClick();
        });

        $(".football-del").on("click",function(){
            alert($(this).attr("pkId"));
        });

        categoryListTable = $('#list-table').dataTable( {
            "sAjaxSource": "${GLOBAL.basePath}/dict/asynList",
            "fnServerData":function(sSource, aDataSet, fnCallback){
                $.ajax({
                    "dataType" : 'json',
                    "type" : "POST",
                    "url" : sSource,
                    "data" : aDataSet,
                    "success" : fnCallback
                });
            },
            "fnServerParams": function ( aoData ) {
                <c:if test="${not empty aoData}">
                aoData.push( ${aoData});
                </c:if>
            },
            "bSort":false,
            "bFilter":false,
            "bServerSide":true,
            "bProcessing":false,
            "bLengthChange":false,
            "bSearching":false,
            "sProcessing": "${GLOBAL.staticImagePath}/loading.gif' />",
            "oLanguage":{
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条",
                "sInfoEmpty": "No data"
            },
            "aoColumns": [
                { "sName": "id","mDataProp":"id" ,"mRender": function(data, type, full) {
                    return "<label>"
                            + "<input type='checkbox' class='ace' value='"+data+"' />"
                            + "<span class='lbl'></span>"
                            + "</label>";}
                },
                { "mDataProp":"itemKey" },
                { "mDataProp":"itemValue" },
                { "mDataProp":"pItemKey" },
                { "mDataProp":"pItemValue" },
                { "mDataProp":"deleted"  ,"mRender":function(data, type, full) {
                    if(data=="0"){
                        return "<div class='col-xs-3'><label><input name='switch-field-1' class='ace ace-switch ace-switch-3' type='checkbox' checked disabled /><span class='lbl'></span></label></div>";
                    }else {
                        return "<div class='col-xs-3'><label><input name='switch-field-1' class='ace ace-switch ace-switch-3' type='checkbox' disabled /><span class='lbl'></span></label></div>";
                    }
                }
                },
                { "mDataProp":"id" ,"sWidth":"100px","mRender": function(data, type, full) {
                    return "<div class='visible-md visible-lg hidden-sm hidden-xs btn-group'>"
                            + "<shiro:hasPermission name="sys:dict:edit"><button class='football-edit btn btn-xs btn-info' pkId='"+data+"' onclick='editClick(this)'>"
                            + "<i class='icon-edit bigger-120'></i>"
                            + "</button></shiro:hasPermission>"
                            + "<shiro:hasPermission name="sys:dict:delete"><button class='football-del btn btn-xs btn-danger' pkId='"+data+"' onclick='delClick(this)'>"
                            + "<i class='icon-trash bigger-120'></i>"
                            + "</button></shiro:hasPermission>"
                            + "</div>";}
                }
            ]
        } );
    });

    var addClick = function(){
        document.location.href = "${GLOBAL.basePath}/dict/add";
    };

    var editClick = function(obj){
        document.location.href = "${GLOBAL.basePath}/dict/edit?id="+$(obj).attr("pkId");
    };

    var delClick = function(obj){
        layer.confirm("确定要禁用此字典吗？", {
            title: '提示',
            btn: ['确认','取消'], //按钮
            area: ['auto', 'auto'] //宽高
        },function(){
            var result = SHIPPARTS.mAjax("${GLOBAL.basePath}/dict/delete","POST",{id:$(obj).attr("pkId")});
            layer.alert("编辑 "+(result > 0 ? "成功":"失败"),{icon: 2,title: 'prompt',btn: ['OK']});
            categoryListTable.fnDraw();
        })
    }

</script>
