<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp"%>
<%@include file="../../common/navbar.jsp"%>
<%@include file="../../common/page_content_pre.jsp"%>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" role="form" action="${GLOBAL.basePath}/sysuser/list" method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label" for="s_account">账户</label>
                        <div class="col-sm-2">
                            <input class="form-control" id="s_account" name="account" type="text" placeholder="请输入账户" value="${sysUser.account}"/>
                        </div>
                        <label class="col-sm-1 control-label" for="s_name">姓名</label>
                        <div class="col-sm-2">
                            <input class="form-control" id="s_name" type="text" name="name" placeholder="请输入姓名" value="${sysUser.name}"/>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" onsubmit="return false;" >
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <shiro:hasPermission name="sys:user:edit">
        <div class="col-sm-12">
            <button class="btn btn-xs btn-success" id="add_btn" >
                添加
                <i class="icon-plus align-top bigger-125 icon-on-right"></i>
            </button>
        </div>
        </shiro:hasPermission>
        <div class="col-xs-12">
            <div class="table-responsive">

                <table id="list-table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>账户</th>
                        <th class="hidden-480">姓名</th>
                        <th>状态</th>
                        <th>最后登录IP</th>
                        <th>登陆次数</th>
                        <th>
                            <i class="icon-time bigger-110 hidden-480"></i>
                            创建时间
                        </th>
                        <th>
                            <i class="icon-time bigger-110 hidden-480 "></i>
                            最后登陆时间
                        </th>
                        <th>操作</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<%@include file="../../common/page_content_suf.jsp"%>
<%@include file="../../common/script.jsp"%>

<script type="text/javascript">

    var userListTable;

    jQuery(function($) {
        $("#add_btn").on("click",function(){
            addClick();
        });

        $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
            $(this).prev().focus();
        });

        userListTable = $('#list-table').dataTable( {
            "sAjaxSource": "${GLOBAL.basePath}/sysuser/asynList",
            "fnServerData":function(sSource, aDataSet, fnCallback){
                $.ajax({
                    "dataType" : 'json',
                    "type" : "POST",
                    "url" : sSource,
                    "data" : aDataSet,
                    "success" : fnCallback
                });
            },
            "fnServerParams": function ( aoData ) {//向服务器传额外的参数
                <c:if test="${not empty aoData}">
                aoData.push( ${aoData});
                </c:if>
            },
            "bSort":false,
            "bFilter":false,
            "bServerSide":true,
            "bProcessing":false,
            "bLengthChange":false,
            "bSearching":false,
            "sProcessing": "${GLOBAL.staticImagePath}/loading.gif' />",
            "oLanguage":{
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条",
                "sInfoEmpty": "No data"
            },
            "aoColumns": [
                { "mDataProp":"account" },
                { "mDataProp":"name","sWidth":"230px"},
                { "mDataProp":"deleted", "sWidth":"80px","mRender":function(data, type, full) {
                    if(data=="0"){
                        return "<div class='col-xs-3'><label><input name='switch-field-1' class='ace ace-switch ace-switch-3' type='checkbox' checked disabled /><span class='lbl'></span></label></div>";
                    }else {
                        return "<div class='col-xs-3'><label><input name='switch-field-1' class='ace ace-switch ace-switch-3' type='checkbox' disabled /><span class='lbl'></span></label></div>";
                    }
                }
                },
                { "mDataProp":"loginIp" },
                { "mDataProp":"loginTimes"},
                { "mDataProp":"createdDatetime","sWidth":"180px"},
                { "mDataProp":"lastLoginTime","sWidth":"180px"},
                { "mDataProp":"id" ,"sWidth":"120px","mRender": function(data, type, full) {
                    if(full.deleted=='0') {
                        return "<div class='visible-md visible-lg hidden-sm hidden-xs btn-group'>"
                                + "<shiro:hasPermission name='sys:user:edit'><button class='football-edit btn btn-xs btn-info' pkId='"+data+"' onclick='editClick(this)'>"
                                + "<i class='icon-edit bigger-120'></i>"
                                + "</button></shiro:hasPermission>"
                                + "<shiro:hasPermission name='sys:user:delete'><button class='football-del btn btn-xs btn-danger' pkId='"+data+"' onclick='delClick(this)'>"
                                + "<i class='icon-trash bigger-120'></i>"
                                + "</button></shiro:hasPermission>"
                                + "</div>";
                    } else {
                        return "<div class='visible-md visible-lg hidden-sm hidden-xs btn-group'>"
                                + "<shiro:hasPermission name='sys:user:edit'><button class='football-edit btn btn-xs btn-info' pkId='"+data+"' onclick='editClick(this)'>"
                                + "<i class='icon-edit bigger-120'></i>"
                                + "</button></shiro:hasPermission>";
                    }
                    }
                }
            ]
        } );
    });

    var addClick = function(){
        document.location.href = "${GLOBAL.basePath}/sysuser/add";
    };

    var editClick = function(obj){
        document.location.href = "${GLOBAL.basePath}/sysuser/edit?id="+$(obj).attr("pkId");
    };

    var delClick = function(obj){
        layer.confirm("确定要禁用此用户吗？", {
            title: '提示',
            btn: ['确认','取消'], //按钮
            area: ['auto', 'auto'] //宽高
        },function(){
            var result = ART.mAjax("${GLOBAL.basePath}/sysuser/delete","POST",{id:$(obj).attr("pkId")});
            layer.alert("编辑成功",{icon: 1,title: '提示',btn: ['OK']});
            userListTable.fnDraw();
        })
    }

</script>
