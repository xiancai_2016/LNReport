<%--
   For com.football
   Copyright [2015/11/13] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp"%>
<%@include file="../../common/navbar.jsp"%>
<%@include file="../../common/page_content_pre.jsp"%>

<div class="page-content">

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>修改密码</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form class="form-horizontal" role="form" action="${GLOBAL.basePath}/auth/updatePwd" method="post" id="sysUserForm">
                            <input  name="account" type="hidden" value="${sysUser.account}"/>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" >用户名：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="s_name" name="name" type="text" placeholder="请输入用户名" value="${sysUser.name}" readonly/>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" >旧密码：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="old_pwd" name="pwd" type="password" placeholder="请输入密码"/>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" >新密码：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="new_pwd" name="newPwd" type="password" placeholder="请输入新密码" />
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" >确认新密码：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="repeat_pwd" name="repeatPwd" type="password" placeholder="请输入确认新密码" />
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-actions center">
                                <button type="button" class="btn btn-sm btn-grey" id="go_back_btn">
                                    返回
                                    <i class="icon-reply icon-on-right bigger-110"></i>
                                </button>
                                <button type="button" class="btn btn-sm btn-success" id="save_btn">
                                    保存
                                    <i class="icon-save icon-on-right bigger-110"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<%@include file="../../common/page_content_suf.jsp"%>
<%@include file="../../common/script.jsp"%>

<script type="text/javascript">
    jQuery(function($) {
        $("#save_btn").on("click",function(){
            $(this).closest('form').submit();
        });

        $("#sysUserForm").validate({
            rules: {
                name:{minlength:1,maxlength:25,required:true},
                pwd:{minlength:6,maxlength:15,required:true},
                newPwd:{minlength:6,maxlength:15,required:true},
                repeatPwd:{"equalTo":"#new_pwd"}
            }
        });
        //对删除结果进行提示
        var result='${resultUp}';
        if(result == '1'){
            layer.alert("修改成功", {icon: 1, title: '提示', btn: ['OK']});
        }else if(result=='-2'){
            layer.alert("原密码输入错误", {icon: 2, title: '提示', btn: ['OK']});
        }else if (result=='-3'){
            layer.alert("用户不存在，请刷新页面后重试", {icon: 2, title: '提示', btn: ['OK']});
        }
        $("#go_back_btn").on("click", function () {
            goBack();
        });
    });

    function goBack() {
        document.location.href = "${GLOBAL.basePath}/sysuser/list";
    }
</script>
