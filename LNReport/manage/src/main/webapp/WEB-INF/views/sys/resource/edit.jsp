<%--
   For com.royal.art
   Copyright [2015/11/13] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp" %>
<%@include file="../../common/navbar.jsp" %>
<%@include file="../../common/page_content_pre.jsp" %>
<%@include file="../../common/script1.jsp" %>
<link href="${GLOBAL.staticJsPath}/jquery-jbox/2.3/Skins/Bootstrap/jbox.min.css" rel="stylesheet"/>
<script src="${GLOBAL.staticJsPath}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<div class="page-content">

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>${not empty sysResource.id?'修改':'添加'}菜单</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form:form class="form-horizontal" modelAttribute="sysResource"
                                   action="${GLOBAL.basePath}/resource/save" method="post" id="sysResourceForm">
                            <form:hidden path="id"/>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">上级菜单：</label>

                                    <div class="col-sm-6">
                                        <sys:treeselect id="sysResource1" name="parent.id"
                                                        value="${sysResource.parent.id}" labelName="parent.name"
                                                        labelValue="${sysResource.parent.name}"
                                                        title="菜单" url="${GLOBAL.basePath}/resource/treeData"
                                                        extId="${sysResource.id}" cssClass="required input-xlarge"
                                                        smallBtn="true"/>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">名称：</label>

                                    <div class="col-sm-6">
                                        <form:input path="name" htmlEscape="false" maxlength="50"
                                                    class="required input-xlarge"/>
                                        <span class="help-inline"><font color="red">*</font> </span>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">链接：</label>

                                    <div class="col-sm-6">
                                        <form:input path="url" htmlEscape="false" maxlength="2000"
                                                    class="input-xlarge"/>
                                        <span class="help-inline">点击菜单跳转的页面</span>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">图标：</label>

                                    <div class="col-sm-4">
                                        <sys:iconselect id="iconcls" name="iconcls" value="${sysResource.iconcls}"/>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">排序：</label>

                                    <div class="col-sm-5">
                                        <form:input path="seq" htmlEscape="false" maxlength="50"
                                                    class="required digits input-small"/>
                                        <span class="help-inline">排列顺序，升序。</span>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">是否可见：</label>

                                    <div class="col-sm-6">
                                        <form:radiobuttons path="isShow" items="${fns:getDictList('SHOW_HIDE')}"
                                                           itemLabel="itemValue" itemValue="itemKey" htmlEscape="false"
                                                           class="required"/>
                                        <span class="help-inline">该菜单或操作是否显示到系统菜单中</span>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">类型：</label>

                                    <div class="col-sm-6">
                                        <form:radiobuttons path="sysResourceType"
                                                           items="${fns:getDictList('MENU_TYPE')}" itemLabel="itemValue"
                                                           itemValue="itemKey" htmlEscape="false" class="required"/>
                                        <span class="help-inline">标识为菜单或按钮</span>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">权限标识：</label>

                                    <div class="col-sm-9">
                                        <form:input path="permission" htmlEscape="false" maxlength="100"
                                                    class="input-xlarge"/>
                                        <span class="help-inline">控制器中定义的权限标识，如：@RequiresPermissions("权限标识")</span>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">备注：</label>

                                    <div class="col-sm-6">
                                        <form:textarea path="descr" htmlEscape="false" rows="3" maxlength="200"
                                                       class="input-xlarge"/>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-actions center">
                                <button type="button" class="btn btn-sm btn-grey" id="go_back_btn">
                                    返回
                                    <i class="icon-reply icon-on-right bigger-110"></i>
                                </button>
                                <shiro:hasPermission name="sys:menu:edit">
                                    <button type="submit" class="btn btn-sm btn-success" id="save_btn">
                                        保存
                                        <i class="icon-save icon-on-right bigger-110"></i>
                                    </button>
                                </shiro:hasPermission>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<%@include file="../../common/page_content_suf.jsp" %>

<script type="text/javascript">
    $(document).ready(function () {
        $("#name").focus();
        $("#go_back_btn").on("click", function () {
            history.back();
            parent.document.location.reload();
        });
        $("#go_back_btn").on("click", function () {
            goBack();
        });
    });
    //<!-- 无框架时，左上角显示菜单图标按钮。
    if (!(self.frameElement && self.frameElement.tagName == "IFRAME")) {
        $("body").prepend("<i id=\"btnMenu\" class=\"icon-th-list\" style=\"cursor:pointer;float:right;margin:10px;\"></i><div id=\"menuContent\"></div>");
        $("#btnMenu").click(function () {
            top.$.jBox('get:${GLOBAL.basePath}/resource/treeselect;JSESSIONID=<shiro:principal property="sessionid"/>', {
                title: '选择菜单',
                buttons: {'关闭': true},
                width: 300,
                height: 350,
                top: 10
            });
        });
    }//-->

    function goBack() {
        document.location.href = "${GLOBAL.basePath}/resource";
    }
</script>
