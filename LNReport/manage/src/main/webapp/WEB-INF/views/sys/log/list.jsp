<%@ page import="java.util.Date" %><%--
   For com.royal.art
   Copyright [2015/11/13] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp"%>
<%@include file="../../common/navbar.jsp"%>
<%@include file="../../common/page_content_pre.jsp"%>

<div class="page-content">
    <div class="row">
        <%--<shiro:hasPermission name="sys:log:view">--%>
        <div class="col-xs-12">
            <form class="form-horizontal" role="form" action="${GLOBAL.basePath}/sys/log/list" method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="margin-left: -76px;">操作菜单：</label>
                        <div class="col-sm-2">
                            <input class="input-xlarge" type="text" name="title" id="s_title" value="${sysLog.title}" />
                        </div>
                        <label class="col-sm-2 control-label" style="margin-left: -76px;">操作用户：</label>
                        <div class="col-sm-2">
                            <input class="input-xlarge" type="text" name="operatorName" id="s_operatorName" value="${sysLog.operatorName}" />
                        </div>
                        <label for="exception"><input id="exception" name="exception" type="checkbox"${sysLog.exception eq '1'?' checked':''} value="1"/>只查询异常信息</label>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="margin-left: -76px;">日期范围：</label>
                        <div class="col-sm-3">
                            <div class=" input-group">
                                <span class="input-group-addon">
                                    <i class="icon-calendar bigger-110"></i>
                                </span>
                                <input class="form-control" type="text" readonly name="serchStratEndDate" id="reservation" value="${sysLog.serchStratEndDate}" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" onsubmit="return false;" >
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <%--</shiro:hasPermission>--%>
        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="list-table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>操作用户</th>
                        <th>操作菜单</th>
                        <%--<th>URL</th>--%>
                        <th>提交方式</th>
                        <th>操作者IP</th>
                        <th>
                            <i class="icon-time bigger-110 hidden-480 "></i>
                            操作时间
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div><!-- /.page-content -->

<%@include file="../../common/page_content_suf.jsp"%>
<%@include file="../../common/script.jsp"%>

<script type="text/javascript">
    var logListTable;
    jQuery(function($) {
        logListTable = $('#list-table').dataTable( {
            "sAjaxSource": "${GLOBAL.basePath}/sys/log/asynList",
            "fnServerData":function(sSource, aDataSet, fnCallback){
                $.ajax({
                    "dataType" : 'json',
                    "type" : "POST",
                    "url" : sSource,
                    "data" : aDataSet,
                    "success" : fnCallback
                });
            },
            "fnServerParams": function ( aoData ) {//向服务器传额外的参数
                <c:if test="${not empty aoData}">
                aoData.push( ${aoData});
                </c:if>
            },
            "bSort":false,
            "bFilter":false,
            "bServerSide":true,
            "bProcessing":false,
            "bLengthChange":false,
            "bSearching":false,
            "sProcessing": "${GLOBAL.staticImagePath}/loading.gif' />",
            "oLanguage":{
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条",
                "sInfoEmpty": "No data"
            },
            "aoColumns": [
                { "mDataProp":"operatorName","sWidth":"150px"},
                { "mDataProp":"title", "sWidth":"230px"},
//                { "mDataProp":"requestUri", "sWidth":"230px" },
                { "mDataProp":"method", "sWidth":"100px"},
                { "mDataProp":"remoteAddr", "sWidth":"120px"},
                { "mDataProp":"createdDatetime","sWidth":"180px"}
            ]
        });

    });


    //时间控件
    $('input[name=serchStratEndDate]').daterangepicker({
        startDate: moment().subtract('days', 30),
        endDate: moment(),
        minDate: '01/01/2012',
//        maxDate: '12/31/2014',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: false,
        opens: 'right',
        buttonClasses: ['btn btn-default'],
        'applyClass': 'btn-sm btn-success',
        'cancelClass': 'btn-sm btn-default',
        format: 'MM/DD/YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }).prev().on(ace.click_event, function () {
        $(this).next().focus();
    });

    $('#reservation').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });

    $('#reservation').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });


</script>
