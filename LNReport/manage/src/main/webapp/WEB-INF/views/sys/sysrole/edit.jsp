<%--
   For com.football
   Copyright [2015/11/13] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp" %>
<%@include file="../../common/navbar.jsp" %>
<%@include file="../../common/page_content_pre.jsp" %>
<div class="page-content">

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>编辑角色</h4>
                </div>

                <div class="widget-body">
                    <form:form class="form-horizontal" modelAttribute="sysRole" action="${GLOBAL.basePath}/sysrole/save"
                               method="post" id="sysRoleForm">
                        <form:hidden path="id"/>
                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">角色名称：</label>

                                <div class="col-sm-5">
                                    <input id="oldName" name="oldName" type="hidden" value="${sysRole.name}">
                                    <form:input path="name" htmlEscape="false" maxlength="50"
                                                class="input-xlarge required" placeholder="请输入角色名称"/>
                                    <span class="help-inline"><font color="red">*</font> </span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">是否系统数据：</label>

                                <div class="col-sm-8">
                                    <form:select path="sysData">
                                        <form:options items="${fns:getDictList('YES_NO')}" itemLabel="itemValue"
                                                      itemValue="itemKey" htmlEscape="false"/>
                                    </form:select>
                                    <span class="help-inline">“是”代表此数据只有超级管理员能进行修改，“否”则表示拥有角色修改人员的权限都能进行修改</span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">排序：</label>

                                <div class="col-sm-4">
                                    <form:input path="seq" htmlEscape="false" maxlength="50"
                                                class="digits input-small"/>
                                    <span class="help-inline">排列顺序，升序。</span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">角色授权：</label>

                                <div class="col-sm-4">
                                    <div id="resourceTree" class="ztree" style="margin-top:3px;float:left;"></div>
                                    <form:hidden path="resourceIds"/>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">角色状态：</label>

                                <div class="col-sm-5">
                                        <%--<form:radiobuttons path="deleted" items="${fns:getDictList('STATUS')}" itemLabel="itemValue" itemValue="itemKey" htmlEscape="false" class="required"/>--%>
                                        <%--<form:radiobutton path="deleted" value="1" checked="${sysRole.deleted}"/>启用--%>
                                        <%--<form:radiobutton path="deleted" value="0" checked="${sysRole.deleted}"/>禁用--%>
                                    <label class="inline">
                                        <input name="deleted" type="radio" value="0"
                                               class="ace" ${sysRole.deleted  ? '':'checked' } />
                                        <span class="lbl">启用</span>
                                    </label>
                                    <label class="inline">&nbsp;&nbsp;</label>
                                    <label class="inline">
                                        <input name="deleted" type="radio" value="1"
                                               class="ace" ${sysRole.deleted  ? 'checked':'' } />
                                        <span class="lbl">禁用</span>
                                    </label>
                                    <span class="help-inline">“启用”代表此数据可用，“禁用”则表示此数据不可用</span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">备注：</label>

                                <div class="col-sm-4">
                                    <form:textarea path="descr" htmlEscape="false" rows="3" maxlength="200"
                                                   class="input-xlarge"/>
                                </div>
                            </div>
                        </fieldset>
                        <div class="form-actions center">
                            <button type="button" class="btn btn-sm btn-grey" id="go_back_btn">
                                返回
                                <i class="icon-reply icon-on-right bigger-110"></i>
                            </button>
                            <shiro:hasPermission name='sys:role:edit'>
                                <button type="submit" class="btn btn-sm btn-success" id="save_btn">
                                    保存
                                    <i class="icon-save icon-on-right bigger-110"></i>
                                </button>
                            </shiro:hasPermission>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>

</div>

</div>

<%@include file="../../common/page_content_suf.jsp" %>
<%@include file="../../common/script1.jsp" %>
<%@include file="../../common/treeview.jsp" %>

<script type="text/javascript">
    //    jQuery(function($) {
    //        $("#save_btn").on("click",function(){
    //            $(this).closest('form').submit();
    //        });
    //    })
    $(document).ready(function () {
        $("#name").focus();
        $("#sysRoleForm").validate({
            rules: {
                name: {
                    remote: {
                        type: "POST",
                        cache: false,
                        url: "${GLOBAL.basePath}/sysrole/checkName",
                        data: {
                            oldName: function () {
                                return "${sysRole.name}";
                            }
                        }
                    }
                }
            },
            messages: {
                name: {required: "角色名不能为空", remote: jQuery.format("角色名已存在")}
            },
            submitHandler: function (form) {
                var ids = [], nodes = tree.getCheckedNodes(true);
                for (var i = 0; i < nodes.length; i++) {
                    ids.push(nodes[i].id);
                }
                $("#resourceIds").val(ids);
                form.submit();
            },
            errorContainer: "#messageBox",
            errorPlacement: function (error, element) {
                $("#messageBox").text("输入有误，请先更正。");
                if (element.is(":checkbox") || element.is(":radio") || element.parent().is(".input-append")) {
                    error.appendTo(element.parent().parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        var setting = {
            check: {enable: true, nocheckInherit: true}, view: {selectedMulti: false},
            data: {simpleData: {enable: true}}, callback: {
                beforeClick: function (id, node) {
                    tree.checkNode(node, !node.checked, true, true);
                    return false;
                }
            }
        };

        // 用户-菜单
        var zNodes = [
            <c:forEach items="${resourceList}" var="resource">
            {
                id: "${resource.id}",
                pId: "${not empty resource.parent.id?resource.parent.id:0}",
                name: "${not empty resource.parent.id?resource.name:'权限列表'}"
            },
            </c:forEach>];
        // 初始化树结构
        var tree = $.fn.zTree.init($("#resourceTree"), setting, zNodes);
        // 不选择父节点
        tree.setting.check.chkboxType = {"Y": "ps", "N": "s"};
        // 默认选择节点
        var ids = "${sysRole.resourceIds}".split(",");
        for (var i = 0; i < ids.length; i++) {
            var node = tree.getNodeByParam("id", ids[i]);
            try {
                tree.checkNode(node, true, false);
            } catch (e) {
            }
        }
        // 默认展开全部节点
        tree.expandAll(true);
        $("#go_back_btn").on("click", function () {
            goBack();
        });
    });
    function goBack() {
        document.location.href = "${GLOBAL.basePath}/sysrole/list";
    }
</script>
