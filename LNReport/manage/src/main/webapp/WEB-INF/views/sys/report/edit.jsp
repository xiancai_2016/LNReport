<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp" %>
<%@include file="../../common/navbar.jsp" %>
<%@include file="../../common/page_content_pre.jsp" %>

<div class="page-content">

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>${not empty sysUser.id?'修改':'添加'}报表</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form:form class="form-horizontal" modelAttribute="sysReport"
                                   action="${GLOBAL.basePath}/report/save" method="post" id="sysReportForm">
                            <form:hidden path="id"/>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"> 报表类型：</label>

                                    <div class="col-sm-4">
                                        <form:select path="reportType" htmlEscape="false" class="input-xlarge required"
                                                     onchange="isReport()">
                                            <c:choose>
                                                <c:when test="sysReport.reportType == 2">
                                                    <form:option value="2" selected = "selected">报表</form:option>
                                                    <form:option value="1">菜单</form:option>
                                                </c:when>
                                                <c:otherwise>
                                                    <form:option value="1" selected = "selected">菜单</form:option>
                                                    <form:option value="2">报表</form:option>
                                                </c:otherwise>
                                            </c:choose>
                                        </form:select>
                                        <span class="help-inline"><font color="red">*</font> </span>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"> 所属类型：</label>

                                    <div class="col-sm-9">
                                        <form:select path="pid" htmlEscape="false" class="input-xlarge required">
                                            <form:option value="0">报表菜单</form:option>
                                            <c:forEach items="${reportsByMenus}" var="item">

                                                <c:if test="${item.id == sysReport.pid}">
                                                    <form:option value="${item.id}" selected =  "selected">${item.name}</form:option>
                                                </c:if>
                                                <c:if test="${item.id != sysReport.pid}">
                                                    <form:option value="${item.id}">${item.name}</form:option>
                                                </c:if>
                                            </c:forEach>
                                        </form:select>
                                        <span class="help-inline"><font color="red">*</font> </span>
                                        <span class="help-inline">如果报表类型是"菜单",则"所属类型"将会变为禁用状态</span>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">报表名称：</label>

                                    <div class="col-sm-5">
                                        <input id="oldName" name="oldName" type="hidden" value="${sysUser.name}">
                                        <form:input path="name" htmlEscape="false" maxlength="50"
                                                    class="input-xlarge required" placeholder="请输入报表名称"/>
                                        <span class="help-inline"><font color="red">*</font> </span>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">排序：</label>

                                    <div class="col-sm-5">
                                        <form:input path="seq" htmlEscape="false" maxlength="50" type="number"
                                                    class="input-xlarge required" placeholder="请输入用户名"/>
                                        <span class="help-inline"><font color="red">*</font> </span>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group addReport">
                                    <label class="col-sm-3 control-label"> 报表链接：</label>

                                    <div class="col-sm-6">
                                        <form:input path="url" htmlEscape="false"
                                                    class="required col-sm-11" placeholder="请输入url"/>
                                        <span class="help-inline"><font color="red">*</font> </span>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group addReport">
                                    <label class="col-sm-3 control-label"> 推送类型：</label>

                                    <div class="col-sm-4">
                                        <form:select path="pushType" htmlEscape="false" class="input-xlarge required">
                                            <form:option value="1">每天推送</form:option>
                                            <form:option value="2">周五推送</form:option>
                                            <form:option value="5">周推送</form:option>
                                            <form:option value="3">财务月推送</form:option>
                                            <form:option value="4">自然月推送</form:option>
                                            <form:option value="6">校区周一报表</form:option>
                                            <form:option value="99">测试推送</form:option>
                                        </form:select>
                                        <span class="help-inline"><font color="red">*</font> </span>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group  addReport">
                                    <label class="col-sm-3 control-label"> 模板类型：</label>

                                    <div class="col-sm-4">
                                        <form:select path="sysReportType" class="input-xlarge required">
                                            <form:options items="${fns:getDictList('REPORT_TYPE')}"
                                                          itemLabel="itemValue"
                                                          itemValue="itemKey" htmlEscape="false"/>
                                        </form:select>
                                        <span class="help-inline"><font color="red">*</font> </span>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group  addReport">
                                    <label class="col-sm-3 control-label"> 是否推送：</label>

                                    <div class="col-sm-4">
                                        <form:select path="state" class="input-xlarge required">
                                            <form:options items="${fns:getDictList('YES_NO')}" itemLabel="itemValue"
                                                          itemValue="itemKey" htmlEscape="false"/>
                                        </form:select>
                                        <span class="help-inline"><font color="red">*</font> </span>
                                    </div>
                                </div>
                            </fieldset>

                            <input id="oldUrl" type="hidden" value="${sysReport.url}">

                            <div class="form-actions center">
                                <button type="button" class="btn btn-sm btn-grey" id="go_back_btn">
                                    返回
                                    <i class="icon-reply icon-on-right bigger-110"></i>
                                </button>
                                <shiro:hasPermission name='sys:report:edit'>
                                    <button type="submit" class="btn btn-sm btn-success" id="save_btn">
                                        保存
                                        <i class="icon-save icon-on-right bigger-110"></i>
                                    </button>
                                </shiro:hasPermission>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../../common/page_content_suf.jsp" %>
<%@include file="../../common/script.jsp" %>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sysReportForm").validate({
            rules: {
                <%--url: {--%>
                    <%--remote: {--%>
                        <%--type: "POST",--%>
                        <%--cache: false,--%>
                        <%--url: "${GLOBAL.basePath}/report/notExists",--%>
                        <%--data: {--%>
                            <%--oldUrl: function () {--%>
                                <%--var ourl = $("#oldUrl").val();--%>
                                <%--return ourl;--%>
                            <%--}--%>
                        <%--}--%>
                    <%--}--%>
                <%--}--%>
            }
//            messages: {
//                url: {required: "报表链接不能为空", remote: jQuery.format("报表链接已存在")},
//            },
//            submitHandler: function (form) {
////                loading('正在提交，请稍等...');
//                form.submit();
//            }
        });

        $("#go_back_btn").on("click", function () {
            goBack();
        });

        isReport();
    });

    function isReport() {
        var reportType = $("#reportType").val()
        if (reportType == 1) {
            $("#pid").attr("disabled", "disabled");
            $("#pid").val("0")
            $(".addReport").hide();
        } else {
            $("#pid").removeAttr("disabled");
            $(".addReport").show();
        }
    }

    function goBack() {
        document.location.href = "${GLOBAL.basePath}/report/list";
    }
</script>