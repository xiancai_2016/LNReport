<%--
   For com.football
   Copyright [2015/11/13] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp" %>
<%@include file="../../common/navbar.jsp" %>
<%@include file="../../common/page_content_pre.jsp" %>
<div class="page-content">

    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>编辑角色报表</h4>
                </div>

                <div class="widget-body">
                    <form:form class="form-horizontal" modelAttribute="sysRole"
                               action="${GLOBAL.basePath}/sysrole/saveReport"
                               method="post" id="sysRoleReportForm">
                        <form:hidden path="id"/>
                        <fieldset>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">分配报表：</label>

                                <div class="col-sm-4">
                                    <div id="reportTree" class="ztree" style="margin-top:3px;float:left;"></div>
                                    <form:hidden path="reportIds"/>
                                </div>
                            </div>
                        </fieldset>

                        <div class="form-actions center">
                            <button type="button" class="btn btn-sm btn-grey" id="go_back_btn">
                                返回
                                <i class="icon-reply icon-on-right bigger-110"></i>
                            </button>
                            <shiro:hasPermission name='sys:sysRoleReport:edit'>
                                <button type="submit" class="btn btn-sm btn-success" id="save_btn">
                                    保存
                                    <i class="icon-save icon-on-right bigger-110"></i>
                                </button>
                            </shiro:hasPermission>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>

</div>

</div>

<%@include file="../../common/page_content_suf.jsp" %>
<%@include file="../../common/script1.jsp" %>
<%@include file="../../common/treeview.jsp" %>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sysRoleReportForm").validate({
            rules: {
            },

            submitHandler: function (form) {
                var ids = [], nodes = tree.getCheckedNodes(true);
                for (var i = 0; i < nodes.length; i++) {
                    ids.push(nodes[i].id);
                }
                $("#reportIds").val(ids);
                form.submit();
            },
            errorContainer: "#messageBox",
            errorPlacement: function (error, element) {
                $("#messageBox").text("输入有误，请先更正。");
                if (element.is(":checkbox") || element.is(":radio") || element.parent().is(".input-append")) {
                    error.appendTo(element.parent().parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        var setting = {
            check: {enable: true, nocheckInherit: true}, view: {selectedMulti: false},
            data: {simpleData: {enable: true}}, callback: {
                beforeClick: function (id, node) {
                    tree.checkNode(node, !node.checked, true, true);
                    return false;
                }
            }
        };

        // 用户-菜单
        var zNodes = [
            <c:forEach items="${reportList}" var="report">
            {
                id: "${report.id}",
                pId: "${not empty report.pid?report.pid:0}",
                name: "${not empty report.pid?report.name:'报表菜单'}"
            },
            </c:forEach>];
        // 初始化树结构
        var tree = $.fn.zTree.init($("#reportTree"), setting, zNodes);
        // 不选择父节点
        tree.setting.check.chkboxType = {"Y": "ps", "N": "s"};
        // 默认选择节点
        var ids = "${sysRole.reportIds}".split(",");
        for (var i = 0; i < ids.length; i++) {
            var node = tree.getNodeByParam("id", ids[i]);
            try {
                tree.checkNode(node, true, false);
            } catch (e) {
            }
        }
        // 默认展开全部节点
        tree.expandAll(true);
        $("#go_back_btn").on("click", function () {
            goBack();
        });
    });

    function goBack() {
        document.location.href = "${GLOBAL.basePath}/sysrole/list";
    }
</script>
