<%--
   For com.football
   Copyright [2015/11/17] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp"%>
<%@include file="../../common/navbar.jsp"%>
<%@include file="../../common/page_content_pre.jsp"%>

<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>编辑字典</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form class="form-horizontal" role="form" action="${GLOBAL.basePath}/dict/save" method="post" id="dictForm">
                            <input  name="id" id="s_id" type="hidden" value="${dict.id}"/>
                                <fieldset>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" >字典键：</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" id="s_itemKey" name="itemKey" type="text" placeholder="请输入键" value="${dict.itemKey}"/>
                                        </div>
                                    </div>
                                </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" >字典值：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="s_itemValue" name="itemValue" type="text" placeholder="请输入值" value="${dict.itemValue}"/>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" >选择父字典：</label>
                                    <div class="col-sm-4">
                                        <input id="s_pItemValue" name="pItemValue" type="hidden" value="${dict.pItemValue}"/>
                                        <select id="s_pItemKey" name ="pItemKey" class="form-control" def_value="${dict.pItemKey}"></select>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" >字典状态：</label>
                                    <div class="col-sm-4 radio">
                                        <label class="inline">
                                            <input name="deleted" type="radio" value="0" class="ace" ${dict.deleted  ? '':'checked' } />
                                            <span class="lbl">启用</span>
                                        </label>
                                        <label class="inline">&nbsp;&nbsp;</label>
                                        <label class="inline">
                                            <input name="deleted" type="radio" value="1" class="ace" ${dict.deleted  ? 'checked':'' } />
                                            <span class="lbl">禁用</span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">字典描述：</label>
                                    <div class="col-sm-4">
                                        <textarea id="descr" name="descr" class="autosize-transition form-control">${dict.descr}</textarea>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-actions center">
                                <button type="button" class="btn btn-sm btn-grey" id="go_back_btn">
                                    返回
                                    <i class="icon-reply icon-on-right bigger-110"></i>
                                </button>
                                <shiro:hasPermission name="sys:dict:edit">
                                <button type="button" class="btn btn-sm btn-success" id="save_btn">
                                    保存
                                    <i class="icon-save icon-on-right bigger-110"></i>
                                </button>
                                </shiro:hasPermission>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<%@include file="../../common/page_content_suf.jsp"%>
<%@include file="../../common/script.jsp"%>

<script type="text/javascript">
    jQuery(function($) {

        ART.mLoadSelectOption("${GLOBAL.basePath}/dict/dictSelList?pItemKey=ROOT&ignores=${dict.id}",{},$("#s_pItemKey"),"itemKey","itemValue");

        $("#save_btn").on("click",function(){
            $("#s_itemKey").val($("#s_itemKey").val().trim());
            $(this).closest('form').submit();
        });

        $("#dictForm").validate({
            rules: {
                itemKey:{maxlength:20,required:true,remote:{
                        url: "${GLOBAL.basePath}/dict/notExists",
                        type: "post",
                        dataType: "json",
                        data: {
                            id : function(){
                                return $("#s_id").val();
                            },
                            itemKey: function() {
                                return $("#s_itemKey").val();
                            },
                            pItemKey:function(){
                                return $("#s_pItemKey").val();
                            }
                        }
                    }
                },
                descr:{maxlength:240}
            }
        });

        $("#s_pItemKey").on("change",function(){
            $("#s_pItemValue").val(ART.getSelectedText($("#s_pItemKey")));
            $("#s_itemKey").val($("#s_itemKey").val().trim()+" ");
            $("#s_itemKey").focus();
        });
        $("#go_back_btn").on("click", function () {
            goBack();
        });
    });

    function goBack() {
        document.location.href = "${GLOBAL.basePath}/dict/list";
    }
</script>
