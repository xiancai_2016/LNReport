<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp"%>
<%@include file="../../common/navbar.jsp"%>
<%@include file="../../common/page_content_pre.jsp"%>
<link href="${GLOBAL.staticJsPath}/treeTable/themes/vsStyle/treeTable.min.css" rel="stylesheet" type="text/css" />
<div class="page-content">
    <div class="row">
        <shiro:hasPermission name="sys:role:edit">
            <div class="col-sm-12">
                <button class="btn btn-xs btn-success" id="add_btn" >
                    添加
                    <i class="icon-plus align-top bigger-125 icon-on-right"></i>
                </button>
            </div>
        </shiro:hasPermission>
        <div class="col-xs-12">
            <form id="listForm" method="post">
                <table id="treeTable" class="table table-striped table-bordered table-condensed">
                    <thead><tr><th>名称</th><th>链接</th><th style="text-align:center;">排序</th><th>是否可见</th><th>权限标识</th><shiro:hasPermission name="sys:menu:edit"><th>操作</th></shiro:hasPermission></tr></thead>
                    <tbody><c:forEach items="${list}" var="menu">
                        <tr id="${menu.id}" pId="${menu.parent.id ne '1'?menu.parent.id:'0'}">
                            <td nowrap><i class="icon-${not empty menu.iconcls?menu.iconcls:' hide'}"></i><a href="${GLOBAL.basePath}/resource/form?id=${menu.id}">${menu.name}</a></td>
                            <td title="${menu.url}">${fns:abbr(menu.url,30)}</td>
                            <td style="text-align:center;">
                                <shiro:hasPermission name="sys:menu:edit">
                                    <input type="hidden" name="ids" value="${menu.id}"/>
                                    <input name="seq" type="text" value="${menu.seq}" style="width:50px;margin:0;padding:0;text-align:center;">
                                </shiro:hasPermission><shiro:lacksPermission name="sys:menu:edit">
                                ${menu.seq}
                            </shiro:lacksPermission>
                            </td>
                            <td>${menu.isShow ? '显示':'隐藏'}</td>
                            <td title="${menu.permission}">${fns:abbr(menu.permission,30)}</td>
                            <shiro:hasPermission name="sys:menu:edit"><td nowrap>
                                <a href="${GLOBAL.basePath}/resource/form?id=${menu.id}">修改</a>
                                <%--<a href="${GLOBAL.basePath}/resource/delete?id=${menu.id}" onclick="return confirmx('要删除该菜单及所有子菜单项吗？', this.href)">删除</a>--%>
                                <a href="#" pkId='${menu.id}' onclick="delClick(this)">删除</a>
                                <a href="${GLOBAL.basePath}/resource/form?parent.id=${menu.id}">添加下级菜单</a>
                            </td></shiro:hasPermission>
                        </tr>
                    </c:forEach></tbody>
                </table>
                <shiro:hasPermission name="sys:menu:edit"><div class="form-actions pagination-left">
                    <input id="btnSubmit" class="btn btn-primary" type="button" value="保存排序" onclick="updateSort();"/>
                </div></shiro:hasPermission>
            </form>
        </div>
    </div>
</div>

<%@include file="../../common/page_content_suf.jsp"%>
<%@include file="../../common/script1.jsp"%>
<link href="${GLOBAL.staticJsPath}/jquery-jbox/2.3/Skins/Bootstrap/jbox.min.css" rel="stylesheet" />
<script src="${GLOBAL.staticJsPath}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<script src="${GLOBAL.staticJsPath}/art.js"></script>
<script src="${GLOBAL.staticJsPath}/treeTable/jquery.treeTable.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#treeTable").treeTable({expandLevel : 3}).show();

        $("#add_btn").on("click",function(){
            addClick();
        });

        var addClick = function(){
            document.location.href = "${GLOBAL.basePath}/resource/form";
        };
    });
    var delClick = function(obj){
        layer.confirm("要删除该菜单及所有子菜单项吗？", {
            title: '提示',
            btn: ['确认','取消'], //按钮
            area: ['auto', 'auto'] //宽高
        },function(){
            var result = ART.mAjax("${GLOBAL.basePath}/resource/delete","POST",{id:$(obj).attr("pkId")});
            layer.alert(result,{icon: 1,title: '提示',btn: ['OK']},function(){
                history.go(0);
            });
        })
    }

    var updateSort = function() {
        loading('正在提交，请稍等...');
        $("#listForm").attr("action", "${GLOBAL.basePath}/resource/updateSort");
        $("#listForm").submit();
    }
</script>
