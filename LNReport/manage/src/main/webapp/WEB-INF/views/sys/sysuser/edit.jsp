<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/base_body_header.jsp" %>
<%@include file="../../common/navbar.jsp" %>
<%@include file="../../common/page_content_pre.jsp" %>
<style>
    label, .lbl {
         vertical-align: baseline;
    }

    .checkbox1 {
        min-height: 20px;
        vertical-align: middle;
    }
</style>

<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>${not empty sysUser.id?'修改':'添加'}用户</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form:form class="form-horizontal" modelAttribute="sysUser"
                                   action="${GLOBAL.basePath}/sysuser/save" method="post" id="sysUserForm">
                            <form:hidden path="id"/>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><span class="help-inline"><font color="red">*</font> </span>用户账户：</label>

                                    <div class="col-sm-5">
                                        <input id="oldAccount" name="oldAccount" type="hidden"
                                               value="${sysUser.account}">
                                        <form:input path="account" htmlEscape="false" maxlength="50"
                                                    class="input-xlarge required" placeholder="请输入用户账户"/>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><span class="help-inline"><font color="red">*</font> </span>用户名：</label>

                                    <div class="col-sm-5">
                                        <form:input path="name" htmlEscape="false" maxlength="50"
                                                    class="input-xlarge required" placeholder="请输入用户名"/>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">密码：</label>

                                    <div class="col-sm-5">
                                        <input class="input-xlarge ${empty sysUser.id?'required':''}" id="s_pwd"
                                               name="pwd" type="password" maxlength="50" minlength="3"
                                               placeholder="请输入密码"/>
                                        <c:if test="${empty sysUser.id}"><span class="help-inline"><font
                                                color="red">*</font> </span></c:if>
                                        <c:if test="${not empty sysUser.id}"><span
                                                class="help-inline">若不修改密码，请留空。</span></c:if>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">确认密码：</label>

                                    <div class="col-sm-5">
                                        <input class="input-xlarge" id="s_repeat_pwd" name="repeatPwd" type="password"
                                               value="" maxlength="50" minlength="3" placeholder="请输入确认密码"
                                               equalTo="#s_pwd"/>
                                        <c:if test="${empty sysUser.id}"><span class="help-inline"><font
                                                color="red">*</font> </span></c:if>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">账户状态：</label>

                                    <div class="col-sm-6">
                                        <label class="inline">
                                            <input name="deleted" type="radio" value="0"
                                                   class="ace" ${sysUser.deleted  ? '':'checked' } />
                                            <span class="lbl">启用</span>
                                        </label>
                                        <label class="inline">&nbsp;&nbsp;</label>
                                        <label class="inline">
                                            <input name="deleted" type="radio" value="1"
                                                   class="ace" ${sysUser.deleted  ? 'checked':'' } />
                                            <span class="lbl">禁用</span>
                                        </label>
                                        <span class="help-inline">“启用”代表此账号允许登录，“禁用”则表示此账号不允许登录</span>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><span class="help-inline"><font color="red">*</font> </span>用户角色：</label>

                                    <div class="col-sm-8">
                                        <%--<form:checkboxes path="roleIdList" items="${allRoles}" itemLabel="name"--%>
                                                         <%--itemValue="id" htmlEscape="false" class="required"/>--%>


                                        <form:checkboxes element="label class='checkbox1' style='width:210px;'"
                                         path="roleIdList" items="${allRoles}"
                                        itemLabel="name" itemValue="id"/>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">备注：</label>

                                    <div class="col-sm-4">
                                        <form:textarea path="descr" htmlEscape="false" rows="3" maxlength="200"
                                                       class="input-xlarge"/>
                                    </div>
                                </div>
                            </fieldset>
                            <c:if test="${not empty sysUser.id}">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">创建时间：</label>

                                        <div class="col-sm-6">
                                            <label class="lbl"><fmt:formatDate value="${sysUser.createdDatetime}"
                                                                               type="both" dateStyle="full"/></label>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">最后登陆：</label>

                                        <div class="col-sm-6">
                                            <label class="lbl">IP: ${sysUser.loginIp}&nbsp;&nbsp;&nbsp;&nbsp;时间：<fmt:formatDate
                                                    value="${sysUser.lastLoginTime}" type="both"
                                                    dateStyle="full"/></label>
                                        </div>
                                    </div>
                                </fieldset>
                            </c:if>
                            <div class="form-actions center">
                                <button type="button" class="btn btn-sm btn-grey" id="go_back_btn">
                                    返回
                                    <i class="icon-reply icon-on-right bigger-110"></i>
                                </button>
                                <shiro:hasPermission name='sys:user:edit'>
                                    <button type="submit" class="btn btn-sm btn-success" id="save_btn">
                                        保存
                                        <i class="icon-save icon-on-right bigger-110"></i>
                                    </button>
                                </shiro:hasPermission>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<%@include file="../../common/page_content_suf.jsp" %>
<%@include file="../../common/script.jsp" %>

<script type="text/javascript">
    //    jQuery(function($) {
    //        $("#save_btn").on("click",function(){
    //            $(this).closest('form').submit();
    //        });

    //        $("#sysUserForm").validate({
    //            rules: {
    //                account: "required",
    //                name:{minlength:1,maxlength:25,required:true},
    //                pwd:{minlength:6,maxlength:15,required:true},
    //                repeatPwd:{"equalTo":"#s_pwd"}
    //            }
    //        });

    //    })
    //展览下拉选项
    ART.mLoadSelectOption("${GLOBAL.basePath}/order/exhibitionList", {}, $("#s_pItemKey"), "id", "title", true);

    $(document).ready(function () {
        $("#account").focus();
        $("#sysUserForm").validate({
            rules: {
                account: {
                    remote: {
                        type: "POST",
                        cache: false,
                        url: "${GLOBAL.basePath}/sysuser/checkAccount",
                        data: {
                            oldAccount: function () {
                                return "${sysUser.account}";
                            }
                        }
                    }
                }
            },
            messages: {
                account: {required: "用户登录名不能为空", remote: jQuery.format("用户登录名已存在")},
                confirmNewPassword: {equalTo: "输入与上面相同的密码"}
            },
            submitHandler: function (form) {
                loading('正在提交，请稍等...');
                form.submit();
            },
            errorContainer: "#messageBox",
            errorPlacement: function (error, element) {
                $("#messageBox").text("输入有误，请先更正。");
                if (element.is(":checkbox") || element.is(":radio") || element.parent().is(".input-append")) {
                    error.appendTo(element.parent().parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        $("#go_back_btn").on("click", function () {
            goBack();
        });
    });

    function goBack() {
        document.location.href = "${GLOBAL.basePath}/sysuser/list";
    }
</script>
