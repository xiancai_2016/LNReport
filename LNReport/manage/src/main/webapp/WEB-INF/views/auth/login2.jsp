<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ include file="../common/taglib.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="icon" href="${GLOBAL.staticImagePath}/bsw.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="${GLOBAL.staticImagePath}/bsw.ico" type="image/x-icon"/>
    <title>${fns:getConfig('productName')}</title>
    <meta name="description" content="User login page"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <meta name="author" content="">
    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/font-awesome.css">

    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace-fonts.css">

    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace.css">
    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace-rtl.min.css">
</head>

<body class="login-layout light-login">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div>&nbsp;</div>
                    <div class="center">
                        <div class="center">
                            <img src="http://lmsadmin.learningedu.com.cn/images/new/logo.png">
                        </div>
                        <h1>
                            <span class="blue">Learning</span>
                            <span class="blue" id="id-text2">ODS</span>
                        </h1>
                    </div>
                    <div class="space-6"></div>
                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header grey lighter bigger">
                                        <i class=""></i>首次请登录
                                    </h4>

                                    <div class="space-6"></div>
                                    <form id="loginForm" class="form-signin" role="form"
                                          action="${GLOBAL.basePath}/wechatLogin/login" method="post">
                                        <label class="block clearfix">
                                            <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control span12" name="username"
                                                       id="username" value="" placeholder="输入账号"
                                                       maxlength="16">
                                            </span>
                                        </label>
                                        <label class="block clearfix">
                                            <span class="block input-icon input-icon-right">
                                                <input class="form-control span12" name="password" type="password"
                                                       value="" placeholder="输入密码" maxlength="8">
                                                <span id="info" style="color: red;"></span>

                                            </span>
                                        </label>

                                        <input type="hidden" name ="openid" value="${openid}">

                                        <div class="clearfix">

                                            <button type="submit" class="width-35 btn btn-sm btn-primary">

                                                <span class="bigger-110">登录</span>
                                            </button>
                                        </div>
                                        <div id="messageBox" class="alert alert-error hide">
                                            <p class="center-block mg-t mg-b text-center">
                                                <label id="loginError" class="error"><font size="3px"
                                                                                           style="color : red"></font></label>
                                            </p>
                                        </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script src='${GLOBAL.staticJsPath}/assets/js/jquery-1.10.2.min.js'></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.validate.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.mobile.custom.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#loginForm").validate({
            messages: {
                username: {required: "请填写用户名."},
                password: {required: "请填写密码."},
            },
            errorLabelContainer: "#messageBox",
            errorPlacement: function (error, element) {
                error.appendTo($("#loginError"));
            }
        });
    });
</script>
</body>
</html>