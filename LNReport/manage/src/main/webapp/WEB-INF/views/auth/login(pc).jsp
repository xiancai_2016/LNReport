<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ include file="../common/taglib.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="icon" href="${GLOBAL.staticImagePath}/bsw.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="${GLOBAL.staticImagePath}/bsw.ico" type="image/x-icon"/>
    <title>${fns:getConfig('productName')}</title>
    <meta name="description" content="User login page"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <meta name="author" content="">
    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/font-awesome.css">

    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace-fonts.css">

    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace.css">
    <link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace-rtl.min.css">
</head>

<body class="login-layout light-login">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div>&nbsp;</div>
                    <div class="center">
                        <h1>
                            <i class="ace-icon fa fa-leaf green"></i>
                            <span class="red">ODS</span>
                            <span class="grey" id="id-text2">报表管理</span>
                        </h1>
                    </div>
                    <div class="space-6"></div>
                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header blue lighter bigger">
                                        <i class="ace-icon fa fa-coffee green"></i> 请输入你的账号和密码
                                    </h4>

                                    <div class="space-6"></div>
                                    <form id="loginForm" class="form-signin" role="form"
                                          action="${GLOBAL.basePath}/auth/login" method="post">
                                        <label class="block clearfix">
                                            <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control span12" name="username"
                                                       id="username" value="${account}" placeholder="输入账号"
                                                       maxlength="16">
                                                <i class="ace-icon fa fa-user"></i>
                                            </span>
                                        </label>
                                        <label class="block clearfix">
                                            <span class="block input-icon input-icon-right">
                                                <input class="form-control span12" name="password" type="password"
                                                       value="${password}" placeholder="输入密码" maxlength="8">
                                                <span id="info" style="color: red;"></span>
                                                <i class="ace-icon fa fa-lock"></i>
                                            </span>
                                        </label>

                                        <c:if test="${isValidateCodeLogin}">
                                            <div class="form-inline validateCode">
                                                <sys:validateCode name="validateCode" placeholder="验证码"
                                                                  inputCssStyle="form-control required"/>
                                            </div>
                                            <div> &nbsp;</div>
                                        </c:if>
                                        <div class="clearfix">
                                            <label class="inline">
                                                <input type="checkbox" class="ace" id="rememberMe"
                                                       name="rememberMe" ${rememberMe ? 'checked' : ''}/>
                                                <span class="lbl"> 记住我</span>
                                            </label>

                                            <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                <i class="ace-icon fa fa-key"></i>
                                                <span class="bigger-110">登录</span>
                                            </button>
                                        </div>
                                        <div id="messageBox" class="alert alert-error ${empty message ? 'hide' : ''}">
                                            <p class="center-block mg-t mg-b text-center">
                                                <label id="loginError" class="error"><font size="3px"
                                                                                           style="color : red">${message}</font></label>
                                            </p>
                                        </div>
                                    </form>

                                    <div class="social-or-login center">
                                        <span class="bigger-110">欢迎</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src='${GLOBAL.staticJsPath}/assets/js/jquery-1.10.2.min.js'></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.validate.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.mobile.custom.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#loginForm").validate({
            rules: {
                validateCode: {remote: "${GLOBAL.basePath}/servlet/validateCodeServlet"}
            },
            messages: {
                username: {required: "请填写用户名."},
                password: {required: "请填写密码."},
                validateCode: {remote: "验证码不正确.", required: "请填写验证码."}
            },
            errorLabelContainer: "#messageBox",
            errorPlacement: function (error, element) {
                error.appendTo($("#loginError"));
            }
        });
    });
</script>
<script type="text/javascript">
    //<![CDATA[
    try {
        if (!window.CloudFlare) {
            var CloudFlare = [{
                verbose: 0,
                p: 0,
                byc: 0,
                owlid: "cf",
                bag2: 1,
                mirage2: 0,
                oracle: 0,
                paths: {
                    cloudflare: "/cdn-cgi/nexp/dok2v=1613a3a185/"
                },
                atok: "1668c19642567e08c574f5d9458345a2",
                petok: "8f7b41b79fe810b6951f62425931df1cfa3f78a5-1419101773-1800",
                zone: "nyasha.me",
                rocket: "0",
                apps: {
                    "ga_key": {
                        "ua": "UA-50530436-1",
                        "ga_bs": "2"
                    }
                }
            }];
            CloudFlare.push({
                "apps": {
                    "ape": "2cb0bbaf515762861a9ada12b07fbc38"
                }
            });
            !function (a, b) {
                a = document.createElement("script"), b = document.getElementsByTagName("script")[0], a.async = !0, a.src = "//ajax.cloudflare.com/cdn-cgi/nexp/dok2v=919620257c/cloudflare.min.js", b.parentNode.insertBefore(a, b)
            }()
        }
    } catch (e) {
    }
    //]]>
</script>
</body>
</html>