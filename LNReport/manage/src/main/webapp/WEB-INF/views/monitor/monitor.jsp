<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp"%>
<%@include file="../common/navbar.jsp"%>
<%@include file="../common/page_content_pre.jsp"%>
<style>
    body{margin:0; padding:0;}
    .outer { height: 100%; padding: 0 0 0; box-sizing: border-box ; }
</style>
<div class="outer">
    <iframe src="${GLOBAL.basePath}/monitoring" id="iframepage" name="iframepage" width="100%" height="582px" frameborder="0" scrolling="yes" marginwidth="0" marginheight="0"></iframe>
</div>
<%@include file="../common/page_content_suf.jsp"%>
<%@include file="../common/script.jsp"%>
