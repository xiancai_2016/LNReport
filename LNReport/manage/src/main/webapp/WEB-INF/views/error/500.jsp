<%
	response.setStatus(500);

// 获取异常类
	Throwable ex = Exceptions.getThrowable(request);
	if (ex != null){
		LoggerFactory.getLogger("500.jsp").error(ex.getMessage(), ex);
	}

// 编译错误信息
	StringBuilder sb = new StringBuilder("错误信息：\n");
	if (ex != null) {
		sb.append(Exceptions.getStackTraceAsString(ex));
	} else {
		sb.append("未知错误.\n\n");
	}

// 如果是异步请求或是手机端，则直接返回信息
	if (Servlets.isAjaxRequest(request)) {
		out.print(sb);
	}

// 输出异常信息页面
	else {
%>
<%@page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.lerning.edu.manage.common.web.Servlets"%>
<%@page import="com.lerning.edu.commons.util.Exceptions"%>
<%@ page import="com.lerning.edu.commons.util.StringUtil" %>
<%@page contentType="text/html;charset=UTF-8" isErrorPage="true"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
	<title>500 - 系统内部错误</title>
	<%@include file="../common/base_body_header.jsp"%>
	<%@include file="../common/navbar.jsp"%>
	<%@include file="../common/page_content_pre.jsp"%>
	<link href="${GLOBAL.staticJsPath}/jquery-jbox/2.3/Skins/Bootstrap/jbox.min.css" rel="stylesheet" />
	<script src="${GLOBAL.staticJsPath}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
</head>
<body>
<div class="container-fluid">
	<div class="page-header"><h1>系统内部错误.</h1></div>
	<div class="errorMessage">
		错误信息：<%=ex==null?"未知错误.":StringUtil.toHtml(ex.getMessage())%> <br/> <br/>
		请点击“查看详细信息”按钮，将详细错误信息发送给系统管理员，谢谢！<br/> <br/>
		<a href="javascript:" onclick="history.go(-1);" class="btn">返回上一页</a> &nbsp;
		<a href="javascript:" onclick="$('.errorMessage').toggle();" class="btn">查看详细信息</a>
	</div>
	<div class="errorMessage hide">
		<%=StringUtil.toHtml(sb.toString())%> <br/>
		<a href="javascript:" onclick="history.go(-1);" class="btn">返回上一页</a> &nbsp;
		<a href="javascript:" onclick="$('.errorMessage').toggle();" class="btn">隐藏详细信息</a>
		<br/> <br/>
	</div>
	<script>try{top.$.jBox.closeTip();}catch(e){}</script>
</div>
</body>
</html>
<%@include file="../common/page_content_suf.jsp"%>
<%@include file="../common/script1.jsp"%>
<%
	} out = pageContext.pushBody();
%>