<%
	response.setStatus(404);

// 如果是异步请求或是手机端，则直接返回信息
	if (Servlets.isAjaxRequest(request)) {
		out.print("页面不存在.");
	}

//输出异常信息页面
	else {
%>
<%@page import="com.lerning.edu.manage.common.web.Servlets"%>
<%@page contentType="text/html;charset=UTF-8" language="java" isErrorPage="true"%>
<!DOCTYPE html>
<html>
<head>
	<title>404 - 页面不存在</title>
	<%@include file="../common/base_body_header.jsp"%>
	<%@include file="../common/navbar.jsp"%>
	<%@include file="../common/page_content_pre.jsp"%>
	<link href="${GLOBAL.staticJsPath}/jquery-jbox/2.3/Skins/Bootstrap/jbox.min.css" rel="stylesheet" />
	<script src="${GLOBAL.staticJsPath}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
</head>
<body>
<div class="container-fluid">
	<div class="container-fluid">
		<div class="page-header"><h1>页面不存在.</h1></div>
		<div><a href="javascript:" onclick="history.go(-1);" class="btn">返回上一页</a></div>
		<script>try{top.$.jBox.closeTip();}catch(e){}</script>
	</div>
</div>
</body>
</html>
<%@include file="../common/page_content_suf.jsp"%>
<%@include file="../common/script1.jsp"%>
<%
		out.print("<!--"+request.getAttribute("javax.servlet.forward.request_uri")+"-->");
	} out = pageContext.pushBody();
%>