<%
response.setStatus(403);

//获取异常类
Throwable ex = Exceptions.getThrowable(request);

// 如果是异步请求或是手机端，则直接返回信息
if (Servlets.isAjaxRequest(request)) {
	if (ex!=null && StringUtil.startsWith(ex.getMessage(), "msg:")){
		out.print(StringUtil.replace(ex.getMessage(), "msg:", ""));
	}else{
		out.print("操作权限不足.");
	}
}

//输出异常信息页面
else {
%>
<%@page import="com.lerning.edu.manage.common.web.Servlets"%>
<%@page import="com.lerning.edu.commons.util.Exceptions"%>
<%@ page import="com.lerning.edu.commons.util.StringUtil" %>
<%@page contentType="text/html;charset=UTF-8" isErrorPage="true"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp"%>
<%@include file="../common/navbar.jsp"%>
<%@include file="../common/page_content_pre.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>403 - 操作权限不足</title>
</head>
<body>
	<div class="container-fluid">
		<div class="page-header"><h1>操作权限不足.</h1></div>
		<%
			if (ex!=null && StringUtil.startsWith(ex.getMessage(), "msg:")){
				out.print("<div>"+StringUtil.replace(ex.getMessage(), "msg:", "")+" <br/> <br/></div>");
			}
		%>
		<div><a href="javascript:" onclick="history.go(-1);" class="btn">返回上一页</a></div>
		<script>try{top.$.jBox.closeTip();}catch(e){}</script>
	</div>
</body>
</html>
<%@include file="../common/page_content_suf.jsp"%>
<%@include file="../common/script1.jsp"%>
<%
} out = pageContext.pushBody();
%>