<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/refSituation/list"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID" id="AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="querySta()">
                                查看统计图
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active">
                    <a href="#SpeedOfProgress" data-toggle="tab">完成进度和校区表现</a>
                </li>
                <li><a href="#RegionRankings" data-toggle="tab">区域排行</a></li>
                <li><a href="#CampusRankings" data-toggle="tab">校区排行</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="table-responsive tab-pane fade in active" id="SpeedOfProgress">
                    <table id="table-data" class="table" width="100%">
                        ${data.year}年${data.month}月Ref渠数据表现
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>AchiLeads</th>
                            <th>周期</th>
                            <th>时间进度</th>
                        </tr>
                        </thead>
                        <tr>
                            <td>财务周期</td>
                            <td>${data.CWDate}</td>
                            <td>${data.dateJD}天</td>
                        </tr>
                        <tr>
                            <td>数据统计</td>
                            <td>${data.TJData}</td>
                            <td>时间进度${data.dataJD}</td>
                        </tr>
                    </table>
                    <hr>
                    <table id="table-Speed" class="table" width="100%">
                        ${data.year}.${data.month}月Ref渠道完成进度
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>Region</th>
                            <th>AchiLeads</th>
                            <th>AchiShow</th>
                            <th>AchiEn</th>
                        </tr>
                        </thead>
                        <c:if test="${!empty map}">
                            <tr>
                                <td>总计:</td>
                                <td>${map.aLeads}%</td>
                                <td>${map.aShow}%</td>
                                <td>${map.aEn}%</td>
                            </tr>
                        </c:if>
                        <c:if test="${!empty SHList}">
                            <c:forEach items="${SHList}" var="item">
                                <tr>
                                    <td>${item.regionName}</td>
                                    <td>${item.RALeads}%</td>
                                    <td>${item.RAShow}%</td>
                                    <td>${item.RAEn}%</td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                    <hr>
                    <table id="table-Progress" class="table" width="100%">
                        ${data.year}.${data.month}月Ref渠道校区表现
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>Ranking</th>
                            <th>AchiLeads</th>
                            <th>AchiShow</th>
                            <th>AchiEn</th>
                        </tr>
                        </thead>
                        <c:if test="${!empty mapList}">
                            <c:forEach items="${mapList}" var="item">
                                <tr>
                                    <td>${item.Ranking}</td>
                                    <td><span style="color: #0b6cbc">${item.LeadsName}</span>&nbsp;&nbsp;<span
                                            style="color: #B8311F">${item.AchiLeads}%</span></td>
                                    <td><span style="color: #0b6cbc">${item.ShowName}</span>&nbsp;&nbsp;<span
                                            style="color: #B8311F">${item.AchiShow}%</span></td>
                                    <td><span style="color: #0b6cbc">${item.EnName}</span>&nbsp;&nbsp;<span
                                            style="color: #B8311F">${item.AchiEn}%</span></td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                    <hr>
                </div>
                <div class="table-responsive tab-pane fade" id="RegionRankings">
                    <c:if test="${!empty SHList}">
                        <c:forEach items="${SHList}" var="item">
                            Ranking-Total(${item.regionName})
                            <table id="table-${item.regionName}" class="table" width="100%">
                                <thead>
                                <tr style="background: #438eb9; color: white">
                                    <th>Center</th>
                                    <th>Leads</th>
                                    <th>Show</th>
                                    <th>En</th>
                                    <th>TarLeads</th>
                                    <th>TarShow</th>
                                    <th>TarEn</th>
                                    <th>AchiLeads</th>
                                    <th>AchiShow</th>
                                    <th>AchiEn</th>
                                </tr>
                                </thead>
                                <c:if test="${!empty item.regions}">
                                    <c:forEach items="${item.regions}" var="item1">
                                        <tr>
                                            <td>${item1.cName}</td>
                                            <td>${item1.lcount}</td>
                                            <td>${item1.scount}</td>
                                            <td>${item1.ecount}</td>
                                            <td>${item1.Tleads}</td>
                                            <td>${item1.Tshow}</td>
                                            <td>${item1.kValue}</td>
                                            <td>${item1.AchiLeads}%</td>
                                            <td>${item1.AchiShow}%</td>
                                            <td>${item1.AchiEn}%</td>
                                        </tr>
                                    </c:forEach>
                                </c:if>

                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${item.Leads}</td>
                                    <td>${item.Show}</td>
                                    <td>${item.En}</td>
                                    <td>${item.tLeads}</td>
                                    <td>${item.tShow}</td>
                                    <td>${item.tEn}</td>
                                    <td>${item.RALeads}%</td>
                                    <td>${item.RAShow}%</td>
                                    <td>${item.RAEn}%</td>
                                </tr>
                            </table>
                        </c:forEach>
                    </c:if>
                </div>
                <div class="table-responsive tab-pane fade" id="CampusRankings">
                    <table id="table-CampusRankings" class="table" width="100%">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>Center</th>
                            <th>Leads</th>
                            <th>Show</th>
                            <th>En</th>
                            <th>TarLeads</th>
                            <th>TarShow</th>
                            <th>TarEn</th>
                            <th>AchiLeads</th>
                            <th>AchiShow</th>
                            <th>AchiEn</th>
                        </tr>
                        </thead>
                        <c:if test="${!empty refs}">
                            <c:forEach items="${refs}" var="item">
                                <tr>
                                    <td>${item.cName}</td>
                                    <td>${item.lcount}</td>
                                    <td>${item.scount}</td>
                                    <td>${item.ecount}</td>
                                    <td>${item.Tleads}</td>
                                    <td>${item.Tshow}</td>
                                    <td>${item.kValue}</td>
                                    <td>${item.AchiLeads}%</td>
                                    <td>${item.AchiShow}%</td>
                                    <td>${item.AchiEn}%</td>
                                </tr>
                            </c:forEach>
                        </c:if>

                        <c:if test="${!empty map}">
                            <tr style="background: #438eb9; color: white">
                                <td>总计:</td>
                                <td>${map.Leads}</td>
                                <td>${map.Show}</td>
                                <td>${map.En}</td>
                                <td>${map.tLeads}</td>
                                <td>${map.tShow}</td>
                                <td>${map.tEn}</td>
                                <td>${map.aLeads}%</td>
                                <td>${map.aShow}%</td>
                                <td>${map.aEn}%</td>
                            </tr>
                        </c:if>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="mask" class="img"></div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }

    function querySta() {
        $("body").mLoading();
        window.location = "${GLOBAL.basePath}/refSituation/statistics?AutoID=" + $("#AutoID").val();
    }
</script>