<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="exceptionRenewForm" role="form" action="${GLOBAL.basePath}/exceptionRenew/renew"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID" id = "AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <label class="col-sm-1 control-label">校区</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="CampusID" id="CampusID">
                                <option value="">全部</option>
                                <c:forEach items="${departs}" var="item">
                                    <option value="${item.cID}"
                                            <c:if test="${item.cID == CampusID}">selected</c:if>>${item.cNAME}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-sm-4">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <button class="btn btn-info btn-sm" type="button" onclick="exportExceptionRenew()">
                                导出
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <span style="color: red">共${exceptionCount}个异常</span>
            <div>&nbsp;</div>
        </div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="table_tin" class="table table-bordered" width="100%">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th>序号</th>
                        <th>校区</th>
                        <th>CC/AS姓名</th>
                        <th>CC/AS工号</th>
                        <th>合同编号</th>
                        <th>缴费日期</th>
                        <th>业绩归属日期</th>
                        <th>学生姓名</th>
                        <th>学生电话</th>
                        <th>当前班级名</th>
                        <th>排课结束日期</th>
                    </tr>
                    </thead>
                    <c:forEach items="${exceptionRenew}" var="item" varStatus="sta">
                        <tr>
                            <td>${sta.index + 1}</td>
                            <td>${item.campusName}</td>
                            <td>${item.cName}</td>
                            <td>${item.cField1}</td>
                            <td>${item.cReceiptNo}</td>
                            <td>${item.cCreateTime}</td>
                            <td>${item.cEffectDate}</td>
                            <td>${item.studentUser}</td>
                            <td>${item.cSMSTel}</td>
                            <td>${item.className}</td>
                            <td>${item.endTime}</td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="mask" class="img"></div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>

<script>
    function showMask() {
        $("body").mLoading();
        $("#exceptionRenewForm").submit();
    }

    function exportExceptionRenew(){
        var CampusID = $("#CampusID").val();
        var AutoID = $("#AutoID").val();
        window.location = "${GLOBAL.basePath}/exceptionRenew/exportInfo?CampusID="+ CampusID+"&AutoID="+AutoID;
    }
</script>