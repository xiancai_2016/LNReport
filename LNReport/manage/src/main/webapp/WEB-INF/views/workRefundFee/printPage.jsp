<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/myBase_body_header.jsp" %>
<%@include file="../common/myNavbar.jsp" %>
<%--<%@include file="../common/myPage_content_pre.jsp" %>--%>
<style>
    button {
        display: inline-block;
        padding: 6px 12px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        border: 1px solid transparent;
        border-radius: 4px;
        border-color: #28a4c9;
        color: #fff;
        background-color: #5bc0de;
        margin: 20px 20px 0 0;
    }
</style>

<div class="page-content" style="margin: 3px;">
    <div class="row">
        <div id="printPage">
            <div class="widget-main no-padding">
                <form:form modelAttribute="sysReport"
                           action="${GLOBAL.basePath}/workOrder/print" method="post" id="auditForm">
                    <input type="hidden" name="workOrderID" id="workOrderID" value="${refundFee.workOrderOrderID}">

                    <div><br><br><br></div>
                    <fieldset>
                        <div style="margin: 0 auto;">
                            <table style=" border-style: dashed; border-width: 1px; border-color: #000000;
                                                   width: 100%; height: 50px; border-radius: 5px; width: 100%; border: 1px solid #ccc;
                                                   border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate">
                                <tr>
                                    <td style="text-align:center;font-size: 12px">申请日期 : ${refundFee.cAskDate}</td>
                                    <td style="text-align:center;font-size: 12px">申请校区 : ${refundFee.campusName}</td>
                                    <td style="text-align:center;font-size: 12px">审核日期 : ${refundFee.cAffirmDate}</td>
                                    <td style="text-align:center;font-size: 12px">单据号 : ${refundFee.cOrderNo}</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label><strong>退费学员</strong></label>

                        <div style="margin: 0 auto;">
                            <table style=" border-style: dashed; border-width: 1px; border-color: #000000;
                                                   width: 100%; height: 50px; border-radius: 5px; width: 100%; border: 1px solid #ccc;
                                                   border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate">
                                <tr>
                                    <td style="text-align:center;font-size: 12px">学生姓名 : ${refundFee.studentName}</td>
                                    <td style="text-align:center;font-size: 12px">学号 : ${refundFee.cSerial}</td>
                                    <td style="text-align:center;font-size: 12px">联系电话 : ${refundFee.cSMSTel}</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label><strong>退款原因</strong></label>
                        &nbsp;<input readonly="readonly" value="${refundFee.cValue}" style="border-color: #d8d3d3;
                                                border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px;
                                                border-left-width: 0px;width: 80%;font-size: 12px"/>
                    </fieldset>

                    <fieldset>
                        <div>
                            <label><strong>备注</strong></label>

                            <div style="margin: 0 auto;">
                                <table style="height: 60px; border-style: dashed; border-width: 0.5px; border-color: #000000;
                                                      width: 100%; height: 50px; border-radius: 5px; width: 100%; border: 1px solid #ccc;
                                                      border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate;">
                                    <tr>
                                        <td style="font-size: 12px;">${refundFee.cDescribe}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label><strong>退费详情</strong></label>

                        <div style="margin: 0 auto;">
                            <table style="text-align:center; border-style: dashed; border-width: 0.5px; border-color: #8c8585; width: 100%; border-radius: 5px;
                                   width: 100%; border: 1px solid #ccc; border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate;" border: 1px>

                                <tr style="height: 50px;">
                                    <td style="font-size: 12px"><strong>课程名称</strong></td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>购买课次</strong>
                                    </td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>购买金额</strong>
                                    </td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>剩余课次</strong>
                                    </td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>退费课次</strong>
                                    </td>
                                    <td style="border-left: 0.5px dashed #8c8585;font-size: 12px">
                                        <strong>退费金额</strong>
                                    </td>
                                </tr>
                                <c:forEach items="${shiftInfo}" var="item">
                                    <tr style="height: 50px;">
                                        <td style="border-top:0.5px dashed #8c8585;font-size: 12px">${item.cName}</td>
                                        <td style=" border-top: 1px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${item.byTime}</td>
                                        <td style=" border-top: 0.5px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${item.byMoney}</td>
                                        <td style=" border-top: 0.5px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${item.xyTime}</td>
                                        <td style=" border-top: 0.5px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${item.refundTime}</td>
                                        <td style=" border-top: 0.5px dashed #8c8585; border-left: 0.5px dashed #8c8585;font-size: 12px">${item.refunMoney}</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label><strong></strong></label>

                        <div style="margin: 0 auto;">
                            <table style=" border-style: dashed; border-width: 1px; border-color: #000000;
                                                   width: 100%; height: 50px; border-radius: 5px; width: 100%; border: 1px solid #ccc;
                                                   border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate">
                                <tr>
                                    <td style="font-size: 12px" width="40%">应退电子钱包余额 : ${refundFee.cReductionReserveMoney}</td>
                                    <td style="font-size: 12px" width="40%">应退金额总计 : ${refundFee.cOughtPay}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 12px" width="40%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 12px" width="40%">扣款金额 : ${refundFee.cDirectReduce}(${refundFee.refunDes})</td>
                                    <td style="font-size: 12px" width="40%">实际退费 : ${refundFee.cPayFact}</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label><strong>退款账户</strong></label>

                        <div style="margin: 0 auto;">
                            <table style=" border-style: dashed; border-width: 1px; border-color: #000000;
                                                   width: 100%; height: 50px; border-radius: 5px; width: 100%; border: 1px solid #ccc;
                                                   border-radius: 5px; box-shadow: 0 1px 1px #ccc; border-collapse: separate"  align="left">
                                <tr>
                                    <td style="font-size: 12px">开户银行/支付宝/微信 : ${refundFee.cBankName}</td>
                                    <td style="font-size: 12px" width="40%">开户人 : ${refundFee.cAccountPerson}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 12px" width="40%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 12px" width="40%">银行账号/支付宝微信流水号 : ${refundFee.cAccount}</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </form:form>
            </div>
        </div>

        <div class="form-actions center">
            <button type="button" onclick="pPrint()"
                    style="font-size: 12px; font-weight:bold; width: 120px; background-color: #3796F1 !important;border-color: #3796F1;">
                打印特况单
            </button>
        </div>
    </div>
</div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/myScript.jsp" %>

<script src="${GLOBAL.staticJsPath}/assets/js/print/jquery.jqprint-0.3.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/print/jquery-migrate-1.2.1.min.js" ;></script>

<script type="text/javascript">
    function pPrint() {
        var workOrderID = $("#workOrderID").val();
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/workOrderInterrupt/print',
            dataType: 'json',
            data: {
                workOrderID: workOrderID
            },
            success: function (data) {
                if (data.data == 1) {
                    $("#printPage").jqprint();
                }
            }
        });
    }
</script>
