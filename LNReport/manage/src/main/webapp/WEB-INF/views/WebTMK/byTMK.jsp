<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="WebTMK" role="form" action="${GLOBAL.basePath}/WebTMK/byTMK"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">查询日期</label>

                        <div class="col-sm-5">
                            <input type="text" required=required style="margin-top: 5px;" onmousedown="cleanInfo()"
                                   value="${BeginDate}"
                                   id="txtBeginDay" name="BeginDate"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,maxDate:'#F{$dp.$D(txtEndDay)}'});"/>
                            至
                            <input type="text" required=required onmousedown="cleanInfo()" value="${EndDate}"
                                   id="txtEndDay" name="EndDate"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,minDate:'#F{$dp.$D(txtBeginDay)}'});"/>
                            <span style="color: red" id="dateInfo"></span>
                        </div>

                        <label class="col-sm-1 control-label">渠道</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="SaleMode" id="SaleMode">
                                <c:forEach items="${querySaleModeByTMK}" var="item">
                                    <option value="${item.cID}"
                                            <c:if test="${item.cID == SaleMode}">selected</c:if>>${item.cName}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <div class="col-xs-6">
                <div class="table-responsive">
                    <table id="table_tin" class="table table-bordered" width="100%">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>姓名</th>
                            <th>Leads</th>
                            <th>App</th>
                            <th>Show</th>
                            <th>Erollment</th>
                            <th>Ratio(Show/APP)</th>
                        </tr>
                        </thead>
                        <c:forEach items="${byTMLList}" var="item">
                            <tr>
                                <td>${item.cName}</td>
                                <td>
                                    <button type="button" id = "${item.cSerial}Leads" style="width: 50px" class="btn btn-xs btn-link byTMLListClass" onclick="queryDetail('Leads','${item.cName}','${item.cSerial}','${item.cUserID}')"><strong>${item.leadsCount}</strong></button>
                                </td>
                                <td>
                                    <button type="button" id = "${item.cSerial}APP" style="width: 50px" class="btn btn-xs btn-link byTMLListClass" onclick="queryDetail('APP','${item.cName}','${item.cSerial}','${item.cUserID}')"><strong>${item.appCount}</strong></button>
                                </td>
                                <td>
                                    <button type="button" id = "${item.cSerial}Show" style="width: 50px" class="btn btn-xs btn-link byTMLListClass" onclick="queryDetail('Show','${item.cName}','${item.cSerial}','${item.cUserID}')"><strong>${item.showCount}</strong></button>
                                </td>
                                <td>
                                    <button type="button" id = "${item.cSerial}En" style="width: 50px" class="btn btn-xs btn-link byTMLListClass" onclick="queryDetail('En','${item.cName}','${item.cSerial}','${item.cUserID}')"><strong>${item.enCount}</strong></button>
                                </td>
                                <td>${item.ASRatio}%</td>
                            </tr>
                        </c:forEach>

                        <tr style="background: #438eb9; color: white">
                            <td>总计</td>
                            <td>${totalMap.leads}</td>
                            <td>${totalMap.app}</td>
                            <td>${totalMap.show}</td>
                            <td>${totalMap.en}</td>
                            <td>${totalMap.totalRatio}%</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <button type="button" class="btn btn-info btn-xs" onclick="exportInfo('Leads')">导出
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-xs" onclick="exportInfo('APP')">导出
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-xs" onclick="exportInfo('Show')">导出
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-xs" onclick="exportInfo('En')">导出</button>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </div>

                <table>
                    <tr>
                        <td><span style="color: red">Leads查询规则：</span>&nbsp;</td>
                        <td><span style="color: red">按 线索产生时间 和 第一任主责任人 查询</span></td>
                    </tr>
                    <tr>
                        <td><span style="color: red">APP查询规则:</span> &nbsp;</td>
                        <td><span style="color: red">按 市场邀约时间 和 APP成功记录的操作者 查询</span></td>
                    </tr>
                    <tr>
                        <td><span style="color: red">Show查询规则: </span>&nbsp;</td>
                        <td><span style="color: red">按 实际到访时间 和 线索所有者 查询</span></td>
                    </tr>
                    <tr>
                        <td><span style="color: red">Erollment查询规则:</span> &nbsp;</td>
                        <td><span style="color: red">按 转化成功时间 和 线索所有者 查询</span></td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-6">
                <div class="table-responsive">
                    <table id="tableDetail" class="table table-bordered" width="100%">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th WIDTH="30%">姓名</th>
                            <th>手机号</th>
                            <th>校区</th>
                            <th>线索所有者</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form class="form-horizontal" id="exportApp" role="form" action="${GLOBAL.basePath}/WebTMK/exportInfo"
          method="post">
        <input type="hidden" name="exportStartTime" id="exportStartTime">
        <input type="hidden" name="exportEndTime" id="exportEndTime">
        <input type="hidden" name="exportSaleType" id="exportSaleType">
        <input type="hidden" name="exportInfo" id="exportInfo">
    </form>
</div>
<div id="mask" class="img"></div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>

<script>
    function showMask() {
        if ($("#txtBeginDay").val() == '') {
            $("#txtBeginDay").focus();
            $("#dateInfo").html("请选开始时间");
        } else if ($("#txtEndDay").val() == '') {
            $("#txtEndDay").focus();
            $("#dateInfo").html("请选结束时间");
        } else {
            $("body").mLoading();
            $("#WebTMK").submit();
        }
    }

    function queryDetail(info, cName,cID,cUserID) {
//        $(".byTMLListClass").removeClass('active');
//
//        $("#"+cID+info).addClass('active');

        var SaleMode = $("#SaleMode").val();
        var txtBeginDay = $("#txtBeginDay").val();
        var txtEndDay = $("#txtEndDay").val();

        var str = "";
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/WebTMK/detail',
            dataType: 'json',
            data: {
                SaleMode: SaleMode,
                exportStartTime: txtBeginDay,
                exportEndTime: txtEndDay,
                info: info,
                cName: cName,
                cUserID:cUserID
            },
            success: function (data) {
                var departs = data.data;
                if (departs.length > 0) {
                    for (i in departs) {
                        str += "<tr>" +
                                "<td>" + departs[i].cName + "</td>" +
                                "<td>" + departs[i].cSMSTel + "</td>" +
                                "<td>" + departs[i].campusID + "</td>" +
                                "<td>" + departs[i].cField5 + "</td>" +
                                "</tr>";
                    }
                } else {
                    str = "<tr align='left'>"+
                            "<td colspan='12'><span style='font-size: 16px;color: red'>暂无数据</span></td>"+
                          "</tr>";
                }
                $("#tableDetail tbody").html(str);
            }
        });
    }

    function exportInfo(info) {
        var SaleMode = $("#SaleMode").val();
        var txtBeginDay = $("#txtBeginDay").val();
        var txtEndDay = $("#txtEndDay").val();

        $("#exportInfo").val(info);
        $("#exportStartTime").val(txtBeginDay);
        $("#exportEndTime").val(txtEndDay);
        $("#exportSaleType").val(SaleMode);

        $("#exportApp").submit();
    }

</script>