<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/campus/dsrByUserByJMS"
                  method="post">
                <input type="hidden" name="CampusID" value="${CampusID}">
                <input type="hidden" name="companyID" value="${companyID}">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID" id = "AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active">
                    <a href="#TIN" data-toggle="tab">TIN</a>
                </li>
                <li><a href="#TOUT" data-toggle="tab">TOUT</a></li>
                <li><a href="#WALK_IN" data-toggle="tab">WALK_IN</a></li>
                <li><a href="#REF" data-toggle="tab">REF</a></li>
                <li><a href="#RENEW" data-toggle="tab">RENEW</a></li>
                <li><a href="#EnByUser" data-toggle="tab">个人业绩达成</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="table-responsive tab-pane fade in active" id="TIN">
                    <div class="table-responsive">
                        <table id="table_tin" class="table" width="100%">
                            <thead>
                            <tr style="background: #438eb9; color: white">
                                <th>工号</th>
                                <th>姓名</th>
                                <th>App</th>
                                <th>Fresh Pres</th>
                                <th>OC</th>
                                <th>ShowRatio</th>
                                <th>Contracts</th>
                                <th>CashIn</th>
                                <th>签订Ratio</th>
                            </tr>
                            </thead>
                            <c:forEach items="${tinByUser}" var="item">
                                <tr>
                                    <td>${item.cField1}</td>
                                    <td>
                                        <c:if test="${CampusID != item.userCampusID}"><span
                                                style="font-size: 10px;color: red">(${item.userCampus})</span></c:if>
                                            ${item.userName}
                                        <c:if test="${item.cStatus != '1'}"><span
                                                style="font-size: 10px;color: red">(离职)</span></c:if>
                                    </td>
                                    <td>${item.app}</td>
                                    <td>${item.fp}</td>
                                    <td>${item.oc1}</td>
                                    <td>${item.ShowRatio}%</td>
                                    <td>${item.Contracts1}</td>
                                    <td>${item.CashInValue}</td>
                                    <td>${item.Ratio}</td>
                                </tr>
                            </c:forEach>
                            <tr style="background: #438eb9; color: white">
                                <td></td>
                                <td>总计:</td>
                                <td>${appSumTIN.sumApp}</td>
                                <td>${appSumTIN.sumFP}</td>
                                <td>${appSumTIN.sumOC}</td>
                                <td>${appSumTIN.sumShowRatio}%</td>
                                <td>${appSumTIN.sumContracts}</td>
                                <td>${appSumTIN.sumCashIn}</td>
                                <td>${appSumTIN.sumRatio}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="table-responsive tab-pane fade" id="TOUT">
                    <div class="table-responsive">
                        <table id="table_tout" class="table" width="100%">
                            <thead>
                            <tr style="background: #438eb9; color: white">
                                <th>工号</th>
                                <th>姓名</th>
                                <th>App</th>
                                <th>Fresh Pres</th>
                                <th>OC</th>
                                <th>ShowRatio</th>
                                <th>Contracts</th>
                                <th>CashIn</th>
                                <th>签订Ratio</th>
                            </tr>
                            </tr>
                            </thead>
                            <c:forEach items="${toutByUser}" var="item">
                                <tr>
                                    <td>${item.cField1}</td>
                                    <td>
                                        <c:if test="${CampusID != item.userCampusID}"><span
                                                style="font-size: 10px;color: red">(${item.userCampus})</span></c:if>
                                            ${item.userName}
                                        <c:if test="${item.cStatus != '1'}"><span
                                                style="font-size: 10px;color: red">(离职)</span></c:if>
                                    </td>
                                    <td>${item.app}</td>
                                    <td>${item.fp}</td>
                                    <td>${item.oc1}</td>
                                    <td>${item.ShowRatio}%</td>
                                    <td>${item.Contracts1}</td>
                                    <td>${item.CashInValue}</td>
                                    <td>${item.Ratio}</td>
                                </tr>
                            </c:forEach>
                            <tr style="background: #438eb9; color: white">
                                <td></td>
                                <td>总计:</td>
                                <td>${appSumTOUT.sumApp}</td>
                                <td>${appSumTOUT.sumFP}</td>
                                <td>${appSumTOUT.sumOC}</td>
                                <td>${appSumTOUT.sumShowRatio}%</td>
                                <td>${appSumTOUT.sumContracts}</td>
                                <td>${appSumTOUT.sumCashIn}</td>
                                <td>${appSumTOUT.sumRatio}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="table-responsive tab-pane fade" id="WALK_IN">
                    <div class="table-responsive">
                        <table id="table_WALK_IN" class="table" width="100%">
                            <thead>
                            <tr style="background: #438eb9; color: white">
                                <th>工号</th>
                                <th>姓名</th>
                                <th>Leads</th>
                                <th>Fresh Pres</th>
                                <th>OC</th>
                                <th>ShowRatio</th>
                                <th>Contracts</th>
                                <th>CashIn</th>
                                <th>签订Ratio</th>
                            </tr>
                            </thead>
                            <c:forEach items="${winByUser}" var="item">
                                <tr>
                                    <td>${item.cField1}</td>
                                    <td>
                                        <c:if test="${CampusID != item.userCampusID}"><span
                                                style="font-size: 10px;color: red">(${item.userCampus})</span></c:if>
                                            ${item.userName}
                                        <c:if test="${item.cStatus != '1'}"><span
                                                style="font-size: 10px;color: red">(离职)</span></c:if>
                                    </td>
                                    <td>${item.app}</td>
                                    <td>${item.fp}</td>
                                    <td>${item.oc1}</td>
                                    <td>${item.ShowRatio}%</td>
                                    <td>${item.Contracts1}</td>
                                    <td>${item.CashInValue}</td>
                                    <td>${item.Ratio}</td>
                                </tr>
                            </c:forEach>
                            <tr style="background: #438eb9; color: white">
                                <td></td>
                                <td>总计:</td>
                                <td>${appSumWKIN.sumApp}</td>
                                <td>${appSumWKIN.sumFP}</td>
                                <td>${appSumWKIN.sumOC}</td>
                                <td>${appSumWKIN.sumShowRatio}%</td>
                                <td>${appSumWKIN.sumContracts}</td>
                                <td>${appSumWKIN.sumCashIn}</td>
                                <td>${appSumWKIN.sumRatio}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="table-responsive tab-pane fade" id="REF">
                    <div class="table-responsive">
                        <table id="table_REF" class="table" width="100%">
                            <thead>
                            <tr style="background: #438eb9; color: white">
                                <th>工号</th>
                                <th>姓名</th>
                                <th>Leads</th>
                                <th>Fresh Pres</th>
                                <th>OC</th>
                                <th>ShowRatio</th>
                                <th>Contracts</th>
                                <th>CashIn</th>
                                <th>签订Ratio</th>
                            </tr>
                            </thead>
                            <c:forEach items="${refByUser}" var="item">
                                <tr>
                                    <td>${item.cField1}</td>
                                    <td>
                                        <c:if test="${CampusID != item.userCampusID}"><span
                                                style="font-size: 10px;color: red">(${item.userCampus})</span></c:if>
                                            ${item.userName}
                                        <c:if test="${item.cStatus != '1'}"><span
                                                style="font-size: 10px;color: red">(离职)</span></c:if>
                                    </td>
                                    <td>${item.app}</td>
                                    <td>${item.fp}</td>
                                    <td>${item.oc1}</td>
                                    <td>${item.ShowRatio}%</td>
                                    <td>${item.Contracts1}</td>
                                    <td>${item.CashInValue}</td>
                                    <td>${item.Ratio}</td>
                                </tr>
                            </c:forEach>
                            <tr style="background: #438eb9; color: white">
                                <td></td>
                                <td>总计:</td>
                                <td>${appSumRef.sumApp}</td>
                                <td>${appSumRef.sumFP}</td>
                                <td>${appSumRef.sumOC}</td>
                                <td>${appSumRef.sumShowRatio}%</td>
                                <td>${appSumRef.sumContracts}</td>
                                <td>${appSumRef.sumCashIn}</td>
                                <td>${appSumRef.sumRatio}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="table-responsive tab-pane fade" id="RENEW">
                    <div class="table-responsive">
                        <table id="table_RENEW" class="table" width="100%">
                            <thead>
                            <tr style="background: #438eb9; color: white">
                                <th>工号</th>
                                <th>姓名</th>
                                <th>Contracts</th>
                                <th>CashIn</th>
                                <th>提前续班Contracts</th>
                                <th>提前续班CashIn</th>
                            </tr>
                            </thead>
                            <c:forEach items="${renewByUser}" var="item">
                                <tr>
                                    <td>${item.cField1}</td>
                                    <td>
                                        <c:if test="${CampusID != item.userCampusID}"><span
                                                style="font-size: 10px;color: red">(${item.userCampus})</span></c:if>
                                            ${item.userName}
                                        <c:if test="${item.cStatus != '1'}"><span
                                                style="font-size: 10px;color: red">(离职)</span></c:if>
                                    </td>
                                    <td>${item.TContracts}</td>
                                    <td>${item.CashIn1}</td>
                                    <td>${item.TContracts2}</td>
                                    <td>${item.CashIn2}</td>
                                </tr>
                            </c:forEach>
                            <tr style="background: #438eb9; color: white">
                                <td></td>
                                <td>总计:</td>
                                <td>${renewByUserTotal.sumTC}</td>
                                <td>${renewByUserTotal.sumTCI}</td>
                                <td>${renewByUserTotal.sumTC2}</td>
                                <td>${renewByUserTotal.sumTCI2}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="table-responsive tab-pane fade" id="EnByUser">
                    <div class="table-responsive">
                        <table id="table_EnByUser" class="table" width="100%">
                            <thead>
                            <tr style="background: #438eb9; color: white">
                                <th>工号</th>
                                <th>姓名</th>
                                <th>ReNew</th>
                                <th>提前续班分摊</th>
                                <th>Ren指标</th>
                                <th>Ren达成率</th>
                                <th>Ref</th>
                                <th>Ref指标</th>
                                <th>Ref达成率</th>
                                <th>MKT</th>
                                <th>MKT指标</th>
                                <th>MKT达成率</th>
                                <th>Total</th>
                                <th>Total指标</th>
                                <th>Total达成率</th>
                                <th>导出</th>
                            </tr>
                            </thead>
                            <c:forEach items="${enByUserSummary}" var="item">
                                <tr>
                                    <td>${item.cField1}</td>
                                    <td> ${item.cName}
                                        <c:if test="${item.cStatus != '1'}">
                                            <span style="font-size: 10px;color: red">(离职)</span>
                                        </c:if>
                                    </td>
                                    <td>${item.renewEn}</td>
                                    <td>${item.renewEn2}</td>
                                    <td>${item.renewKpi}</td>
                                    <td>${item.renewRatio}%</td>

                                    <td>${item.refEn}</td>
                                    <td>${item.refKpi}</td>
                                    <td>${item.refRatio}%</td>

                                    <td>${item.mktEn}</td>
                                    <td>${item.mktKpi}</td>
                                    <td>${item.mktRatio}%</td>

                                    <td>${item.totalEn}</td>
                                    <td>${item.totalKpi}</td>
                                    <td>${item.totalRatio}%</td>

                                    <td>
                                        <button class="btn btn-success btn-xs" type="button" onclick="exportDetail(${item.cField1})">
                                            导出
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            <tr style="background: #438eb9; color: white">
                                <td></td>
                                <td>总计:</td>
                                <td>${ebsTotal.renewEn}</td>
                                <td>${ebsTotal.renewEn2}</td>
                                <td>${ebsTotal.renewKpi}</td>
                                <td>${ebsTotal.renewRatio}%</td>

                                <td>${ebsTotal.refEn}</td>
                                <td>${ebsTotal.refKpi}</td>
                                <td>${ebsTotal.refRatio}%</td>

                                <td>${ebsTotal.mktEn}</td>
                                <td>${ebsTotal.mktKpi}</td>
                                <td>${ebsTotal.mktRatio}%</td>

                                <td>${ebsTotal.totalEn}</td>
                                <td>${ebsTotal.totalKpi}</td>
                                <td>${ebsTotal.totalRatio}%</td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="mask" class="img"></div>
</div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }

    function exportDetail(cField1){
        var AutoID = $("#AutoID").val();
        var CampusID =  $("#CampusID").val();
        window.location = "${GLOBAL.basePath}/campus/export?AutoID="+AutoID+"&CampusID="+CampusID+"&cField1="+cField1;
    }
</script>