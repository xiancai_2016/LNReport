<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/dsr/monthKpiList"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <div id="myTabContent" class="tab-content">
                <div class="table-responsive tab-pane fade in active" id="2018DSR">
                    <div class="col-xs-6">
                        <table id="table_E2018" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">MKT</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>目标</th>
                                <th>实际</th>
                                <th>达成率</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty monthKpis}">
                                <c:forEach items="${monthKpis}" var="item">
                                    <tr>
                                        <td>${item.cName}</td>
                                        <td>${item.mktKpi}</td>
                                        <td>${item.mash}</td>
                                        <c:if test="${item.mktRateP == 'Y'}">
                                            <td><span style="color: red">${item.mktRate}%</span></td>
                                        </c:if>
                                        <c:if test="${item.mktRateP == 'N'}">
                                            <td>${item.mktRate}%</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.totalKPIMkt}</td>
                                    <td>${totalMap.totalMkt}</td>
                                    <td>${totalMap.totalMktRate}%</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>

                    <div class="col-xs-6">
                        <table id="table_E2017" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Ref</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>目标</th>
                                <th>实际</th>
                                <th>达成率</th>

                            </tr>
                            </thead>
                            <c:if test="${!empty monthKpis}">
                                <c:forEach items="${monthKpis}" var="item">
                                    <tr>
                                        <td>${item.cName}</td>
                                        <td>${item.refKpi}</td>
                                        <td>${item.fash}</td>
                                        <c:if test="${item.refRateP == 'Y'}">
                                            <td><span style="color: red">${item.refRate}%</span></td>
                                        </c:if>
                                        <c:if test="${item.refRateP == 'N'}">
                                            <td>${item.refRate}%</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.totalKPIRef}</td>
                                    <td>${totalMap.totalRef}</td>
                                    <td>${totalMap.totalRefRate}%</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>

                    <div class="col-xs-6">
                        <table id="table_C2018" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Renew</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>目标</th>
                                <th>实际</th>
                                <th>达成率</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty monthKpis}">
                                <c:forEach items="${monthKpis}" var="item">
                                    <tr>
                                        <td>${item.cName}</td>
                                        <td>${item.renewKpi}</td>
                                        <td>${item.renew}</td>
                                        <c:if test="${item.renewRateP == 'Y'}">
                                            <td><span style="color: red">${item.renewRate}%</span></td>
                                        </c:if>
                                        <c:if test="${item.renewRateP == 'N'}">
                                            <td>${item.renewRate}%</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.totalKPIRenew}</td>
                                    <td>${totalMap.totalRenew}</td>
                                    <td>${totalMap.totalRenewRate}%</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>

                    <div class="col-xs-6">
                        <table id="table_C2017" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Enrollment</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>目标</th>
                                <th>实际</th>
                                <th>达成率</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty monthKpis}">
                                <c:forEach items="${monthKpis}" var="item">
                                    <tr>
                                        <td>${item.cName}</td>
                                        <td>${item.totalKPIByCampus}</td>
                                        <td>${item.totalEnrollmentByCampus}</td>
                                        <c:if test="${item.totalRateP == 'Y'}">
                                            <td><span style="color: red">${item.totalRate}%</span></td>
                                        </c:if>
                                        <c:if test="${item.totalRateP == 'N'}">
                                            <td>${item.totalRate}%</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.totalKpi}</td>
                                    <td>${totalMap.totalEnrollment}</td>
                                    <td>${totalMap.totalRate}%</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }
</script>