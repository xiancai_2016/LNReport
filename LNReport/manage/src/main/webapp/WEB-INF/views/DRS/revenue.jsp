<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="revenue" role="form" action="${GLOBAL.basePath}/revenue/list"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <table id="table_revenue" class="table" width="100%">
                <thead>
                <tr style="background: #438eb9; color: white">
                    <th>校区</th>
                    <th>目标</th>
                    <th>累计</th>
                    <th>达成比</th>
                </tr>
                </thead>
                <c:if test="${!empty revenues}">
                    <c:forEach items="${revenues}" var="item">
                        <tr>
                            <td>${item.cName}</td>
                            <td>${item.kpi}</td>
                            <td>${item.fstWeek}</td>
                            <c:if test="${item.kpiRateP == 'Y'}">
                                <td> <span style="color: red">${item.kpiRate}%</span></td>
                            </c:if>
                            <c:if test="${item.kpiRateP == 'N'}">
                                <td>${item.kpiRate}%</td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </c:if>

                <c:if test="${!empty totalMap}">
                    <tr style="background: #438eb9; color: white">
                        <td>总计</td>
                        <td>${totalMap.totalKpi}</td>
                        <td>${totalMap.wFst}</td>
                        <td>${totalMap.kpiRate}%</td>
                    </tr>
                </c:if>
            </table>
        </div>
    </div>
</div>
<div id="mask" class="img"></div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    function showMask() {
        $("body").mLoading();
        $("#revenue").submit();
    }
</script>