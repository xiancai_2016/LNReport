<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/dsr/channelList"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active">
                    <a href="#2018DSR" data-toggle="tab">DSR</a>
                </li>
                <li><a href="#Increase" data-toggle="tab">Increase</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="table-responsive tab-pane fade in active" id="2018DSR">
                    <div class="col-xs-6">
                        <table id="table_E2018" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5"style="background: white">2018 Enrollment</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.mash}</td>
                                        <td>${item.fash}</td>
                                        <td>${item.tashf}</td>
                                        <td>${item.allashf}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.emkt2018}</td>
                                    <td>${totalMap.eref2018}</td>
                                    <td>${totalMap.erenew2018}</td>
                                    <td>${totalMap.etotal2018}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>

                    <div class="col-xs-6">
                        <table id="table_E2017" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">2017 Enrollment</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>

                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.emkt}</td>
                                        <td>${item.eref}</td>
                                        <td>${item.erenew}</td>
                                        <td>${item.etotal}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.emkt2017}</td>
                                    <td>${totalMap.eref2017}</td>
                                    <td>${totalMap.erenew2017}</td>
                                    <td>${totalMap.etotal2017}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>

                    <div class="col-xs-6">
                        <table id="table_C2018" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">2018 Cash In</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.mcostf}</td>
                                        <td>${item.fcostf}</td>
                                        <td>${item.tcostf}</td>
                                        <td>${item.allccostf}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.cmkt2018}</td>
                                    <td>${totalMap.cref2018}</td>
                                    <td>${totalMap.crenew2018}</td>
                                    <td>${totalMap.ctotal2018}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>

                    <div class="col-xs-6">
                        <table id="table_C2017" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">2017 Cash In</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>

                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.cmkt}</td>
                                        <td>${item.cref}</td>
                                        <td>${item.crenew}</td>
                                        <td>${item.ctotal}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.cmkt2017}</td>
                                    <td>${totalMap.cerf2017}</td>
                                    <td>${totalMap.crenew2017}</td>
                                    <td>${totalMap.ctotal2017}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                </div>
                <div class="table-responsive tab-pane fade" id="Increase">
                    <div class="col-xs-7">
                        <table id="table_EC" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Enrollment增长数量</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.emktInCount}</td>
                                        <td>${item.erefInCount}</td>
                                        <td>${item.erenewInCount}</td>
                                        <td>${item.etotalInCount}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.emkttatolCount}</td>
                                    <td>${totalMap.ereftatolCount}</td>
                                    <td>${totalMap.erenewtatolCount}</td>
                                    <td>${totalMap.ettatolCount}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>

                    <div class="col-xs-5">
                        <table id="table_EA" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Enrollment增长率</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>

                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.emktInRate}</td>
                                        <td>${item.erefInRate}</td>
                                        <td>${item.erenewInRate}</td>
                                        <td>${item.etotalInRate}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.emkttatolrate}</td>
                                    <td>${totalMap.ereftatolrate}</td>
                                    <td>${totalMap.erenewtatolrate}</td>
                                    <td>${totalMap.ettatolrate}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>

                    <div class="col-xs-7">
                        <table id="table_CC" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Cash In增长数量</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.cmktCashInCount}</td>
                                        <td>${item.crefCashInCount}</td>
                                        <td>${item.crenewCashInCount}</td>
                                        <td>${item.ctotalCashInCount}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.cmkttatolCount}</td>
                                    <td>${totalMap.creftatolCount}</td>
                                    <td>${totalMap.crenewtatolCount}</td>
                                    <td>${totalMap.cttatolCount}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>

                    <div class="col-xs-5">
                        <table id="table_CA" class="table" width="100%" style="font-size: 8px">
                            <thead>
                            <tr>
                                <th colSpan="5" style="background: white">Cash In增长率</th>
                            </tr>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>MKT</th>
                                <th>Referral</th>
                                <th>Retention</th>
                                <th>Total</th>

                            </tr>
                            </thead>
                            <c:if test="${!empty channels}">
                                <c:forEach items="${channels}" var="item">
                                    <tr>
                                        <td>${item.cAddress}</td>
                                        <td>${item.cmktCashInRate}</td>
                                        <td>${item.crefCashInRate}</td>
                                        <td>${item.crenewCashInRate}</td>
                                        <td>${item.ctotalCashInRate}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>

                            <c:if test="${!empty totalMap}">
                                <tr style="background: #438eb9; color: white">
                                    <td>总计:</td>
                                    <td>${totalMap.cmkttatolrate}</td>
                                    <td>${totalMap.creftatolrate}</td>
                                    <td>${totalMap.crenewtatolrate}</td>
                                    <td>${totalMap.cttatolrate}</td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }
</script>