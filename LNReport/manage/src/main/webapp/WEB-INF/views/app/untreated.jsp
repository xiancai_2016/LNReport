<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="untreatedApp" role="form" action="${GLOBAL.basePath}/untreatedApp/list"
                  method="post">
                <input type="hidden" name="campusID" id="campusID">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">查询市场邀约时间</label>

                        <div class="col-sm-5">
                            <input type="text" required=required style="margin-top: 5px;" onmousedown="cleanInfo()"
                                   value="${startTime}"
                                   id="txtBeginDay" name="startTime"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,maxDate:'#F{$dp.$D(txtEndDay)}'});"/>
                            至
                            <input type="text" required=required onmousedown="cleanInfo()" value="${endTime }"
                                   id="txtEndDay" name="endTime"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,minDate:'#F{$dp.$D(txtBeginDay)}'});"/>
                            <span style="color: red" id="dateInfo"></span>
                        </div>

                        <label class="col-sm-1 control-label">渠道</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="saleType" id="saleType">
                                <option value="TIN" <c:if test="${saleType == 'TIN'}">selected</c:if>>TIN渠道</option>
                                <option value="TOUT" <c:if test="${saleType == 'TOUT'}">selected</c:if>>TOUT渠道</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                            &nbsp;
                            <button class="btn btn-info btn-sm" type="button" onclick="exportApp()">
                                导出
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <div class="col-xs-2">
                <div class="table-responsive">
                    <table id="table_tin" class="table table-bordered" width="100%">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>校区</th>
                            <th>总数</th>
                        </tr>
                        </thead>
                        <c:forEach items="${campusAndCount}" var="item">
                            <tr <c:if test="${item.cID == campusID}">style="background:orange"</c:if>>
                                <td>${item.cName}</td>
                                <td><a onclick="queryDetail('${item.cID}')">${item.totalCount}</a></td>
                            </tr>
                        </c:forEach>

                        <tr style="background: #438eb9; color: white">
                            <td>总计</td>
                            <td>${totalCount}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-xs-10">
                <div class="table-responsive">
                    <table id="table_tout" class="table table-bordered" width="100%">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th width="10%">校区</th>
                            <th width="8%">姓名</th>
                            <th>手机号</th>
                            <th width="6%">渠道</th>
                            <th width="9%">主责任人</th>
                            <th>最后沟通内容</th>
                            <th width="10%">线索产生人</th>
                            <th width="12%">市场邀约时间</th>
                            <th width="11%">线索所有者</th>
                        </tr>
                        </thead>
                        <c:forEach items="${detail}" var="item">
                            <tr>
                                <td>${item.campus}</td>
                                <td>${item.name}</td>
                                <td>${item.cSMSTel}</td>
                                <td>${item.sale}</td>
                                <td>${item.cName}</td>
                                <td>${item.cContent}</td>
                                <td>${item.cField2}</td>
                                <td>${item.cField6}</td>
                                <td>${item.cField5}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form class="form-horizontal" id="exportApp" role="form" action="${GLOBAL.basePath}/untreatedApp/exportApp"
          method="post">
        <input type="hidden" name="exportStartTime" id="exportStartTime">
        <input type="hidden" name="exportEndTime" id="exportEndTime">
        <input type="hidden" name="exportSaleType" id="exportSaleType">
        <input type="hidden" name="exportCampus" id="exportCampus" value="${campusID}">
    </form>
</div>
<div id="mask" class="img"></div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>

<script>
    function showMask() {
        if ($("#txtBeginDay").val() == '') {
            $("#txtBeginDay").focus();
            $("#dateInfo").html("请选开始时间");
        } else if ($("#txtEndDay").val() == '') {
            $("#txtEndDay").focus();
            $("#dateInfo").html("请选结束时间");
        } else {
            $("body").mLoading();
            $("#untreatedApp").submit();
        }
    }

    function queryDetail(cID) {
        if ($("#txtBeginDay").val() == '') {
            $("#txtBeginDay").focus();
            $("#dateInfo").html("请选开始时间");
        } else if ($("#txtEndDay").val() == '') {
            $("#txtEndDay").focus();
            $("#dateInfo").html("请选结束时间");
        } else {
            $("#campusID").val(cID);
            $("#untreatedApp").submit();
        }
    }

    function cleanInfo() {
        $("#dateInfo").html("");
    }

    function exportApp() {
        if ($("#txtBeginDay").val() == '') {
            $("#txtBeginDay").focus();
            $("#dateInfo").html("请选开始时间");
        } else if ($("#txtEndDay").val() == '') {
            $("#txtEndDay").focus();
            $("#dateInfo").html("请选结束时间");
        }else {
            $("#exportStartTime").val($("#txtBeginDay").val());
            $("#exportEndTime").val($("#txtEndDay").val());
            $("#exportSaleType").val($("#saleType").val());
            $("#exportApp").submit();
        }
    }
</script>