<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>
<link rel="stylesheet" type="text/css" href="${GLOBAL.staticJsPath}/jquery-select2/3.4/select2.min.css"/>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="untreatedApp" role="form" action="${GLOBAL.basePath}/radar/chart"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">校区</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="CampusID" id="CampusID" onchange="changeCampus()">
                                <option value="">全部</option>
                                <c:forEach items="${departs}" var="item">
                                    <option value="${item.cID}"
                                            <c:if test="${item.cID == CampusID}">selected</c:if>>${item.cNAME}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <label class="col-sm-1 control-label">部门</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="departID" id="departID" onchange="changeDepart()">
                            </select>
                        </div>
                        <label class="col-sm-1 control-label">员工</label>

                        <div class="col-sm-3">
                            <select id="employeeID" name="employeeID" class="js-example-basic-single required"
                                    multiple="multiple" data-live-search="true" style="width:80%;">
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">查询时间</label>

                        <div class="col-sm-5">
                            <input type="text" required=required style="margin-top: 5px;" onmousedown="cleanInfo()"
                                   value="${startTime}"
                                   id="txtBeginDay" name="startTime"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,maxDate:'#F{$dp.$D(txtEndDay)}'});"/>
                            至
                            <input type="text" required=required onmousedown="cleanInfo()" value="${endTime }"
                                   id="txtEndDay" name="endTime"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,minDate:'#F{$dp.$D(txtBeginDay)}'});"/>
                            <span style="color: red" id="dateInfo"></span>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <br>

            <div id="container" style="height: 400%"></div>
        </div>
    </div>
</div>
<div id="mask" class="img"></div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>
<script src="${GLOBAL.staticJsPath}/jquery-select2/3.4/select2.min.js"></script>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="${GLOBAL.staticJsPath}/echarts/echarts.min.js"></script>

<script>
    $(function () {
        $(".js-example-basic-single").select2();
    });

    function showMask() {
        if ($("#txtBeginDay").val() == '') {
            $("#txtBeginDay").focus();
            $("#dateInfo").html("请选开始时间");
        } else if ($("#txtEndDay").val() == '') {
            $("#txtEndDay").focus();
            $("#dateInfo").html("请选结束时间");
        } else {
            $("body").mLoading();
            $("#untreatedApp").submit();
        }
    }

    function changeCampus() {
        $("#departID").empty();
        var CampusID = $("#CampusID").val();
        var optionValue
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/radar/queryDepart',
            dataType: 'json',
            data: {
                CampusID: CampusID
            },
            success: function (data) {
                var departs = data.data;
                if (departs.length > 0) {
                    for (var i = 0; i < departs.length; i++) {
                        optionValue = "<option value='" + departs[i].cID + "'>" + departs[i].cName + "</option>";
                        $("#departID").append(optionValue);
                    }
                } else {
                    $("#departID").html("<option value=''>请选择部门</option>");
                }
            }
        });
    }

    function changeDepart() {
        $("#employeeID").empty();
        var departID = $("#departID").val();
        var optionValue
        $.ajax({
            type: 'post',
            url: '${GLOBAL.basePath}/radar/queryEmployee',
            dataType: 'json',
            data: {
                departID: departID
            },
            success: function (data) {
                var employees = data.data;
                if (employees.length > 0) {
                    for (var i = 0; i < employees.length; i++) {
                        optionValue = "<option value='" + employees[i].cUserID + "'>" + employees[i].cName + "</option>";
                        $("#employeeID").append(optionValue);
                    }
                } else {
                    $("#employeeID").html("<option value=''>请选择员工</option>");
                }
            }
        });
    }

    var dom = document.getElementById("container");
    var dom1 = document.getElementById("container1");
    var myChart = echarts.init(dom);
    option = null;
    option = {
        title: {
            text: 'admin'
        },
        tooltip: {},

        radar: {
            // shape: 'circle',
            name: {
                textStyle: {
                    color: '#fff',
                    backgroundColor: '#999',
                    borderRadius: 3,
                    padding: [3, 5]
                }
            },
            indicator: [
                {name: '销售（sales）', max: 6500},
                {name: '管理（Administration）', max: 16000},
                {name: '信息技术（Information Techology）', max: 30000},
                {name: '客服（Customer Support）', max: 38000},
                {name: '研发（Development）', max: 52000},
                {name: '市场（Marketing）', max: 25000}
            ]
        },
        series: [{
            name: '预算 vs 开销（Budget vs spending）',
            type: 'radar',
            // areaStyle: {normal: {}},
            data: [
                {
                    value: [4468, 10000, 28000, 35000, 50000, 19000],
                    name: '预算分配（Allocated Budget）'
                },
                {
                    value: [5000, 14000, 28000, 31000, 42000, 21000],
                    name: '实际开销（Actual Spending）'
                }
            ]
        }]
    };

    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }
</script>