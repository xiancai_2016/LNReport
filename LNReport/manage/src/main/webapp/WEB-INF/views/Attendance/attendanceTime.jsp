<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>
<%@include file="../common/navbar.jsp" %>
<%@include file="../common/page_content_pre.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="fullAttendance" role="form"
                  action="${GLOBAL.basePath}/fullAttendance/attendanceTime"
                  method="post">
            </form>
        </div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="table_tin" class="table table-bordered" width="100%">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th>校区</th>
                        <th>参加人数</th>
                        <th>已开班人数</th>
                        <th>已失败人数</th>
                        <th>幸存人数</th>
                        <th>已成功人数</th>
                    </tr>
                    </thead>
                    <c:forEach items="${attendanceTime}" var="item">
                        <tr>
                            <td>${item.cName}</td>
                            <td>${item.cjCount}</td>
                            <td>${item.ykCount}</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                        </tr>
                    </c:forEach>

                    <tr style="background: #438eb9; color: white">
                        <td></td>
                        <td>${totalTime.totalcjCount}</td>
                        <td>${totalTime.totalykCount}</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="mask" class="img"></div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script></script>