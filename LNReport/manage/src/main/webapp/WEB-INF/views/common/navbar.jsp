<%--
   For com.football
   Copyright [2015/11/12] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<style>
    .icon-leaf:before{
        content: url(${GLOBAL.staticJsPath}/assets/avatars/logo_ma.png);
    }
</style>
<%@ include file="taglib.jsp"%>
<div class="navbar navbar-default" id="navbar">
    <script type="text/javascript">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>

    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left">
            <a href="#" class="navbar-brand">
                <small>
                    <i class="glyphicon glyphicon-th-list"></i>
                    ${fns:getConfig('productName')}
                </small>
            </a><!-- /.brand -->
        </div><!-- /.navbar-header -->

        <div class="navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
               <%-- <li class="grey">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-tasks"></i>
                        <span class="badge badge-grey">0</span>
                    </a>
                </li>
                <li class="purple">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-bell-alt icon-animated-bell"></i>
                        <span class="badge badge-important">0</span>
                    </a>
                </li>

                <li class="green">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-envelope icon-animated-vertical"></i>
                        <span class="badge badge-success">0</span>
                    </a>
                </li>--%>
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="${GLOBAL.staticJsPath}/assets/avatars/user.png" />
                        <span class="user-info" >
                            <small> 欢迎您：   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                            <strong>${fns:getUser().name}</strong>
                        </span>
                        <i class="icon-caret-down"></i>
                    </a>

                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                         <li>
                            <a href="${GLOBAL.basePath}/auth/userOne">
                                <i class="icon-user"></i>
                                修改密码
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="${GLOBAL.basePath}/auth/logout">
                                <i class="icon-off"></i>
                                注销
                            </a>
                        </li>
                    </ul>
                </li>
            </ul><!-- /.ace-nav -->
        </div><!-- /.navbar-header -->
    </div><!-- /.container -->
</div>

