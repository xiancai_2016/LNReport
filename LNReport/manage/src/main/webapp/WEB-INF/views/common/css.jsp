<%--
   For com.football
   Copyright [2015/11/12] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<!-- basic styles -->
<link href="${GLOBAL.staticJsPath}/assets/css/bootstrap.min.css" rel="stylesheet" />
<%--<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">--%>
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/font-awesome.min.css" />

<!--[if IE 7]>
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/font-awesome-ie7.min.css" />
<![endif]-->

<!-- page specific plugin styles -->

<!-- fonts -->
<%--<link rel="stylesheet" href="http://fonts.useso.com/css?family=Open+Sans:400,300" />--%>
<link rel="stylesheet" href="${GLOBAL.staticCssPath}/common/fonts.css">


<!-- ace styles -->
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace-fonts.css" />
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace.min.css" />
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace-rtl.min.css" />
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace-skins.min.css" />
<%--<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/datepicker.css" />--%>
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/daterangepicker.css" />
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/bootstrap-datetimepicker.css" />

<!--[if lte IE 8]>
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/ace-ie.min.css" />
<![endif]-->
<link rel="stylesheet" href="${GLOBAL.staticJsPath}/assets/css/validate.css" />

<link rel="stylesheet" type="text/css" href="${GLOBAL.staticCssPath}/auth/style.css">
<link rel="stylesheet" type="text/css" href="${GLOBAL.staticCssPath}/common/art.css">
<link rel="stylesheet" type="text/css" href="${GLOBAL.staticJsPath}/assets/css//jquery.mloading.css">
<link rel="stylesheet" type="text/css" href="${GLOBAL.staticJsPath}/model/style.css">

