<%--
   For com.football
   Copyright [2015/11/12] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<!-- basic scripts -->

<!-- ace settings handler -->

<script src="${GLOBAL.staticJsPath}/assets/js/ace-extra.min.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

<!--[if lt IE 9]>
<script src="${GLOBAL.staticJsPath}/assets/js/html5shiv.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script src="${GLOBAL.staticJsPath}/assets/js/spin.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery-2.0.3.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery-2.0.3.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery-1.10.2.min.js"></script>
<![endif]-->

<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<script src='${GLOBAL.staticJsPath}/assets/js/jquery-2.0.3.min.js'>"+"<"+"script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<script src='${GLOBAL.staticJsPath}/assets/js/jquery-1.10.2.min.js'>"+"<"+"script>");
</script>
<![endif]-->

<script src="${GLOBAL.staticJsPath}/assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/bootstrap.min.js"></script>
<%--<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>--%>
<script src="${GLOBAL.staticJsPath}/assets/js/typeahead-bs2.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.dataTables.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.dataTables.bootstrap.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.autosize.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/layer/layer.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="${GLOBAL.staticJsPath}/assets/js/excanvas.min.js"></script>
<![endif]-->

<!-- ace scripts -->
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.hotkeys.min.js"></script>

<script src="${GLOBAL.staticJsPath}/assets/js/ace-elements.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/ace.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/date-time/moment.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/date-time/daterangepicker.min.js"></script>

<!-- form vaidate -->
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.validate.min.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.validate.message_cn.js"></script>

<script src="${GLOBAL.staticJsPath}/assets/js/jquery.form.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/accounting.min.js"></script>
<script src="${GLOBAL.staticJsPath}/common_util.js"></script>
<script src="${GLOBAL.staticJsPath}/assets/js/jquery.mloading.js"></script>

<script type="text/javascript">
    jQuery(function($) {

        $("#sidebar ul a.menu-item").on("click",function(){
            ART.selectMenu($(this).attr("id"));
        });

        autoSelMenu();

        $('table th input:checkbox').on('click' , function(){
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                .each(function(){
                    this.checked = that.checked;
                    $(this).closest('tr').toggleClass('selected');
                });
        });
    });

    /**
     * 自动选中菜单
     */
    var autoSelMenu = function(){
        //判断是否为主页
        var basePath = "${GLOBAL.basePath}";
        var fullPath = document.location.href ;
        var index = fullPath.indexOf("?");
        if(index != -1){
            fullPath = fullPath.substring(0,index);
        }
        index = fullPath.indexOf("#");
        if(index != -1){
            fullPath = fullPath.substring(0,index);
        }
        if(basePath === fullPath
                || (basePath+"/") === fullPath
                || (basePath+"/index") === fullPath ){
            ART.selectMenu(null);
        }
        var selMenu = ART.getSelMenu() || "1_0";
        $("#"+selMenu).closest('li').addClass("open active");
        $("#"+selMenu).closest('ul').closest('li').addClass("open active");

        var bar_tip = "<li>"
                        + "<i class='icon-home home-icon'></i>"
                        + "<a href='"+basePath+"' >首页</a>"
                        + "</li>";
        if(selMenu != "1_0"){
            var tip1 = $("#"+selMenu).closest('ul').closest('li').find("a span.menu-text").text();
            bar_tip += "<li>"+tip1+"</li>";
            var tip2 = $("#"+selMenu).closest('li').text();
            bar_tip += "<li class='active'>"+tip2+"</li>";
        }
        $("#page_bar_tip").html(bar_tip);

    };
</script>