<%--
   For com.football
   Copyright [2015/11/12] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<%@ include file="taglib.jsp"%>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title>${fns:getConfig('productName')}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-store">
    <link rel="icon" href="${GLOBAL.staticImagePath}/favicon_16x16.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="${GLOBAL.staticImagePath}/favicon_16x16.ico" type="image/x-icon" />
    <%@include file="css.jsp"%>


</head>

<body>