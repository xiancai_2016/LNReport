<%--
   For com.football
   Copyright [2015/11/12] By RICK
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<%@ include file="taglib.jsp"%>
<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
       <%-- <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>--%>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- #sidebar-shortcuts -->
    <ul class="nav nav-list">
        <li >
            <a href="${GLOBAL.basePath}" id="1_0">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> 后台使用说明 </span>
            </a>
        </li>
        <c:forEach items="${fns:getMenuList()}" var="menu" varStatus="idxStatus">
            <c:if test="${menu.parent.id eq '1'&& menu.isShow}">
                <li id="${menu.id}_0">
                    <a href="#" class="dropdown-toggle">
                        <i class="icon-${menu.iconcls}"></i>
                        <span class="menu-text">${menu.name} </span>
                        <b class="arrow icon-angle-down"></b>
                    </a>
                    <ul class="submenu">
                        <c:set var="id" value="${menu.id}" scope="page" />
                        <c:forEach items="${fns:getSubMenuList(id)}" var="subMenu" varStatus="_idxStatus">
                            <c:if test="${subMenu.sysResourceType eq '0' && subMenu.isShow}">
                                <li>
                                    <a class="menu-item" href="${GLOBAL.basePath}${subMenu.url}"  id="${menu.id}_${subMenu.id}">
                                        <i class="icon-${subMenu.iconcls}"></i>
                                            ${subMenu.name}
                                    </a>
                                </li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </li>
            </c:if>
        </c:forEach>
        <%--<li  id="3_0">--%>
            <%--<a href="#" class="dropdown-toggle">--%>
                <%--<i class="icon-cogs"></i>--%>
                <%--<span class="menu-text">系统 </span>--%>
                <%--<b class="arrow icon-angle-down"></b>--%>
            <%--</a>--%>
            <%--<ul class="submenu">--%>
                <%--<li>--%>
                    <%--<a class="menu-item" href="${GLOBAL.basePath}/sysuser"  id="3_1">--%>
                        <%--<i class="icon-double-angle-right"></i>--%>
                        <%--用户管理--%>
                    <%--</a>--%>
                <%--</li>--%>
                <%--<li>--%>
                    <%--<a class="menu-item" href="${GLOBAL.basePath}/dict"  id="3_2">--%>
                        <%--<i class="icon-double-angle-right"></i>--%>
                        <%--字典管理--%>
                    <%--</a>--%>
                <%--</li>--%>
                <%--<li>--%>
                    <%--<a class="menu-item" href="${GLOBAL.basePath}/sysrole"  id="3_3">--%>
                        <%--<i class="icon-double-angle-right"></i>--%>
                        <%--角色管理--%>
                    <%--</a>--%>
                <%--</li>--%>
            <%--</ul>--%>
        <%--</li>--%>
        <%--<li  id="4_1">--%>
                <%--<a href="#" class="dropdown-toggle">--%>
                    <%--<i class="icon-file-text"></i>--%>
                    <%--<span class="menu-text">展览管理 </span>--%>
                    <%--<b class="arrow icon-angle-down"></b>--%>
                <%--</a>--%>
                <%--<ul class="submenu">--%>
                    <%--<li>--%>
                        <%--<a class="menu-item" href="${GLOBAL.basePath}/exhibition"  id="8_2">--%>
                            <%--<i class="icon-double-angle-right"></i>--%>
                            <%--展览列表--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
        <%--<li  id="5_1">--%>
                <%--<a href="#" class="dropdown-toggle">--%>
                    <%--<i class="icon-book"></i>--%>
                    <%--<span class="menu-text">书籍管理 </span>--%>
                    <%--<b class="arrow icon-angle-down"></b>--%>
                <%--</a>--%>

                <%--<ul class="submenu">--%>
                    <%--<li>--%>
                        <%--<a class="menu-item" href="${GLOBAL.basePath}/file"  id="4_2">--%>
                            <%--<i class="icon-double-angle-right"></i>--%>
                            <%--书籍列表--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
        <%--<li  id="6_1">--%>
            <%--<a href="#" class="dropdown-toggle">--%>
                <%--<i class="icon-ticket"></i>--%>
                <%--<span class="menu-text">门票配置 </span>--%>
                <%--<b class="arrow icon-angle-down"></b>--%>
            <%--</a>--%>
            <%--<ul class="submenu">--%>
                <%--<li>--%>
                    <%--<a class="menu-item" href="${GLOBAL.basePath}/ticket"  id="5_2">--%>
                        <%--<i class="icon-double-angle-right"></i>--%>
                        <%--门票列表--%>
                    <%--</a>--%>
                <%--</li>--%>
            <%--</ul>--%>
        <%--</li>--%>
        <%--<li  id="8_1">--%>
            <%--<a href="#" class="dropdown-toggle">--%>
                <%--<i class="icon-file-text"></i>--%>
                <%--<span class="menu-text">订单管理 </span>--%>
                <%--<b class="arrow icon-angle-down"></b>--%>
            <%--</a>--%>
            <%--<ul class="submenu">--%>
                <%--<li>--%>
                    <%--<a class="menu-item" href="${GLOBAL.basePath}/order"  id="6_2">--%>
                        <%--<i class="icon-double-angle-right"></i>--%>
                        <%--订单列表--%>
                    <%--</a>--%>
                <%--</li>--%>
            <%--</ul>--%>
        <%--</li>--%>
        <%--<li  id="9_1">--%>
            <%--<a href="#" class="dropdown-toggle">--%>
                <%--<i class="icon-file-text"></i>--%>
                <%--<span class="menu-text">录入 </span>--%>
                <%--<b class="arrow icon-angle-down"></b>--%>
            <%--</a>--%>
            <%--<ul class="submenu">--%>
                <%--<li>--%>
                    <%--<a class="menu-item" href="${GLOBAL.basePath}/count/list"  id="7_2">--%>
                        <%--<i class="icon-double-angle-right"></i>--%>
                        <%--商家列表--%>
                    <%--</a>--%>
                <%--</li>--%>
            <%--</ul>--%>
        <%--</li>--%>
        <%--<li  id="0_1">--%>
            <%--<a href="#" class="dropdown-toggle">--%>
                <%--<i class="icon-file-text"></i>--%>
                <%--<span class="menu-text">录入数据管理 </span>--%>
                <%--<b class="arrow icon-angle-down"></b>--%>
            <%--</a>--%>
            <%--<ul class="submenu">--%>
                <%--<li>--%>
                    <%--<a class="menu-item" href="${GLOBAL.basePath}/count/enterlist"  id="0_2">--%>
                        <%--<i class="icon-double-angle-right"></i>--%>
                        <%--数据列表--%>
                    <%--</a>--%>
                <%--</li>--%>
            <%--</ul>--%>
        <%--</li>--%>
    </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>
