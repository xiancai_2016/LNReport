<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common/base_body_header.jsp"%>
<%@include file="common/navbar.jsp"%>
<%@include file="common/page_content_pre.jsp"%>

<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>错误</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main no-padding">
                        ${exception.msg}
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<%@include file="common/page_content_suf.jsp"%>
<%@include file="common/script.jsp"%>

 