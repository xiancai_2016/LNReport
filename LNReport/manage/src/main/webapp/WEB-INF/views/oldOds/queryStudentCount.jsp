<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>

<link rel="stylesheet" type="text/css" href="${GLOBAL.staticJsPath}/jquery-select2/3.4/select2.min.css"/>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form"
                  action="${GLOBAL.basePath}/oldOds/continuedList"
                  method="post">
                <input type="hidden" name="key" value="${key}">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">自然月</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">

                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <label class="col-sm-1 control-label">校区</label>

                        <div class="col-sm-5">
                            <select id="CampusIDID" name="CampusID" class="js-example-basic-single required"
                                    multiple="multiple" data-live-search="true" style="width:80%;">
                                <option value="" <c:if test="${empty CampusIDS}">selected</c:if>>全部</option>
                                <c:forEach items="${departs}" var="item">
                                    <c:choose>
                                        <c:when test="${!empty CampusIDS}">
                                            <option value="${item.cID}"
                                                    <c:forEach items="${CampusIDS}" var="item1">
                                                        <c:if test="${item.cID == item1}">selected</c:if>
                                                    </c:forEach>
                                            >${item.cNAME}
                                            </option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${item.cID}">${item.cNAME}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <%--<button type="button" class="btn btn-info btn-sm" onclick="emptyCampus()">清空</button>--%>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="table_REF" class="table" width="100%" style="font-size: 8px">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th></th>
                        <th>课程类型</th>
                        <th>可续班人数</th>
                        <th>已续班人数</th>
                        <th>续班率</th>
                        <th>满班率</th>
                    </tr>
                    </thead>
                    <c:if test="${!empty queryStudentCountByK}">
                        <c:forEach items="${queryStudentCountByK}" var="item">
                            <tr>
                                <td></td>
                                <td>${item.grade}</td>
                                <td>${item.kdCount}</td>
                                <td>${item.ssCount}</td>
                                <td>${item.agvXb}%</td>
                                <td>${item.tmbCount}%</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapK}">
                        <tr style="background: #438eb9; color: white">
                            <td>小计:</td>
                            <td></td>
                            <td>${mapK.kdCount}</td>
                            <td>${mapK.ssCount}</td>
                            <td>${mapK.agvXb}%</td>
                            <td>${mapK.tmbCount}%</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty queryStudentCountByC}">
                        <c:forEach items="${queryStudentCountByC}" var="item">
                            <tr>
                                <td></td>
                                <td>${item.grade}</td>
                                <td>${item.kdCount}</td>
                                <td>${item.ssCount}</td>
                                <td>${item.agvXb}%</td>
                                <td>${item.tmbCount}%</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapC}">
                        <tr style="background: #438eb9; color: white">
                            <td>小计:</td>
                            <td></td>
                            <td>${mapC.kdCount}</td>
                            <td>${mapC.ssCount}</td>
                            <td>${mapC.agvXb}%</td>
                            <td>${mapC.tmbCount}%</td>
                        </tr>
                    </c:if>

                    <c:if test="${!empty queryStudentCountByY}">
                        <c:forEach items="${queryStudentCountByY}" var="item">
                            <tr>
                                <td></td>
                                <td>${item.grade}</td>
                                <td>${item.kdCount}</td>
                                <td>${item.ssCount}</td>
                                <td>${item.agvXb}%</td>
                                <td>${item.tmbCount}%</td>
                            </tr>
                        </c:forEach>
                    </c:if>

                    <c:if test="${!empty mapY}">
                        <tr style="background: #438eb9; color: white">
                            <td>小计:</td>
                            <td></td>
                            <td>${mapY.kdCount}</td>
                            <td>${mapY.ssCount}</td>
                            <td>${mapY.agvXb}%</td>
                            <td>${mapY.tmbCount}%</td>
                        </tr>
                    </c:if>
                    <tr></tr>
                    <c:if test="${!empty mapT}">
                        <tr style="background: #438eb9; color: white">
                            <td>总计:</td>
                            <td></td>
                            <td>${mapT.tkdCount}</td>
                            <td>${mapT.tssCount}</td>
                            <td>${mapT.tagvXb}%</td>
                            <td>${mapT.ttmbCount}%</td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </div>
    </div>

    <div id="mask" class="img"></div>
</div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>
<script src="${GLOBAL.staticJsPath}/jquery-select2/3.4/select2.min.js"></script>

<script>
    $(window).on('load', function () {
        $(".js-example-basic-single").select2(); //
    });

    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }

    function emptyCampus() {
        $("#CampusID option").each(function (i) {
            if ($(this).text() == "micorosft") {
                $(this).attr("selected", "");
            }
        });
    }
</script>