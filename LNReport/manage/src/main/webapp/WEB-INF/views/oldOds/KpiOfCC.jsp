<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>

<div class="page-content">
    <div class="row">
        <form class="form-horizontal" id="revenue" role="form" action="${GLOBAL.basePath}/oldOds/saveUpdateByCC"
              method="post">
            <div class="col-xs-12">
                <input type="hidden" name="campusID" value="D4AA9269-0B2C-4992-8B1B-769B46080BFF">
                <input type="hidden" name="companyID" value="D852BC17-AD40-4F17-9F47-4A365DA2A38B">
                <input type="hidden" name="key" value="${key}">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">日期</label>

                        <div class="col-sm-2">
                            <select class="form-control" id = "AutoID" name="AutoID" onchange="queryKpiByMonth()">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                保存
                            </button>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-info btn-sm" type="button" onclick="totalValueSum()">
                                刷新
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="col-xs-12">
                <table id="table_revenue" class="table" width="100%">
                    <thead>
                    <tr style="background: #438eb9; color: white">
                        <th>加盟商</th>
                        <th>ReNew</th>
                        <th>Refel</th>
                        <th>MKT</th>
                        <th>Total</th>
                        <th>Ratio</th>
                    </tr>
                    </thead>
                    <c:if test="${!empty KPIByCC}">
                        <c:forEach items="${KPIByCC}" var="item" varStatus="status">
                            <tr>
                                <td style="text-align: right">${item.cName}</td>
                                <td><input onkeyup="value=(parseInt((value=value.replace(/\D/g,''))==''||parseInt((value=value.replace(/\D/g,''))==0)?'0':value,10))"
                                           onafterpaste="value=(parseInt((value=value.replace(/\D/g,''))==''||parseInt((value=value.replace(/\D/g,''))==0)?'0':value,10))"
                                           id="JMS_ReNew_${status.index}" name="reNewKpi" value="${item.ReNew}"></td>
                                <td><input onkeyup="value=(parseInt((value=value.replace(/\D/g,''))==''||parseInt((value=value.replace(/\D/g,''))==0)?'0':value,10))"
                                           onafterpaste="value=(parseInt((value=value.replace(/\D/g,''))==''||parseInt((value=value.replace(/\D/g,''))==0)?'0':value,10))"
                                           id="JMS_REF_${status.index}" name="refKpi" value="${item.REF}"></td>
                                <td><input onkeyup="value=(parseInt((value=value.replace(/\D/g,''))==''||parseInt((value=value.replace(/\D/g,''))==0)?'0':value,10))"
                                           onafterpaste="value=(parseInt((value=value.replace(/\D/g,''))==''||parseInt((value=value.replace(/\D/g,''))==0)?'0':value,10))"
                                          id="JMS_MKT_${status.index}" name="mktKpi" value="${item.MKT}"></td>
                                <td><span id="totalText_${status.index}"></span></td>
                                <td><span id="ratioText_${status.index}"></span></td>
                            </tr>
                            <input value="${item.cID}" type="hidden" name = "cID">
                            <input value="${item.cName}" type="hidden" name = "cName">
                        </c:forEach>
                    </c:if>
                    <tr style="background: #438eb9; color: white">
                        <th style="text-align: right;">total</th>
                        <th><span id="totalReNew"></span></th>
                        <th><span id="totalRef"></span></th>
                        <th><span id="totalMkt"></span></th>
                        <th><span id="totalValue"></span></th>
                        <th><span>100%</span></th>
                    </tr>
                </table>
            </div>
        </form>
    </div>
    <input id="listSizeId" type="hidden" value="${listSize}">
</div>
<div id="mask" class="img"></div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    $(function(){
        totalValueSum();
    });

    function showMask() {
        $("body").mLoading();
        $("#revenue").submit();
    };

    function totalValueSum() {
        var liseSize = $("#listSizeId").val();

        var totalReNew = 0;
        var totalRef = 0;
        var totalMkt = 0;
        var totalValue = 0;

        for (var i = 0; i < liseSize; i++) {
            var renew = $("#JMS_ReNew_"+i).val();
            var ref = $("#JMS_REF_"+i).val();
            var mkt = $("#JMS_MKT_"+i).val();

            var total = parseFloat(renew) + parseFloat(ref) + parseFloat(mkt);


            totalReNew = parseFloat(renew) + parseFloat(totalReNew);
            totalRef = parseFloat(ref) + parseFloat(totalRef);
            totalMkt = parseFloat(mkt) + parseFloat(totalMkt);

            totalValue = parseFloat(totalValue) + parseFloat(total);

            $("#totalText_"+i).html(total);
        }

        for (var i = 0; i < liseSize; i++) {
            var renew = $("#JMS_ReNew_"+i).val();
            var ref = $("#JMS_REF_"+i).val();
            var mkt = $("#JMS_MKT_"+i).val();

            var total = parseFloat(renew) + parseFloat(ref) + parseFloat(mkt);

            var ratio = 0;
            if(total != 0){
                ratio = total/totalValue*100
            }

            $("#ratioText_"+i).html(ratio.toFixed(2)+"%");
        }

        $("#totalReNew").html(totalReNew);
        $("#totalRef").html(totalRef);
        $("#totalMkt").html(totalMkt);

        $("#totalValue").html(totalValue);
    };

    function queryKpiByMonth(){
        window.location = "${GLOBAL.basePath}/jmsODS/kpiByCC?AutoID=" + $("#AutoID").val()+"&campusID="+$("#campusID").val();
    };
</script>