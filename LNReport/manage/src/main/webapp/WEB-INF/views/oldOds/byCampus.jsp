<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="WebCampus" role="form" action="${GLOBAL.basePath}/oldOds/byCampus"
                  method="post">
                <input type="hidden" name="key" value="${key}">

                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">查询日期</label>

                        <div class="col-sm-5">
                            <input type="text" required=required style="margin-top: 5px;" onmousedown="cleanInfo()"
                                   value="${BeginDate}"
                                   id="txtBeginDay" name="BeginDate"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,maxDate:'#F{$dp.$D(txtEndDay)}'});"/>
                            至
                            <input type="text" required=required onmousedown="cleanInfo()" value="${EndDate}"
                                   id="txtEndDay" name="EndDate"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd' ,minDate:'#F{$dp.$D(txtBeginDay)}'});"/>
                            <span style="color: red" id="dateInfo"></span>
                        </div>

                        <label class="col-sm-1 control-label">渠道</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="SaleMode" id="SaleMode">
                                <c:forEach items="${querySaleModeByTMK}" var="item">
                                    <option value="${item.cID}"
                                            <c:if test="${item.cID == SaleMode}">selected</c:if>>${item.cName}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                            &nbsp;
                            <button class="btn btn-info btn-sm" type="button" onclick="exportWebTMK()">
                                导出
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table id="table_tin" class="table table-bordered" width="100%">
                        <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>区域</th>
                            <th>校区</th>
                            <th>App</th>
                            <th>Show</th>
                            <th>Erollment</th>
                            <th>Ratio(Show/APP)</th>
                            <th>Ratio(En/Show)</th>
                        </tr>
                        </thead>
                        <c:forEach items="${byCampusList}" var="item">
                            <tr>
                                <td>${item.cTel}</td>
                                <td> ${item.cName}</td>
                                <td> ${item.appCount}</td>
                                <td> ${item.showCount}</td>
                                <td> ${item.enCount}</td>
                                <td>${item.ASRatio}%</td>
                                <td> ${item.ESRatio}%</td>
                            </tr>
                        </c:forEach>

                        <tr style="background: #438eb9; color: white">
                            <td></td>
                            <td>总计</td>
                            <td>${totalMap.app}</td>
                            <td>${totalMap.show}</td>
                            <td>${totalMap.en}</td>
                            <td>${totalMap.totalASRatio}%</td>
                            <td>${totalMap.totalESRatio}%</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form class="form-horizontal" id="exportApp" role="form" action="${GLOBAL.basePath}/oldOds/WebTMK/exportByCampus"
          method="post">
        <input type="hidden" name="exportStartTime" id="exportStartTime">
        <input type="hidden" name="exportEndTime" id="exportEndTime">
        <input type="hidden" name="exportSaleType" id="exportSaleType">
    </form>
</div>
<div id="mask" class="img"></div>

<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>
<script src="${GLOBAL.staticJsPath}/My97DatePicker/WdatePicker.js"></script>

<script>
    function showMask() {
        if ($("#txtBeginDay").val() == '') {
            $("#txtBeginDay").focus();
            $("#dateInfo").html("请选开始时间");
        } else if ($("#txtEndDay").val() == '') {
            $("#txtEndDay").focus();
            $("#dateInfo").html("请选结束时间");
        } else {
            $("body").mLoading();
            $("#WebCampus").submit();
        }
    }

    function exportWebTMK() {
        var SaleMode = $("#SaleMode").val();
        var txtBeginDay = $("#txtBeginDay").val();
        var txtEndDay = $("#txtEndDay").val();

        $("#exportStartTime").val(txtBeginDay);
        $("#exportEndTime").val(txtEndDay);
        $("#exportSaleType").val(SaleMode);

        $("#exportApp").submit();
    }
</script>