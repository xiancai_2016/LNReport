<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/oldOds/statistics/"
                  method="post">
                <input type="hidden" name="key" value="${key}" ID="keyID">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID" id="AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="returnList()">
                                返回列表
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <br>

            <div id="type" class="dataTables_length">
                <p>统计类型 :
                    <button class="btn btn-white btn-link <c:if test="${type == 'AchiLeads'}">active</c:if>" value="Leads" onclick="chooseType('leads')">Achi Leads</button>
                    <button class="btn btn-white btn-link <c:if test="${type == 'AchiShow'}">active</c:if>" value="Show" onclick="chooseType('show')">Achi Show</button>
                    <button class="btn btn-white btn-link <c:if test="${type == 'AchiEn'}">active</c:if>" value="En" onclick="chooseType('en')">Achi En</button>
                </p>
            </div>
            <div id="container" style="height: 400%"></div>
        </div>
    </div>
    <form action="${GLOBAL.basePath}/oldOds/refSituation" id = "refFrom" method="post">
        <input type="hidden" name = "AutoID" id = "oName">
        <input type="hidden" name = "key" id = "okey">
    </form>

    <form action="${GLOBAL.basePath}/oldOds/statistics" id = "staFrom"  method="post">
        <input type="hidden" name = "AutoID" id = "ooName">
        <input type="hidden" name = "key" id = "ookey">
        <input type="hidden" name = "type" id = "ootype">
    </form>
</div>
<div id="mask" class="img"></div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script type="text/javascript" src="${GLOBAL.staticJsPath}/echarts/echarts.min.js"></script>

<script>

    var cNames = ${cNames};
    var values = ${values};

    //
    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    };

    //
    function returnList() {
        $("body").mLoading();
        $("#oName").val($("#AutoID").val());
        $("#okey").val($("#keyID").val());
        $("#refFrom").submit();
    };

    //
    function chooseType(type){
        $("body").mLoading();
        $("#ooName").val($("#AutoID").val());
        $("#ookey").val($("#keyID").val())
        $("#ootype").val(type)
        $("#staFrom").submit();
    };

    var dom = document.getElementById("container");
    var myChart = echarts.init(dom);
    option = null;

    option = {
        //color: ['#3398DB'],
        tooltip: {
            trigger: 'axis',
            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                data: cNames,
                axisTick: {
                    alignWithLabel: false
                },
                axisLabel: {
                    interval: 0,
                    rotate: 40
                }
            }
        ],

        yAxis: [
            {
                type: 'value',
                axisLabel: {
                    show: true,
                    interval: 'auto',
                    formatter: '{value} %'
                },
                show: true
            }
        ],
        series: [
            {
                name: '完成率(%)',
                type: 'bar',
                barWidth: '60%',
                label: {
                    normal: {
                        show: true,
                        position: 'top',
                        //formatter: '{b}\n{c}%'
                        formatter: '{c}%'
                    }
                },
                data: values
            },
        ]
    };

    if (option && typeof option === "object") {
        myChart.setOption(option);
    }
</script>