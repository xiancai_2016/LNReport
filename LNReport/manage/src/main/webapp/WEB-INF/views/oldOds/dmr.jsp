<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/base_body_header.jsp" %>

<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" id="campusdsr" role="form" action="${GLOBAL.basePath}/oldOds/DMR"
                  method="post">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">财务月</label>

                        <input type="hidden" name="key" value="${key}">

                        <div class="col-sm-2">
                            <select class="form-control" name="AutoID" id="AutoID">
                                <c:forEach items="${FinanceMonths}" var="item">
                                    <option value="${item.AutoID}"
                                            <c:if test="${item.AutoID == AutoID}">selected</c:if>>${item.Text}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <label class="col-sm-1 control-label">渠道</label>

                        <div class="col-sm-2">
                            <select class="form-control" name="channelType" id="channelType">
                                <option value="1" <c:if test="${channelType == '1'}">selected</c:if>>TIN</option>
                                <option value="2" <c:if test="${channelType == '2'}">selected</c:if>>TOUT</option>
                                <option value="3" <c:if test="${channelType == '3'}">selected</c:if>>TIN+TOUT</option>
                                <option value="4" <c:if test="${channelType == '4'}">selected</c:if>>WALKIN</option>
                                <option value="5" <c:if test="${channelType == '5'}">selected</c:if>>MKT</option>
                                <option value="6" <c:if test="${channelType == '6'}">selected</c:if>>REF</option>
                                <option value="7" <c:if test="${channelType == '7'}">selected</c:if>>MKT+REF</option>
                                <option value="8" <c:if test="${channelType == '8'}">selected</c:if>>WALKIN+REF</option>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-sm" type="button" onclick="showMask()">
                                查询
                                <i class="icon-search align-top bigger-125 icon-on-right"></i>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="col-xs-12">
            <c:if test="${!empty SHList}">
                <c:forEach items="${SHList}" var="item">
                    <table id="table-${item.regionName}" class="table table-bordered" width="100%">
                        <h4>${item.regionName}
                            <c:if test="${item.regionName == 'SH1'}">Alex</c:if>
                            <c:if test="${item.regionName == 'SH2'}">Annie</c:if>
                            <c:if test="${item.regionName == 'SH3'}">Ken</c:if>
                            <c:if test="${item.regionName == 'SH4'}">Sarah</c:if>
                            <c:if test="${item.regionName == 'SH5'}">Wendy</c:if>
                        </h4>
                        <thead>
                            <tr style="background: #438eb9; color: white">
                                <th>Center</th>
                                <th>${appOrLeads}</th>
                                <th>Show</th>
                                <th>En</th>
                                <th>${ShowToAppOrLeads}</th>
                                <th>ShowToEn</th>
                                <th>TargetApp</th>
                                <th>AchiApp</th>
                                <th>TargetShow</th>
                                <th>AchiShow</th>
                                <th>TargetEn</th>
                                <th>AchiEn</th>
                            </tr>
                        </thead>
                        <c:if test="${!empty item.regions}">
                            <c:forEach items="${item.regions}" var="item1" varStatus="states">
                                <tr>
                                    <td>${item1.cName}</td>
                                    <td>${item1.appCount}</td>
                                    <td>${item1.scount}</td>
                                    <td>${item1.ecount}</td>
                                    <td>${item1.ShowToApp}%</td>
                                    <td>${item1.ShowToEn}</td>
                                    <td>${item1.regionApp}</td>
                                    <td>${item1.aApp}%</td>
                                    <td>${item1.regionShow}</td>
                                    <td>${item1.aShow}%</td>
                                    <td>${item1.regionAEn}</td>
                                    <td>${item1.aEn}%</td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        <tr style="background: #438eb9; color: white">
                            <td>Sub-Total:</td>
                            <td>${item.app}</td>
                            <td>${item.Show}</td>
                            <td>${item.En}</td>
                            <td>${item.showToApp}%</td>
                            <td>${item.showToEn}</td>
                            <td>${item.tApp}</td>
                            <td>${item.RAApp}%</td>
                            <td>${item.tShow}</td>
                            <td>${item.RAShow}%</td>
                            <td>${item.tEn}</td>
                            <td>${item.RAEn}%</td>
                        </tr>
                    </c:forEach>
                    <tr style="background: #438eb9; color: white">
                        <td>Total:</td>
                        <td>${map.app}</td>
                        <td>${map.Show}</td>
                        <td>${map.En}</td>
                        <td>${map.showToApp}%</td>
                        <td>${map.showToEn}</td>
                        <td>${map.tApp}</td>
                        <td>${map.aApp}%</td>
                        <td>${map.tShow}</td>
                        <td>${map.aShow}%</td>
                        <td>${map.tEn}</td>
                        <td>${map.aEn}%</td>
                    </tr>
                </table>
            </c:if>
            <c:if test="${empty SHList}">
                <table id="table-empty" class="table table-bordered" width="100%">
                    <thead>
                        <tr style="background: #438eb9; color: white">
                            <th>Center</th>
                            <th>App</th>
                            <th>Show</th>
                            <th>En</th>
                            <th>ShowToApp</th>
                            <th>ShowToEn</th>
                            <th>TargetApp</th>
                            <th>AchiApp</th>
                            <th>TargetShow</th>
                            <th>AchiShow</th>
                            <th>TargetEn</th>
                            <th>AchiEn</th>
                        </tr>
                    </thead>
                </table>
            </c:if>
        </div>
    </div>
</div>
<div id="mask" class="img"></div>
<%@include file="../common/page_content_suf.jsp" %>
<%@include file="../common/script.jsp" %>

<script>
    function showMask() {
        $("body").mLoading();
        $("#campusdsr").submit();
    }
</script>