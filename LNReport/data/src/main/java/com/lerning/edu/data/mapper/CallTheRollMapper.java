package com.lerning.edu.data.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/27
 */
public interface CallTheRollMapper {
    List queryStudentInfo(Map map);

    int addCallTheRoll(Map map);
}
