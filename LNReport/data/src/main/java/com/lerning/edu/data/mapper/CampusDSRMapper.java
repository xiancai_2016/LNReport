package com.lerning.edu.data.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/5/17
 */
public interface CampusDSRMapper {
    List queryDepartList();

    List queryTIN(Map map);

    List queryREF(Map map);

    List queryRENEW(Map map);

    List queryFinanceMonths();

    List queryNaturalMonths();

    Map queryFinanceMonth(int AutoID);

    Map queryNaturalMonth(int AutoID);

    List chargeAndRefund(Map map);

    Map totalChargeAndRefund(Map map);

    List queryClassCountByK(Map map);

    List queryStudentCount(Map map);

    List queryFinanceMonthOfDq(Map dqd);

    List queryNatureMonthOfDq(Map dqd);

    Map querySCKCount(Map map);

    int insertChargeAndRefund(List chargeAndRefund);

    int insertClassCount(List queryClassCount);

    int insertSKC(Map map);

    int insertStudentCount(List queryStudentCount);

    List queryStudentCountOfLock(Map map);

    List queryStudentCountOfLockAll(Map map);

    List chargeAndRefundOfLockAll(Map map);//报名退费(全部校区)

    List chargeAndRefundOfLock(Map map);//报名退退费(单个校区)

    Map totalChargeAndRefundLock(Map map);//报名退费合计

    List queryClassCountByKOfLockAll(Map map);//开班概况(全部校区)

    List queryClassCountByKOfLock(Map map);//开班概况(单个校区)

    List queryDepart(Map map);//

    List queryEmployee(Map map);//

    List queryUserOfDeparts(Map map);

    Map queryDepartByEmployee(Map map);

    List queryDepartListByWorkOrder(Map map);

    List queryDepartById(Map map);
}
