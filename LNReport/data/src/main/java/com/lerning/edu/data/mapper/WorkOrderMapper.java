package com.lerning.edu.data.mapper;

import com.lerning.edu.beans.WorkOrder;

import java.util.List;
import java.util.Map;

public interface WorkOrderMapper extends BaseMapper<WorkOrder>{
    Map queryByUserId(Map map);

    Map queryMaxOrderNo(Map map);

    Map queryDetail(Map map);

    Map queryDetail1(Map map);

    Map queryChangeSchoolOfClass(Map map);

    List findWorkOrder(Map map);

    int findWorkOrderCount(Map map);

    List findWorkOrderByAdd(Map map);

    int findWorkOrderCountByAdd(Map map);

    List queryEmployeeByCfield(String account);

    List queryEmployeeByUserID(String userID);

    List queryEmployeeByCfieldOfLN(String account);

    List queryEmployeeByUserIDOfLN(String userID);

    List findMyTaskList(Map map);

    int findMyTaskListCount(Map map);

    Map queryWorkOrderRecordById(String cID);

    List queryByRefundFee(Map map);

    List queryByShiftInfo(Map map);

    List queryByRefundFeeLock(Map map);

    List findWorkOrderOfFundFee(Map map);

    int findWorkOrderCountOfFundFee(Map map);

    List queryOdsUser(Map map);
}