package com.lerning.edu.data.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/3
 */
public interface UntrustedMapper {
    List queryCampusAndCount(Map map);
    List queryDetail(Map map);
}
