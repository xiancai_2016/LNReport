package com.lerning.edu.data.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/5
 */
public interface RevenueMapper {
    List queryRevenueByWeek(Map map);
    int insertRevenues(List revenues);
    List queryRevenuesData(Map dateMap);
    Map queryRevenueSum(Map dateMap);
}
