package com.lerning.edu.data.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/9/6
 */
public interface FullAttendanceMapper {
    List fullAttendance();

    Map fullAttendanceTotal();
}
