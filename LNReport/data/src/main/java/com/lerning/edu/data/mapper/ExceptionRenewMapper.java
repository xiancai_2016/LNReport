package com.lerning.edu.data.mapper;

import org.apache.poi.ss.formula.functions.T;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/8/13
 */
public interface ExceptionRenewMapper extends BaseMapper<T>{
    List queryExceptionRenew(Map map);

    int queryExceptionRenewCount(Map map);
}
