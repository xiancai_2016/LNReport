/*
 * For com.royal.art
 * Copyright [2015/11/26] By FCHEN
 */
package com.lerning.edu.data.mapper;

import com.lerning.edu.beans.SysDict;

import java.util.List;

/**
 * SysDictMapper
 * 字典
 * @author FCHEN
 * @date 2015/11/26
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    List<SysDict> getItemAndNum(SysDict sysDict);


    List<SysDict> queryDay(String pKey);
}
