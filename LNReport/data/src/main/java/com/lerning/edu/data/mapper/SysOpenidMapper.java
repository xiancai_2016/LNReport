package com.lerning.edu.data.mapper;

import com.lerning.edu.beans.SysOpenid;

public interface SysOpenidMapper extends BaseMapper<SysOpenid>{
    SysOpenid queryByOpenid(String openid);
}