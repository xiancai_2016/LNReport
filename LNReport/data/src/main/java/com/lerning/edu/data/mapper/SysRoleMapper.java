/*
 * SysRoleMapper.java
 * Copyright(C) 2015-2016 �Ϻ��ʼ�����Ƽ����޹�˾
 * All rights reserved.
 * -----------------------------------------------
 * 2016-12-09 Created
 */
package com.lerning.edu.data.mapper;

import com.lerning.edu.beans.SysRole;

public interface SysRoleMapper extends BaseMapper<SysRole> {

    SysRole getRoleByName(SysRole sysRole);

    /**
     * 维护角色与菜单权限关系
     * @param sysRole
     * @return
     */
    int deleteRoleResource(SysRole sysRole);

    int insertRoleResource(SysRole sysRole);

    int deleteRoleReport(SysRole sysRole);

    int insertRoleReport(SysRole sysRole);
}