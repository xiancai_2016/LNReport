package com.lerning.edu.data.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/10/25
 */
public interface JMSODSKPIMapper {
    List queryKPIByCampus(Map map);

    void update(Map map);

    void addReNew(Map map);

    void addRef(Map map);

    void addMkt(Map map);

    List queryKPIByCC(Map dateMap);

    void updateByCC(Map map);

    void addReNewByCC(Map map);

    void addRefByCC(Map map);

    void addMktByCC(Map map);
}
