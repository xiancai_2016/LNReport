/*
 * For com.royal.art
 * Copyright [2015/11/14] By FCHEN
 */
package com.lerning.edu.data.mapper;

import com.lerning.edu.beans.SysUserSession;

import java.util.Map;

/**
 * @author FCHEN
 */
public interface SysUserSessionMapper extends BaseMapper<SysUserSession>{

    int add(SysUserSession sysUserSession);

    int update(Map params);

    SysUserSession queryByTokenKey(Map params) ;

    SysUserSession queryByTokenContent(Map params) ;

}
