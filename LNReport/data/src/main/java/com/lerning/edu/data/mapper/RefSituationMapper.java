package com.lerning.edu.data.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/7/9
 */
public interface RefSituationMapper {
    List queryRef(Map map);

    Map queryEnByCampus(Map map);
}
