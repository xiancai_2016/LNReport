/*
 * SysResourceMapper.java
 * Copyright(C) 2015-2016 �Ϻ��ʼ�����Ƽ����޹�˾
 * All rights reserved.
 * -----------------------------------------------
 * 2016-12-09 Created
 */
package com.lerning.edu.data.mapper;

import com.lerning.edu.beans.SysResource;

import java.util.List;

public interface SysResourceMapper extends BaseMapper<SysResource> {

    List<SysResource> findByUserId(SysResource sysResource);

    List<SysResource> findByParentIdsLike(SysResource sysResource);

    int updateParentIds(SysResource sysResource);

    int updateSort(SysResource sysResource);
}