package com.lerning.edu.data.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/6/21
 */
public interface ChannelMapper {
    Map queryAll(Map map);
    Map queryAllByChannel2017(Map map);
    Map queryByOdsReNew18(Map map);

    int insertDsrByCampus(List channels);

    int insertDsrByCampusSum(Map totalMap);

    int insertDsr(Map map);

    List queryChannelsLockData(Map dateMap);//查询dsr锁定数据
    List queryChannelsLockDataSum(Map dateMap);//查询dsr锁定数据汇总
}
