/*
 * For com.royal.art
 * Copyright [2015/11/14] By FCHEN
 */
package com.lerning.edu.data.mapper;

import java.util.List;

/**
 * BaseMapper
 * 基础mapper 封装基本的增删查改
 * @author FCHEN
 * @date 2015/11/14
 */
public interface BaseMapper<T> {

    /**
     * 新增
     * @param bean
     * @return
     */
    int add(T bean);

    /**
     * 修改
     * @param bean
     * @return
     */
    int update(T bean);

    /**
     * 删除
     * @param bean
     * @return
     */
    int delete(T bean);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    T queryById(long id);

    /**
     * 查询列表
     * @param bean
     * @return
     */
    List<T> queryList(T bean);

    /**
     * 是否已存在
     * @param bean
     * @return
     */
    int exists(T bean);

    /**
     * 查询所有数据列表
     * @param bean
     * @return
     */
    List<T> findAllList(T bean);

    /**
     * 查询所有数据列表
     * @see public List<T> findAllList(T entity)
     * @return
     */
    @Deprecated
    public List<T> findAllList();

    /**
     * 获取最大的排序值
     * @return
     */
    int getMaxSeq();

}
