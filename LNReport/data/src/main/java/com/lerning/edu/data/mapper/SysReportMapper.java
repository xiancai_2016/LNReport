package com.lerning.edu.data.mapper;

import com.lerning.edu.beans.SysReport;

import java.util.List;
import java.util.Map;

public interface SysReportMapper extends BaseMapper<SysReport> {
    SysReport queryReportById(Integer id);

    List<SysReport> findByUserId(SysReport sysReport);

    List<SysReport> queryListByReport(SysReport sysReport);

    List<Map> queryReportAndOpenId(Map map);

    int addPushRecord(Map map);
}