package com.lerning.edu.data.mapper;

import com.lerning.edu.beans.WorkOrderAuditRecord;

public interface WorkOrderAuditRecordMapper extends BaseMapper<WorkOrderAuditRecord>{
}