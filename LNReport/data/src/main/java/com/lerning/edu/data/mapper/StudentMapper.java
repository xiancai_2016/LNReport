package com.lerning.edu.data.mapper;

import com.lerning.edu.beans.Student;

import java.util.List;
import java.util.Map;

public interface StudentMapper extends BaseMapper<Student>{
    List findStudent(Map map);

    int findStudentCount(Map map);

    Map findStudentOne(Map map);

    List findClass(Map map);

    List queryResumeList(Map map);

    List findClassByStop(Map map);

    List findClassByCampus(Map map);

    List findClassByResume(Map map);

    Student queryByUserId(String userID);

    int findClassByCampusCount(Map map);

    List queryReceipt(Map map);

    Map queryElectronicBalance(Map map);

    Map queryClassByCost(Map map);

    List queryEmployees(Map map);
}