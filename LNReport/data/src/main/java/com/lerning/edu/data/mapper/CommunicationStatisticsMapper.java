package com.lerning.edu.data.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author jiangwei
 * @since 18/10/22
 */
public interface CommunicationStatisticsMapper {
    List queryComStatis(Map map);
    List queryComStatisInfo(Map map);
}
