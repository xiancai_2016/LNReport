package com.lerning.edu.commons.excel;

import com.lerning.edu.commons.excel.template.model.TTitleColumn;
import com.lerning.edu.commons.excel.template.model.TCell;

import java.util.*;

/**
 * Created by FCHEN on 2016/1/16.
 */
public class ExcelUtil {

    public static TTitleColumn getSalesExcelColumn(LinkedHashMap<String,String> cellMap){
        List<TCell> cells = new ArrayList<TCell>();
        if( cellMap != null && !cellMap.isEmpty() ) {
            Iterator<String> it = cellMap.keySet().iterator();
            int i = 0;
            while ( it.hasNext() ) {
                String key = it.next();
                String value = cellMap.get(key);
                cells.add(new TCell(key,value,i));
                i++;
            }
            return new TTitleColumn(cells.toArray(new TCell[]{}));
        }
        return null;
    }
}
