
package com.lerning.edu.commons.excel.exception;

/**
 * @ClassName: IllegalParamsException
 * @Description: 不符合要求的参数

 */
public class IllegalParamsException extends ExcelAbstractException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 746242568205019583L;

	/**
	 * 
	 */
	public IllegalParamsException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public IllegalParamsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public IllegalParamsException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}


}
