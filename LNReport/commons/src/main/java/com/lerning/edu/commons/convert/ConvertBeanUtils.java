
package com.lerning.edu.commons.convert;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * ConvertUtils
 *
 * @author JW
 * @date
 */
public final class ConvertBeanUtils {

    public static <T> T  convertJsonToBean(JSON json, Class<T> clasz){
        return JSONObject.toJavaObject(json,clasz);
    }


}
