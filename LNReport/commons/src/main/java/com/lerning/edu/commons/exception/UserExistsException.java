
package com.lerning.edu.commons.exception;

/**
 * @author
 */
public class UserExistsException extends AbstractException {

    public UserExistsException(int code, String msg) {
        super(code, msg);
    }

    public UserExistsException(APIGlobalErrorMsg errorMsg,Throwable t){
        super(errorMsg.getCode(),String.format(errorMsg.getMsg(),t.getMessage()) );
    }

    public UserExistsException(APIGlobalErrorMsg errorMsg){
        super(errorMsg.getCode(),String.format(errorMsg.getMsg(),"") );
    }

}
