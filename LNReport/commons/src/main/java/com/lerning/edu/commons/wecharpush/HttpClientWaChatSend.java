package com.lerning.edu.commons.wecharpush;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClientWaChatSend {

    private static final Logger log = LoggerFactory.getLogger(HttpClientWaChatSend.class);

//    public static void main(String[] args) throws IOException {
//        Gson gson = new Gson();
//        // 传入的参数
//        Map map = new HashMap();
//        map.put("touser", "oqQEj1qf0strkuAMYurR2_C0fzYo");
//        map.put("template_id", Constants.template_id);
//        map.put("url", "http://www.baidu.com");
//
//        Map param = new HashMap();
//        param.put("first", new TemplateData("", "#000000"));
//        param.put("keyword1", new TemplateData("666", "#000000"));
//        param.put("keyword2", new TemplateData(Constants.DATE_FORMART, "#000000"));
//        param.put("keyword3", new TemplateData("999", "#000000"));
//        param.put("remark", new TemplateData("祝你周末愉快！", "#000000"));
//        map.put("data", param);
//
//        JSON jbB = JSON.parseObject(sendPostRequest(Constants.SEND_URL + WxUtils.getAccessToken(Constants.GET_TOKEN_URL), gson.toJson(map)));
//        System.out.println(jbB);
//    }

    public static synchronized String sendPostRequest(String url, String param) {
        HttpURLConnection httpURLConnection = null;
        OutputStream out = null; // 写
        InputStream in = null; // 读
        int responseCode = 0; // 远程主机响应的HTTP状态码
        String result = "";
        try {
            URL sendUrl = new URL(url);
            httpURLConnection = (HttpURLConnection) sendUrl.openConnection();
            // post方式请求
            httpURLConnection.setRequestMethod("POST");
            // 设置头部信息
            httpURLConnection.setRequestProperty("headerdata", "ceshiyongde");
            // 一定要设置 Content-Type 要不然服务端接收不到参数
            httpURLConnection.setRequestProperty("Content-Type",
                    "application/Json; charset=UTF-8");
            // 指示应用程序要将数据写入URL连接,其值默认为false（是否传参）
            httpURLConnection.setDoOutput(true);
            // httpURLConnection.setDoInput(true);

            httpURLConnection.setUseCaches(false);
            httpURLConnection.setConnectTimeout(30000); // 30秒连接超时
            httpURLConnection.setReadTimeout(30000); // 30秒读取超时
            // 传入参数
            out = httpURLConnection.getOutputStream();
            out.write(param.getBytes("UTF-8"));
            out.flush(); // 清空缓冲区,发送数据
            out.close();
            responseCode = httpURLConnection.getResponseCode();
            // 获取请求的资源
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    httpURLConnection.getInputStream(), "UTF-8"));
            result = br.readLine();
        } catch (Exception e) {
            e.printStackTrace();
            log.info("推送出错");
        }
        return result;
    }
}