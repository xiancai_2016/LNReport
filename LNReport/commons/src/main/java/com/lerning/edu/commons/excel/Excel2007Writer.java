
package com.lerning.edu.commons.excel;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.lerning.edu.commons.excel.template.model.TCell;
import com.lerning.edu.commons.excel.template.model.TTitleColumn;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @ClassName: Excel2007Writer
 * @Description: 2007版excel写入器
 * @author WW
 * @date
 * 
 * Company royal
 */
public class Excel2007Writer extends ExcelAbstractWriter {
	
	/**
	 * 不使用默认构造器
	 * 实例化方法 @see {@link ExcelFactory#getExcelWriter(ExcelVersion)}
	 */
	protected Excel2007Writer(){
		setWorkbook(new XSSFWorkbook());
	}

	/* 
	 * <p>Title: write</p>
	 * <p>Description: excel2007 写入实现</p>
	 * @param dataList
	 * @param tTitleColumn
	 * @param os
	 * @throws IOException
	 * @see com.football.commons.excel.ExcelWriter#write(java.util.List, com.football.commons.excel.template.model.TTitleColumn, java.io.OutputStream)
	 */
	@Override
	public void write(List<Map<String, Object>> dataList,TTitleColumn tTitleColumn) throws IOException {
		XSSFWorkbook xssfWorkbook = (XSSFWorkbook)getWorkbook();
		TCell[] cells = tTitleColumn.getCells();
		XSSFSheet sheet = xssfWorkbook.createSheet();
		setSheetData(sheet, dataList, cells);
		writeClose();
	}

}
