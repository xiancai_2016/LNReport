package com.lerning.edu.commons.bean;

/**
 * Created by FCHEN on 2016/8/7.
 */
public class TcBean {
    private String commercial;
    private String technical;

    public String getCommercial() {
        return commercial;
    }

    public void setCommercial(String commercial) {
        this.commercial = commercial;
    }

    public String getTechnical() {
        return technical;
    }

    public void setTechnical(String technical) {
        this.technical = technical;
    }
}
