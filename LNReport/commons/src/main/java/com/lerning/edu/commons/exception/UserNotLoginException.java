
package com.lerning.edu.commons.exception;

/**
 * @author
 */
public class UserNotLoginException extends AbstractException {

    public UserNotLoginException(int code, String msg) {
        super(code, msg);
    }

    public UserNotLoginException(APIGlobalErrorMsg errorMsg,Throwable t){
        super(errorMsg.getCode(),String.format(errorMsg.getMsg(),t.getMessage()) );
    }

    public UserNotLoginException(APIGlobalErrorMsg errorMsg){
        super(errorMsg.getCode(),String.format(errorMsg.getMsg(),"") );
    }
}
