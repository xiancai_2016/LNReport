
package com.lerning.edu.commons.excel.exception;

/**
 * @ClassName: ValidateCellException
 * @Description: excel数据验证异常
 */
public class ValidateCellException extends ExcelAbstractException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5700664847664390604L;
	
	public ValidateCellException(String msg){
		super(msg);
	}
	
	public ValidateCellException(){
		super("excel 数据验证失败 ");
	}
	
}
