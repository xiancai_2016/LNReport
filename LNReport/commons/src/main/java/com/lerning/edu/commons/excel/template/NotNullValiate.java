
package com.lerning.edu.commons.excel.template;

/**
 * @ClassName: NotNullValiate
 * @Description: null 验证
 */
public class NotNullValiate implements Validate {

	/* 
	 * <p>Title: validate</p>
	 * <p>Description: </p>
	 * @param obj
	 * @return
	 * @see com.football.commons.excel.template.Validate#validate(java.lang.Object)
	 */
	@Override
	public boolean validate(Object obj) {
		return obj != null;
	}

}
