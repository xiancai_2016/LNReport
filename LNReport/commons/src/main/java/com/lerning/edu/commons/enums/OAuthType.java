
package com.lerning.edu.commons.enums;

import org.apache.commons.lang.StringUtils;

/**
 * OAuthType
 *
 * @author JW
 * @date
 */
public enum OAuthType {

    /**
     * 手机
     */
    PHONE("phone",1),
    /**
     * 微信
     */
    WEIXIN("weixin",2),
    /**
     * 新浪微博
     */
    SINAWEIBO("sinaweibo",3),

    ;

    private int code;
    private String value;
    OAuthType(String value,int code){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public int getCode() {
        return code;
    }

    /**
     * 根据value找对应的用户认证类型
     * 默认返回手机认证类型
     * @param value
     * @return
     */
    public static OAuthType get(String value){
        if(StringUtils.isBlank(value)){
            return OAuthType.PHONE;
        }
        OAuthType[] types = OAuthType.values();
        for (OAuthType type: types  ) {
            if(type.value.equals(value)){
                return type;
            }
        }
        return OAuthType.PHONE;
    }
}
