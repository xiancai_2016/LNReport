
package com.lerning.edu.commons.excel.template;

/**
 * @ClassName: DataType
 * @Description: excel表格数据类型
 */
@SuppressWarnings("rawtypes")
public enum DataType {
	
	String(java.lang.String.class),
	
	Integer(java.lang.Integer.class),
	
	Long(java.lang.Long.class),
	
	Float(java.lang.Float.class),
	
	Double(java.lang.Double.class),
	
	Date(java.util.Date.class),
	
	;
	
	private Class clasz;
	DataType(Class clasz){
		this.clasz = clasz;
	}
	/**
	 * @return the clasz
	 */
	public Class getClasz() {
		return clasz;
	}


}
