package com.lerning.edu.commons.util;

/**
 * @author FCHEN
 */
public class ResultCode {

    public final static Integer OK = 0;

    public final static Integer ERROR = 1;

    public final static Integer SUCCESS = OK;

    public final static Integer FAILURE = ERROR;

    public final static Integer SMS_NOT_EXISTS = 10;

    public final static Integer SMS_OVERDUE = 11;

    public final static Integer MOBILE_PHONE_EXISTS = 20;

    public final static Integer MOBIL_PHONE_NOT_EXISTS = 21;

    public final static Integer USER_EXISTS = 30;

    public final static Integer USER_NOT_EXISTS = 31;

    public final static Integer USER_NOT_LOGIN = 40;
    
}
