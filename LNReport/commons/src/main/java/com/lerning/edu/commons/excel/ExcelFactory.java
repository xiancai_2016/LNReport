
package com.lerning.edu.commons.excel;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import com.lerning.edu.commons.excel.template.model.TCell;
import com.lerning.edu.commons.excel.template.model.TTitleColumn;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;


/**
 * @ClassName: ExcelWriterFactory
 * @Description: excel 写入 工厂类

 * 
 */
public class ExcelFactory {
	
	private final static Logger LOGGER= Logger.getLogger(ExcelFactory.class);
	
	private ExcelFactory(){}
	
	/**
	 * 
	 * @Title: getExcelWriter
	 * @Description: 获取excelWriter
	 * @param version
	 * @return
	 */
	public static ExcelWriter getExcelWriter(ExcelVersion version,OutputStream os){
		try {
			ExcelAbstractWriter writer = version.getWriterClass().newInstance();
			Workbook workbook = version.getWorkbook().newInstance();
			writer.setWorkbook(workbook);
			writer.setOs(os);
			return writer;
		} catch (Exception e) {
			LOGGER.error(e);
			throw new RuntimeException(e);
		} 
	}
	
	/**
	 * 
	 * @Title: getExcelReader
	 * @Description: 获取excelReader
	 * @param version
	 * @param is
	 * @return
	 */
	public static ExcelReader getExcelReader(ExcelVersion version,InputStream is){
		try {
			ExcelAbstractReader reader = version.getReaderClass().newInstance();
			reader.loadInputStream(is);
			return reader;
		} catch (Exception e) {
			LOGGER.error(e);
			throw new RuntimeException(e);
		} 
	}
	
	
	public static void main(String[] args) throws Exception {
		TCell[] cells = new TCell[4];
		cells[0] = new TCell("k1", 0);
		cells[1] = new TCell("null", 1);
		cells[2] = new TCell("k2", 2);
		cells[3] = new TCell("dddd", 3);
		TTitleColumn title = new TTitleColumn(cells);
		/*List<Map<String,Object>> datalist = new ArrayList<Map<String,Object>>();
		for(int i=0;i<10;i++){
			Map<String,Object> m = new HashMap<String, Object>();
			m.put("k1", "m1"+i);
			m.put("k2", "m2"+i);
			datalist.add(m);
		}
		FileOutputStream fos = new FileOutputStream("d:/test.xlsx");
		ExcelFactory.getExcelWriter(ExcelVersion.EXCEL2007,fos).write(datalist, title);*/
		
		FileInputStream fis = new FileInputStream("d:/建议采购单 (测试环境).xlsx");
		List<Map<String,Object>> datalist = ExcelFactory.getExcelReader(ExcelVersion.EXCEL2007, fis).reader(tTitleColumn);
		/*for(int i=0;i<datalist.size();i++){
			Map<String,Object> m = datalist.get(i);
			for(TCell cell : cells){
				System.out.print("["+cell.getCellKey()+"-"+m.get(cell.getCellKey())+"]");
			}
			System.out.println("");
		}*/
		
		/*TCell[] cells = new TCell[4];
		cells[0] = new TCell("c1", "主模板编号");
		cells[1] = new TCell("c2", "属性");
		cells[2] = new TCell("c3", "值");
		cells[3] = new TCell("c4", "顺序");
		
		
		List<Map<String,Object>> datalist = new ArrayList<Map<String,Object>>();
		int count = 1;
		// TODO Auto-generated method stub
		for (int i = 0; i < 64683; i++) {			 
            // 第四步，创建单元格，并设置值  
			for (int j = 0; j < 4; j++) {
				Map<String,Object> m = new HashMap<String, Object>();
				m.put("c1", i+1);
				if(j==0){
					m.put("c2", "first");  
					m.put("c3", "您有一张10元立减券已绑定至账户，有效期2015.7.3-7.9，请尽快使用哦！");  
					m.put("c4", "1"); 
				}else if(j==1){
					 
					m.put("c2", "orderTicketStore");  
					m.put("c3", "仅限小时速达下单使用");  
					m.put("c4", "2"); 					
				}else if(j==2){
					m.put("c2", "orderTicketRule");  
					m.put("c3", "购物满37元即可使用");  
					m.put("c4", "3"); 						
				}else {
					m.put("c2", "remark");  
					m.put("c3", "");  
					m.put("c4", "4"); 	
				}
				datalist.add(m);
			}
		}
		

		TTitleColumn title = new TTitleColumn(cells);
		FileOutputStream fos = new FileOutputStream("d:/test2.xlsx");
		ExcelFactory.getExcelWriter(ExcelVersion.EXCEL2007,fos).write(datalist, title);*/
		
		
		
	}
	private static TTitleColumn tTitleColumn;
	static{
		TCell[] cells = new TCell[30];
		cells[0] = new TCell("stores_id","门店编号",0);
		cells[1] = new TCell("stores_name","门店名称",1);
		cells[2] = new TCell("depot_goods_id","商品库存编号",2);
		cells[3] = new TCell("goods_name","商品名称",3);
		cells[4] = new TCell("category1","大分类",4);
		cells[5] = new TCell("category2","中分类",5);
		cells[6] = new TCell("category3","小分类",6);
		cells[7] = new TCell("units","单位编号",7);
		cells[8] = new TCell("units_name","售卖单位",8);
		cells[9] = new TCell("pre_days","商品到货周期(T+N)",9);
		cells[10] = new TCell("supplier_id","采购商编号",10);
		cells[11] = new TCell("supplier_all_name","采购商名称",11);
		cells[12] = new TCell("empty_stock","零库存",12);
		cells[13] = new TCell("tax_rate","税率%",13);
		cells[14] = new TCell("tax_purchase_price","到店成本(含税)",14);
		cells[15] = new TCell("cu_price","系统零售价(含税)",15);
		cells[16] = new TCell("gross_margin","毛利率%",16);
		cells[17] = new TCell("d4","前第四天销量",17);
		cells[18] = new TCell("d3","前第三天销量",18);
		cells[19] = new TCell("d2","前第二天销量",19);
		cells[20] = new TCell("d1","昨日销量",20);
		cells[21] = new TCell("avg_p","前四天平均销量",21);
		cells[22] = new TCell("avg_damaged_num","前四天平均报损货量",22);
		cells[23] = new TCell("stock_valid_num","动态库存数量",23);
		cells[24] = new TCell("stock_price","动态库存金额",24);
		cells[25] = new TCell("avg_d","系统建议补货量",25);
		cells[26] = new TCell("min_num","最小起订量",26);
		cells[27] = new TCell("audit_num","采购审核订货量",27);
		cells[28] = new TCell("final_num","最终订单订货量",28);
		cells[29] = new TCell("final_price","最终订单订货金额",29);
		tTitleColumn = new TTitleColumn(cells);
	}
	

}
