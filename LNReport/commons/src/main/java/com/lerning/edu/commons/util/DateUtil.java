package com.lerning.edu.commons.util;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 日期工具类
 * Created by FCHEN on 2016/1/16.
 */
public class DateUtil extends org.apache.commons.lang3.time.DateUtils {

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    /**
     * 日期转字符串
     *
     * @param date    日期
     * @param pattern 格式
     * @return
     */
    public static String dateToString(Date date, String pattern) {
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.format(date);
        }
        return "";
    }

    /**
     * 日期转字符串
     *
     * @param date
     * @return
     */
    public static String dateToString(Date date) {
        return dateToString(date, Constants.DATE_FORMART2);
    }

    /**
     * 一个公共的处理方法，将字符串的日期转换成日期对象 日期格式由参数format指定
     */
    public static Date formateStringToDate(String stringDate, String format) {
        if (stringDate == null || stringDate.trim().equals(""))
            return null;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(stringDate);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * 一个公共的处理方法，将字符串的日期转换成日期对象
     *
     * @param stringDate
     * @return
     */
    public static Date formateStringToDate(String stringDate) {
        return formateStringToDate(stringDate, Constants.DATE_FORMART);
    }

    /**
     * 获取当前系统日期
     *
     * @return Timestamp
     */
    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 当前系统日期
     *
     * @return String
     */
    public static String getSysDateForString(String format) {
        String strDate = null;
        SimpleDateFormat a = new SimpleDateFormat(format);
        try {
            strDate = a.format(new Date()).toString();
            // System.out.println("The now date is :"+strDate);
        } catch (Exception ex) {
            ex.getMessage();
        }
        return strDate;
    }

    /**
     * 把yyymmdd日期格式转换成指定的日期格式(例如1994-10-21)
     *
     * @return String:指定的日期格式)
     */
    public static String chgStrDateToFormate(String strDate, String formate) {
        String rtnDate = null;
        try {
            if (strDate != null && strDate.length() == 8) {
                rtnDate = strDate.subSequence(0, 4) + formate
                        + strDate.substring(4, 6) + formate
                        + strDate.substring(6);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        return rtnDate;
    }

    /**
     * 把hhmmss时间格式转换成指定的时间格式(例如：10:10:10)
     *
     * @return String)
     */
    public static String chgStrTimeToFormate(String strTime, String formate) {
        String rtnTime = null;
        try {
            if (strTime != null && strTime.length() == 6) {
                rtnTime = strTime.subSequence(0, 2) + formate
                        + strTime.substring(2, 4) + formate
                        + strTime.substring(4);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        return rtnTime;
    }

    /**
     * 把yyy-mm-dd日期格式(或者其他的格式)转换成yyyymmdd
     *
     * @return String
     */
    public static String chgStrDateNoFormate(String strDate) {
        String rtnDate = null;
        try {
            if (strDate != null && strDate.length() == 10) {
                rtnDate = strDate.subSequence(0, 4) + strDate.substring(5, 7)
                        + strDate.substring(8);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        return rtnDate;
    }

    /**
     * 把hh:mm:ss(或者其他时间格式)时间格式转换成hhmmss
     *
     * @return String
     */
    public static String chgStrTimeNoFormate(String strTime) {
        String rtnTime = null;
        try {
            if (strTime != null && strTime.length() == 8) {
                rtnTime = strTime.subSequence(0, 2) + strTime.substring(3, 5)
                        + strTime.substring(6);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        return rtnTime;
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd）
     */
    public static String getDate() {
        return getDate("yyyy-MM-dd");
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String getDate(String pattern) {
        return DateFormatUtils.format(new Date(), pattern);
    }

    /**
     * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String formatDate(Date date, Object... pattern) {
        String formatDate = null;
        if (pattern != null && pattern.length > 0) {
            formatDate = DateFormatUtils.format(date, pattern[0].toString());
        } else {
            formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
        }
        return formatDate;
    }

    /**
     * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String formatDateTime(Date date) {
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     */
    public static String getTime() {
        return formatDate(new Date(), "HH:mm:ss");
    }

    /**
     * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String getDateTime() {
        return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前年份字符串 格式（yyyy）
     */
    public static String getYear() {
        return formatDate(new Date(), "yyyy");
    }

    /**
     * 得到当前月份字符串 格式（MM）
     */
    public static String getMonth() {
        return formatDate(new Date(), "MM");
    }

    /**
     * 得到当天字符串 格式（dd）
     */
    public static String getDay() {
        return formatDate(new Date(), "dd");
    }

    /**
     * 得到当前星期字符串 格式（E）星期几
     */
    public static String getWeek() {
        return formatDate(new Date(), "E");
    }

    /**
     * 日期型字符串转化为日期 格式
     * { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm",
     * "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm",
     * "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm" }
     */
    public static Date parseDate(Object str) {
        if (str == null) {
            return null;
        }
        try {
            return parseDate(str.toString(), parsePatterns);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 获取过去的天数
     *
     * @param date
     * @return
     */
    public static long pastDays(Date date) {
        long t = new Date().getTime() - date.getTime();
        return t / (24 * 60 * 60 * 1000);
    }

    /**
     * 获取过去的小时
     *
     * @param date
     * @return
     */
    public static long pastHour(Date date) {
        long t = new Date().getTime() - date.getTime();
        return t / (60 * 60 * 1000);
    }

    /**
     * 获取过去的分钟
     *
     * @param date
     * @return
     */
    public static long pastMinutes(Date date) {
        long t = new Date().getTime() - date.getTime();
        return t / (60 * 1000);
    }

    /**
     * 转换为时间（天,时:分:秒.毫秒）
     *
     * @param timeMillis
     * @return
     */
    public static String formatDateTime(long timeMillis) {
        long day = timeMillis / (24 * 60 * 60 * 1000);
        long hour = (timeMillis / (60 * 60 * 1000) - day * 24);
        long min = ((timeMillis / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (timeMillis / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        long sss = (timeMillis - day * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000 - min * 60 * 1000 - s * 1000);
        return (day > 0 ? day + "," : "") + hour + ":" + min + ":" + s + "." + sss;
    }

    /**
     * 获取两个日期之间的天数
     *
     * @param before
     * @param after
     * @return
     */
    public static double getDistanceOfTwoDate(Date before, Date after) {
        long beforeTime = before.getTime();
        long afterTime = after.getTime();
        return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
    }

    /**
     * 获取两个日期之间的分钟数据
     *
     * @param before
     * @param after
     * @return
     */
    public static double getDistanceOfTwoMinute(Date before, Date after) {
        long beforeTime = before.getTime();
        long afterTime = after.getTime();
        return (afterTime - beforeTime) / (1000 * 60);
    }


    /**
     * 返回当前时间前一天日期 格式:yyyy-MM-dd
     *
     * @return
     */
    public static String getLastDateStr1() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, -1);
        Date last = now.getTime();
        String lastDay = new SimpleDateFormat(Constants.DATE_FORMART).format(last);
        return lastDay;
    }

    /**
     * 返回当前时间前N天日期 格式:yyyy-MM-dd
     *
     * @return
     */
    public static String getLastDateStrN(int n) {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, -n);
        Date last = now.getTime();
        String lastDay = new SimpleDateFormat(Constants.DATE_FORMART).format(last);
        return lastDay;
    }

    /**
     * 返回上月当天的前一天日期 格式:yyyy-MM-dd
     *
     * @return
     */
    public static String getLastMonthDateStr1() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MONTH, -1);
        now.add(Calendar.DAY_OF_MONTH, -1);
        Date last = now.getTime();
        String lastDay = new SimpleDateFormat(Constants.DATE_FORMART).format(last);
        return lastDay;
    }

    /**
     * 返回上月当天的日期 格式:yyyy-MM-dd
     *
     * @return
     */
    public static String getLastMonthDateStr2() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MONTH, -1);
        Date last = now.getTime();
        String lastDay = new SimpleDateFormat(Constants.DATE_FORMART).format(last);
        return lastDay;
    }

    /**
     * 返回当前时间前一天日期  格式:yyyyMMdd
     *
     * @return
     */
    public static String getLastDateStr2() {
        String lastDay = getLastDateStr1();
        String lastDayFormat = new SimpleDateFormat(Constants.DATE_FORMART3).format(lastDay);
        return lastDayFormat;
    }

    /**
     * 返回 当前时间 减去 minute 的时间 时间格式 yyyy-MM-dd HH:mm:ss
     *
     * @param minute
     * @return
     */
    public static Date getBeforeDate(int minute) {
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MINUTE, -minute);//30分钟前的时间
        String dateStr = new SimpleDateFormat(Constants.DATE_FORMART2).format(nowTime.getTime());
        return formateStringToDate(dateStr, Constants.DATE_FORMART2);
    }

    /***
     * 获取去n天的日期
     *
     * @param f
     * @param n
     * @param date
     * @return
     */
    public static String getBeforeDateToString(String f, int n, Date date) {
        SimpleDateFormat format = new SimpleDateFormat(f);
        Calendar c = Calendar.getInstance();

        c.setTime(date);
        c.add(Calendar.DATE, -n);
        Date d = c.getTime();
        return format.format(d);

    }

    /***
     * 获取当前年月
     *
     * @return
     */
    public static String getYearAndMonth() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String m;
        if (month < 10) {
            m = "0" + month;
        } else {
            m = month + "";
        }
        String yearAndMonth = year + "-" + m;
        return yearAndMonth;
    }

    /***
     * 判断两个时间大小,DATE1晚,则返回true
     *
     * @param DATE1
     * @param DATE2
     * @return
     */
    public static boolean compare_date(String DATE1, String DATE2) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        try {
            Date dt1 = df.parse(DATE1);
            Date dt2 = df.parse(DATE2);
            if (dt1.getTime() > dt2.getTime()) {
                return true;
            } else if (dt1.getTime() < dt2.getTime()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return true;
    }

    /***
     * 获取strDate前n天的时间
     *
     * @param strDate
     * @param n
     * @return
     * @throws Exception
     */
    public static String getNowBefoerDate(String strDate, int n) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dt = null;
        try {
            dt = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(dt);
        rightNow.add(Calendar.DAY_OF_YEAR, n);//日期加n天
        Date dt1 = rightNow.getTime();
        return sdf.format(dt1);
    }

    /***
     * 返回当前年月
     */
    public static Map dayMap() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String m;
        Map dqd = new HashMap();
        if (month < 10) {
            m = "0" + month;
        } else {
            m = month + "";
        }
        dqd.put("dqDay", year + "-" + m);

        return dqd;
    }

    /***
     * 日期转换为星期
     *
     * @param datetime
     * @return
     */
    public static String dateToWeek(String datetime) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance(); // 获得一个日历
        Date datet = null;
        try {
            datet = f.parse(datetime);
            cal.setTime(datet);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1; // 指示一个星期中的某天。
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    //月份加1
    public static String addMonth(String strDate){
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtil.formateStringToDate(strDate));//设置起时间
        cal.add(Calendar.MONTH, 1);//增加一个月

        return sf.format(cal.getTime());
    }

    /***
     * 时间加上N小时
     * @param strDate
     * @param n
     * @return
     */
    public static String addHour(String strDate, int n){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date=null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar ca=Calendar.getInstance();
        ca.setTime(date);
        ca.add(Calendar.HOUR_OF_DAY, n);

        return sdf.format(ca.getTime());
    }

    /***
     * 获取一个周的区间
     * @param date
     * @return
     */
    public static Map getTimeInterval(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        // 获得当前日期是一个星期的第几天
        int day = cal.get(Calendar.DAY_OF_WEEK);
        // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
        String imptimeBegin = sdf.format(cal.getTime());
        cal.add(Calendar.DATE, 6);
        String imptimeEnd = sdf.format(cal.getTime());

        Map map = new HashMap();
        map.put("start", imptimeBegin);
        map.put("end", imptimeEnd);

        return map;
    }
}
