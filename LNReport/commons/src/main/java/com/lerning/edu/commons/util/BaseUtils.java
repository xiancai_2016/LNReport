package com.lerning.edu.commons.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.ProtocolException;

/**
 * @author FCHEN
 */
public class BaseUtils {

    private static final Logger logger =
            LoggerFactory.getLogger(BaseUtils.class);

    public static String firstLetterToUpper(String str){
        char[] array = str.toCharArray();
        array[0] -= 32;
        return String.valueOf(array);
    }

    public static void writeOutputStream(InputStream inputStream , OutputStream outputStream) {

        try {

            byte[] data = new byte[102400];   // 100k

            int n = 0;

            while( ( n = inputStream.read( data ) ) > 0 ) {
                outputStream.write( data, 0, n );
            }

            outputStream.flush();

        } catch (MalformedURLException e) {
            logger.error( "exception occurred", e );
        } catch (ProtocolException e) {
            logger.error( "exception occurred", e );
        } catch (IOException e) {
            logger.error( "exception occurred", e );
        } finally {
            try {
                outputStream.close();
                inputStream.close();
            } catch (IOException e) {
                logger.error( "exception occurred", e );
            }
        }

    }

}
