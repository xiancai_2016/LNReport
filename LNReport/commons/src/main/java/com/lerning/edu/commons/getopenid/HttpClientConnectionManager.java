package com.lerning.edu.commons.getopenid;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpClientConnectionManager {
	
	/**
	 * 获取SSL验证的HttpClient
	 * @param httpClient
	 * @return
	 */
	public static HttpClient getSSLInstance(HttpClient httpClient){
		ClientConnectionManager ccm = httpClient.getConnectionManager();
		SchemeRegistry sr = ccm.getSchemeRegistry();
		sr.register(new Scheme("https", MySSLSocketFactory.getInstance(), 443));
		httpClient =  new DefaultHttpClient(ccm, httpClient.getParams());
		return httpClient;
	}
	
	/**
	 * 模拟浏览器post提交
	 * 
	 * @param url
	 * @return
	 */
	public static HttpPost getPostMethod(String url) {
		HttpPost pmethod = new HttpPost(url); // 设置响应头信息
		pmethod.addHeader("Connection", "keep-alive");
		pmethod.addHeader("Accept", "*/*");
		pmethod.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		pmethod.addHeader("Host", "mp.weixin.qq.com");
		pmethod.addHeader("X-Requested-With", "XMLHttpRequest");
		pmethod.addHeader("Cache-Control", "max-age=0");
		pmethod.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
//		pmethod.a
		return pmethod;
	}
	
	/**
	 * 获取微信二维码的post提交
	 */
	public static HttpPost getPostQRMethod(String url){
		HttpPost pmethod = new HttpPost(url); // 设置响应头信息
		pmethod.addHeader("Accept-Ranges", "bytes-alive");
		pmethod.addHeader("Cache-control", "max-age=604800");
		pmethod.addHeader("Connection", "28026");
		pmethod.addHeader("Content-Length", "28026");
		pmethod.addHeader("Content-Type", "image/jpg");
//		pmethod.addHeader("Date", "Wed, 16 Oct 2013 06:37:10 GMT");
//		pmethod.addHeader("Expires", "Wed, 23 Oct 2013 14:37:10 +0800");
		pmethod.addHeader("Server", "nginx/1.4.1");
		return pmethod;
	}
	
	/**
	 *  获取微信二维码的get提交
	 * @param url
	 * @return
	 */
	public static HttpGet getQRGetMethod(String url) {
		HttpGet pmethod = new HttpGet(url);
		// 设置响应头信息
		pmethod.addHeader("Connection", "keep-alive");
		pmethod.addHeader("Cache-Control", "max-age=0");
		pmethod.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
		pmethod.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/;q=0.8");
		return pmethod;
	}

	/**
	 * 模拟浏览器GET提交
	 * @param url
	 * @return
	 */
	public static HttpGet getGetMethod(String url) {
		HttpGet pmethod = new HttpGet(url);
		// 设置响应头信息
		pmethod.addHeader("Connection", "keep-alive");
		pmethod.addHeader("Cache-Control", "max-age=0");
		pmethod.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
		pmethod.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/;q=0.8");
		return pmethod;
	}
	
	/**
	 * 模拟微信get提交
	 */
	public static HttpGet getWxMethod(String url) {
		String html = null;
		HttpClient httpClient = new DefaultHttpClient();// 创建httpClient对象
		HttpGet httpget = new HttpGet(url);// 以get方式请求该URL
		httpget.setHeader("User-Agent","Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; GT-I9300 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 MicroMessenger/5.2.380");
//		try {
//			HttpResponse responce = httpClient.execute(httpget);// 得到responce对象
//			int resStatu = responce.getStatusLine().getStatusCode();// 返回码
//			if (resStatu == HttpStatus.SC_OK) {// 200正常 其他就不对
//				// 获得相应实体
//				HttpEntity entity = responce.getEntity();
//				if (entity != null) {
//					html = new String(EntityUtils.toString(entity));// 获得html源代码
//				}
//			}
//		} catch (Exception e) {
//			System.out.println("访问【" + url + "】出现异常!");
//			e.printStackTrace();
//		} finally {
//			httpClient.getConnectionManager().shutdown();
//		}
		return httpget;
	}
	
	
}
