
package com.lerning.edu.commons.excel.template.model;

import com.lerning.edu.commons.excel.template.DataType;
import com.lerning.edu.commons.excel.template.Validate;

/**
 * @ClassName: TCell
 * @Description: cell model
 */
public class TCell {
	
	/**
	 * 读写cell数据的key
	 */
	private String cellKey;
	/**
	 * cell所在row的title
	 */
	private String cellTitle;
	/**
	 * cell的位置索引
	 */
	private Integer cellIndex;
	/**
	 * cell数据类型
	 */
	private DataType dataType;
	
	
	/**
	 * cell数据验证
	 */
	private Validate validate;
	
	public TCell(String cellKey,String cellTitle){
		this(cellKey,cellTitle,DataType.String);
	}
	
	public TCell(String cellKey,String cellTitle,DataType dataType){
		this(cellKey,cellTitle,0,dataType,null);
	}
	
	public TCell(String cellKey,Integer cellIndex){
		this(cellKey,cellIndex,DataType.String);
	}
	
	public TCell(String cellKey,int cellIndex,DataType dataType){
		this(cellKey,"",cellIndex,dataType,null);
	}
	
	public TCell(String cellKey,String cellTitle,int cellIndex){
		this(cellKey,cellTitle,cellIndex,DataType.String,null);
	}
	
	public TCell(String cellKey,String cellTitle,int cellIndex,DataType dataType){
		this(cellKey,cellTitle,cellIndex,dataType,null);
	}
	
	public TCell(String cellKey,String cellTitle,int cellIndex,Validate validate){
		this(cellKey,cellTitle,cellIndex,DataType.String,validate);
	}
	
	public TCell(String cellKey,String cellTitle,int cellIndex,DataType dataType,Validate validate){
		this.cellKey = cellKey;
		this.cellTitle = cellTitle;
		this.cellIndex = cellIndex;
		this.dataType = dataType;
		this.validate = validate;
	}

	
	/**
	 * @return the cellKey
	 */
	public String getCellKey() {
		return cellKey;
	}
	/**
	 * @param cellKey the cellKey to set
	 */
	public void setCellKey(String cellKey) {
		this.cellKey = cellKey;
	}
	/**
	 * @return the cellTitle
	 */
	public String getCellTitle() {
		return cellTitle;
	}
	/**
	 * @param cellTitle the cellTitle to set
	 */
	public void setCellTitle(String cellTitle) {
		this.cellTitle = cellTitle;
	}
	/**
	 * @return the dataType
	 */
	public DataType getDataType() {
		return dataType;
	}
	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the cellIndex
	 */
	public Integer getCellIndex() {
		return cellIndex;
	}

	/**
	 * @param cellIndex the cellIndex to set
	 */
	public void setCellIndex(Integer cellIndex) {
		this.cellIndex = cellIndex;
	}

	/**
	 * @return the validate
	 */
	public Validate getValidate() {
		return validate;
	}

	/**
	 * @param validate the validate to set
	 */
	public void setValidate(Validate validate) {
		this.validate = validate;
	}
	

}
