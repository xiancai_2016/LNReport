
package com.lerning.edu.commons.exception;

/**
 * @author
 */
public class UserNotExistsException extends AbstractException {

    public UserNotExistsException(int code, String msg) {
        super(code, msg);
    }

    public UserNotExistsException(APIGlobalErrorMsg errorMsg,Throwable t){
        super(errorMsg.getCode(),String.format(errorMsg.getMsg(),t.getMessage()) );
    }

    public UserNotExistsException(APIGlobalErrorMsg errorMsg){
        super(errorMsg.getCode(),String.format(errorMsg.getMsg(),"") );
    }
}
