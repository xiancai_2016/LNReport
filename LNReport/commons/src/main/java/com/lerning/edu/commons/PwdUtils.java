
package com.lerning.edu.commons;

import com.google.common.base.Charsets;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * PwdUtils
 * 密码工具类
 * @author JW
 * @date 2015/11/15
 */
public final class PwdUtils {

    private PwdUtils(){}


    public final static String getSalt(){
        return SequnceUtil.getUUID();
    }

    /**
     *
     * @param pwd   密码
     * @param salt  盐值
     * @return
     */
    public final static String saltPwd(String pwd,String salt){
        return  DigestUtils.md5Hex((pwd+salt).getBytes(Charsets.UTF_8)).toUpperCase();
    }

    /**
     *
     * @param pwd   密码
     * @param salt  盐值
     * @param key   秘钥
     * @return
     */
    public final static String saltPwd(String pwd,String salt,String key){
        return  DigestUtils.md5Hex((pwd + key + salt).getBytes(Charsets.UTF_8)).toUpperCase();
    }


    public static void main(String[] args){
        System.out.println(saltPwd("123456","E6C65535BF394833BE87238F979250F9"));
    }





}
