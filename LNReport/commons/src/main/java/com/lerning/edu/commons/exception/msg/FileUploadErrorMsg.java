
package com.lerning.edu.commons.exception.msg;

/**
 * FileUploadErrorMsg
 * 文件上传类异常
 */
public enum  FileUploadErrorMsg  {

    EMPTY_FILE(1300,"上传文件为空"),

    UNACCEPTED_FILE(1301,"不合格的文件类型"),




    ;


    private int code;
    private String msg;
    FileUploadErrorMsg(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


}
