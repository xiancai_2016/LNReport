/*
 * Copyright (C), 2014-2016
 * FileName: BaseViewObject.java
 * Author:   FCHEN
 * Date:     2014年11月14日 下午8:09:08
 * Description:  
 * History: 
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间                     版本号                  描述
 * FCHEN       2013.11.14  1.0.0
 */
package com.lerning.edu.commons.view;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Timestamp;



/**
 * 视图类基类
 * 
 * @author FCHEN
 * @version 1.0
 *
 */
public class BaseViewObject<V extends BaseViewObject<V>> 
        implements Serializable {

    private static final long serialVersionUID = 8950660914544659827L;

    private Class<V> viewObjectClass;

	private Long id;                                    // 记录流水号
	private Integer version = 0;                        // 乐观锁版本号
	private Long operator = 0L;                         // 记录操作人
	private Timestamp createdDatetime;                  // 记录创建日期
	private Timestamp updatedDatetime;                  // 记录修改日期
	private Boolean actived = Boolean.TRUE;             // 记录是否启用、激活或有效（默认为true）
	private Boolean deleted = Boolean.FALSE;            // 记录是否已被删除（默认为false）
	private String descr ;                              // 描述

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Boolean getActived() {
		return actived;
	}

	public void setActived(Boolean actived) {
		this.actived = actived;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Class<V> getViewObjectClass() {
		return viewObjectClass;
	}

	public void setViewObjectClass(Class<V> viewObjectClass) {
		this.viewObjectClass = viewObjectClass;
	}


	@SuppressWarnings("unchecked")
    public BaseViewObject() {
        Type type = getClass().getGenericSuperclass();
        if( type != null && type instanceof ParameterizedType ) {
            viewObjectClass = ( Class<V> )( ( ParameterizedType )type )
                    .getActualTypeArguments()[0];
        }else {
            throw new IllegalArgumentException( 
                    "Type argument <V> is empty!!" );
        }
    }
    

    
    @SuppressWarnings("unchecked")
    @Override
    final public boolean equals( Object object ) {
        boolean b = false;
        if( object == null ) {
            b = false;
        }else if( object == this ) {
            b = true;
        }else if( object.getClass() == this.viewObjectClass ) {
            b = equals( ( V )object );
        }
        return b;
    }
    
    public boolean equals( V object ) {
        return super.equals( object );
    }
    
    
}
