
package com.lerning.edu.commons.exception;

/**
 * @author
 */
public class ParamNotNullException extends AbstractException {

    public ParamNotNullException(int code, String msg) {
        super(code, msg);
    }

    public ParamNotNullException(APIGlobalErrorMsg errorMsg,Throwable t){
        super(errorMsg.getCode(),String.format(errorMsg.getMsg(),t.getMessage()) );
    }

    public ParamNotNullException(APIGlobalErrorMsg errorMsg){
        super(errorMsg.getCode(),String.format(errorMsg.getMsg(),"") );
    }

}
