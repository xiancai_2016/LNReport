
package com.lerning.edu.commons.excel.template.model;

import java.util.List;

import com.lerning.edu.commons.excel.exception.IllegalParamsException;

/**
 * @ClassName: TTitleColumn
 * @Description: excel title
 */
public class TTitleColumn {
	/**
	 * title cell
	 */
	private TCell[] cells;
	/**
	 * titles所在行数
	 */
	private int titleRowCount = 1;
	
	public TTitleColumn(TCell[] cells){
		if(null == cells || cells.length < 1)
			throw new IllegalParamsException(" this field[cells] can't be null or empty");
		this.cells = cells;
	}
	
	public TTitleColumn(List<TCell> cellsList){
		if(null == cellsList || cellsList.isEmpty())
			throw new IllegalParamsException(" this field[cells] can't be null or empty");
		this.cells = cellsList.toArray(new TCell[cellsList.size()]);
	}


	/**
	 * @return the cells
	 */
	public TCell[] getCells() {
		return cells;
	}

	/**
	 * @param cells the cells to set
	 */
	public void setCells(TCell[] cells) {
		this.cells = cells;
	}

	/**
	 * @return the titleRowCount
	 */
	public int getTitleRowCount() {
		return titleRowCount;
	}

	/**
	 * @param titleRowCount the titleRowCount to set
	 */
	public void setTitleRowCount(int titleRowCount) {
		this.titleRowCount = titleRowCount;
	}
	
	
	

}
