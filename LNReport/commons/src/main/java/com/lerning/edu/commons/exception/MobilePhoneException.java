
package com.lerning.edu.commons.exception;

import com.lerning.edu.commons.exception.msg.MobilePhoneErrorMsg;

/**
 * MobilePhoneException
 * 手机号异常
 */
public class MobilePhoneException extends AbstractException {

    private static final long serialVersionUID = 2651880335166377513L;

    public MobilePhoneException(int code, String msg) {
        super(code, msg);
    }

    public MobilePhoneException(MobilePhoneErrorMsg msg) {
        super(msg.getCode(), msg.getMsg());
    }

}
