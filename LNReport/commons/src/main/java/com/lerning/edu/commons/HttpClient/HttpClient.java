package com.lerning.edu.commons.HttpClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClient {

    private static final Logger log = LoggerFactory.getLogger(HttpClient.class);

    public static synchronized String postRequest(String url, String param) {
        HttpURLConnection httpURLConnection = null;
        OutputStream out = null; // 写
        InputStream in = null; // 读
        int responseCode = 0; // 远程主机响应的HTTP状态码
        String result = "";
        try {
            URL sendUrl = new URL(url);
            httpURLConnection = (HttpURLConnection) sendUrl.openConnection();
            // post方式请求
            httpURLConnection.setRequestMethod("POST");
            // 设置头部信息
            httpURLConnection.setRequestProperty("headerdata", "ceshiyongde");
            // 一定要设置 Content-Type 要不然服务端接收不到参数
            httpURLConnection.setRequestProperty("Content-Type",
                    "application/Json; charset=UTF-8");
            // 指示应用程序要将数据写入URL连接,其值默认为false（是否传参）
            httpURLConnection.setDoOutput(true);
            // httpURLConnection.setDoInput(true);

            httpURLConnection.setUseCaches(false);
            httpURLConnection.setConnectTimeout(30000); // 30秒连接超时
            httpURLConnection.setReadTimeout(30000); // 30秒读取超时
            // 传入参数
            out = httpURLConnection.getOutputStream();
            out.write(param.getBytes("UTF-8"));
            out.flush(); // 清空缓冲区,发送数据
            out.close();
            responseCode = httpURLConnection.getResponseCode();
            // 获取请求的资源
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    httpURLConnection.getInputStream(), "UTF-8"));
            result = br.readLine();
        } catch (Exception e) {
            e.printStackTrace();
            log.info("推送出错");
        }
        return result;
    }
}