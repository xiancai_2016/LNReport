
package com.lerning.edu.commons.excel.template;


/**
 * @ClassName: Validate {
 * @Description: excel 验证接口
 */
public interface Validate {
	
	/**
	 * 
	 * @Title: validate
	 * @Description: 值验证
	 * @param obj
	 * @return
	 */
	boolean validate(Object obj);
	

}
