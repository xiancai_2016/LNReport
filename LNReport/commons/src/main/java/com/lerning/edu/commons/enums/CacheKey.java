package com.lerning.edu.commons.enums;

/**
 * Created by FCHEN on 2016/1/30.
 */
public enum CacheKey {
    /**
     * 首页类目KEY
     */
    SHIPCATEGORY_MEM_KEY("SHIPCATEGORY_MEM_KEY_"),

    /**
     * 首页类目KEY2
     */
    SHIPCATEGORY_MEM_KEY2("SHIPCATEGORY_MEM_KEY2_"),
    /**
     * 国家城市
     */
    SHIPLOCATION_MEM_KEY("SHIPLOCATION_MEM_KEY_");

    private String code;
    CacheKey(String _code){
        this.code = _code;
    }

    @Override
    public String toString() {
        return this.code;
    }
}
