
package com.lerning.edu.commons.validate;

import com.lerning.edu.commons.exception.MobilePhoneException;
import com.lerning.edu.commons.exception.msg.MobilePhoneErrorMsg;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * MobileValidator
 *  手机号码校验
 */
public final class MobileValidator {

    private final static List<String> numHeader = new ArrayList<String>();


    /**
     * 验证手机号码  返回true or false
     * @param mobilephone
     * @return
     */
    public final static boolean validate(String mobilephone){
        return isMobile(mobilephone,false);
    }

    /**
     * 验证手机号码 错误抛出异常
     * @param mobilephone
     */
    public final static void validateAndThrowEx(String mobilephone){
        isMobile(mobilephone,true);
    }


    final static boolean isMobile(String mobilephone,boolean throwex){
        boolean right = true;
        if(StringUtils.isBlank(mobilephone) ){
            if(throwex)
                throw new MobilePhoneException(MobilePhoneErrorMsg.EMPTY_PHONE);
            right = false;
        } else if(StringUtils.length(mobilephone) != 11
                || !StringUtils.isNumeric(mobilephone) ){
            if(throwex)
                throw new MobilePhoneException(MobilePhoneErrorMsg.WRONG_PHONENUM);
            right = false;
        } else if(!numHeader.contains(mobilephone.substring(0,3))){
            if(throwex)
                throw new MobilePhoneException(MobilePhoneErrorMsg.WRONG_PHONENUM);
            right = false;
        }
        return right;
    }

    static {
        numHeader.add("130");
        numHeader.add("131");
        numHeader.add("132");
        numHeader.add("133");
        numHeader.add("134");//0-8
        numHeader.add("135");
        numHeader.add("136");
        numHeader.add("137");
        numHeader.add("138");
        numHeader.add("139");
        numHeader.add("145");
        numHeader.add("147");
        numHeader.add("150");
        numHeader.add("151");
        numHeader.add("152");
        numHeader.add("153");
        numHeader.add("157");
        numHeader.add("158");
        numHeader.add("159");
        numHeader.add("170");//(0,5,9)
        numHeader.add("176");
        numHeader.add("177");
        numHeader.add("178");
        numHeader.add("180");
        numHeader.add("181");
        numHeader.add("182");
        numHeader.add("183");
        numHeader.add("184");
        numHeader.add("185");
        numHeader.add("186");
        numHeader.add("187");
        numHeader.add("188");
        numHeader.add("189");
    }




}
