
package com.lerning.edu.commons.exception;

/**
 * MobilePhoneException
 * 手机号异常
 */
public class NickNameException extends AbstractException {

    private static final long serialVersionUID = 2651880335166377513L;

    public NickNameException(int code, String msg) {
        super(code, msg);
    }

    public NickNameException(APIGlobalErrorMsg msg) {
        super(msg.getCode(), msg.getMsg());
    }

}
