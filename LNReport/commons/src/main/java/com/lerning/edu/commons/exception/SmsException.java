
package com.lerning.edu.commons.exception;

import com.lerning.edu.commons.exception.msg.SmsErrorMsg;

/**
 * SmsException
 *
 */
public class SmsException extends AbstractException {


    private static final long serialVersionUID = -3999463655370951183L;

    public SmsException(int code, String msg) {
        super(code, msg);
    }

    public SmsException(SmsErrorMsg errorMsg) {
        super(errorMsg.getCode(), errorMsg.getMsg());
    }

}
