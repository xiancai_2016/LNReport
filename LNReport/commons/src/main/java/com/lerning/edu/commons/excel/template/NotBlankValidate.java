
package com.lerning.edu.commons.excel.template;

/**
 * @ClassName: NotBlankValidate
 * @Description: TODO(这里用一句话描述这个类的作用)
 */
public class NotBlankValidate implements Validate{

	/* 
	 * <p>Title: validate</p>
	 * <p>Description: </p>
	 * @param obj
	 * @return
	 * @see com.football.commons.excel.template.Validate#validate(java.lang.Object)
	 */
	@Override
	public boolean validate(Object obj) {
		if(obj == null)
			return false;
		String __obj = obj.toString();
		return __obj.length() > 0;
	}

}
