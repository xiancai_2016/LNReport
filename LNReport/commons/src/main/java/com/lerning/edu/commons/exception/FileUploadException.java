
package com.lerning.edu.commons.exception;

import com.lerning.edu.commons.exception.msg.FileUploadErrorMsg;

/**
 * FileUploadException
 * 文件上传类异常
 */
public class FileUploadException extends AbstractException {


    private static final long serialVersionUID = -4669929993084064316L;

    public FileUploadException(int code, String msg) {
        super(code, msg);
    }

    public FileUploadException(FileUploadErrorMsg errorMsg) {
        super(errorMsg.getCode(), errorMsg.getMsg());
    }
}
