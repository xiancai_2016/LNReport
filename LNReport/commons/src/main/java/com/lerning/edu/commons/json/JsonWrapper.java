
package com.lerning.edu.commons.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.ArrayList;
import java.util.List;

/**
 * JsonWrapper
 * JSON 封装
 */
public final class JsonWrapper {

    static List<SerializerFeature> serializerFeatureList = new ArrayList<SerializerFeature>();

    static {
        //保证和spring 使用的构造器一致
        serializerFeatureList.add(SerializerFeature.WriteMapNullValue);
        serializerFeatureList.add(SerializerFeature.WriteNullStringAsEmpty);
        serializerFeatureList.add(SerializerFeature.WriteDateUseDateFormat);
    }

    public final static String toJSONString(Object obj){
        return JSONObject.toJSONString(obj,serializerFeatureList.toArray(new SerializerFeature[]{}));
    }

    public static final  Object parseObject(String text, Class clazz) {
        Object obj = JSON.parseObject(text, clazz);
        return obj;
    }

//    public static void main(String[] args){
//        TcBean tcBean = (TcBean) parseObject("{\"commercial\":\"http://test.shipparts.com/temp/c.doc\",\"technical\":\"http://test.shipparts.com/temp/t.doc\"}", TcBean.class);
//        System.out.println(tcBean.getCommercial());
//        System.out.println(tcBean.getTechnical());
//    }

}
