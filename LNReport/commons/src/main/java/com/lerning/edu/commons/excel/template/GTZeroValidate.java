
package com.lerning.edu.commons.excel.template;

import java.math.BigDecimal;

/**
 * @ClassName: GTZeroValidate
 * @Description: 大于0
 */
public class GTZeroValidate implements Validate{

	/* 
	 * <p>Title: validate</p>
	 * <p>Description: </p>
	 * @param obj
	 * @return
	 * @see com.yummy77.excel.template.Validate#validate(java.lang.Object)
	 */
	@Override
	public boolean validate(Object obj) {
		if(obj == null)
			return false;
		BigDecimal __obj = new BigDecimal(String.valueOf(obj));
		return __obj.compareTo(BigDecimal.ZERO) >= 0;
	}

}
