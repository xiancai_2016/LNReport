package com.lerning.edu.commons.util;

import com.aliyun.openservices.oss.model.OSSObject;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

/**
 * @author FCHEN
 */
public interface OSSClientHelper {

    public String postObject(File file,
                             Constants.OssUploadAccessFolder ossUploadAccessFolder) ;

    public String postObject(File file,
                             String randomStringFileName,
                             Constants.OssUploadAccessFolder ossUploadAccessFolder) ;

    public String postObject(MultipartFile file,
                             Constants.OssUploadAccessFolder ossUploadAccessFolder) ;

    public String postObject(MultipartFile file,
                             String randomStringFileName,
                             Constants.OssUploadAccessFolder ossUploadAccessFolder) ;

    public String postObject(InputStream inputStream,
                             Long fileSize,
                             String randomStringFileName,
                             Constants.OssUploadAccessFolder ossUploadAccessFolder,
                             Constants.OssUploadFileFormat ossUploadFileFormat) ;

    public String postObject(InputStream inputStream,
                             Long fileSize,
                             String randomStringFileName,
                             Constants.OssUploadAccessUri ossUploadAccessUri,
                             Constants.OssUploadAccessBucket ossUploadAccessBucket,
                             Constants.OssUploadAccessFolder ossUploadAccessFolder,
                             Constants.OssUploadFileFormat ossUploadFileFormat);

    public OSSObject operateObject(Constants.OssFileAccessOperate ossFileAccessOperate, String uri) ;

    public OSSObject operateObject(Constants.OssUploadAccessUri ossUploadAccessUri,
                                   Constants.OssUploadAccessBucket ossUploadAccessBucket,
                                   Constants.OssFileAccessOperate ossFileAccessOperate,
                                   String uri) ;

}

