package com.lerning.edu.commons;

import org.springframework.util.StringUtils;

import java.util.Random;
import java.util.UUID;

/**
 * SequnceUtil
 * 序列号生成
 * @author JW
 * @date
 */
public final class SequnceUtil {

    private SequnceUtil(){}

    final static char[] numbers = {'0','1','2','3','4','5','6','7','8','9'};

    public final static String getUUID(){
        return StringUtils.replace(UUID.randomUUID().toString(),"-","").toUpperCase();
    }

    /**
     * 指定位数随机码
     * @param length
     * @return
     */
    public final static String getRandomCode(int length){
        char[] strs = new char[length];
        do{
            strs[--length] = numbers[new Random().nextInt(10)];
        } while(length > 0 );
        return new String(strs);
    }

    /**
     * 指定位数唯一码
     * @param uniqueType 当flag为true时有效，指定生成的唯一码以某个字符开头代表该唯一码的象征意义
     * @param machineId 机器码，集群时的机器代码，可以1-9任意。部署时，分别为部署的项目手动修改该值，以确保集群的多台机器在系统中唯一
     * @param flag 是否拼接uniqueType在首位
     * @return
     */
    public final static String getUniqueNo(String uniqueType, int machineId, boolean flag){
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if(hashCodeV < 0)//有可能是负数
        {
            hashCodeV = Math.abs(hashCodeV);
        }
        // %015d：0表示前面补0，15表示长度为15，d表示参数为正整数
        if ( flag ) {
            return uniqueType + machineId + String.format("%015d",hashCodeV);
        }
        return machineId + String.format("%015d",hashCodeV);
    }


}
