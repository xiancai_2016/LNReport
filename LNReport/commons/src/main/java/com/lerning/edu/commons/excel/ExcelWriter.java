
package com.lerning.edu.commons.excel;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.lerning.edu.commons.excel.template.model.TTitleColumn;

/**
 * @ClassName: ExcelWriter
 * @Description: excel 写入接口
 */
public interface ExcelWriter {
	
	/**
	 * 
	 * @Title: write
	 * @Description: 将数据写入 excel
	 * @param dataList
	 * @param tTitleColumn
	 * @throws IOException
	 */
	void write(List<Map<String, Object>> dataList, TTitleColumn tTitleColumn) throws IOException ;
	

}
