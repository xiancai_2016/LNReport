package com.lerning.edu.commons.util;

/**
 * @author
 */
public class Constants {

    // 设置HTTP最大连接数为10
    public static final Integer OSS_UPLOAD_MAX_CONNECTIONS = 100 ;

    // 设置TCP连接超时为5000毫秒
    public static final Integer OSS_UPLOAD_CONNECTION_TIMEOUT = 50000;

    // 设置最大的重试次数为3
    public static final Integer OSS_UPLOAD_MAX_ERROR_RETRY = 5;

    // 设置Socket传输数据超时的时间为2000毫秒
    public static final Integer OSS_UPLOAD_SOCKET_TIMEOUT = 50000;

    public static final Integer PAGE_SIZE = 20;

    public static final String DEFAULT_FORBIDDEN = "0" ;

    public static final String DEFAULT_SIGNATURE = "这家伙很懒 , 什么都没有留下" ;

    public static final Integer TRUE = 1 ;

    public static final Integer FALSE = 0 ;

    public static final String DATE_FORMART = "yyyy-MM-dd";

    public static final String DATE_FORMART2 = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_FORMART3 = "yyyyMMdd";

    public static final String DATE_FORMART4 = "yyyyMMddHHmmss";

    public static final String DATE_FORMART5 = "MM/dd/yyyy HH:mm:ss";

    public static final String DATE_FORMART6 = "MM/dd/yyyy";

    public static final String ENCODE_KEY = "ADEFE3161F3578E0DFDFABD28C9E2F567A27EE5F2F8A2C9B";

    public static final String SEPARATOR = "#&";

    public static final String CHAR_SET = "UTF-8";

    public static Integer MEM_TIMES = 600;

    public static String BASE_PATH_CERT = "http://cert.shipparts.com/";

    public static final int MINUTE = 15;//设置创建订单超时时间

    //微信公众号推送
    public static final String APP_ID = "wx3957122d51c47b90";
    public static final String SECRET = "b0d1fed98af16ef9684f947d35dd6d06";
    public static final String GET_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+APP_ID+"&secret="+SECRET;// access_token
    public static final String SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";
    public static final String template_id = "d95eKxGtcGfC81E1waAzdCklFgVZ9ZTlJKpp8nmtdz4";

    // 记录读取方向枚举
    public enum ReadDirection {

        UP( 0 ),
        DOWN( 1 );

        private final int value;

        ReadDirection( int value ) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum Gender {

        F( "F" ),
        M( "M" );

        private final String value;

        Gender( String value ) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    public enum MatchesStatus {

        BEGINNING( 0 , "在线售票" ),
        NOT_BEGINNING( 1 , "预选购票");

        private final Integer value ;
        private final String descr ;

        public static MatchesStatus getMatchesStatus(Integer key){
            for(MatchesStatus sx:MatchesStatus.values()){
                if(sx.getValue() == key){
                    return sx;
                }
            }
            return null;
        }

        MatchesStatus(Integer value, String descr) {
            this.value = value;
            this.descr = descr;
        }

        public int getValue() {
            return value;
        }

        public String getDescr() {
            return descr;
        }
    }

    public enum OssUploadFileFormat {

        JPEG( ".jpg" ),         // JPEG图片
        PNG( ".png" ),          // PNG图片
        BMP( ".bmp" ),          // BMP图片
        GIF( ".gif" ),          // GIF图片
        PDF( ".pdf" ),          // PDF
        RAR( ".rar" ),          // RAR
        ZIP( ".zip" );          // ZIP

        private final String ossUploadFileFormat;

        OssUploadFileFormat(String ossUploadFileFormat) {
            this.ossUploadFileFormat = ossUploadFileFormat;
        }

        public static OssUploadFileFormat getOssUploadFileFormat(String key){
            for(OssUploadFileFormat sx:OssUploadFileFormat.values()){
                if(sx.toString().equals(key)){
                    return sx;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return this.ossUploadFileFormat;
        }

    }

    public enum OssUploadAccessFolder {

        AVATAR( "Picture/Head/" ),        // 头像图片
        CONTENT( "Picture/Matches/" );    // 赛事图片

        private final String ossUploadAccessFolder;

        OssUploadAccessFolder(String ossUploadAccessFolder) {
            this.ossUploadAccessFolder = ossUploadAccessFolder;
        }

        public static OssUploadAccessFolder getOssUploadAccessFolder(String key){
            for(OssUploadAccessFolder sx:OssUploadAccessFolder.values()){
                if(sx.toString().equals(key)){
                    return sx;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return this.ossUploadAccessFolder;
        }

    }

    public enum OssFileAccessOperate {

        DELETE( "DELETE"),
        GET   ( "GET"   );

        private final String ossFileAccessOperate;

        OssFileAccessOperate(String ossFileAccessOperate) {
            this.ossFileAccessOperate = ossFileAccessOperate;
        }

        public static OssFileAccessOperate getOssFileAccessOperate(String key){
            for(OssFileAccessOperate sx:OssFileAccessOperate.values()){
                if(sx.toString().equalsIgnoreCase(key)){
                    return sx;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return this.ossFileAccessOperate.toString();
        }

    }

    public enum OssUploadAccessBucket {

        BEIJING( "football-bj-img" ),     // 北京Bucket节点
        SHANGHAI( "football-sh-img" );    // 上海Bucket节点

        private final String ossUploadAccessBucket;

        OssUploadAccessBucket(String ossUploadAccessBucket) {
            this.ossUploadAccessBucket = ossUploadAccessBucket;
        }

        @Override
        public String toString() {
            return this.ossUploadAccessBucket;
        }

    }

    public enum OssUploadAccessUri {

        BEIJING( "http://oss-cn-beijing.aliyuncs.com" ),     // 北京服务器节点
        SHANGHAI( "http://oss-cn-shanghai.aliyuncs.com" );   // 上海服务器节点

        private final String ossUploadAccessUri;

        OssUploadAccessUri(String ossUploadAccessUri) {
            this.ossUploadAccessUri = ossUploadAccessUri;
        }

        @Override
        public String toString() {
            return this.ossUploadAccessUri;
        }

    }

}
