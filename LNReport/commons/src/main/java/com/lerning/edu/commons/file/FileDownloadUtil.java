package com.lerning.edu.commons.file;

import com.lerning.edu.commons.util.Constants;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;

/**
 * Created by FCHEN on 2016/8/6.
 */
public class FileDownloadUtil {
    public HttpServletResponse download(String path, HttpServletResponse response) {
        try {
            // path是指欲下载的文件的路径。
            File file = new File(path);
            // 取得文件名。
            String filename = file.getName();
            // 取得文件的后缀名。
            String ext = filename.substring(filename.lastIndexOf(".") + 1).toUpperCase();

            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return response;
    }

    public void downloadLocal(HttpServletResponse response) throws FileNotFoundException {
        // 下载本地文件
        String fileName = "Operator.doc".toString(); // 文件的默认保存名
        // 读到流中
        InputStream inStream = new FileInputStream("c:/Operator.doc");// 文件的存放路径
        // 设置输出的格式
        response.reset();
        response.setContentType("bin");
        response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        // 循环取出流中的数据
        byte[] b = new byte[100];
        int len;
        try {
            while ((len = inStream.read(b)) > 0)
                response.getOutputStream().write(b, 0, len);
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String downloadNet(String netUrl) throws Exception {

        if(StringUtils.isBlank(netUrl)) {
            return "";
        }
        // 下载网络文件
        int byteread = 0;

        URL url = new URL(netUrl);

//        String fileName = System.currentTimeMillis()+".docx";

        URLConnection conn = url.openConnection();
        String fileName = conn.getHeaderField(6);
        fileName = URLDecoder.decode(fileName.substring(fileName.indexOf("filename=")+9),"UTF-8");
        System.out.println("文件名为："+fileName);
        System.out.println("文件大小："+(conn.getContentLength()/1024)+"KB");
        InputStream inStream = conn.getInputStream();
        FileOutputStream fs = new FileOutputStream("/data/shipparts/c/"+fileName);
//        FileOutputStream fs = new FileOutputStream("D:/aa/"+fileName);

        byte[] buffer = new byte[1024];
        while ((byteread = inStream.read(buffer)) != -1) {
            fs.write(buffer, 0, byteread);
        }
        return Constants.BASE_PATH_CERT+"c/"+fileName;
    }

    public static void main(String[] args) throws Exception {
//        downloadNet("http://27.115.28.206/marine/zz/download.aspx?id=E1E79140B28028C7D039F836");
//        System.out.println("http://cert.shipparts.com/c/contract1-QU10000713.docx".indexOf("contract11"));
    }

}
