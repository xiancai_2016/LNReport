
package com.lerning.edu.commons.exception;

/**
 * APIGlobalErrorMsg
 * 全局异常定义
 * 全局异常code 统一定义为负数，便于识别
 * 全局异常msg  统一末尾需要加上%s,用于接收具体的错误消息
 * @author FCHEN
 * @date 2015/11/9
 */
public enum APIGlobalErrorMsg {

    /**
     * 未知的异常
     */
    UnknownException(-9999,"未知的异常"),
    MobilePhoneExistsException(-7001,"手机号码已存在"),
    MobilePhoneNotExistsException(-7002,"手机号码不存在"),
    UserExistsException(-6001,"账户已存在"),
    UserNotExistsException(-6002,"账户未注册"),
    UserPasswordExistsException(-6003,"账户密码未匹配"),
    NickNameExistsException(-5001,"昵称已存在"),
    ;

    private int code;
    private String msg;
    APIGlobalErrorMsg(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
