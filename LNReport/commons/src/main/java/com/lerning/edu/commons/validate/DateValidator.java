
package com.lerning.edu.commons.validate;

import org.apache.commons.lang.time.DateUtils;

/**
 * DateValidator
 * 时间类验证
 */
public final class DateValidator {

    private DateValidator(){}

    public static final boolean isDateString(String dateStr,DateType type){
        try{
            DateUtils.parseDate(dateStr,new String[]{type.getValue()});
            return true;
        }catch(Exception ex){
        }
        return false;
    }

    /**
     * 支持验证的时间类型
     */
    public static enum DateType{

        SHORT("yyyy-MM-dd"),

        TIME_ZONE("yyyy-MM-dd HH:mm:ss"),
        ;

        private String value;
        private DateType(String value){
            this.value = value;
        }

        public String getValue(){
            return this.value;
        }






    }


}
