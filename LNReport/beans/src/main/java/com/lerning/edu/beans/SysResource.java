/*
 * SysResource.java
 * Copyright(C) 2015-2016 上海皇家网络科技有限公司
 * All rights reserved.
 * -----------------------------------------------
 * 2016-12-09 Created
 */
package com.lerning.edu.beans;

import java.util.List;

/**
 * 资源表
 *
 * @author FCHEN
 * @version 1.0 2016-12-09
 */
public class SysResource extends BaseBean {
    private static final long serialVersionUID = -730503188209285757L;
    /**
     * 资源名称
     */
    private String name;
    /**
     * 图标
     */
    private String iconcls;
    /**
     * 排序
     */
    private Integer seq;
    /**
     * 目标
     */
    private String target;
    /**
     * 资源路径
     */
    private String url;
    /**
     * 资源类型
     */
    private String sysResourceType;

    /**
     * 是否在菜单中显示
     */
    private Boolean isShow;
    /**
     * 权限标识
     */
    private String permission;

    private Long userId;

    private SysResource parent;	// 父级菜单

    private String parentIds; // 所有父级编号

    public SysResource(){
        super();
        this.seq = 30;
        this.isShow = true;
    }

    public SysResource(Long id){
        super(id);
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
    public String getIconcls() {
        return iconcls;
    }
    public void setIconcls(String iconcls) {
        this.iconcls = iconcls == null ? null : iconcls.trim();
    }
    public Integer getSeq() {
        return seq;
    }
    public void setSeq(Integer seq) {
        this.seq = seq;
    }
    public String getTarget() {
        return target;
    }
    public void setTarget(String target) {
        this.target = target == null ? null : target.trim();
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }
    public String getSysResourceType() {
        return sysResourceType;
    }
    public void setSysResourceType(String sysResourceType) {
        this.sysResourceType = sysResourceType == null ? null : sysResourceType.trim();
    }
    public Boolean getIsShow() {
        return isShow;
    }
    public void setIsShow(Boolean isShow) {
        this.isShow = isShow;
    }
    public String getPermission() {
        return permission;
    }
    public void setPermission(String permission) {
        this.permission = permission == null ? null : permission.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public SysResource getParent() {
        return parent;
    }

    public void setParent(SysResource parent) {
        this.parent = parent;
    }

    public Long getParentId() {
        return parent != null && parent.getId() != null ? parent.getId() : 0;
    }

    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds;
    }

    public static Long getRootId(){
        return 1L;
    }

    public static void sortList(List<SysResource> list, List<SysResource> sourcelist, Long parentId, boolean cascade){
        for (int i=0; i<sourcelist.size(); i++){
            SysResource e = sourcelist.get(i);
            if (e.getParent()!=null && e.getParent().getId()!=null
                    && e.getParent().getId()== parentId ){
                list.add(e);
                if (cascade){
                    // 判断是否还有子节点, 有则继续获取子节点
                    for (int j=0; j<sourcelist.size(); j++){
                        SysResource child = sourcelist.get(j);
                        if (child.getParent()!=null && child.getParent().getId()!=null
                                && child.getParent().getId().equals(e.getId())){
                            sortList(list, sourcelist, e.getId(), true);
                            break;
                        }
                    }
                }
            }
        }
    }
}