/*
 * For com.royal.art
 * Copyright [2015/11/14] By FCHEN
 */
package com.lerning.edu.beans;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * BaseBean
 * 基础类
 * @author
 * @date 2015/11/14
 */
public class BaseBean implements Serializable {

    private static final long serialVersionUID = 3269705471269679273L;
    protected Long id;                                  // 记录流水号
    private Integer version = 0;                        // 乐观锁版本号
    private Long operator = 0L;                         // 记录操作人
    private Timestamp createdDatetime;                  // 记录创建日期
    private Timestamp updatedDatetime;                  // 记录修改日期
    private Boolean actived = Boolean.TRUE;             // 记录是否启用、激活或有效（默认为true）
    private Boolean deleted = Boolean.FALSE;            // 记录是否已被删除（默认为false）
    private String descr ;                              // 描述

    public BaseBean() {
        super();
    }

    public BaseBean(Long id) {
        this();
        this.id = id;
    }
    /**
     *  查询开始时间 --只做查询
     */
    private String searchStartTime;
    /**
     * 查询结束时间 --只做查询
     */
    private String searchEndTime;

    private String serchStratEndDate;

    public String getSerchStratEndDate() {
        return serchStratEndDate;
    }

    public void setSerchStratEndDate(String serchStratEndDate) {
        this.serchStratEndDate = serchStratEndDate;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getOperator() {
        return operator;
    }

    public void setOperator(Long operator) {
        this.operator = operator;
    }

    public Timestamp getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(Timestamp createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public Timestamp getUpdatedDatetime() {
        return updatedDatetime;
    }

    public void setUpdatedDatetime(Timestamp updatedDatetime) {
        this.updatedDatetime = updatedDatetime;
    }

    public Boolean getActived() {
        return actived;
    }

    public void setActived(Boolean actived) {
        this.actived = actived;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getSearchStartTime() {
        return searchStartTime;
    }

    public void setSearchStartTime(String searchStartTime) {
        this.searchStartTime = searchStartTime;
    }

    public String getSearchEndTime() {
        return searchEndTime;
    }

    public void setSearchEndTime(String searchEndTime) {
        this.searchEndTime = searchEndTime;
    }

    /**
     * 删除标记（0：正常；1：删除；2：审核；）
     */
    public static final Long DEL_FLAG_NORMAL = 0L;
    public static final Long DEL_FLAG_DELETE = 1L;
}
