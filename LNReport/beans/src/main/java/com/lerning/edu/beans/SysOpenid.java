package com.lerning.edu.beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class SysOpenid implements Serializable{

    private static final long serialVersionUID = 326973231269679273L;

    private Long uoid;

    private Long userId;

    private String name;

    private String account;

    private Timestamp reserve;

    private String reserve1;

    private String reserve2;

    private String openid;

    public Long getUoid() {
        return uoid;
    }

    public void setUoid(Long uoid) {
        this.uoid = uoid;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Timestamp getReserve() {
        return reserve;
    }

    public void setReserve(Timestamp reserve) {
        this.reserve = reserve;
    }

    public String getReserve1() {
        return reserve1;
    }

    public void setReserve1(String reserve1) {
        this.reserve1 = reserve1;
    }

    public String getReserve2() {
        return reserve2;
    }

    public void setReserve2(String reserve2) {
        this.reserve2 = reserve2;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }
}