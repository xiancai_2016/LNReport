package com.lerning.edu.beans.page;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * PageList
 *
 * @author JW
 * @date 2015/11/7
 */
public class PageList<T> implements Serializable {

    private static final long serialVersionUID = 6525692408648747939L;


    /**
     * 分页标识 原值返回
     */
    private Object sEcho;
    /**
     * 开始页
     */
    private int iDisplayStart;
    /**
     * 每页数量 default
     */
    private int iDisplayLength = 10;

    /**
     * 数据结果集总数
     */
    private long iTotalRecords;
    /**
     * 暂时不用，但是一定要返回的参数，默认值按照 iTotalRecords
     */
    private long iTotalDisplayRecords;
    /**
     * 返回结果集
     */
    private List<T> aaData;

    public PageList(){

    }

    public Object getsEcho() {
        return sEcho;
    }

    public void setsEcho(Object sEcho) {
        this.sEcho = sEcho;
    }

    public long getiTotalRecords() {
        return iTotalRecords;
    }

    public void setiTotalRecords(long iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public long getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(long iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public List<T> getAaData() {
        return aaData;
    }

    public void setAaData(List<T> aaData) {
        this.aaData = aaData;
    }

    public int getiDisplayStart() {
        return iDisplayStart;
    }

    public void setiDisplayStart(int iDisplayStart) {
        this.iDisplayStart = iDisplayStart;
    }

    public int getiDisplayLength() {
        return iDisplayLength;
    }

    public void setiDisplayLength(int iDisplayLength) {
        this.iDisplayLength = iDisplayLength;
    }

}
