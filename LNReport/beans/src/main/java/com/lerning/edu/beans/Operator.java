package com.lerning.edu.beans;

public class Operator {
    private Integer cid;

    private String cname;

    private String cfield1;

    private String cemail;

    private Integer cpriority;

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCfield1() {
        return cfield1;
    }

    public void setCfield1(String cfield1) {
        this.cfield1 = cfield1;
    }

    public String getCemail() {
        return cemail;
    }

    public void setCemail(String cemail) {
        this.cemail = cemail;
    }

    public Integer getCpriority() {
        return cpriority;
    }

    public void setCpriority(Integer cpriority) {
        this.cpriority = cpriority;
    }
}