package com.lerning.edu.beans;

import java.util.List;

/**
 * @author FCHEN
 */
public class SysDict extends BaseBean {


    private static final long serialVersionUID = -8206290586952829387L;
    private String itemKey ;
    private String itemValue ;
    private String pItemKey ;
    private String pItemValue ;
    private Integer status ;
    private Integer ticket_num;
    private String Date;
    /**
     * 用于查询  忽略id
     */
    private List<String> ignores;

    public String getItemKey() {
        return itemKey;
    }

    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    public String getpItemKey() {
        return pItemKey;
    }

    public void setpItemKey(String pItemKey) {
        this.pItemKey = pItemKey;
    }

    public String getpItemValue() {
        return pItemValue;
    }

    public void setpItemValue(String pItemValue) {
        this.pItemValue = pItemValue;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<String> getIgnores() {
        return ignores;
    }

    public void setIgnores(List<String> ignores) {
        this.ignores = ignores;
    }

    public Integer getTicket_num() {
        return ticket_num;
    }

    public void setTicket_num(Integer ticket_num) {
        this.ticket_num = ticket_num;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
