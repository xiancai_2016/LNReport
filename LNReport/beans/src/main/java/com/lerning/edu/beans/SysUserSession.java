package com.lerning.edu.beans;

import java.sql.Timestamp;

/**
 * @author FCHEN
 */
public class SysUserSession extends BaseBean {


    private static final long serialVersionUID = -64598306121060046L;

    private String tokenKey ;
    private String tokenContent ;
    private String sourceCode ;
    private String deviceCode ;
    private Timestamp expire ;

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    public String getTokenContent() {
        return tokenContent;
    }

    public void setTokenContent(String tokenContent) {
        this.tokenContent = tokenContent;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public Timestamp getExpire() {
        return expire;
    }

    public void setExpire(Timestamp expire) {
        this.expire = expire;
    }
}
