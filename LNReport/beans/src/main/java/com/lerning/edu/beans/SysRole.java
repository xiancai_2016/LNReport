/*
 * SysRole.java
 * Copyright(C) 2015-2016 上海皇家网络科技有限公司
 * All rights reserved.
 * -----------------------------------------------
 * 2016-12-09 Created
 */
package com.lerning.edu.beans;

import com.google.common.collect.Lists;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 *
 *
 * @author FCHEN
 * @version 1.0 2016-12-09
 */
public class SysRole extends BaseBean {
    private static final long serialVersionUID = -5187022888717232753L;
    /**
     * 角色名称
     */
    private String name;

    /**
     * 原角色名称
     */
    private String oldName;
    /**
     * 排序
     */
    private Integer seq;

    /**
     * 是否是系统数据
     */
    private String sysData;

    /**
     * 根据用户ID查询角色列表
     */
    private SysUser sysUser;

    private List<SysResource> resourceList = Lists.newArrayList(); // 拥有菜单列表
    private List<SysReport> reportList = Lists.newArrayList(); // 拥有菜单列表

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
    public Integer getSeq() {
        return seq;
    }
    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public SysRole() {
        super();
    }

    public SysRole(SysUser sysUser) {
        this();
        this.sysUser = sysUser;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    // 数据范围（1：所有数据；2：所在公司及以下数据；3：所在公司数据；4：所在部门及以下数据；5：所在部门数据；8：仅本人数据；9：按明细设置）
    public static final String DATA_SCOPE_ALL = "1";
    public static final String DATA_SCOPE_COMPANY_AND_CHILD = "2";
    public static final String DATA_SCOPE_COMPANY = "3";
    public static final String DATA_SCOPE_OFFICE_AND_CHILD = "4";
    public static final String DATA_SCOPE_OFFICE = "5";
    public static final String DATA_SCOPE_SELF = "8";
    public static final String DATA_SCOPE_CUSTOM = "9";

    public List<SysResource> getResourceList() {
        return resourceList;
    }

    public void setResourceList(List<SysResource> resourceList) {
        this.resourceList = resourceList;
    }

    public List<Long> getResourceIdList() {
        List<Long> resourceIdList = Lists.newArrayList();
        for (SysResource resource : resourceList) {
            resourceIdList.add(resource.getId());
        }
        return resourceIdList;
    }

    public void setResourceIdList(List<Long> resourceIdList) {
        resourceList = Lists.newArrayList();
        for (Long resourceId : resourceIdList) {
            SysResource resource = new SysResource();
            resource.setId(resourceId);
            resourceList.add(resource);
        }
    }

    public String getResourceIds() {
        return StringUtils.join(getResourceIdList(), ",");
    }

    public void setResourceIds(String resourceIds) {
        resourceList = Lists.newArrayList();
        if (resourceIds != null){
            String[] ids = StringUtils.split(resourceIds, ",");
            Long[] _ids = stringToLong(ids);
            setResourceIdList(Lists.newArrayList(_ids));
        }
    }

    public List<SysReport> getReportList() {
        return reportList;
    }

    public void setReportList(List<SysReport> reportList) {
        this.reportList = reportList;
    }

    public List<Long> getReportIdList() {
        List<Long> reportIdList = Lists.newArrayList();
        for (SysReport report : reportList) {
            reportIdList.add(report.getId());
        }
        return reportIdList;
    }

    public void setReportIdList(List<Long> reportIdList) {
        reportList = Lists.newArrayList();
        for (Long reportId : reportIdList) {
            SysReport report = new SysReport();
            report.setId(reportId);
            reportList.add(report);
        }
    }

    public String getReportIds() {
        return StringUtils.join(getReportIdList(), ",");
    }

    public void setReportIds(String reportIds) {
        reportList = Lists.newArrayList();
        if (reportIds != null){
            String[] ids = StringUtils.split(reportIds, ",");
            Long[] _ids = stringToLong(ids);
            setReportIdList(Lists.newArrayList(_ids));
        }
    }

    /**
     * 获取权限字符串列表
     */
    public List<String> getPermissions() {
        List<String> permissions = Lists.newArrayList();
        for (SysResource resource : resourceList) {
            if (resource.getPermission()!=null && !"".equals(resource.getPermission())){
                permissions.add(resource.getPermission());
            }
        }
        return permissions;
    }

    public static Long[] stringToLong(String stringArray[]) {
        if (stringArray == null)
            return null;
        return (Long[]) ConvertUtils.convert(stringArray, Long.class);
    }

    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }

    public String getSysData() {
        return sysData;
    }

    public void setSysData(String sysData) {
        this.sysData = sysData;
    }
}