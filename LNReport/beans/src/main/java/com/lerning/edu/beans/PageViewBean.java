package com.lerning.edu.beans;

/**
 * Created by FCHEN on 2016/8/18.
 */
public class PageViewBean {
    private String name;
    private Long statCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatCount() {
        return statCount;
    }

    public void setStatCount(Long statCount) {
        this.statCount = statCount;
    }
}
