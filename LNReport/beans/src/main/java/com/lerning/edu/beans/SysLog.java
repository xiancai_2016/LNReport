/*
 * SysLog.java
 * Copyright(C) 2015-2016 上海皇家网络科技有限公司
 * All rights reserved.
 * -----------------------------------------------
 * 2016-12-23 Created
 */
package com.lerning.edu.beans;

import com.lerning.edu.commons.util.StringUtil;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Map;

/**
 * 日志表
 *
 * @author FCHEN
 * @version 1.0 2016-12-23
 */
public class SysLog extends BaseBean {
    private static final long serialVersionUID = 1L;
    /**
     * 日志类型（1：接入日志；2：错误日志）
     */
    private String type;
    /**
     * 日志标题
     */
    private String title;
    /**
     * 操作IP地址
     */
    private String remoteAddr;
    /**
     * 用户代理
     */
    private String userAgent;
    /**
     * 请求URI
     */
    private String requestUri;
    /**
     * 操作方式
     */
    private String method;
    /**
     * 操作提交的数据
     */
    private String params;
    /**
     * 异常信息
     */
    private String exception;

    private String operatorName;

    // 日志类型（1：接入日志；2：错误日志）
    public static final String TYPE_ACCESS = "1";
    public static final String TYPE_EXCEPTION = "2";

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }
    public String getRemoteAddr() {
        return remoteAddr;
    }
    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr == null ? null : remoteAddr.trim();
    }
    public String getUserAgent() {
        return userAgent;
    }
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent == null ? null : userAgent.trim();
    }
    public String getRequestUri() {
        return requestUri;
    }
    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri == null ? null : requestUri.trim();
    }
    public String getMethod() {
        return method;
    }
    public void setMethod(String method) {
        this.method = method == null ? null : method.trim();
    }
    public String getParams() {
        return params;
    }
    public void setParams(String params) {
        this.params = params == null ? null : params.trim();
    }
    public String getException() {
        return exception;
    }
    public void setException(String exception) {
        this.exception = exception == null ? null : exception.trim();
    }

    /**
     * 设置请求参数
     * @param paramMap
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void setParams(Map paramMap){
        if (paramMap == null){
            return;
        }
        StringBuilder params = new StringBuilder();
        for (Map.Entry<String, String[]> param : ((Map<String, String[]>)paramMap).entrySet()){
            params.append(("".equals(params.toString()) ? "" : "&") + param.getKey() + "=");
            String paramValue = (param.getValue() != null && param.getValue().length > 0 ? param.getValue()[0] : "");
            params.append(StringUtil.abbr(StringUtil.endsWithIgnoreCase(param.getKey(), "password") ? "" : paramValue, 100));
        }
        this.params = params.toString();
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
}