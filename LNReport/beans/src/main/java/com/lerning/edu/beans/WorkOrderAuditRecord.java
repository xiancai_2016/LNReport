package com.lerning.edu.beans;

import com.lerning.edu.commons.util.DateUtil;

import java.util.Date;

public class WorkOrderAuditRecord {
    private Integer cid;

    private String cworkorderid;

    private String cuserid;

    private Integer cstatus;

    private String cauditreason;

    private Date ccreatedate;

    private Integer deleted;

    private Integer version;

    private String cName;

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCworkorderid() {
        return cworkorderid;
    }

    public void setCworkorderid(String cworkorderid) {
        this.cworkorderid = cworkorderid;
    }

    public String getCuserid() {
        return cuserid;
    }

    public void setCuserid(String cuserid) {
        this.cuserid = cuserid;
    }

    public Integer getCstatus() {
        return cstatus;
    }

    public void setCstatus(Integer cstatus) {
        this.cstatus = cstatus;
    }

    public String getCauditreason() {
        return cauditreason;
    }

    public void setCauditreason(String cauditreason) {
        this.cauditreason = cauditreason;
    }

    public String getCcreatedate() {

        if(ccreatedate != null){
            return DateUtil.dateToString(ccreatedate);
        }else{
            return  "";
        }
    }

    public void setCcreatedate(Date ccreatedate) {
        this.ccreatedate = ccreatedate;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }
}