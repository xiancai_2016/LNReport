package com.lerning.edu.beans;

import com.lerning.edu.commons.util.Constants;
import com.lerning.edu.commons.util.DateUtil;

import java.util.Date;

public class WorkOrder {
    private Integer cid;

    private String cuserid;

    private String cclassid;

    private Integer cstatus;

    private Integer ctype;

    private Date cstopclassdate;

    private Date crecoverydate;

    private String creason;

    private Integer cisoriginal;

    private String cremarks;

    private String ccreateuser;

    private Date ccreatedate;

    private Integer cisprint;

    private Integer deleted;

    private Integer version;

    private String corderno;

    private String campus;

    private String userName;

    private String employName;

    private String auditTime;

    private String subName;

    private String resumeClassID;

    private String startDate;
    private String endDate;

    private String changeCampusID;

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCuserid() {
        return cuserid;
    }

    public void setCuserid(String cuserid) {
        this.cuserid = cuserid;
    }

    public String getCclassid() {
        return cclassid;
    }

    public void setCclassid(String cclassid) {
        this.cclassid = cclassid;
    }

    public Integer getCstatus() {
        return cstatus;
    }

    public void setCstatus(Integer cstatus) {
        this.cstatus = cstatus;
    }

    public Integer getCtype() {
        return ctype;
    }

    public void setCtype(Integer ctype) {
        this.ctype = ctype;
    }

    public Date getCstopclassdate() {
        return cstopclassdate;
    }

    public void setCstopclassdate(Date cstopclassdate) {
        this.cstopclassdate = cstopclassdate;
    }

    public Date getCrecoverydate() {
        return crecoverydate;
    }

    public void setCrecoverydate(Date crecoverydate) {
        this.crecoverydate = crecoverydate;
    }

    public String getCreason() {
        return creason;
    }

    public void setCreason(String creason) {
        this.creason = creason;
    }

    public Integer getCisoriginal() {
        return cisoriginal;
    }

    public void setCisoriginal(Integer cisoriginal) {
        this.cisoriginal = cisoriginal;
    }

    public String getCremarks() {
        return cremarks;
    }

    public void setCremarks(String cremarks) {
        this.cremarks = cremarks;
    }

    public String getCcreateuser() {
        return ccreateuser;
    }

    public void setCcreateuser(String ccreateuser) {
        this.ccreateuser = ccreateuser;
    }

    public String getCcreatedate() {
        return DateUtil.dateToString(ccreatedate, Constants.DATE_FORMART) ;
    }

    public void setCcreatedate(Date ccreatedate) {
        this.ccreatedate = ccreatedate;
    }

    public Integer getCisprint() {
        return cisprint;
    }

    public void setCisprint(Integer cisprint) {
        this.cisprint = cisprint;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCorderno() {
        return corderno;
    }

    public void setCorderno(String corderno) {
        this.corderno = corderno;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(String auditTime) {
        this.auditTime = auditTime;
    }

    public String getEmployName() {
        return employName;
    }

    public void setEmployName(String employName) {
        this.employName = employName;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getResumeClassID() {
        return resumeClassID;
    }

    public void setResumeClassID(String resumeClassID) {
        this.resumeClassID = resumeClassID;
    }

    public String getChangeCampusID() {
        return changeCampusID;
    }

    public void setChangeCampusID(String changeCampusID) {
        this.changeCampusID = changeCampusID;
    }
}