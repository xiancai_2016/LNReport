package com.lerning.edu.beans;

import com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;

/**
 * @author FCHEN
 */
public class SysUser extends BaseBean {


    private static final long serialVersionUID = -5953745902386609932L;
    private String account ;                                // 登录账户
    private String pwd ;                                    // 密码
    private String repeatPwd ;                              // 重复密码
    private Integer loginTimes ;                            // 登录次数
    private Date lastLoginTime ;                            // 最后登录时间
    private String loginIp;	                                // 最后登陆IP
    private String salt ;                                   // 盐值
    private String name ;                                   // 用户名
    private String loginFlag;	                            // 是否允许登陆
    private SysRole role;	                                // 根据角色查询用户条件
    private List<SysRole> roleList = Lists.newArrayList();  // 拥有角色列表
//    protected SysUser currentUser;                          //当前用户
    private String oldAccount;                              // 原登录名

    public SysUser() {
        super();
        this.loginFlag = "1";
    }

    public SysUser(SysRole role){
        super();
        this.role = role;
    }

    public String getLoginFlag() {
        return loginFlag;
    }

    public void setLoginFlag(String loginFlag) {
        this.loginFlag = loginFlag;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getRepeatPwd() {
        return repeatPwd;
    }

    public void setRepeatPwd(String repeatPwd) {
        this.repeatPwd = repeatPwd;
    }

    public Integer getLoginTimes() {
        return loginTimes;
    }

    public void setLoginTimes(Integer loginTimes) {
        this.loginTimes = loginTimes;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SysRole getRole() {
        return role;
    }

    public void setRole(SysRole role) {
        this.role = role;
    }

    public List<SysRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<SysRole> roleList) {
        this.roleList = roleList;
    }

    public boolean isAdmin(){
        return isAdmin(this.id);
    }

    public static boolean isAdmin(Long id){
        return id != null && 2L == id;
    }

//    public SysUser getCurrentUser() {
//        if(currentUser == null){
//            currentUser = UserUtils.getUser();
//        }
//        return currentUser;
//    }
//
//    public void setCurrentUser(SysUser currentUser) {
//        this.currentUser = currentUser;
//    }

    public List<Long> getRoleIdList() {
        List<Long> roleIdList = Lists.newArrayList();
        for (SysRole role : roleList) {
            roleIdList.add(role.getId());
        }
        return roleIdList;
    }

    public void setRoleIdList(List<Long> roleIdList) {
        roleList = Lists.newArrayList();
        for (Long roleId : roleIdList) {
            SysRole role = new SysRole();
            role.setId(roleId);
            roleList.add(role);
        }
    }

    public String getOldAccount() {
        return oldAccount;
    }

    public void setOldAccount(String oldAccount) {
        this.oldAccount = oldAccount;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }
}
