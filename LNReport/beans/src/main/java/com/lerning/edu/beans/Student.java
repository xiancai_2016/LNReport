package com.lerning.edu.beans;

import java.util.Date;

public class Student {
    private String cid;

    private Date ccreatetime;

    private Date cupdatetime;

    private String ccompanyid;

    private String ccampusid;

    private String cserial;

    private String cname;

    private String cenglishname;

    private String cpinyinpre;

    private String cphotopath;

    private String cidnumber;

    private Integer csex;

    private Date cbirthday;

    private Date cbirthdaymoon;

    private Date cindate;

    private String cliveplace;

    private String cqq;

    private String csmstel;

    private String cfather;

    private String cfathertel;

    private String cfathervocation;

    private String cmother;

    private String cmothertel;

    private String cmothervocation;

    private String cothertel;

    private String cfulltimeschool;

    private String cclassname;

    private Integer cgrade;

    private Date cgradebasedate;

    private String csalemode;

    private Integer cflag;

    private String cmasteruserid;

    private Date cmasteradjustdate;

    private String cdescribe;

    private Integer cstatus;

    private Integer ctrystatus;

    private Date ctrystatusdate;

    private Date coutdate;

    private Integer cisexceptionout;

    private String coutcause;

    private String ctype;

    private Date cimporttime;

    private Date cfeeupdatetime;

    private String coutcauseid;

    private String ccustomercampusid;

    private String cfield1;

    private String cfield2;

    private String cfield3;

    private String cfield4;

    private String cfield5;

    private String cfield6;

    private String cintroducer;

    private Date csynctime;

    private String cfield7;

    private String cfield8;

    private String cfield9;

    private String cfield10;

    private Integer ciscustomerstatus;

    private Date creturndate;

    private Date clastclassdate;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Date getCcreatetime() {
        return ccreatetime;
    }

    public void setCcreatetime(Date ccreatetime) {
        this.ccreatetime = ccreatetime;
    }

    public Date getCupdatetime() {
        return cupdatetime;
    }

    public void setCupdatetime(Date cupdatetime) {
        this.cupdatetime = cupdatetime;
    }

    public String getCcompanyid() {
        return ccompanyid;
    }

    public void setCcompanyid(String ccompanyid) {
        this.ccompanyid = ccompanyid;
    }

    public String getCcampusid() {
        return ccampusid;
    }

    public void setCcampusid(String ccampusid) {
        this.ccampusid = ccampusid;
    }

    public String getCserial() {
        return cserial;
    }

    public void setCserial(String cserial) {
        this.cserial = cserial;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCenglishname() {
        return cenglishname;
    }

    public void setCenglishname(String cenglishname) {
        this.cenglishname = cenglishname;
    }

    public String getCpinyinpre() {
        return cpinyinpre;
    }

    public void setCpinyinpre(String cpinyinpre) {
        this.cpinyinpre = cpinyinpre;
    }

    public String getCphotopath() {
        return cphotopath;
    }

    public void setCphotopath(String cphotopath) {
        this.cphotopath = cphotopath;
    }

    public String getCidnumber() {
        return cidnumber;
    }

    public void setCidnumber(String cidnumber) {
        this.cidnumber = cidnumber;
    }

    public Integer getCsex() {
        return csex;
    }

    public void setCsex(Integer csex) {
        this.csex = csex;
    }

    public Date getCbirthday() {
        return cbirthday;
    }

    public void setCbirthday(Date cbirthday) {
        this.cbirthday = cbirthday;
    }

    public Date getCbirthdaymoon() {
        return cbirthdaymoon;
    }

    public void setCbirthdaymoon(Date cbirthdaymoon) {
        this.cbirthdaymoon = cbirthdaymoon;
    }

    public Date getCindate() {
        return cindate;
    }

    public void setCindate(Date cindate) {
        this.cindate = cindate;
    }

    public String getCliveplace() {
        return cliveplace;
    }

    public void setCliveplace(String cliveplace) {
        this.cliveplace = cliveplace;
    }

    public String getCqq() {
        return cqq;
    }

    public void setCqq(String cqq) {
        this.cqq = cqq;
    }

    public String getCsmstel() {
        return csmstel;
    }

    public void setCsmstel(String csmstel) {
        this.csmstel = csmstel;
    }

    public String getCfather() {
        return cfather;
    }

    public void setCfather(String cfather) {
        this.cfather = cfather;
    }

    public String getCfathertel() {
        return cfathertel;
    }

    public void setCfathertel(String cfathertel) {
        this.cfathertel = cfathertel;
    }

    public String getCfathervocation() {
        return cfathervocation;
    }

    public void setCfathervocation(String cfathervocation) {
        this.cfathervocation = cfathervocation;
    }

    public String getCmother() {
        return cmother;
    }

    public void setCmother(String cmother) {
        this.cmother = cmother;
    }

    public String getCmothertel() {
        return cmothertel;
    }

    public void setCmothertel(String cmothertel) {
        this.cmothertel = cmothertel;
    }

    public String getCmothervocation() {
        return cmothervocation;
    }

    public void setCmothervocation(String cmothervocation) {
        this.cmothervocation = cmothervocation;
    }

    public String getCothertel() {
        return cothertel;
    }

    public void setCothertel(String cothertel) {
        this.cothertel = cothertel;
    }

    public String getCfulltimeschool() {
        return cfulltimeschool;
    }

    public void setCfulltimeschool(String cfulltimeschool) {
        this.cfulltimeschool = cfulltimeschool;
    }

    public String getCclassname() {
        return cclassname;
    }

    public void setCclassname(String cclassname) {
        this.cclassname = cclassname;
    }

    public Integer getCgrade() {
        return cgrade;
    }

    public void setCgrade(Integer cgrade) {
        this.cgrade = cgrade;
    }

    public Date getCgradebasedate() {
        return cgradebasedate;
    }

    public void setCgradebasedate(Date cgradebasedate) {
        this.cgradebasedate = cgradebasedate;
    }

    public String getCsalemode() {
        return csalemode;
    }

    public void setCsalemode(String csalemode) {
        this.csalemode = csalemode;
    }

    public Integer getCflag() {
        return cflag;
    }

    public void setCflag(Integer cflag) {
        this.cflag = cflag;
    }

    public String getCmasteruserid() {
        return cmasteruserid;
    }

    public void setCmasteruserid(String cmasteruserid) {
        this.cmasteruserid = cmasteruserid;
    }

    public Date getCmasteradjustdate() {
        return cmasteradjustdate;
    }

    public void setCmasteradjustdate(Date cmasteradjustdate) {
        this.cmasteradjustdate = cmasteradjustdate;
    }

    public String getCdescribe() {
        return cdescribe;
    }

    public void setCdescribe(String cdescribe) {
        this.cdescribe = cdescribe;
    }

    public Integer getCstatus() {
        return cstatus;
    }

    public void setCstatus(Integer cstatus) {
        this.cstatus = cstatus;
    }

    public Integer getCtrystatus() {
        return ctrystatus;
    }

    public void setCtrystatus(Integer ctrystatus) {
        this.ctrystatus = ctrystatus;
    }

    public Date getCtrystatusdate() {
        return ctrystatusdate;
    }

    public void setCtrystatusdate(Date ctrystatusdate) {
        this.ctrystatusdate = ctrystatusdate;
    }

    public Date getCoutdate() {
        return coutdate;
    }

    public void setCoutdate(Date coutdate) {
        this.coutdate = coutdate;
    }

    public Integer getCisexceptionout() {
        return cisexceptionout;
    }

    public void setCisexceptionout(Integer cisexceptionout) {
        this.cisexceptionout = cisexceptionout;
    }

    public String getCoutcause() {
        return coutcause;
    }

    public void setCoutcause(String coutcause) {
        this.coutcause = coutcause;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public Date getCimporttime() {
        return cimporttime;
    }

    public void setCimporttime(Date cimporttime) {
        this.cimporttime = cimporttime;
    }

    public Date getCfeeupdatetime() {
        return cfeeupdatetime;
    }

    public void setCfeeupdatetime(Date cfeeupdatetime) {
        this.cfeeupdatetime = cfeeupdatetime;
    }

    public String getCoutcauseid() {
        return coutcauseid;
    }

    public void setCoutcauseid(String coutcauseid) {
        this.coutcauseid = coutcauseid;
    }

    public String getCcustomercampusid() {
        return ccustomercampusid;
    }

    public void setCcustomercampusid(String ccustomercampusid) {
        this.ccustomercampusid = ccustomercampusid;
    }

    public String getCfield1() {
        return cfield1;
    }

    public void setCfield1(String cfield1) {
        this.cfield1 = cfield1;
    }

    public String getCfield2() {
        return cfield2;
    }

    public void setCfield2(String cfield2) {
        this.cfield2 = cfield2;
    }

    public String getCfield3() {
        return cfield3;
    }

    public void setCfield3(String cfield3) {
        this.cfield3 = cfield3;
    }

    public String getCfield4() {
        return cfield4;
    }

    public void setCfield4(String cfield4) {
        this.cfield4 = cfield4;
    }

    public String getCfield5() {
        return cfield5;
    }

    public void setCfield5(String cfield5) {
        this.cfield5 = cfield5;
    }

    public String getCfield6() {
        return cfield6;
    }

    public void setCfield6(String cfield6) {
        this.cfield6 = cfield6;
    }

    public String getCintroducer() {
        return cintroducer;
    }

    public void setCintroducer(String cintroducer) {
        this.cintroducer = cintroducer;
    }

    public Date getCsynctime() {
        return csynctime;
    }

    public void setCsynctime(Date csynctime) {
        this.csynctime = csynctime;
    }

    public String getCfield7() {
        return cfield7;
    }

    public void setCfield7(String cfield7) {
        this.cfield7 = cfield7;
    }

    public String getCfield8() {
        return cfield8;
    }

    public void setCfield8(String cfield8) {
        this.cfield8 = cfield8;
    }

    public String getCfield9() {
        return cfield9;
    }

    public void setCfield9(String cfield9) {
        this.cfield9 = cfield9;
    }

    public String getCfield10() {
        return cfield10;
    }

    public void setCfield10(String cfield10) {
        this.cfield10 = cfield10;
    }

    public Integer getCiscustomerstatus() {
        return ciscustomerstatus;
    }

    public void setCiscustomerstatus(Integer ciscustomerstatus) {
        this.ciscustomerstatus = ciscustomerstatus;
    }

    public Date getCreturndate() {
        return creturndate;
    }

    public void setCreturndate(Date creturndate) {
        this.creturndate = creturndate;
    }

    public Date getClastclassdate() {
        return clastclassdate;
    }

    public void setClastclassdate(Date clastclassdate) {
        this.clastclassdate = clastclassdate;
    }
}