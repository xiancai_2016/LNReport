/*
 * For com.royal.art
 * Copyright [2015/11/14] By FCHEN
 */
package com.lerning.edu.beans;

import java.util.Date;

/**
 * BaseSysBean
 * 后台基础bean
 * @author
 */
public class BaseSysBean extends BaseBean{

    private static final long serialVersionUID = -4481734966001768679L;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者账号
     */
    private String createAccount;
    /**
     * 最后修改者账号
     */
    private String lastUpdateAccount;
    /**
     * 最后修改时间
     */
    private Date lastUpdateTime;

    public String getCreateAccount() {
        return createAccount;
    }

    public void setCreateAccount(String createAccount) {
        this.createAccount = createAccount;
    }

    public String getLastUpdateAccount() {
        return lastUpdateAccount;
    }

    public void setLastUpdateAccount(String lastUpdateAccount) {
        this.lastUpdateAccount = lastUpdateAccount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
}
